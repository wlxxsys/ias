package cn.tit.ias.controller.android;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.DicUserRoleInfoService;
import cn.tit.ias.service.UserInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;
import cn.tit.ias.util.security.MD5Util;

/**
 * 
 *  
 * @Description:  android用户信息管理请求
 * @author: maotao 
 * @date:   2019年6月16日 上午7:45:07       
 *
 */
@Controller
@RequestMapping("/androidUser")
public class AndroidUserController {
	
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private DicUserRoleInfoService userRoleService;
	@RequestMapping("/androidmainPage")
	public String androidmainPage() {
		return "androidmainPage";
	}
	
	/**getUserpassword
	 * 
	 * @Title: login   
	 * @Description:  登陆用户的验证
	 * @param: @param userInfo
	 * @param: @return 返回一个 Map 对象，其中包含登陆操作的结果   
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "用户模块",option = "登陆")
	public Map<String, Object> login(@RequestBody Map<String, Object> userInfo,HttpServletRequest request){
		/** 自定义的map对象，用于前后端json传输 */
		ControllerResponse response = ControllerResponseFactory.newInstance();
		String username = (String) userInfo.get("username");
		String password = (String) userInfo.get("password");
		String ip = (String) userInfo.get("ip");
		/** 信息载体的初始化 */
		String result = "";
		String msg = "";
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		// shiro验证
		password = MD5Util.MD5(password);
		UsernamePasswordToken token = new UsernamePasswordToken(username,password);
		try {
			//登陆成功，添加ip到shiro的session域
			subject.login(token);
			System.out.println(token);
			session.setAttribute("ip", ip);
			msg = "登陆成功";
			result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		} catch (Exception  e) {
			msg = "账号或密码错误" ;
			result = ControllerResponse.RESPONSE_RESULT_ERROR;
		}
				
		/** 以json格式响应的信息 */
		response.setResponseResult(result);
		response.setResponseMsg(msg);
		System.out.println(response.generateResponse());
		/** 返回一个map对象json格式 */
		return response.generateResponse();
	}
}
