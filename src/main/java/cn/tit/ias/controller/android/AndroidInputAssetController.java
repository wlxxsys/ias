package cn.tit.ias.controller.android;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ObjectUtils.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.sql.visitor.functions.If;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.DicProfitLossInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.service.DicProfitLossInfoService;
import cn.tit.ias.service.StoreInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;

/**
 * 
 *  
 * @Description:往标签中录入资产编号   
 * @author: maotao 
 * @date:   2019年6月16日 上午9:19:52       
 *
 */
@Controller
@RequestMapping("/AndroidInputAsset")
public class AndroidInputAssetController {
    
	@Autowired
	private AssetInfoService assetInfoService;
	@Autowired	
	private StoreInfoService storeInfoService;
	@Autowired
	private DicProfitLossInfoService dicProfitLossInfoService;
	@Autowired  
	private DepartInfoService departInfoService;
	
	
	/**
	 * 判断本部门的盘点批次是否为空
	 * */
//	@RequestMapping(value = "getInventoryBatch",method = RequestMethod.POST)
//	@ResponseBody
//	public Map<String,Object> getInventoryBatch(HttpServletRequest request,@RequestBody Map<String,Object> msg)
//	{
//		ControllerResponse response=ControllerResponseFactory.newInstance();
//		/*获取前端传来的存放地名称*/
//		String result="";
//		String storeName=(String) msg.get("storeName");
//		/*将前端的存放地写进session中*/
//		HttpSession session = request.getSession();
//		/*清空上一次盘点的storeName*/
//		session.setAttribute("storeName", "");
//		/*重新将盘点的信息写入*/
//		try {
//			/*写进域-成功*/
//			session.setAttribute("storeName", storeName);
//			result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
//		}catch(Exception e) {
//			/*写进域-失败*/
//			result=ControllerResponse.RESPONSE_RESULT_ERROR;
//		}
//		response.setResponseResult(result);
//		return response.generateResponse();
//	}
	/**
	 * 
	 * @Title: setStoreName
	 * @Description: 将前端盘点的存放地存进session中(判断盘点批次是否为空)
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "setStoreName",method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> setStoreName(HttpServletRequest request,@RequestBody Map<String,Object> message)
	{
		ControllerResponse response=ControllerResponseFactory.newInstance();
		/*获取前端传来的存放地名称*/
		String result="";
		String msg="";
		/*将前端的存放地写进session中*/
		HttpSession session = request.getSession();
		/*清空上一次盘点的storeName*/
		session.setAttribute("storeName", "");
		String storeName=(String) message.get("storeName");
		//获取本扫描地的部门
		String departNum="";
		try {
			departNum=storeInfoService.getStoreByName(storeName).getDepartNum();
		} catch (Exception e) {
			departNum="";
		}
		if(departNum!="") {
			//获取本部门的最大批次号
			String maxBatch="";
			try {
				maxBatch=assetInfoService.getMaxBatch(departNum);
				if(maxBatch==null) {
					maxBatch="";
				}
			} catch (Exception e) {
				maxBatch="";
			}
			if(maxBatch!="") {
				/*重新将盘点的信息写入*/
				try {
					/*写进域-成功*/
					session.setAttribute("storeName", storeName);
					result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
					msg="开始盘点";
				}catch(Exception e) {
					/*写进域-失败*/
					result=ControllerResponse.RESPONSE_RESULT_ERROR;
					msg="请选中存放地";
				}
			}else {
				result=ControllerResponse.RESPONSE_RESULT_ERROR;
				msg="本部门未开始新的盘点批次";
			}
		}
		response.setResponseMsg(msg);
		response.setResponseResult(result);
		return response.generateResponse();
	}
	/**
	 * 
	 * @Title: listDicProfitLoss
	 * @Description: 获取所有损益类型名称
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "listDicProfitLoss",method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> listDicProfitLoss()
	{
		ControllerResponse response=ControllerResponseFactory.newInstance();
		List<DicProfitLossInfo> listdicprofitloss=new ArrayList<DicProfitLossInfo>();
		try {
			listdicprofitloss=dicProfitLossInfoService.listAllObject(); 	//获取所有损益类型
			List<String> dicName=new ArrayList<String>();
			for(int i=0;i<listdicprofitloss.size();i++)
			{
				dicName.add(listdicprofitloss.get(i).getDicProfitLossName());
			}
			response.setResponseData(dicName);
		}catch(Exception e) {
			response.setResponseData("空");

		}
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: listDepartStore
	 * @Description: 获取部门的所有存放地名称
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "listDepartStore",method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> listDepartStore(HttpServletRequest request)
	{
		ControllerResponse response=ControllerResponseFactory.newInstance();
		
			HttpSession session = request.getSession();
			UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
			String DepartNum=loginUser.getDepartNum();
			
			// 判断当前部门是否正在运行的盘点批次
			Map<String, Object> condition = new HashMap<>();
			condition.put("departNum", DepartNum);
			try {
				if(assetInfoService.getSwitch_v1(condition)==null){
					response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
					response.setResponseMsg("没有开启盘点任务");
				}else{
					response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
				}
			} catch (Exception e) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				response.setResponseMsg("服务器异常");
			}
			try {
			// 获取下拉框存放地名称
			List<StoreInfo> store=new ArrayList<StoreInfo>();
			store=storeInfoService.listStoreByDepart(DepartNum);
			List<String> storename=new ArrayList<String>();
			for(int i=0;i<store.size();i++)
			{
				storename.add(store.get(i).getStoreName());
			}
			response.setResponseData(storename);
			
		}catch(Exception e) {
			response.setResponseData("空");
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("存放地服务器异常");
		}
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: listDepart
	 * @Description: 获取所有部门的名称
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "listDepart",method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> listDepart()
	{
		ControllerResponse response=ControllerResponseFactory.newInstance();
		
		try {
			List<DepartInfo> depart=departInfoService.listAllObject();
			List<String> departName=new ArrayList<String>();
			for(int i=0;i<depart.size();i++)
			{
				departName.add(depart.get(i).getDepartName());
			}
			response.setResponseData(departName);
		}catch(Exception e) {
			response.setResponseData("空");
		}
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: insetAssetInfo   
	 * @Description: 录入资产 
	 * @param: @param assetInfo
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "insertAssetInfo",method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "添加资产信息")
	public Map<String, Object> insetAssetInfo(@RequestBody Map<String,Object> assetInfo){
		/** 初始化response */
		ControllerResponse response = ControllerResponseFactory.newInstance();
		/** 接收前端传来的参数 */
		String goodsID = (String) assetInfo.get("goodsID");
		String goodsName = (String) assetInfo.get("goodsName");
		String repositoryID = (String) assetInfo.get("repositoryID");
		String goodsprice = (String) assetInfo.get("goodsprice");
		String goodsfactory = (String) assetInfo.get("goodsfactory");
		String documentnum = (String) assetInfo.get("documentnum");
		String goodsbuydate = (String) assetInfo.get("goodsbuydate");
		String goodsPerson = (String) assetInfo.get("goodsPerson");
		String goodsremrk = (String) assetInfo.get("goodsremrk");
		String goodsAddr = (String) assetInfo.get("goodsAddr");
		String goodsType = (String) assetInfo.get("goodsType");
		String goodsDepart = (String) assetInfo.get("goodsDepart");
		String msg="";
		String result="";
		AssetInfo asset=new AssetInfo();
		try {
			asset=assetInfoService.getObject(goodsID);
		} catch (Exception e) {
			asset=null;
		}
		if(asset!=null) {
			result=ControllerResponse.RESPONSE_RESULT_ERROR;
			msg="该资产已存在";
		}else {
			/** 将前端获取数据封装成UserInfo对象 */
			AssetInfo addasset=new AssetInfo();
			try {
				addasset.setAssetNum(goodsID);
			} catch (Exception e) {
				System.out.println("goodsID");
			}
			try {
				addasset.setAssetName(goodsName);
			} catch (Exception e) {
				System.out.println("goodsName");
			}
			try {
				addasset.setAssetTakePeople(goodsPerson);
			} catch (Exception e) {
				System.out.println("goodsPerson");
			}
			try {
				addasset.setStoreNum(storeInfoService.getStoreByName(goodsAddr).getStoreNum());
			} catch (Exception e) {
				System.out.println("setStoreNum");
			}
			try {
				addasset.setDepartNum(departInfoService.getDepartByName(goodsDepart).getDepartNum());
			} catch (Exception e) {
				System.out.println("setDepartNum");
			}
			try {
				addasset.setDicProfitLossNum(dicProfitLossInfoService.getDicProfitLossByName(goodsType).getDicProfitLossNum());
			} catch (Exception e) {
				System.out.println("setDicProfitLossNum");
			}
			try {
				addasset.setAssetModel(repositoryID);
			} catch (Exception e) {
				System.out.println("repositoryID");
			}
			try {
				addasset.setAssetPrice(Float.parseFloat(goodsprice));
			} catch (Exception e) {
				System.out.println("goodsprice");
			}
			try {
				addasset.setAssetDocumentNum(documentnum);
			} catch (Exception e) {
				System.out.println("documentnum");
			}
			try {
				addasset.setAssetBuyDate(goodsbuydate);
			} catch (Exception e) {
				System.out.println("goodsbuydate");
			}
			try {
				addasset.setAssetFactory(goodsfactory);
			} catch (Exception e) {
				System.out.println("goodsfactory");
			}
			try {
				addasset.setAssetRemrk(goodsremrk);
			} catch (Exception e) {
				System.out.println("goodsremrk");
			}
			assetInfoService.addObject(addasset);
			/**添加资产信息操作*/
			msg="添加成功";
			result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
		}

		response.setResponseMsg(msg);
		response.setResponseResult(result);
		return response.generateResponse();
	}
	/***
	 * (扫描过程中...)
	 * return map<string,object>
	 */
	@RequestMapping(value = "InventoryProcess",method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "扫描盘点资产")
	public Map<String, Object> InventoryProcess(HttpServletRequest request,@RequestBody Map<String,Object> assetInfo){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		/*将后端数据进行封装*/
		Map<String,Object> information=new HashMap<String,Object>();
		/*写入盘点时间*/
		Date now=new Date();
		/*将日期格式化*/
		SimpleDateFormat  normalize = new SimpleDateFormat("yyyy-MM-dd");
		String time= normalize.format(now);
		/*存储本部门的所有批次号*/
		List<String> AllBatch=new ArrayList<String>();
		String DepartNum="";
		String assetnum="";
		String storeName="";
		String userCount="";
		int StatusId = 0;
		StoreInfo store=new StoreInfo();
		try {
			UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
			
			DepartNum=loginUser.getDepartNum();
			userCount=loginUser.getUserCount();
			assetnum=(String)assetInfo.get("assetNum");
			storeName=(String)assetInfo.get("storeName");
			try {
				store=storeInfoService.getStoreByName(storeName);
			}catch(Exception e) {
				store=null;
			}
		}catch(Exception e) {
			/*获取接收前端数据/登陆错误*/
			System.out.println("获取接收前端数据/登陆错误");
		}
		/*取出本部门的最新盘点批次号*/
		/*最新批次号*/
		String LatestBatch="";
		try {
			String MaxBatch="";
			AllBatch=assetInfoService.getAllBatchByDepart(DepartNum);
			List<String> TrueBatch=new ArrayList<String>();
			for(int i=0;i<AllBatch.size();i++)
			{
				/*截取批次号的部门编号*/
				String getdepartbybatch=(String)AllBatch.get(i).subSequence(4,6);
				int depart_num=Integer.parseInt(getdepartbybatch);
				String transdepartnum=String.valueOf(depart_num);
				if(transdepartnum.equals(DepartNum))
				{
					TrueBatch.add(AllBatch.get(i));
				}
				else {
					;/*不是本部门的批次号不做处理*/
				}
			}
			/*取出最大批次号*/
			MaxBatch=Collections.max(TrueBatch);
			if(MaxBatch!="")/*正确取出最大批次号*/
			{
				LatestBatch=MaxBatch;
				session.setAttribute("LatestBatch", LatestBatch);
			}else {
				LatestBatch="";
			}
		}catch(Exception e) {
			/*获取最新批次号失败*/
			LatestBatch="";
			System.out.println("获取最新批次号失败");
		}
		/*判断该批次是否开启状态*/
		if(assetInfoService.getSwitch(LatestBatch)==null){
			response.setResponseMsg("管理员未开启新的盘点");
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			return response.generateResponse();
		}
		/*由资产编号获取资产信息*/
		AssetInfo asset_information=new AssetInfo();
		try {
			asset_information=assetInfoService.getObject(assetnum);
		}catch(Exception e) {
			asset_information=null;
		}
		/*资产表中是否存在该资产*/
		if(asset_information!=null)
		{
			/*资产表中存在该资产*/
			String assetdepart=asset_information.getDepartNum();
			/*是否是本部门的资产*/
			if(DepartNum.equals(assetdepart))
			{
				if(store.getStoreNum()!=null)
				{
					/*之前有存放地*/
					try {
						/*是本存放地*/
						if(asset_information.getStoreNum().equals(store.getStoreNum())) {
							//封装条件(获取盘点表的信息)
							Map<String,Object> Conditions=new HashMap<String,Object>();
							Conditions.put("assetNum", asset_information.getAssetNum());
							Conditions.put("inventoryBatch", LatestBatch);
							//获取inventory对象
							Map<String,Object> getinventory=new HashMap<String,Object>();
							try {
								getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							}catch(Exception e) {
								getinventory=null;
							}
							if(getinventory==null){
								/**盘点表的信息被删除*/
								/**将盘点信息写入**/
								assetInfoService.createBatch(LatestBatch, asset_information.getAssetNum());
							}else {
								;
							}
							getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							/*修改扫描地、盘点员编号、盘点时间、盘点状态*/
							try {
								int statusid=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName("已盘点").get("inventoryStatusId");
								Map<String,Object> inventory=new HashMap<String,Object>();
								inventory.put("assetNum", asset_information.getAssetNum());
								inventory.put("inventoryBatch", LatestBatch);
								inventory.put("spanAdress", store.getStoreNum());
								inventory.put("userCount", userCount);
								inventory.put("inventoryDate", time);
								inventory.put("inventoryStatusId", statusid);
								assetInfoService.updateInventory(inventory);
							}catch(Exception e) {
								System.out.println("修改扫描地、盘点员编号、盘点时间、盘点状态-失败");
							}
							//获取修改后的信息
							getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							//将信息传回前端
							information.put("assetNum",asset_information.getAssetNum());
							information.put("assetName", asset_information.getAssetName());
							information.put("inventoryBatch", LatestBatch);
							/*显示盘点状态*/
							try {
								StatusId = (int) getinventory.get("inventoryStatusId");
							} catch (Exception e) {
								StatusId = -1;
							}
							if(StatusId!=-1) {
								try {
									information.put("inventoryStatus", assetInfoService.getInventoryStatusObjByInventoryStatusId(StatusId).get("inventoryStatusName"));
								}catch(Exception e){
									information.put("inventoryStatus","空");
								}
							}
							/*显示盘点时间*/
							try {
								information.put("inventoryDate", getinventory.get("inventoryDate").toString());
							}catch(Exception e) {
								information.put("inventoryDate", "空");
							}
							/*显示存放地*/
							try {
								information.put("inventoryStore",storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
							}catch(Exception e) {
								information.put("inventoryStore", "无");
							}
							/*显示部门*/
							try {
								information.put("inventoryDepart",departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
							}catch(Exception e)
							{
								information.put("inventoryDepart", "空");
							}
							/*显示扫描地*/
							try {
								information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
							}catch(Exception e) {
								information.put("inventoryspanAdrr","空");
							}
						}else {
							/*不是本存放地*/
							//封装条件(获取盘点表的信息)
							Map<String,Object> Conditions=new HashMap<String,Object>();
							Conditions.put("assetNum", asset_information.getAssetNum());
							Conditions.put("inventoryBatch", LatestBatch);
							//获取inventory对象
							Map<String,Object> getinventory=new HashMap<String,Object>();
							try {
								getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							}catch(Exception e) {
								getinventory=null;
							}
							if(getinventory==null){
								assetInfoService.createBatch(LatestBatch, asset_information.getAssetNum());
								getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							}
							/*修改扫描地、盘点员编号、盘点时间、盘点状态*/
							try {
								int statusid=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName("存放地异常").get("inventoryStatusId");
								Map<String,Object> inventory=new HashMap<String,Object>();
								inventory.put("assetNum", asset_information.getAssetNum());
								inventory.put("inventoryBatch", LatestBatch);
								inventory.put("spanAdress", store.getStoreNum());
								inventory.put("userCount", userCount);
								inventory.put("inventoryDate", time);
								inventory.put("inventoryStatusId", statusid);
								assetInfoService.updateInventory(inventory);
							}catch(Exception e) {
								System.out.println("修改扫描地、盘点员编号、盘点时间、盘点状态-失败");
							}
							//获取修改后的信息
							getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							//将信息传回前端
							information.put("assetNum",asset_information.getAssetNum());
							information.put("assetName", asset_information.getAssetName());
							information.put("inventoryBatch", LatestBatch);
							/*显示盘点状态*/
							try {
								StatusId = (int) getinventory.get("inventoryStatusId");
							} catch (Exception e) {
								StatusId = -1;
							}
							if(StatusId!=-1) {
								try {
									information.put("inventoryStatus", assetInfoService.getInventoryStatusObjByInventoryStatusId(StatusId).get("inventoryStatusName"));
								}catch(Exception e){
									information.put("inventoryStatus","空");
								}
							}
							/*显示盘点时间*/
							try {
								information.put("inventoryDate", getinventory.get("inventoryDate").toString());
							}catch(Exception e) {
								information.put("inventoryDate", "空");
							}
							/*显示存放地*/
							try {
								information.put("inventoryStore",storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
							}catch(Exception e) {
								information.put("inventoryStore", "无");
							}
							/*显示部门*/
							try {
								information.put("inventoryDepart",departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
							}catch(Exception e)
							{
								information.put("inventoryDepart", "空");
							}
							/*显示扫描地*/
							try {
								information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
							}catch(Exception e) {
								information.put("inventoryspanAdrr","空");
							}
						}
					}catch(Exception e)
					{
						/*之前无存放地*/
						//封装条件(获取盘点表的信息)
						Map<String,Object> Conditions=new HashMap<String,Object>();
						Conditions.put("assetNum", asset_information.getAssetNum());
						Conditions.put("inventoryBatch", LatestBatch);
						//获取inventory对象
						Map<String,Object> getinventory=new HashMap<String,Object>();
						try {
							getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
						}catch(Exception e1) {
							getinventory=null;
						}
						if(getinventory==null){
							assetInfoService.createBatch(LatestBatch, asset_information.getAssetNum());
							getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
						}
						/*修改扫描地、盘点员编号、盘点时间、盘点状态*/
						try {
							int statusid=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName("已盘点").get("inventoryStatusId");
							Map<String,Object> inventory=new HashMap<String,Object>();
							inventory.put("assetNum", asset_information.getAssetNum());
							inventory.put("inventoryBatch", LatestBatch);
							inventory.put("spanAdress", store.getStoreNum());
							inventory.put("userCount", userCount);
							inventory.put("inventoryDate", time);
							inventory.put("inventoryStatusId", statusid);
							assetInfoService.updateInventory(inventory);
						}catch(Exception e1) {
							System.out.println("修改扫描地、盘点员编号、盘点时间、盘点状态-失败");
						}
						//获取修改后的信息
						getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
						//将信息传回前端
						information.put("assetNum",asset_information.getAssetNum());
						information.put("assetName", asset_information.getAssetName());
						information.put("inventoryBatch", LatestBatch);
						/*显示盘点状态*/
						try {
							StatusId = (int) getinventory.get("inventoryStatusId");
						} catch (Exception e1) {
							StatusId = -1;
						}
						if(StatusId!=-1) {
							try {
								information.put("inventoryStatus", assetInfoService.getInventoryStatusObjByInventoryStatusId(StatusId).get("inventoryStatusName"));
							}catch(Exception e1){
								information.put("inventoryStatus","空");
							}
						}
						/*显示盘点时间*/
						try {
							information.put("inventoryDate", getinventory.get("inventoryDate").toString());
						}catch(Exception e1) {
							information.put("inventoryDate", "空");
						}
						/*显示存放地*/
						try {
							information.put("inventoryStore",storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
						}catch(Exception e1) {
							information.put("inventoryStore", "无");
						}
						/*显示部门*/
						try {
							information.put("inventoryDepart",departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
						}catch(Exception e1)
						{
							information.put("inventoryDepart", "空");
						}
						/*显示扫描地*/
						try {
							information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
						}catch(Exception e1) {
							information.put("inventoryspanAdrr","空");
						}
						/*将存放地写入到资产表中*/
						try {
							/*修改资产表中的信息*/
							AssetInfo newasset=new AssetInfo();
							newasset.setAssetNum(asset_information.getAssetNum());
							newasset.setAssetName(asset_information.getAssetName());
							newasset.setAssetModel(asset_information.getAssetModel());
							newasset.setAssetPrice(asset_information.getAssetPrice());
							newasset.setAssetFactory(asset_information.getAssetFactory());
							newasset.setAssetDocumentNum(asset_information.getAssetDocumentNum());
							newasset.setAssetBuyDate(asset_information.getAssetBuyDate());
							newasset.setAssetTakePeople(asset_information.getAssetTakePeople());
							newasset.setAssetRemrk(asset_information.getAssetRemrk());
							newasset.setStoreNum(store.getStoreNum());
							newasset.setDicProfitLossNum(asset_information.getDicProfitLossNum());
							newasset.setDepartNum(asset_information.getDepartNum());
							assetInfoService.updateObject(newasset);
							System.out.println("写入资产表-成功");
						}catch(Exception e1) {
							System.out.println("写入资产表-失败");
						}
					}
				}else {
					/*前端传来的存放地获取失败*/
				}
			}
			else {
				/*不是本部门的资产信息*/
				//判断盘点表中是否已存在
				/*将信息写入盘点表*/
				//封装条件(获取盘点表的信息)
				Map<String,Object> Conditions=new HashMap<String,Object>();
				Conditions.put("assetNum", asset_information.getAssetNum());
				Conditions.put("inventoryBatch", LatestBatch);
				//获取inventory对象
				Map<String,Object> getinventory=new HashMap<String,Object>();
				try {
					getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
				}catch(Exception e) {
					getinventory=null;
				}
				if(getinventory==null){
					assetInfoService.createBatch(LatestBatch, asset_information.getAssetNum());
					System.out.println("部门异常-信息写入盘点表");
					getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
				}
				/*修改扫描地、盘点员、盘点时间、盘点状态*/
				try {
					int statusid=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName("部门异常").get("inventoryStatusId");
					Map<String,Object> inventory=new HashMap<String,Object>();
					inventory.put("assetNum", asset_information.getAssetNum());
					inventory.put("inventoryBatch", LatestBatch);
					inventory.put("spanAdress", store.getStoreNum());
					inventory.put("userCount", userCount);
					inventory.put("inventoryDate", time);
					inventory.put("inventoryStatusId", statusid);
					assetInfoService.updateInventory(inventory);
				}catch(Exception e1) {
					System.out.println("修改扫描地、盘点员、盘点时间、盘点状态-失败");
				}
				//获取修改后的信息
				getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
				//将信息传回前端
				information.put("assetNum", asset_information.getAssetNum());
				information.put("assetName", asset_information.getAssetName());
				information.put("inventoryBatch", LatestBatch);
				/*显示盘点状态*/
				try {
					StatusId = (int) getinventory.get("inventoryStatusId");
				} catch (Exception e1) {
					StatusId = -1;
				}
				if(StatusId!=-1) {
					try {
						information.put("inventoryStatus", assetInfoService.getInventoryStatusObjByInventoryStatusId(StatusId).get("inventoryStatusName"));
					}catch(Exception e1){
						information.put("inventoryStatus","空");
					}
				}
				/*显示盘点时间*/
				try {
					information.put("inventoryDate", getinventory.get("inventoryDate").toString());
				}catch(Exception e1) {
					information.put("inventoryDate", "空");
				}
				/*显示存放地*/
				try {
					information.put("inventoryStore",storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
				}catch(Exception e1) {
					information.put("inventoryStore", "无");
				}
				/*显示部门*/
				try {
					information.put("inventoryDepart",departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
				}catch(Exception e1)
				{
					information.put("inventoryDepart", "空");
				}
				/*显示扫描地*/
				try {
					information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
				}catch(Exception e) {
					information.put("inventoryspanAdrr","空");
				}
			}
		}else {
			/*资产表中不存在该资产*/
		}
	response.setObjectInfo("LatestBatch", LatestBatch);
	response.setResponseData(information);
	return response.generateResponse();
	}
}