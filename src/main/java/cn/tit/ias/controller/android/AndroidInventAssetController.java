package cn.tit.ias.controller.android;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.service.DicProfitLossInfoService;
import cn.tit.ias.service.StoreInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;

/**
 * 
 * 
 * @Description:盘点资产信息请求   
 * @author: maotao 
 * @date:   2019年6月16日 上午8:27:51       
 *
 */
@Controller
@RequestMapping("/AndroidInventAsset")
public class AndroidInventAssetController {
	@Autowired
	private AssetInfoService assetInfoService;
	@Autowired  
	private DepartInfoService departInfoService;
	@Autowired
	private StoreInfoService storeInfoService;
	@Autowired
	private DicProfitLossInfoService dicprofitlossInfoService;
	
	
	/**
	 * 
	 * @Title: query   
	 * @Description: 通用记录查询   
	 * @param: @param searchType 查询类型
	 * @param: @return  返回所有符合条件的记录    
	 * @return: Map<String,Object>      
	 * @throws
	 */
	private Map<String, Object> query(String searchType){
		
		return null;
	}
	
	/**
	 * 
	 * @Title: createInventoryBatch   
	 * @Description: 创建盘点批次号   
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "createInventoryBatch",method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> createInventoryBatch(){
		
		return null;
	}
	
	/**
	 * 
	 * @Title: autoInventory   
	 * @Description: 自动盘点   
	 * @param: @param assetInfoNum
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "autoInventory",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> autoInventory(@RequestParam("assetInfoNum") String assetInfoNum ){
		return null;
	}
	
	/**
	 * 
	 * @Title: MoreInformation   
	 * @Description: 详细信息  
	 * @param: @param assetInfoNum
	 * @param: @return   返回
	 * @return: Map<String,Object>
	 * @throws
	 */
	@RequestMapping(value = "MoreInformation",method = RequestMethod.POST)
	@ResponseBody
	//@MethodLog(name = "android模块",option = "查看资产详情")
	public Map<String, Object> MoreInformation(HttpServletRequest request,@RequestBody Map<String,Object> assetInfo){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		/*获取前端传来资产编号*/
		String assetNum=(String) assetInfo.get("assetNum");
		Map<String,Object> information=new HashMap<String,Object>();
		AssetInfo asset=new AssetInfo();
		try {
			asset=assetInfoService.getObject(assetNum);
		}catch(Exception e) {
			asset=null;
		}
		if(asset!=null)
		{
			information.put("AssetNum", asset.getAssetNum());
			information.put("AssetName", asset.getAssetName());
			information.put("AssetModel", asset.getAssetModel());
			try {
				information.put("AssetStoreNum", storeInfoService.getObject(asset.getStoreNum()).getStoreName());
			}catch(Exception e) {
				information.put("AssetStoreNum", "空");
			}
			information.put("AssetTakePeople", asset.getAssetTakePeople());
			try {
				information.put("AssetDepartNum", departInfoService.getObject(asset.getDepartNum()).getDepartName());
			}catch(Exception e) {
				information.put("AssetDepartNum", "空");
			}
			information.put("AssetPrice", asset.getAssetPrice());
			try {
				information.put("DicProfitLossNum", dicprofitlossInfoService.getObject(asset.getDicProfitLossNum()).getDicProfitLossName());
			}catch(Exception e) {
				information.put("DicProfitLossNum", "空");
			}
			information.put("AssetDocumentNum", asset.getAssetDocumentNum());
			information.put("AssetBuyDate", asset.getAssetBuyDate());
			information.put("AssetFactory", asset.getAssetFactory());
			information.put("AssetRemrk", asset.getAssetRemrk());
			response.setResponseData(information);
		}else {
			/*资产信息获取错误*/
		}
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: haveInventory   
	 * @Description: 已盘点
	 * @param selectStore 是直接查看盘点结果时，选择的扫描地
	 * session 域中获取的store时盘点时的扫描地  
	 * @param: @return   
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "haveInventory" ,method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "查看已盘点")
	public Map<String, Object> haveInventory(HttpServletRequest request,@RequestBody Map<String,Object>selectStore){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		String msg="";
		/*盘点状态*/
		int Status=-1;
		/*存放地的所有资产*/
		List<String> listassetinfo=new ArrayList<String>();
		/*获取登陆用户的部门最新批次号*/
		String LatestBatch="";
		/*已盘点个数*/
		int inventoryCount = 0;
		try {
			UserInfo loginUser=new UserInfo();
			try {
				loginUser =(UserInfo) session.getAttribute("loginUser");
			}catch(Exception e) {
				loginUser=null;
			}
			if(loginUser!=null)
			{
			   String DepartNum=loginUser.getDepartNum();
			   List<Integer> TrueBatch=new ArrayList<Integer>();
			 //获取该部门的所有批次号
			  	List<String> AllBatch=assetInfoService.getAllBatchByDepart(DepartNum);
			  	//判断是不是本部门的批次
			  	for(int i=0;i<AllBatch.size();i++)
			  	{
			  		//截取批次号里的部门编号
			  		String getdepartbybatch=(String) AllBatch.get(i).subSequence(4,6);
			  		int depart_num=Integer.parseInt(getdepartbybatch);
			  		String transdepartnum=String.valueOf(depart_num);
			  		//本部门的放到List中
			  		if(transdepartnum.equals(DepartNum))
			  		{
			  			int transbatch=Integer.parseInt(AllBatch.get(i));
			  			TrueBatch.add(transbatch);
			  		}
			  		else
			  		{;}
			  	}
			  	//找出这个部门批次号的最大值
			  	if(TrueBatch.size()!=0)
			  	{
			  		int	MaxBatch=Collections.max(TrueBatch);
			  		LatestBatch=String.valueOf(MaxBatch);
			  	}
			}else
			{
				LatestBatch="";
			}
		}catch(Exception e) {
			LatestBatch="";
		}
		/*扫描地名称*/
		String storeName="";
		/*扫描地编号*/
		int storenum=0;
		try {
			if(selectStore!=null){
				storeName=(String) selectStore.get("select_store");
			}else{
				storeName=(String) session.getAttribute("storeName");
			}
			
		}catch(Exception e) {
			storeName=null;
		}
		if(LatestBatch.equals("")) {
			/*该部门没有批次号*/
			msg="该部门没有批次号";
		}else {
			/*该部门有批次号*/
			msg="";
			if(storeName!=null)
			{
				/*由扫描地名称获取存放地编号*/
				try {
					storenum=storeInfoService.getStoreByName(storeName).getStoreNum();
				}catch(Exception e)
				{
					storenum=0;
				}
				if(storenum!=0) {
					/*存放地编号和批次号获取所有资产编号*/
					try {
						listassetinfo=assetInfoService.listAssetNumByBatchStoreNum(LatestBatch, storenum);
					}catch(Exception e) {
						listassetinfo=null;
					}
					if(listassetinfo!=null)
					{
						for(int i=0;i<listassetinfo.size();i++)
						{
							//封装条件
							Map<String,Object> Conditions=new HashMap<String,Object>();
							Conditions.put("assetNum", listassetinfo.get(i));
							Conditions.put("inventoryBatch", LatestBatch);
							//获取inventory对象
							Map<String,Object> getinventory=new HashMap<String,Object>();
							getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
							
							try {
							Status=(int) getinventory.get("inventoryStatusId");
							}catch(Exception e) {
								Status=-1;
							}
							if(Status!=-1)
							{
								String statusName=(String) assetInfoService.getInventoryStatusObjByInventoryStatusId(Status).get("inventoryStatusName");
								if(statusName.equals("已盘点"))
								{
									/*已盘点*/
									Map<String, Object> information = new HashMap<String,Object>();
									try {
										AssetInfo asset=assetInfoService.getObject(listassetinfo.get(i));
										information.put("assetNum",asset.getAssetNum());
										information.put("assetName", asset.getAssetName());
										inventoryCount++;
									} catch (Exception e) {
										information.put("assetNum",listassetinfo.get(i));
										information.put("assetName", "已移除");
									}
									try {
										information.put("inventoryStore", storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
									}catch(Exception e){
										information.put("inventoryStore", "空");
									}
									try {
										information.put("inventoryDepart", departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
									}catch(Exception e){
										information.put("inventoryDepart", "空");
									}
									information.put("inventoryBatch", LatestBatch);
									try {
										information.put("inventoryDate", getinventory.get("inventoryDate").toString());
									}catch(Exception e) {
										information.put("inventoryDate", "空");
									}
									try {
										information.put("inventoryStatus",statusName);
									}catch(Exception e) {
										information.put("inventoryStatus", "空");
									}
									/*显示扫描地*/
									try {
										information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
									}catch(Exception e) {
										information.put("inventoryspanAdrr","空");
									}
									listFinalResult.add(information);
									response.setResponseData(listFinalResult);
									}else {
										/*不是未盘点不做操作*/
									}
								}else {
									/*盘点状态获取错误*/
								}
						}
					}else
					{
						/*该存放地不存在资产*/
					}
				}else {
					//获取存放地编号错误
				}
			}else {
				/*获取session中的部门名称失败*/
				System.out.println("获取session中的扫描地名称失败");
			}
		}
		String result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		response.setResponseMsg(msg);
		response.setResponseTotal(inventoryCount);
		inventoryCount=0;
		response.setResponseResult(result);
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: notInventory   
	 * @Description: 未盘点   
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "notInventory" ,method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "查看未盘点")
	public Map<String, Object> notInventory(HttpServletRequest request,@RequestBody Map<String, Object>selectStore){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		/*盘点状态*/
		int Status=-1;
		/*存放地的所有资产*/
		List<String> listassetinfo=new ArrayList<String>();
		/*获取登陆用户的部门最新批次号*/
		String LatestBatch="";
		/*未盘点个数*/
		int notInventoryCount = 0;
		try {
			UserInfo loginUser=new UserInfo();
			try {
				loginUser =(UserInfo) session.getAttribute("loginUser");
			}catch(Exception e) {
				loginUser=null;
			}
			if(loginUser!=null)
			{
			   String DepartNum=loginUser.getDepartNum();
			   List<Integer> TrueBatch=new ArrayList<Integer>();
			 //获取该部门的所有批次号
			  	List<String> AllBatch=assetInfoService.getAllBatchByDepart(DepartNum);
			  	//判断是不是本部门的批次
			  	for(int i=0;i<AllBatch.size();i++)
			  	{
			  		//截取批次号里的部门编号
			  		String getdepartbybatch=(String) AllBatch.get(i).subSequence(4,6);
			  		int depart_num=Integer.parseInt(getdepartbybatch);
			  		String transdepartnum=String.valueOf(depart_num);
			  		//本部门的放到List中
			  		if(transdepartnum.equals(DepartNum))
			  		{
			  			int transbatch=Integer.parseInt(AllBatch.get(i));
			  			TrueBatch.add(transbatch);
			  		}
			  		else
			  		{;}
			  	}
			  	//找出这个部门批次号的最大值
			  	if(TrueBatch.size()!=0)
			  	{
			  		int	MaxBatch=Collections.max(TrueBatch);
			  		LatestBatch=String.valueOf(MaxBatch);
			  	}
			}else
			{
				LatestBatch="";
			}
		}catch(Exception e) {
			LatestBatch="";
		}
		/*扫描地名称*/
		String storeName="";
		/*扫描地编号*/
		int storenum=0;
		try {
			if(selectStore != null){
				storeName = (String) selectStore.get("select_store");
			}else{
				storeName=(String) session.getAttribute("storeName");
			}
			
		}catch(Exception e) {
			storeName=null;
		}
		if(storeName!=null)
		{
			/*由扫描地名称获取存放地编号*/
			try {
				storenum=storeInfoService.getStoreByName(storeName).getStoreNum();
			}catch(Exception e)
			{
				storenum=0;
			}
			if(storenum!=0) {
				/*扫描地编号和批次号获取所有资产编号*/
				try {
					listassetinfo=assetInfoService.listAssetNumByBatchStoreNum(LatestBatch, storenum);
					
				}catch(Exception e) {
					listassetinfo=null;
				}
				if(listassetinfo!=null)
				{
					for(int i=0;i<listassetinfo.size();i++)
					{
						//封装条件
						Map<String,Object> Conditions=new HashMap<String,Object>();
						Conditions.put("assetNum", listassetinfo.get(i));
						Conditions.put("inventoryBatch", LatestBatch);
						//获取inventory对象
						Map<String,Object> getinventory=new HashMap<String,Object>();
						getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
						
						try {
						Status=(int) getinventory.get("inventoryStatusId");
						}catch(Exception e) {
							Status=-1;
						}
						if(Status!=-1)
						{
							String statusName=(String) assetInfoService.getInventoryStatusObjByInventoryStatusId(Status).get("inventoryStatusName");
							if(statusName.equals("未盘点"))
							{
								/*未盘点*/
								Map<String, Object> information = new HashMap<String,Object>();
								try {
									AssetInfo asset=assetInfoService.getObject(listassetinfo.get(i));
									information.put("assetNum",asset.getAssetNum());
									information.put("assetName", asset.getAssetName());
									notInventoryCount++;
								} catch (Exception e) {
									information.put("assetNum",listassetinfo.get(i));
									information.put("assetName", "已移除");
								}
								try {
									information.put("inventoryStore", storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
								}catch(Exception e){
									information.put("inventoryStore", "空");
								}
								try {
									information.put("inventoryDepart", departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
								}catch(Exception e){
									information.put("inventoryDepart", "空");
								}
								information.put("inventoryBatch", LatestBatch);
								try {
									information.put("inventoryDate", getinventory.get("inventoryDate").toString());
								}catch(Exception e) {
									information.put("inventoryDate", "空");
								}
								try {
									information.put("inventoryStatus",statusName);
								}catch(Exception e) {
									information.put("inventoryStatus", "空");
								}
								/*显示扫描地*/
								try {
									information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
								}catch(Exception e) {
									information.put("inventoryspanAdrr","空");
								}
								listFinalResult.add(information);
								response.setResponseData(listFinalResult);
								}else {
									/*不是未盘点不做操作*/
								}
							}else {
								/*盘点状态获取错误*/
							}
					}
				}else
				{
					/*该存放地不存在资产*/
				}
			}else {
				//获取存放地编号错误
			}
		}else {
			/*获取session中的扫描地名称失败*/
		}
		String result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		response.setResponseResult(result);
		response.setResponseTotal(notInventoryCount);
		notInventoryCount=0;
		return response.generateResponse();
	}
	
	
	/**
	 * @param selectStore 是直接查看盘点结果时，选择的扫描地
     * session 域中获取的store时盘点时的扫描地  
	 * @Title: errorInventoryStore   
	 * @Description: 盘点异常（存放地）   
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "errorInventoryStore" ,method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "查看存放地异常")
	public Map<String, Object> errorInventoryStore(HttpServletRequest request,@RequestBody Map<String, Object>selectStore){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		/*盘点状态*/
		int Status=-1;
		/*存放地的所有资产*/
		List<String> listassetinfo=new ArrayList<String>();
		/*获取登陆用户的部门最新批次号*/
		String LatestBatch="";
		/*存放地异常个数*/
		int abnormalStoreCount = 0;
		try {
			UserInfo loginUser=new UserInfo();
			try {
				loginUser =(UserInfo) session.getAttribute("loginUser");
			}catch(Exception e) {
				loginUser=null;
			}
			if(loginUser!=null)
			{
			   String DepartNum=loginUser.getDepartNum();
			   List<Integer> TrueBatch=new ArrayList<Integer>();
			 //获取该部门的所有批次号
			  	List<String> AllBatch=assetInfoService.getAllBatchByDepart(DepartNum);
			  	//判断是不是本部门的批次
			  	for(int i=0;i<AllBatch.size();i++)
			  	{
			  		//截取批次号里的部门编号
			  		String getdepartbybatch=(String) AllBatch.get(i).subSequence(4,6);
			  		int depart_num=Integer.parseInt(getdepartbybatch);
			  		String transdepartnum=String.valueOf(depart_num);
			  		//本部门的放到List中
			  		if(transdepartnum.equals(DepartNum))
			  		{
			  			int transbatch=Integer.parseInt(AllBatch.get(i));
			  			TrueBatch.add(transbatch);
			  		}
			  		else
			  		{;}
			  	}
			  	//找出这个部门批次号的最大值
			  	if(TrueBatch.size()!=0)
			  	{
			  		int	MaxBatch=Collections.max(TrueBatch);
			  		LatestBatch=String.valueOf(MaxBatch);
			  	}
			}else
			{
				LatestBatch="";
			}
		}catch(Exception e) {
			LatestBatch="";
		}
		/*扫描地名称*/
		String storeName="";
		/*扫描地编号*/
		int storenum=0;
		try {
			if(selectStore != null){
				storeName = (String) selectStore.get("select_store");
			}else{
				storeName=(String) session.getAttribute("storeName");
			}
		}catch(Exception e) {
			storeName=null;
		}
		if(storeName!=null)
		{
			/*由扫描地名称获取存放地编号*/
			try {
				storenum=storeInfoService.getStoreByName(storeName).getStoreNum();
			}catch(Exception e)
			{
				storenum=0;
			}
			if(storenum!=0) {
				/*扫描地编号和批次号获取所有资产编号*/
				try {
					listassetinfo=assetInfoService.listAssetNumByBatchSpanAdress(LatestBatch, storenum);
				}catch(Exception e) {
					listassetinfo=null;
				}
				if(listassetinfo!=null)
				{
					for(int i=0;i<listassetinfo.size();i++)
					{
						//封装条件
						Map<String,Object> Conditions=new HashMap<String,Object>();
						Conditions.put("assetNum", listassetinfo.get(i));
						Conditions.put("inventoryBatch", LatestBatch);
						//获取inventory对象
						Map<String,Object> getinventory=new HashMap<String,Object>();
						getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
						
						try {
						Status=(int) getinventory.get("inventoryStatusId");
						}catch(Exception e) {
							Status=-1;
						}
						if(Status!=-1)
						{
							String statusName=(String) assetInfoService.getInventoryStatusObjByInventoryStatusId(Status).get("inventoryStatusName");
							if(statusName.equals("存放地异常"))
							{
								/*存放地异常*/
								Map<String, Object> information = new HashMap<String,Object>();
								try {
									AssetInfo asset=assetInfoService.getObject(listassetinfo.get(i));
									information.put("assetNum",asset.getAssetNum());
									information.put("assetName", asset.getAssetName());
									abnormalStoreCount++;
								} catch (Exception e) {
									information.put("assetNum",listassetinfo.get(i));
									information.put("assetName", "已移除");
								}
								try {
									information.put("inventoryStore", storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
								}catch(Exception e){
									information.put("inventoryStore", "空");
								}
								try {
									information.put("inventoryDepart", departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
								}catch(Exception e){
									information.put("inventoryDepart", "空");
								}
								information.put("inventoryBatch", LatestBatch);
								try {
									information.put("inventoryDate", getinventory.get("inventoryDate").toString());
								}catch(Exception e) {
									information.put("inventoryDate", "空");
								}
								try {
									information.put("inventoryStatus",statusName);
								}catch(Exception e) {
									information.put("inventoryStatus", "空");
								}
								/*显示扫描地*/
								try {
									information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
								}catch(Exception e) {
									information.put("inventoryspanAdrr","空");
								  }
								listFinalResult.add(information);
								response.setResponseData(listFinalResult);
								}else {
									/*不是未盘点不做操作*/
								}
							}else {
								/*盘点状态获取错误*/
							}
					}
				}else
				{
					/*该存放地不存在资产*/
				}
			}else {
				//获取存放地编号错误
			}
		}else {
			/*获取session中的扫描地名称失败*/
		}
		String result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		response.setResponseResult(result);
		response.setResponseTotal(abnormalStoreCount);
		abnormalStoreCount=0;
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: errorInventoryStore   
	 * @Description: 盘点异常（部门）   
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "errorInventoryDepart" ,method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "查看部门异常")
	public Map<String, Object> errorInventoryDepart(HttpServletRequest request,@RequestBody Map<String, Object> selectStore){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		/*盘点状态*/
		int Status=-1;
		/*扫描的所有资产*/
		List<String> listassetinfo=new ArrayList<String>();
		/*获取登陆用户的部门最新批次号*/
		String LatestBatch="";
		/*部门异常个数*/
		int abnormalDepartCount = 0;
		try {
			UserInfo loginUser=new UserInfo();
			try {
				loginUser =(UserInfo) session.getAttribute("loginUser");
			}catch(Exception e) {
				loginUser=null;
			}
			if(loginUser!=null)
			{
			   String DepartNum=loginUser.getDepartNum();
			   List<Integer> TrueBatch=new ArrayList<Integer>();
			 //获取该部门的所有批次号
			  	List<String> AllBatch=assetInfoService.getAllBatchByDepart(DepartNum);
			  	//判断是不是本部门的批次
			  	for(int i=0;i<AllBatch.size();i++)
			  	{
			  		//截取批次号里的部门编号
			  		String getdepartbybatch=(String) AllBatch.get(i).subSequence(4,6);
			  		int depart_num=Integer.parseInt(getdepartbybatch);
			  		String transdepartnum=String.valueOf(depart_num);
			  		//本部门的放到List中
			  		if(transdepartnum.equals(DepartNum))
			  		{
			  			int transbatch=Integer.parseInt(AllBatch.get(i));
			  			TrueBatch.add(transbatch);
			  		}
			  		else
			  		{;}
			  	}
			  	//找出这个部门批次号的最大值
			  	if(TrueBatch.size()!=0)
			  	{
			  		int	MaxBatch=Collections.max(TrueBatch);
			  		LatestBatch=String.valueOf(MaxBatch);
			  	}
			}else
			{
				LatestBatch="";
			}
		}catch(Exception e) {
			LatestBatch="";
		}
		/*扫描地名称*/
		String storeName="";
		/*扫描地编号*/
		int storenum=0;
		try {
			if(selectStore != null){
				storeName = (String) selectStore.get("select_store");
			}else{
				storeName=(String) session.getAttribute("storeName");
			}
			
		}catch(Exception e) {
			storeName=null;
		}
		if(storeName!=null)
		{
			/*由扫描地名称获取存放地编号*/
			try {
				storenum=storeInfoService.getStoreByName(storeName).getStoreNum();
			}catch(Exception e)
			{
				storenum=0;
			}
			if(storenum!=0) {
				/*扫描地编号和批次号获取所有资产编号*/
				try {
					listassetinfo=assetInfoService.listAssetNumByBatchSpanAdress(LatestBatch, storenum);
				}catch(Exception e) {
					listassetinfo=null;
				}
				if(listassetinfo!=null)
				{
					for(int i=0;i<listassetinfo.size();i++)
					{
						//封装条件
						Map<String,Object> Conditions=new HashMap<String,Object>();
						Conditions.put("assetNum", listassetinfo.get(i));
						Conditions.put("inventoryBatch", LatestBatch);
						//获取inventory对象
						Map<String,Object> getinventory=new HashMap<String,Object>();
						getinventory=assetInfoService.getInventoryObjByCondition(Conditions);
						
						try {
						Status=(int) getinventory.get("inventoryStatusId");
						}catch(Exception e) {
							Status=-1;
						}
						if(Status!=-1)
						{
							String statusName=(String) assetInfoService.getInventoryStatusObjByInventoryStatusId(Status).get("inventoryStatusName");
							if(statusName.equals("部门异常"))
							{
								/*部门异常*/
								Map<String, Object> information = new HashMap<String,Object>();
								try {
									AssetInfo asset=assetInfoService.getObject(listassetinfo.get(i));
									information.put("assetNum",asset.getAssetNum());
									information.put("assetName", asset.getAssetName());
									abnormalDepartCount++;
								} catch (Exception e) {
									information.put("assetNum",listassetinfo.get(i));
									information.put("assetName", "已移除");
								}
								try {
									information.put("inventoryStore", storeInfoService.getObject((int)getinventory.get("storeNum")).getStoreName());
								}catch(Exception e){
									information.put("inventoryStore", "空");
								}
								try {
									information.put("inventoryDepart", departInfoService.getObject(getinventory.get("departNum").toString()).getDepartName());
								}catch(Exception e){
									information.put("inventoryDepart", "空");
								}
								information.put("inventoryBatch", LatestBatch);
								try {
									information.put("inventoryDate", getinventory.get("inventoryDate").toString());
								}catch(Exception e) {
									information.put("inventoryDate", "空");
								}
								try {
									information.put("inventoryStatus",statusName);
								}catch(Exception e) {
									information.put("inventoryStatus", "空");
								}
								/*显示扫描地*/
								try {
									information.put("inventoryspanAdrr",storeInfoService.getObject((int)getinventory.get("spanAdress")).getStoreName());
								}catch(Exception e) {
									information.put("inventoryspanAdrr","空");
								}
								listFinalResult.add(information);
								response.setResponseData(listFinalResult);
								}else {
									/*不是未盘点不做操作*/
								}
							}else {
								/*盘点状态获取错误*/
							}
					}
				}else
				{
					/*该存放地不存在资产*/
				}
			}else {
				//获取存放地编号错误
			}
		}else {
			/*获取session中的扫描地名称失败*/
		}
		String result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		response.setResponseResult(result);
		response.setResponseTotal(abnormalDepartCount);
		System.out.println("部门异常个数"+abnormalDepartCount);
		abnormalDepartCount=0;
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: closeInventory   
	 * @Description: 结束盘点
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "closeInventory" ,method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "结束盘点")
	public Map<String, Object> closeInventory(HttpServletRequest request){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		/*清空扫描地信息*/
		session.setAttribute("storeName","");
		/*清空批次号*/
		session.setAttribute("LatestBatch","");
		return response.generateResponse();
	}
	/**
	 * 
	 * @Title: batchUpdate   
	 * @Description: 批量修改
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "batchUpdate" ,method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "android模块",option = "批量修改")
	public Map<String, Object> batchUpdate(HttpServletRequest request,@RequestBody Map<String,Object> assetinfo){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		List<String> ArrayAssetNum=new ArrayList<String>();
		String type="";
		String record="";
		String result="";
		String msg="";
		Integer spanAddr=000;
		/*获取登陆用户的部门最新批次号*/
		String LatestBatch="";
		try {
			UserInfo loginUser=new UserInfo();
			try {
				loginUser =(UserInfo) session.getAttribute("loginUser");
			}catch(Exception e) {
				loginUser=null;
			}
			if(loginUser!=null)
			{
			   String DepartNum=loginUser.getDepartNum();
			   List<Integer> TrueBatch=new ArrayList<Integer>();
			 //获取该部门的所有批次号
			  	List<String> AllBatch=assetInfoService.getAllBatchByDepart(DepartNum);
			  	//判断是不是本部门的批次
			  	for(int i=0;i<AllBatch.size();i++)
			  	{
			  		//截取批次号里的部门编号
			  		String getdepartbybatch=(String) AllBatch.get(i).subSequence(4,6);
			  		int depart_num=Integer.parseInt(getdepartbybatch);
			  		String transdepartnum=String.valueOf(depart_num);
			  		//本部门的放到List中
			  		if(transdepartnum.equals(DepartNum))
			  		{
			  			int transbatch=Integer.parseInt(AllBatch.get(i));
			  			TrueBatch.add(transbatch);
			  		}
			  		else
			  		{;}
			  	}
			  	//找出这个部门批次号的最大值
			  	if(TrueBatch.size()!=0)
			  	{
			  		int	MaxBatch=Collections.max(TrueBatch);
			  		LatestBatch=String.valueOf(MaxBatch);
			  	}
			}else
			{
				LatestBatch="";
			}
		}catch(Exception e) {
			LatestBatch="";
		}
		/*获取前端传来的数据*/
		try {
			ArrayAssetNum=(List<String>) assetinfo.get("assetNum");
			type=(String) assetinfo.get("type");
			record=(String) assetinfo.get("data");
		}catch(Exception e)
		{
			ArrayAssetNum=null;
			type=null;
			record=null;
		}
		if(ArrayAssetNum!=null)
		{
			if(type.equals("盘点状态")){
				/*获取盘点状态编号*/
				int StatusID=-1;
				try {
					String spanAddrName = (String) assetinfo.get("span_addr");
					spanAddr =storeInfoService.getStoreByName(spanAddrName).getStoreNum();
					if(record.equals("未盘点")){
						spanAddr = 000;
					}
					
					
					StatusID=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName(record).get("inventoryStatusId");
				}catch(Exception e) {
					StatusID=-1;
				}
				try {
					for(int i=0;i<ArrayAssetNum.size();i++)
						{
							Map<String,Object> updateinventory=new HashMap<String,Object>();
							updateinventory.put("assetNum", ArrayAssetNum.get(i));
							updateinventory.put("inventoryBatch", LatestBatch);
							updateinventory.put("spanAdress", spanAddr);
							/*修改状态*/
							updateinventory.put("inventoryStatusId", StatusID);
							assetInfoService.updateInventory(updateinventory);
						}
					msg="修改盘点状态成功-"+record;
					result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
				}catch(Exception e) {
					/*修改失败*/
					result=ControllerResponse.RESPONSE_RESULT_ERROR;
					msg="修改盘点状态失败";
				}
			}
			else if(type.equals("损益类型")) {
				try {
					for(int i=0;i<ArrayAssetNum.size();i++)
					{
						AssetInfo asset=new AssetInfo();
						asset=assetInfoService.getObject(ArrayAssetNum.get(i));
						/*修改损益类型-资产表*/
						asset.setAssetNum(asset.getAssetNum());
						asset.setAssetName(asset.getAssetName());
						asset.setAssetModel(asset.getAssetModel());
						asset.setAssetPrice(asset.getAssetPrice());
						asset.setAssetFactory(asset.getAssetFactory());
						asset.setAssetDocumentNum(asset.getAssetDocumentNum());
						asset.setAssetBuyDate(asset.getAssetBuyDate());
						asset.setAssetTakePeople(asset.getAssetTakePeople());
						asset.setStoreNum(asset.getStoreNum());
						asset.setDepartNum(asset.getDepartNum());
						asset.setDicProfitLossNum(dicprofitlossInfoService.getDicProfitLossByName(record).getDicProfitLossNum());
						assetInfoService.updateObject(asset);
						/*修改损益类型-盘点表*/
						Map<String,Object> updateinventory=new HashMap<String,Object>();
						updateinventory.put("assetNum", asset.getAssetNum());
						updateinventory.put("inventoryBatch", LatestBatch);
						updateinventory.put("dicProfitLossNum", dicprofitlossInfoService.getDicProfitLossByName(record).getDicProfitLossNum());
						assetInfoService.updateInventory(updateinventory);
					}
					msg="修改损益类型成功-"+record;
					result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
				}catch(Exception e) {
					/*修改失败*/
					result=ControllerResponse.RESPONSE_RESULT_ERROR;
				}
			}
			else if(type.equals("领用人")) {
				for(int i=0;i<ArrayAssetNum.size();i++)
				{
					AssetInfo asset=new AssetInfo();
					asset=assetInfoService.getObject(ArrayAssetNum.get(i));
					/*修改资产表*/
					asset.setAssetNum(asset.getAssetNum());
					asset.setAssetName(asset.getAssetName());
					asset.setAssetModel(asset.getAssetModel());
					asset.setAssetPrice(asset.getAssetPrice());
					asset.setAssetFactory(asset.getAssetFactory());
					asset.setAssetDocumentNum(asset.getAssetDocumentNum());
					asset.setAssetBuyDate(asset.getAssetBuyDate());
					asset.setAssetTakePeople(record);
					asset.setStoreNum(asset.getStoreNum());
					asset.setDepartNum(asset.getDepartNum());
					asset.setDicProfitLossNum(asset.getDicProfitLossNum());
					assetInfoService.updateObject(asset);
					/*修改盘点表*/
					Map<String,Object> updateinventory=new HashMap<String,Object>();
					updateinventory.put("assetNum", asset.getAssetNum());
					updateinventory.put("inventoryBatch", LatestBatch);
					updateinventory.put("assetTakePeople",record);
					assetInfoService.updateInventory(updateinventory);
				}
				msg="修改领用人成功-"+record;
				result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
			}
			else if(type.equals("存放地")) {
				for(int i=0;i<ArrayAssetNum.size();i++)
				{
					AssetInfo asset=new AssetInfo();
					asset=assetInfoService.getObject(ArrayAssetNum.get(i));
					/*修改资产表*/
					asset.setAssetNum(asset.getAssetNum());
					asset.setAssetName(asset.getAssetName());
					asset.setAssetModel(asset.getAssetModel());
					asset.setAssetPrice(asset.getAssetPrice());
					asset.setAssetFactory(asset.getAssetFactory());
					asset.setAssetDocumentNum(asset.getAssetDocumentNum());
					asset.setAssetBuyDate(asset.getAssetBuyDate());
					asset.setAssetTakePeople(asset.getAssetTakePeople());
					asset.setStoreNum(storeInfoService.getStoreByName(record).getStoreNum());
					asset.setDepartNum(asset.getDepartNum());
					asset.setDicProfitLossNum(asset.getDicProfitLossNum());
					assetInfoService.updateObject(asset);
					/*修改盘点表*/
					Map<String,Object> updateinventory=new HashMap<String,Object>();
					updateinventory.put("assetNum", asset.getAssetNum());
					updateinventory.put("inventoryBatch", LatestBatch);
					updateinventory.put("storeNum",storeInfoService.getStoreByName(record).getStoreNum());
					assetInfoService.updateInventory(updateinventory);
				}
				msg="修改存放地成功-"+record;
				result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
			}else {
				msg="修改失败-错误属性";
				result=ControllerResponse.RESPONSE_RESULT_ERROR;
			}
			
		}else {
			/*没有修改的数据*/
			result=ControllerResponse.RESPONSE_RESULT_ERROR;
			msg="修改失败-没有修改的数据";
		}
		response.setResponseMsg(msg);
		response.setResponseResult(result);
		return response.generateResponse();
	}
}
