package cn.tit.ias.controller.android;
/**
 * 
 *  
 * @Description:打印标签  
 * @author: maotao 
 * @date:   2019年6月16日 上午9:26:47       
 *
 */

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
@RequestMapping("/AndroidPrintTag")
public class AndroidPrintTagController {
    /**
     * 
     * @Title: printTag   
     * @Description: 打印标签   
     * @param: @return      
     * @return: Map<String,Object>      
     * @throws
     */
	@RequestMapping(value = "printTag",method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> printTag(@RequestParam("assetNum") String assetNum){
	
		return null;
	}
}
