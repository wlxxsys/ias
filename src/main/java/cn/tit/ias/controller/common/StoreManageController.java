package cn.tit.ias.controller.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.management.relation.Role;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.org.apache.bcel.internal.generic.NEW;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.service.CollegeInfoService;
import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.service.StoreInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;

/**
 * 
 * 
 * @Description:存放地管理
 * @author: maotao
 * @date: 2019年7月25日 下午12:15:42
 *
 */
@Controller
@RequestMapping("/storeManage")
public class StoreManageController {

	private static final String SUPER_ADMIN_ROLE = "超级管理员";
	private static final String ADMIN_ROLE = "管理员";
	@Autowired
	StoreInfoService service;
	@Autowired
	CollegeInfoService collegeServie;
	@Autowired
	DepartInfoService departService;

	@RequestMapping(value = "listStoresByDepart", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listStoresByDepart(@RequestParam("depart")String departname) {

		// 创建统一json格式的响应
		ControllerResponse response = ControllerResponseFactory.newInstance();
		
		// 查询存放地的条件---部门编号
		String departNum = null;
		StoreInfo condition = new StoreInfo();
		// 按条件查询出来的存放地
		List<StoreInfo> stores = new ArrayList<>();
		
		// 通过shiro获取当前登录的对象
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		// 获取session域中当前登陆用户
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		// 获取当前用户的角色
		String userRole = (String) session.getAttribute("loginUserRole");
		
		
		// 超级管理员
		if(userRole.equals(SUPER_ADMIN_ROLE)){
			if(departname==""){
				System.out.println(stores);
				response.setObjectInfo("stores",stores);
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
				return response.generateResponse();
			}
			try {
				departNum = departService.getDepartByName(departname).getDepartNum(); 
			} catch (Exception e) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				response.setResponseMsg("不存在该部门");
				return response.generateResponse();
			}
			
		}else{
			// 普通管理员
			try {
				// 查询用户所在部门
				departNum = loginUser.getDepartNum();
			} catch (Exception e) {
				response.setResponseMsg("不存在该部门");
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				return response.generateResponse();
			}
		}
		condition.setDepartNum(departNum);
		// 查询成功返回前端
		stores = (List<StoreInfo>) service.listStoreObjByCondition(condition).get("data");
		response.setObjectInfo("stores", stores);
		response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		return response.generateResponse();
	}

	
	
}
