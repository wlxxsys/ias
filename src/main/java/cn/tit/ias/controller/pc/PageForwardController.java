package cn.tit.ias.controller.pc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.druid.sql.dialect.oracle.ast.clause.ModelClause.ReturnRowsClause;

/**
 * 
 *  
 * @Description:页面重定向   
 * @author: maotao 
 * @date:   2019年9月28日 下午4:56:44       
 *
 */
@Controller
@RequestMapping("/pageForward")
public class PageForwardController {

	@RequestMapping("entryTag")
	public String entryTag() {
		return "pageComponent/entryTag";
	}
	@RequestMapping("/accessRecordManagement")
	public String accessRecordManagement(){
		return "pageComponent/accessRecordManagement";
	}
	
	@RequestMapping("/AssetInfoManagement")
	public String AssetInfoManagement() {
		return "pageComponent/AssetInfoManagement";
	}
	
	@RequestMapping("collegeInfoManagement")
	public String collegeInfoManagement() {
		return "pageComponent/collegeInfoManagement";
	}
	
	@RequestMapping("departInfoManagement")
	public String departInfoManagement() {
		return "pageComponent/departInfoManagement";
	}
	@RequestMapping("excelManagement")
	public String excelManagement() {
		return "pageComponent/excelManagement";
	}
	@RequestMapping("inventoryGoods")
	public String inventoryGoods() {
		return "pageComponent/inventoryGoods";
	}
	
	@RequestMapping("passwordModification")
	public String passwordModification() {
		return "pageComponent/passwordModification";
	}
	@RequestMapping("storeInfoManagement")
	public String storeInfoManagement() {
		return "pageComponent/storeInfoManagement";
	}
	
	@RequestMapping("unauthorizedManagement")
	public String unauthorizedManagement(){
		return "pageComponent/unauthorizedManagement";
	}
	@RequestMapping("userManagement")
	public String userManagement() {
		return "pageComponent/userManagement";
	}
	@RequestMapping("userOperationManagement")
	public String userOperationManagement() {
		return "pageComponent/userOperationManagement";
	}
	@RequestMapping("welcomePage")
	public String welcomePage() {
		return "pageComponent/welcomePage";
	}
	@RequestMapping("inventoryBatchManagement")
	public String inventoryBatchManagement(){
		return "pageComponent/inventoryBatchManagement";
	}
	@RequestMapping("echarts")
	public String echarts(){
		return "pageComponent/echarts";
	}
}
