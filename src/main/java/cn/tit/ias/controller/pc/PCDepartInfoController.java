package cn.tit.ias.controller.pc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.CollegeInfoService;
import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;

/**
 * 
 * 
 * @Description:depart Controller
 * @author: maotao
 * @date: 2019年9月7日 下午4:56:05
 *
 */
@Controller
@RequestMapping("/PCDepartInfoHandler")
public class PCDepartInfoController {
	@Autowired
	DepartInfoService departInfoService;
	@Autowired
	CollegeInfoService collegeInfoService;

	/**
	 * 
	 * @Title: addObject @Description: 添加部门 @param: @param
	 *         request @param: @param departInfo @param: @return @return:
	 *         Map<String,Object> @throws
	 */
	@RequestMapping(value = "addDepart", method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "部门管理模块",option = "添加部门信息")
	public Map<String, Object> addObject(HttpServletRequest request, @RequestBody Map<String, Object> departInfo) {
		// 初始化 response
		ControllerResponse response = ControllerResponseFactory.newInstance();

		// 获取登陆用户的信息
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");

		// 获取添加要添加的部门信息
		String departName = (String) departInfo.get("departName");
		String departRemark = (String) departInfo.get("departRemark");
		String collegeName = (String) departInfo.get("collegeName");
		int collegeNum = collegeInfoService.getCollegeByName(collegeName).getCollegeNum();

		// 验证要添加的部门信息
		DepartInfo condition = new DepartInfo();
		condition.setDepartName(departName);
		if ((int)departInfoService.listDepartObjByCondition(condition).get("total") != 0) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("该名字已存在");
		} else {

			// 添加部门
			DepartInfo entity = new DepartInfo();
			entity.setDepartName(departName);
			entity.setCollegeNum(collegeNum);
			entity.setDepartRemark(departRemark);
			int result = departInfoService.addObject(entity);
			if (result == 1) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			} else {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				response.setResponseMsg("添加失败");
			}
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listObject @Description: 分页查询部门 @param: @param
	 *         collegeName @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "listDepart", method = RequestMethod.GET)
	@ResponseBody
	//@MethodLog(name = "部门管理模块",option = "查看部门信息")
	public Map<String, Object> listObject(@Param("offset") int offset, @Param("limit") int limit,
			HttpServletRequest request, @Param("collegeName") String collegeName) {

		// 初始化response
		ControllerResponse response = ControllerResponseFactory.newInstance();

		// 获取登陆者信息
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");

		// 获取查询的部门
		int total = 0;
		List<Map<String, Object>> listResult = new ArrayList<>();
		DepartInfo condition = new DepartInfo();

		if (!collegeName.equals("")) {
			int collegeNum = collegeInfoService.getCollegeByName(collegeName).getCollegeNum();
			condition.setCollegeNum(collegeNum);
			// 查询结果
			Map<String, Object> queryRsult = departInfoService.listDepartObjByConditionPage(offset, limit, condition);
			List<DepartInfo> listDepart = (List<DepartInfo>) queryRsult.get("data");
			total = (int) queryRsult.get("total");
			for (DepartInfo departInfo : listDepart) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("departName", departInfo.getDepartName());
				map.put("departRemark", departInfo.getDepartRemark());
				map.put("collegeName", collegeName);
				listResult.add(map);
			}
		}

		response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		response.setObjectInfo("rows", listResult);
		;
		response.setResponseTotal(total);

		return response.generateResponse();
	}
	/**
	 * 
	 * @Title: listObject_v1   
	 * @Description: 获取下拉框部门数据源 
	 * @param: @param collegeNum
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "listDepart_v1", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listObject_v1(@RequestParam("collegeNum")String collegeNum) {

		// 初始化response
		ControllerResponse response = ControllerResponseFactory.newInstance();
		DepartInfo departInfo = new DepartInfo();
		System.out.println("打印departController中："+collegeNum);
		// 判断学校编号是否为空
		if(collegeNum == null || "".equals(collegeNum)){
			response.setResponseData(null);
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("未选择学校");
			return response.generateResponse();
		}else{
			departInfo.setCollegeNum(Integer.valueOf(collegeNum));
			// 查询选择学校的所有部门
			try {
				Map<String, Object> result =  departInfoService.listDepartObjByCondition(departInfo);
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
				response.setResponseData(result.get("data"));
				return response.generateResponse();
			} catch (Exception e) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				response.setResponseMsg("查询部门异常");
				response.setResponseData(null);
				return response.generateResponse();
			}
			
		}
		
	}

	@RequestMapping(value = "updateDepart", method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "部门管理模块",option = "修改部门信息")
	public Map<String, Object> updateObject(@RequestBody Map<String, Object> departInfo) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 获取修改数据
		String oldDepartName = (String) departInfo.get("oldDepartName");
		String newDepartName = (String) departInfo.get("newDepartName");
		String departRemark = (String) departInfo.get("departRemark");
		String collegeName = (String) departInfo.get("collegeName");

		// 验证修改值的合法性
		if (newDepartName == null || newDepartName.equals("")) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("名称不能为空");
			return response.generateResponse();
		}
		if (!newDepartName.equals(oldDepartName)) {

			DepartInfo condition = new DepartInfo();
			int collegeNum = collegeInfoService.getCollegeByName(collegeName).getCollegeNum();
			condition.setCollegeNum(collegeNum);
			condition.setDepartName(newDepartName);
			Map<String, Object> queryRsult = departInfoService.listDepartObjByCondition(condition);
			System.out.println(queryRsult.get("total"));
			if ((int) queryRsult.get("total") != 0) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				response.setResponseMsg("该名称已存在");
				return response.generateResponse();
			}
		}

		// 通过验证，进行修改
		String departNum = departInfoService.getDepartByName(oldDepartName).getDepartNum();
		DepartInfo condition_ = new DepartInfo();
		condition_.setDepartNum(departNum);
		condition_.setDepartName(newDepartName);
		condition_.setDepartRemark(departRemark);
		int result = departInfoService.updateObject(condition_);
		if (result == 1) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			response.setResponseMsg("修改成功");
		} else {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("修改失败");
		}

		return response.generateResponse();
	}
	
	@RequestMapping(value = "deleteObject",method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "部门管理模块",option = "删除部门信息")
	public Map<String,Object> deleteObject(@RequestParam("selectDepartName")String departName) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		System.out.println(departName);
		// 删除结果
		int result=-1;
		try {
			String departNum = departInfoService.getDepartByName(departName).getDepartNum();
			result = departInfoService.deleteObject(departNum);
		} catch (Exception e) {
			result=-1;
		}
		if(result == -1) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("删除失败");
			
		}else{
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			response.setResponseMsg("删除成功");
		}
		return response.generateResponse();
	}

}
