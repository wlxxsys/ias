package cn.tit.ias.controller.pc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 
 *  
 * @Description:学校管理   
 * @author: maotao 
 * @date:   2019年9月9日 下午4:02:00       
 *
 */
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.CollegeInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;
@Controller
@RequestMapping("/PCCollegeHandler")
public class PCCollegeInfoController {

	@Autowired
	CollegeInfoService collegeInfoService;
	
	@RequestMapping(value = "/listCollege",method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listObject() {
		// 统一响应格式
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 查询所有学校
		List<CollegeInfo> listCollege = collegeInfoService.listAllObject();
		String[] collegeNames = new String[listCollege.size()];
		int i = 0;
		for (CollegeInfo collegeInfo : listCollege) {
			collegeNames[i] = collegeInfo.getCollegeName();
			i++;
		}
		response.setResponseData(collegeNames);
		return response.generateResponse();
	}
	
	@RequestMapping(value = "/listCollege_v1",method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listObject_v1() {
		// 统一响应格式
		ControllerResponse response = ControllerResponseFactory.newInstance();
		try {
			// 查询所有学校
			List<CollegeInfo> listCollege = collegeInfoService.listAllObject();
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			response.setResponseData(listCollege);
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("查询学校下拉框失败");
			response.setResponseData(null);
		}
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: listObject @Description: 分页查询学校 @param: @param
	 *         collegeName @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "getCollege", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getCollege(@Param("offset") int offset, @Param("limit") int limit,
			HttpServletRequest request, @Param("collegeName") String collegeName) {
		// 初始化response
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 获取查询的部门
		int total = 0;
		List<Map<String, Object>> listResult = new ArrayList<>();
		if (!collegeName.equals("")) {
			if(collegeName.equals("所有"))
			{
				CollegeInfo condition = new CollegeInfo();
				Map<String, Object> queryRsult = collegeInfoService.listCollegeObjByConditionPage(offset, limit, condition);
				List<CollegeInfo> listCollege = (List<CollegeInfo>) queryRsult.get("data");
				total = (int) queryRsult.get("total");
				for (CollegeInfo collegeInfo : listCollege) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("collegeNum", collegeInfo.getCollegeNum());
					map.put("collegeName", collegeInfo.getCollegeName());
					listResult.add(map);
				}
			}
			else {
				CollegeInfo condition = new CollegeInfo();
				condition.setCollegeName(collegeName);
				// 查询结果
				Map<String, Object> queryRsult = collegeInfoService.listCollegeObjByConditionPage(offset, limit, condition);
				List<CollegeInfo> listCollege = (List<CollegeInfo>) queryRsult.get("data");
				total = (int) queryRsult.get("total");
				for (CollegeInfo collegeInfo : listCollege) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("collegeNum", collegeInfo.getCollegeNum());
					map.put("collegeName", collegeInfo.getCollegeName());
					listResult.add(map);
				}
			}
			
		}
		response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		response.setObjectInfo("rows", listResult);
		response.setResponseTotal(total);
		return response.generateResponse();
	}
	
	/**修改学校信息*/
	@RequestMapping(value="updateCollege",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> updateCollege(@RequestBody Map<String,Object> college)
	{
		ControllerResponse response = ControllerResponseFactory.newInstance();
		String result = ControllerResponse.RESPONSE_RESULT_ERROR;
		int CollegeNum=-1;
		String CollegeName="";
		try {
			CollegeNum=Integer.parseInt((String) college.get("collegeNum"));
			CollegeName=college.get("collegeName").toString();
		}catch(Exception e)
		{
			CollegeNum=-1;
		}
		if(CollegeNum!=-1)
		{
			//名字是否已经存在
			try {
				collegeInfoService.getCollegeByName(CollegeName).getCollegeName();
				response.setResponseMsg("该名字已存在");
				result = ControllerResponse.RESPONSE_RESULT_ERROR;
			}catch(Exception e) {
				CollegeInfo collegeInfo=new CollegeInfo();
				collegeInfo.setCollegeNum(CollegeNum);
				collegeInfo.setCollegeName(CollegeName);
				collegeInfoService.updateObject(collegeInfo);
				result = ControllerResponse.RESPONSE_RESULT_SUCCESS;	
				response.setResponseMsg("修改成功");
			}
		}else {
			/*前端传来的数据发生错误*/
			result = ControllerResponse.RESPONSE_RESULT_ERROR;
		}
		response.setResponseResult(result);
		return response.generateResponse();
	}
	
	/**添加学校*/
	@RequestMapping(value = "addCollege",method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> addCollege(@RequestBody Map<String,Object> message){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		String CollegeName="";
		String result = ControllerResponse.RESPONSE_RESULT_ERROR;
		/*获取前端传来的信息*/
		try {
			CollegeName=message.get("collegeName").toString();
		}catch(Exception e) {
			CollegeName="";
		}
		if(!CollegeName.equals(""))
		{
			/*验证学校*/
			CollegeInfo condition = new CollegeInfo();
			condition.setCollegeName(CollegeName);
			if ((int)collegeInfoService.listCollegeObjByCondition(condition).get("total") != 0) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				response.setResponseMsg("该名字已存在");
			} else  {
				//添加学校
				CollegeInfo collegeInfo=new CollegeInfo();
				collegeInfo.setCollegeName(CollegeName);
				collegeInfoService.addObject(collegeInfo);
				response.setResponseMsg("添加成功");
				result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
			}
		}else{
			/*部门获取错误-添加失败*/
			result = ControllerResponse.RESPONSE_RESULT_ERROR;
		}
		response.setResponseResult(result);
		return response.generateResponse();
	}
	
	/*删除学校*/
	@RequestMapping(value = "deleteCollege",method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> deleteCollege(@RequestParam("collegeNum")int collegeNum) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 删除结果
		int result = -1;
		
		String msg="";
		String result1="";
		try{
			int collegenum=collegeInfoService.getObject(collegeNum).getCollegeNum();
			result=collegeInfoService.deleteObject(collegenum);
		}catch(Exception e)
		{
			result=-1;
		}
		if(result == -1) {
			result1=ControllerResponse.RESPONSE_RESULT_ERROR;
			msg="删除失败";
		}else{
			result1=ControllerResponse.RESPONSE_RESULT_SUCCESS;
			msg="删除成功";
		}
		
		response.setResponseMsg(msg);
		response.setResponseResult(result1);
		return response.generateResponse();
	}
}
