package cn.tit.ias.controller.pc;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;
import cn.tit.ias.util.handleString.HandleSysConvert;

/**
 * 
 * 
 * @Description:pc端处理NFC标签
 * @author: maotao
 * @date: 2019年6月16日 上午11:16:40
 *
 */
@Controller
@RequestMapping("/PCHanderTag")
public class PCHanderTagController {

	/**
	 * 
	 * @Title: handleDatastr @Description: 规范写入标签中数据的格式为 统一的48位 @param: @param
	 *         strData @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "handleWriteData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> handleWriteData(@RequestParam("datastr") String strData) {
		ControllerResponse response = ControllerResponseFactory.newInstance();

		int lenth = 48 - strData.length();
		// 同过二进制移位获取补零字符串（nfc标签中的数据块每一块必须十六个byte）
		String ss = Integer.toBinaryString(1 << lenth - 1).replace('1', '0');
		strData = ss + strData;
		String strDataHex = HandleSysConvert.str2HexStr(strData);
		System.out.println("写入的十六进制数" + strDataHex);
		
		String writeData = (String) handlereadDate(strDataHex).get("data");
		// 返回写入成功的字符串显示在数据区
		response.setObjectInfo("writeDataStr",writeData);
		// 返回要写入标签中的十六进制字符串，
		response.setResponseData(strDataHex);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: handlereadDate @Description: 处理读取的数据 @param: @param
	 *         strData @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "handleReadData", method = RequestMethod.POST)
	@MethodLog(name = "pc模块",option = "转换读取的资产编号编码格式")
	@ResponseBody
	public Map<String, Object> handlereadDate(@RequestParam("datastr") String strData) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		System.out.println(strData);
		String strDataHex = HandleSysConvert.hexStr2Str(strData);
		String result = "";
		// 去除数据前面的占位的零
		for (int i = 0; i < strDataHex.length(); i++) {
			if (strDataHex.charAt(i) != '0') {
				result = result + strDataHex.substring(i, strDataHex.length());
				break;
			}
		}
		System.out.println("读取的十六进制数" + result);
		response.setResponseData(result);
		return response.generateResponse();
	}
	
}
