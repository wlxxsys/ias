package cn.tit.ias.controller.pc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.service.SystemLogService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;

/**
 * 
 * 
 * @Description:日志controller层
 * @author: maotao
 * @date: 2019年9月18日 上午9:08:02
 *
 */
@Controller
@RequestMapping("systemLog")
public class PCSystemLogInfoController {

	@Autowired
	SystemLogService logService;
	@Autowired
	DepartInfoService departInfoService;

	/**
	 * 
	 * @Title: getAccessRecords @Description: 查询用户登陆日志 @param: @param
	 * userIDStr @param: @param accessType @param: @param
	 * startDateStr @param: @param endDateStr @param: @param
	 * departName @param: @param offset @param: @param
	 * limit @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "getAccessRecords", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getAccessRecords(@RequestParam("userID") String userIDStr,
			@RequestParam("accessType") String accessType, @RequestParam("startDate") String startDateStr,
			@RequestParam("endDate") String endDateStr, @RequestParam("departName") String departName,
			@RequestParam("offset") int offset, @RequestParam("limit") int limit) {

		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<Map<String, Object>> rows = new ArrayList<>();
		int total = 0;
		String departNum = null;
		
		// 当条件都为空时
		if(userIDStr.equals("")&&startDateStr.equals("")&&departName.equals("")){
			response.setObjectInfo("rows", rows);
			response.setResponseTotal(total);
			return response.generateResponse();
		}
		try {
			departNum = departInfoService.getDepartByName(departName).getDepartNum();
		} catch (Exception e) {
			System.out.println("无此部门");
		}

		// 检查日期参数
		String regex = "([0-9]{4})-([0-9]{2})-([0-9]{2})";
		boolean startDateFormatCheck = (StringUtils.isEmpty(startDateStr) || startDateStr.matches(regex));
		boolean endDateFormatCheck = (StringUtils.isEmpty(endDateStr) || endDateStr.matches(regex));
		if (startDateFormatCheck && endDateFormatCheck) {
			// 转到 Service 执行查询
			if (endDateStr == null || endDateStr == "") {
				Date date = new Date();
				endDateStr = date.toString();
			}
			Map<String, Object> condition = new HashMap<>();
			condition.put("userCount", userIDStr);
			condition.put("accessType", accessType);
			condition.put("startDate", startDateStr);
			condition.put("endDate", endDateStr);
			condition.put("departNum", departNum);
			Map<String, Object> queryResult = logService.getLoginRecordByConditionPage(offset, limit, condition);
			if (queryResult != null) {
				rows = (List<Map<String, Object>>) queryResult.get("data");
				total = (int) queryResult.get("total");
			}
		} else
			response.setResponseMsg("Request Argument Error");
		if (rows == null)
			rows = new ArrayList<>(0);

		// 返回 Response
		response.setObjectInfo("rows", rows);
		response.setResponseTotal(total);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: getUserOperationLog @Description: 查询用户操作日志 @param: @param
	 * userIDStr @param: @param startDateStr @param: @param
	 * endDateStr @param: @param departName @param: @param offset @param: @param
	 * limit @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "getUserOperationLog", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getUserOperationLog(@RequestParam("userID") String userIDStr,
			@RequestParam("startDate") String startDateStr, @RequestParam("endDate") String endDateStr,
			@RequestParam("departName") String departName, @RequestParam("offset") int offset,
			@RequestParam("limit") int limit) {

		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<Map<String, Object>> rows = new ArrayList<>();
		int total = 0;
		// 当条件都为空时
		if(userIDStr.equals("")&&startDateStr.equals("")&&departName.equals("")){
			response.setObjectInfo("rows", rows);
			response.setResponseTotal(total);
			return response.generateResponse();
		}
		String departNum = null;
		try {
			departNum = departInfoService.getDepartByName(departName).getDepartNum();
		} catch (Exception e) {
			System.out.println("无此部门");
		}

		// 检查日期参数
		String regex = "([0-9]{4})-([0-9]{2})-([0-9]{2})";
		boolean startDateFormatCheck = (StringUtils.isEmpty(startDateStr) || startDateStr.matches(regex));
		boolean endDateFormatCheck = (StringUtils.isEmpty(endDateStr) || endDateStr.matches(regex));
		if (startDateFormatCheck && endDateFormatCheck) {
			// 转到 Service 执行查询
			if (endDateStr == null || endDateStr == "") {
				Date date = new Date();
				endDateStr = date.toString();
			}
			Map<String, Object> condition = new HashMap<>();
			condition.put("userCount", userIDStr);
			condition.put("startDate", startDateStr);
			condition.put("endDate", endDateStr);
			condition.put("departNum", departNum);
			Map<String, Object> queryResult = logService.getUserOperationByConditionPage(offset, limit, condition);
			if (queryResult != null) {
				rows = (List<Map<String, Object>>) queryResult.get("data");
				total = (int) queryResult.get("total");
			}
		} else
			response.setResponseMsg("Request Argument Error");
		if (rows == null)
			rows = new ArrayList<>(0);

		// 返回 Response
		response.setObjectInfo("rows", rows);
		response.setResponseTotal(total);
		return response.generateResponse();
	}
}
