package cn.tit.ias.controller.pc;
/**
 * 
 *  
 * @Description:PC端资产信息管理   
 * @author: maotao 
 * @date:   2019年6月16日 上午10:09:32       
 *
 */

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.filechooser.FileSystemView;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.xml.internal.utils.ObjectPool;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.DicProfitLossInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.service.DicProfitLossInfoService;
import cn.tit.ias.service.StoreInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;
import cn.tit.ias.util.handlexcel.ExcelExportUtil_;

@Controller
@RequestMapping("/PCAssetInfoHandler")
public class PCAssetInfoController {
	public static final Logger log = LoggerFactory.getLogger(PCAssetInfoController.class);
	private static final String SUPER_ADMIN_ROLE = "超级管理员";
	private static final String ADMIN_ROLE = "管理员";
	@Autowired
	private StoreInfoService storeInfoService;
	@Autowired
	private AssetInfoService assetInfoService;
	@Autowired
	private DicProfitLossInfoService dicProfitLossInfoService;
	@Autowired
	private DepartInfoService departInfoService;

	/**
	 * @Title: query @Description: 通用结果查询 @param: @param storeNum
	 *         存放地编号 @param: @param offset 分页偏移值 @param: @param limit
	 *         分页大小 @param: @return 返回指定条件查询的结果 @return:
	 *         Map<String,Object> @throws
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "query", method = RequestMethod.GET)
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "查询资产信息")
	private Map<String, Object> query(@RequestParam("offset") int offset, @RequestParam("limit") int limit,
			@RequestParam("repositoryBelong") String repositoryBelong, @RequestParam("keyword") String keyword,
			@RequestParam("departName") String depart) {

		ControllerResponse response = ControllerResponseFactory.newInstance();
		String result = ControllerResponse.RESPONSE_RESULT_ERROR;
		int total = 0;
		List<AssetInfo> rows = new ArrayList<AssetInfo>();
		Map<String, Object> map = null;
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		String departNum = null;

		// 获取当前登陆用户
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		// 获取部门编号
		if (!currentUser.hasRole("超级管理员")) {
			departNum = loginUser.getDepartNum();
		} else if (!depart.equals("请选择部门") && !depart.equals("none")) {
			System.out.println(depart);
			departNum = departInfoService.getDepartByName(depart).getDepartNum();
		}

		// 按编号查询
		if (keyword.equals("资产编号")) {
			Map<String, Object> finalResult = new HashMap<>();
			AssetInfo asset = new AssetInfo();
			try {
				asset = assetInfoService.getObject(repositoryBelong);
			} catch (Exception e) {
				asset = null;
			}
			if (asset == null) {
				System.out.println("无此编号");
			} else {
				finalResult.put("goodsID", asset.getAssetNum());
				finalResult.put("goodsName", asset.getAssetName());
				finalResult.put("goodsPerson", asset.getAssetTakePeople());
				try {
					finalResult.put("goodsDepart", departInfoService.getObject(asset.getDepartNum()).getDepartName());
				} catch (Exception e) {
					finalResult.put("goodsDepart", "无");
				}
				try {
					finalResult.put("goodsType",
							dicProfitLossInfoService.getObject(asset.getDicProfitLossNum()).getDicProfitLossName());
				} catch (Exception e) {
					finalResult.put("goodsType", "无");
				}
				try {
					finalResult.put("goodsAddr", storeInfoService.getObject(asset.getStoreNum()).getStoreName());
				} catch (Exception e) {
					finalResult.put("goodsAddr", "无");
				}
				finalResult.put("repositoryID", asset.getAssetModel());
				finalResult.put("goodsprice", asset.getAssetPrice());
				finalResult.put("documentnum", asset.getAssetDocumentNum());
				finalResult.put("goodsbuydate", asset.getAssetBuyDate());
				finalResult.put("goodsfactory", asset.getAssetFactory());
				finalResult.put("goodsremrk", asset.getAssetRemrk());
				listFinalResult.add(finalResult);
				total = 1;
			}
			result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		} else {
			// 根据条件查询部门资产
			if (keyword.equals("所有") && !depart.equals("请选择部门")) {
				AssetInfo condition = new AssetInfo();
				condition.setDepartNum(departNum);
				map = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
			} else if (keyword.equals("资产名称") && !depart.equals("请选择部门") && !repositoryBelong.equals("")) {
				AssetInfo condition = new AssetInfo();
				condition.setDepartNum(departNum);
				condition.setAssetName(repositoryBelong);
				map = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
			} else if (keyword.equals("领用人") && !depart.equals("请选择部门") && !repositoryBelong.equals("")) {
				AssetInfo condition = new AssetInfo();
				condition.setDepartNum(departNum);
				condition.setAssetTakePeople(repositoryBelong);
				map = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
			} else if (keyword.equals("存放地") && !depart.equals("请选择部门") && !repositoryBelong.equals("")) {
				AssetInfo condition = new AssetInfo();
				condition.setDepartNum(departNum);
				try {
					condition.setStoreNum(storeInfoService.getStoreByName(repositoryBelong).getStoreNum());
				} catch (Exception e) {
					condition.setStoreNum(00);
				}

				map = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
			} else if (keyword.equals("损溢类型") && !depart.equals("请选择部门") && !repositoryBelong.equals("")) {
				AssetInfo condition = new AssetInfo();
				condition.setDepartNum(departNum);
				try {
					condition.setDicProfitLossNum(
							dicProfitLossInfoService.getDicProfitLossByName(repositoryBelong).getDicProfitLossNum());
				} catch (Exception e) {
					condition.setDicProfitLossNum(00);
				}
				map = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
			} else {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
				response.setObjectInfo("rows", null);
				response.setResponseTotal(0);
				return response.generateResponse();
			}

			rows = (List<AssetInfo>) map.get("data");
			Iterator<AssetInfo> iterator = rows.iterator();
			while (iterator.hasNext()) {
				Map<String, Object> finalResult = new HashMap<>();
				AssetInfo asset = iterator.next();
				finalResult.put("goodsID", asset.getAssetNum());
				finalResult.put("goodsName", asset.getAssetName());
				finalResult.put("goodsPerson", asset.getAssetTakePeople());
				try {
					finalResult.put("goodsDepart", departInfoService.getObject(asset.getDepartNum()).getDepartName());
				} catch (Exception e) {
					finalResult.put("goodsDepart", "无");
				}
				try {
					finalResult.put("goodsType",
							dicProfitLossInfoService.getObject(asset.getDicProfitLossNum()).getDicProfitLossName());
				} catch (Exception e) {
					finalResult.put("goodsType", "无");
				}
				try {
					finalResult.put("goodsAddr", storeInfoService.getObject(asset.getStoreNum()).getStoreName());
				} catch (Exception e) {
					finalResult.put("goodsAddr", "无");
				}
				finalResult.put("repositoryID", asset.getAssetModel());
				finalResult.put("goodsprice", asset.getAssetPrice());
				finalResult.put("documentnum", asset.getAssetDocumentNum());
				finalResult.put("goodsbuydate", asset.getAssetBuyDate());
				finalResult.put("goodsfactory", asset.getAssetFactory());
				finalResult.put("goodsremrk", asset.getAssetRemrk());
				listFinalResult.add(finalResult);
			}
			total = (int) map.get("total");
			result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		}
		response.setResponseResult(result);
		response.setObjectInfo("rows", listFinalResult);
		response.setResponseTotal(total);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listdicProfitLoss @Description: 查找所有损益类型 @param: @return @return:
	 *         Map<String,Object> @throws
	 */
	@RequestMapping(value = "listdicProfitLoss", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listdicProfitLoss(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<DicProfitLossInfo> dicProfitLoss = dicProfitLossInfoService.listAllObject();
		List<String> name = new ArrayList<>();
		for (int i = 0; i < dicProfitLoss.size(); i++) {
			name.add(dicProfitLoss.get(i).getDicProfitLossName());
		}
		response.setResponseData(name);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listStoreName @Description: 获取所有存放地名称 @param: @return @return:
	 *         Map<String,Object> @throws
	 */
	@RequestMapping(value = "listStoreName", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listStoreName(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 获取登陆者的学校
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		String DepartNum = loginUser.getDepartNum();
		List<StoreInfo> ListStore = new ArrayList<StoreInfo>();
		try {
			ListStore = storeInfoService.listStoreByCollege(departInfoService.getObject(DepartNum).getCollegeNum());
		} catch (Exception e) {
			ListStore = storeInfoService.listAllObject();
		}
		List<String> storename = new ArrayList<String>();
		for (int i = 0; i < ListStore.size(); i++) {
			storename.add(ListStore.get(i).getStoreName());
		}
		response.setResponseData(storename);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listdepartNum @Description: 查找所有部门 @param: @return @return:
	 *         Map<String,Object> @throws
	 */
	@RequestMapping(value = "listdepartNum", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listdepartNum(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<DepartInfo> departInfo = departInfoService.listAllObject();
		List<String> name = new ArrayList<>();

		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserInfo userInfo = (UserInfo) session.getAttribute("loginUser");
		if (currentUser.hasRole("超级管理员")) {
			for (int i = 0; i < departInfo.size(); i++) {
				name.add(departInfo.get(i).getDepartName());
			}
		} else {
			name.add(departInfoService.getObject(userInfo.getDepartNum()).getDepartName());
		}

		response.setResponseData(name);
		return response.generateResponse();
	}

	@RequestMapping(value = "listAdddepartNum", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listAdddepartNum(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<DepartInfo> departInfo = departInfoService.listAllObject();
		List<String> name = new ArrayList<>();

		for (int i = 0; i < departInfo.size(); i++) {
			name.add(departInfo.get(i).getDepartName());
		}
		response.setResponseData(name);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: updateAssetInfoList @Description: 批量编辑修改 @param: @param editType
	 *         要批量修改的字段(部门编号，领用人，存放地，损益类型，盘点状态) @param: @param editMessage
	 *         要修改的信息 @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "updateAssetInfoList", method = RequestMethod.GET)
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "批量编辑修改")
	public Map<String, Object> updateAssetInfoList(@RequestParam("keyWord") String keyWord,
			@RequestParam("editcode") String editcode, @RequestParam("goodsID") String[] goodsID) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		System.out.println(keyWord);
		String[] tag = { "编辑选项", "领用人", "部门", "存放地", "损益类型" };
		try {
			if (keyWord.equals(tag[0])) {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			} else {
				for (int i = 0; i < goodsID.length; i++) {
					/** 由前端ID获取资产信息 */
					AssetInfo oldasset = new AssetInfo();
					oldasset = assetInfoService.getObject(goodsID[i]);
					AssetInfo newasset = new AssetInfo();
					if (keyWord.equals(tag[1])) {
						newasset.setAssetTakePeople(editcode);
					} else {
						newasset.setAssetTakePeople(oldasset.getAssetTakePeople());
					}
					if (keyWord.equals(tag[2])) {
						newasset.setDepartNum(departInfoService.getDepartByName(editcode).getDepartNum());
					} else {
						newasset.setDepartNum(oldasset.getDepartNum());
					}
					if (keyWord.equals(tag[3])) {
						newasset.setStoreNum(storeInfoService.getStoreByName(editcode).getStoreNum());
					} else {
						newasset.setStoreNum(oldasset.getStoreNum());
					}
					if (keyWord.equals(tag[4])) {
						newasset.setDicProfitLossNum(
								dicProfitLossInfoService.getDicProfitLossByName(editcode).getDicProfitLossNum());
					} else {
						newasset.setDicProfitLossNum(oldasset.getDicProfitLossNum());
					}
					newasset.setAssetNum(oldasset.getAssetNum());
					newasset.setAssetName(oldasset.getAssetName());
					newasset.setAssetModel(oldasset.getAssetModel());
					newasset.setAssetPrice((float) oldasset.getAssetPrice());
					newasset.setAssetFactory(oldasset.getAssetFactory());
					newasset.setAssetDocumentNum(oldasset.getAssetDocumentNum());
					newasset.setAssetBuyDate(oldasset.getAssetBuyDate());
					newasset.setAssetRemrk(oldasset.getAssetRemrk());
					assetInfoService.updateObject(newasset);
				}
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			}
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: addAssetInfo @Description: 添加一条资产信息 @param: @param
	 *         assetInfo @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "addAssetInfo", method = RequestMethod.POST)
	// 接收前端传来的json字符串中的数据
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "添加一条资产信息")
	public Map<String, Object> addAssetInfo(@RequestBody Map<String, Object> assetInfo) {
		/** 初始化response */
		ControllerResponse response = ControllerResponseFactory.newInstance();
		/** 接收前端传来的参数 */
		String goodsID = (String) assetInfo.get("goodsID");
		String goodsName = (String) assetInfo.get("goodsName");
		String repositoryID = (String) assetInfo.get("repositoryID");
		String goodsprice = (String) assetInfo.get("goodsprice");
		String goodsfactory = (String) assetInfo.get("goodsfactory");
		String documentnum = (String) assetInfo.get("documentnum");
		String goodsbuydate = (String) assetInfo.get("goodsbuydate");
		String goodsPerson = (String) assetInfo.get("goodsPerson");
		String goodsremrk = (String) assetInfo.get("goodsremrk");
		String goodsAddr = (String) assetInfo.get("goodsAddr");
		String goodsType = (String) assetInfo.get("goodsType");
		String goodsDepart = (String) assetInfo.get("goodsDepart");
		try {
			/** 验证资产是否存在 */
			try {
				if (assetInfoService.getObject(goodsID).getAssetNum().equals(goodsID)) {
					response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
					response.setResponseMsg("exit");
				}
			} catch (Exception e) {
				/** 将前端获取数据封装成UserInfo对象 */
				AssetInfo addasset = new AssetInfo();
				addasset.setAssetNum(goodsID);
				addasset.setAssetName(goodsName);
				addasset.setAssetTakePeople(goodsPerson);
				addasset.setStoreNum(storeInfoService.getStoreByName(goodsAddr).getStoreNum());
				addasset.setDepartNum(departInfoService.getDepartByName(goodsDepart).getDepartNum());
				addasset.setDicProfitLossNum(
						dicProfitLossInfoService.getDicProfitLossByName(goodsType).getDicProfitLossNum());
				addasset.setAssetModel(repositoryID);
				addasset.setAssetPrice(Float.parseFloat(goodsprice));
				addasset.setAssetDocumentNum(documentnum);
				addasset.setAssetBuyDate(goodsbuydate);
				addasset.setAssetFactory(goodsfactory);
				addasset.setAssetRemrk(goodsremrk);
				int result = assetInfoService.addObject(addasset);
				if (result == 1) {
					response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
				} else {
					response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				}
			}
		} catch (NullPointerException e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: deleteAssetInfo @Description: 删除一条资产信息 @param: @param
	 *         assetInfoNum @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "deleteAssetInfo", method = RequestMethod.GET)
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "删除一条资产信息")
	public Map<String, Object> deleteAssetInfo(@RequestParam("goodsID") String assetInfoNum) {
		/** 初始化response */
		ControllerResponse response = ControllerResponseFactory.newInstance();
		try {
			int deleteResult = assetInfoService.deleteObject(assetInfoNum);
			if (deleteResult == 1) {
				/** 删除操作成功 */
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			} else
				/** 删除操作失败 */
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: deleteAssetInfoList @Description: 批量删除信息 @param: @param:String[]
	 *         goodsID @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "deleteAssetInfoList", method = RequestMethod.GET)
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "批量删除资产信息")
	public Map<String, Object> Batchdeleteinventory(@RequestParam("goodsID") String[] goodsID) {
		/** 初始化response */
		System.out.println(goodsID[0]);
		ControllerResponse response = ControllerResponseFactory.newInstance();
		try {
			for (int i = 0; i < goodsID.length; i++) {
				assetInfoService.deleteObject(goodsID[i]);
			}
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/*
	 * 查找存放地所有资产
	 */
	@RequestMapping(value = "listAssetNumsByStore", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listAssetNumsByStore(@RequestParam("offset") int offset,
			@RequestParam("limit") int limit, @RequestParam("storeName") String storeName) {
		// System.out.println("存放地名称" + storeName);
		// 创建统一响应格式
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 接收分页查找的结果集
		Map<String, Object> resultSet = new HashMap<>();

		if (storeName.equals("")) {
			response.setObjectInfo("rows", resultSet);
			response.setObjectInfo("total", 0);
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		}
		try {
			// 获取存放地对象
			int storeNum = storeInfoService.getStoreByName(storeName).getStoreNum();
			// System.out.println(storeNum);
			AssetInfo condition = new AssetInfo();
			condition.setStoreNum(storeNum);
			// 获取存放地中所有资产编号
			resultSet = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
			// System.out.println(resultSet);
			response.setObjectInfo("rows", resultSet.get("data"));
			response.setObjectInfo("total", resultSet.get("total"));
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();

	}

	/**
	 * 
	 * @Title: updateAssetLog @Description: 修改一条资产信息 @param: @param
	 *         assetInfo @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "updateAssetLog", method = RequestMethod.POST)
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "修改一条资产信息")
	public Map<String, Object> updateAssetLog(@RequestBody Map<String, Object> assetInfo) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		/** 接收前端传来的参数 */
		String goodsID = (String) assetInfo.get("goodsID");
		String goodsName = (String) assetInfo.get("goodsName");
		String goodsAddr = (String) assetInfo.get("goodsAddr");
		String goodsPerson = (String) assetInfo.get("goodsPerson");
		String goodsDepart = (String) assetInfo.get("goodsDepart");
		String goodsType = (String) assetInfo.get("goodsType");
		try {
			AssetInfo oldasset = assetInfoService.getObject(goodsID);
			AssetInfo asset = new AssetInfo();

			asset.setAssetNum(goodsID);
			asset.setAssetName(goodsName);
			asset.setAssetModel(oldasset.getAssetModel());
			asset.setAssetPrice(oldasset.getAssetPrice());
			asset.setAssetFactory(oldasset.getAssetFactory());
			asset.setAssetDocumentNum(oldasset.getAssetDocumentNum());
			asset.setAssetBuyDate(oldasset.getAssetBuyDate());
			asset.setAssetTakePeople(goodsPerson);
			try {
				asset.setStoreNum(storeInfoService.getStoreByName(goodsAddr).getStoreNum());
			} catch (Exception e) {
				asset.setStoreNum(oldasset.getStoreNum());
			}
			try {
				asset.setDepartNum(departInfoService.getDepartByName(goodsDepart).getDepartNum());
			} catch (Exception e) {
				asset.setDepartNum(oldasset.getDepartNum());
			}
			try {
				asset.setDicProfitLossNum(
						dicProfitLossInfoService.getDicProfitLossByName(goodsType).getDicProfitLossNum());
			} catch (Exception e) {
				asset.setDicProfitLossNum(oldasset.getDicProfitLossNum());
			}
			/** 修改操作 */
			assetInfoService.updateObject(asset);
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		} catch (Exception e) {
			/*** 修改失败 */
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listAssetInfo @Description: 查询资产信息 @param: @param storeNum
	 *         存放地编号 @param: @param offset 如有多条记录时分页的偏移值 @param: @param limit
	 *         如有多条记录时分页的大小 @param: @return @return: Map<String,Object> @throws
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "listAssetInfo", method = RequestMethod.GET)
	@ResponseBody
	// @MethodLog(name = "资产信息模块",option = "查询一条资产信息")
	public Map<String, Object> listAssetInfo(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		String result = ControllerResponse.RESPONSE_RESULT_ERROR;
		List<AssetInfo> rows = null;
		int total = 0;
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		AssetInfo condition = new AssetInfo();
		Map<String, Object> queryResult = assetInfoService.listObjectByConditionByPage(offset, limit, condition);
		if (queryResult != null) {
			rows = (List<AssetInfo>) queryResult.get("data");
			Iterator<AssetInfo> iterator = rows.iterator();
			while (iterator.hasNext()) {
				Map<String, Object> finalResult = new HashMap<>();
				AssetInfo row = iterator.next();
				finalResult.put("goodsID", row.getAssetNum());
				finalResult.put("goodsName", row.getAssetName());
				finalResult.put("goodsPerson", row.getAssetTakePeople());
				finalResult.put("goodsDepart", departInfoService.getObject(row.getDepartNum()).getDepartName());
				finalResult.put("goodsType",
						dicProfitLossInfoService.getObject(row.getDicProfitLossNum()).getDicProfitLossName());
				finalResult.put("goodsAddr", storeInfoService.getObject(row.getStoreNum()).getStoreName());
				listFinalResult.add(finalResult);
			}
			total = (int) queryResult.get("total");
		}
		result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		response.setResponseResult(result);
		response.setObjectInfo("rows", listFinalResult);
		response.setResponseTotal(total);
		return response.generateResponse();
	}

/**
 * 
 * @Title: exportAssetInfo_v1   
 * @Description: 导出选择的资产信息   根据选择的方式和内容导出需要的资产
 * @param: @param request
 * @param: @param response      
 * @return: void      
 * @throws
 */
	@RequestMapping(value = "exportAssetInfo_v1", method = RequestMethod.GET)
	@ResponseBody
	public void exportAssetInfo_v1(HttpServletRequest request, HttpServletResponse response) {
		ControllerResponse data = ControllerResponseFactory.newInstance();
		Integer excel_id = 2;
		String keyType = request.getParameter("keyType");
		String keyValue = request.getParameter("keyValue");
		// 初始化response
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		log.debug("----开始导出资产信息的excel表格----");
		assetInfoService.exportAssetInfoByCondition(keyType, keyValue, loginUser,excel_id, request, response);
	}

	/***
	 * @Title: listDepartStore
	 * @Description: 获取部门的所有存放地名称
	 * @param: @param
	 *             request
	 * @param: @return
	 * @return: Map<String,Object>
	 */
	@RequestMapping(value = "listDepartStore", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listDepartStore(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		String DepartNum = loginUser.getDepartNum();
		List<StoreInfo> store = new ArrayList<StoreInfo>();
		store = storeInfoService.listStoreByDepart(DepartNum);
		List<String> storename = new ArrayList<String>();
		for (int i = 0; i < store.size(); i++) {
			storename.add(store.get(i).getStoreName());
		}
		response.setResponseData(storename);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: countEchartsData @Description:
	 * 统计部门中每个存放地中不同损益类型资产的个数 @param: @param request @param: @return @return:
	 * Map<String,Object> @throws
	 */
	@RequestMapping(value = "countEchartsData_v1", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> countEchartsData(HttpServletRequest request,
			@RequestParam("inventoryBatch") String inventoryBatch, @RequestParam("departNum") String departNum) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserInfo userInfo = (UserInfo) session.getAttribute("loginUser");
		// 获取当前用户的角色
		String userRole = (String) session.getAttribute("loginUserRole");
		String departNumBySearch = "";
		// 用户权限校验
		if(userRole.equals(SUPER_ADMIN_ROLE)){
			departNumBySearch = departNum;
		}else{
			departNumBySearch = userInfo.getDepartNum();
		}
		// 1.获取所有的损益类型
		// 获取的损益类型集合转数组
		List<DicProfitLossInfo> listProfitlossList = dicProfitLossInfoService.listAllObject();
		DicProfitLossInfo[] dicProfitLossInfos = listProfitlossList
				.toArray(new DicProfitLossInfo[listProfitlossList.size()]);

		List<Map<String, Object>> listStoreProfitlossNumObj = new ArrayList<>();
		// 查询条件
		Map<String, Object> condition = new HashMap<>();
		condition.put("inventoryBatch", inventoryBatch);
		// 2.根据部门编号获取每种损益类型的个数。查询条件：部门编号，损益类型编号。存放要返回前端的数据格式：value:该部门某一损益类型的格式，name:损益类型名称。
		//condition.put("departNum", departNum);
		for (int i = 0; i < dicProfitLossInfos.length; i++) {
			Map<String, Object> profitlossNumObj = new HashMap<>();
			condition.put("dicProfitLossNum", dicProfitLossInfos[i].getDicProfitLossNum());
			profitlossNumObj.put("value", assetInfoService.listInventoryAssetNumberByCondition_v1(condition));
			profitlossNumObj.put("name", dicProfitLossInfos[i].getDicProfitLossName());
			listStoreProfitlossNumObj.add(profitlossNumObj);
		}

		// 3.将部门名称封装为表头
		String title = departInfoService.getObject(departNumBySearch).getDepartName();
		// 4.需要返回前台的json数据
		// 所有损益类型名称封装到一个数组。
		List<String> dicProfitLossNames = (List<String>) listdicProfitLoss(request).get("data");
		response.setObjectInfo("title", title+"损益类型统计图");
		response.setObjectInfo("dicProfitLossNames", dicProfitLossNames);
		response.setResponseData(listStoreProfitlossNumObj);
		return response.generateResponse();
	}

}
