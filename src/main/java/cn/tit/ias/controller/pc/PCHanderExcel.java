package cn.tit.ias.controller.pc;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.filechooser.FileSystemView;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.service.ExcelService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;
import cn.tit.ias.util.handlexcel.ExcelExportUtil_;
import cn.tit.ias.util.handlexcel.FileUtil;
import cn.tit.ias.util.handlexcel.easyexcel.EasyExcelHanderUtil;
import cn.tit.ias.util.pcode.CodeConverListUtil;

/**
 * 
 * 
 * @Description:处理excel
 * @author: maotao
 * @date: 2019年8月3日 上午11:29:30
 *
 */
@Controller
@RequestMapping("/PCHanerExcel")
public class PCHanderExcel {
	public static final Logger log = LoggerFactory.getLogger(PCHanderExcel.class);

	@Autowired
	ExcelService excelService;
	@Autowired
	AssetInfoService assetInfoService;

	/**
	 * 
	 * @Title: importAssetInfoExcel
	 * @Description: 导入资产到临时表
	 * @param: file
	 *             上传的文件
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "importAssetInfoExcel", method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "Excel表格模块", option = "导入资产到临时表")
	public Map<String, Object> importAssetInfoExcel(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) throws EncryptedDocumentException, InvalidFormatException, IOException {
		// 初始化 response对象
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 获取登陆用户
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		// 读取文件
		int total = 0;
		int available = 0;
		int importSuccess = 0;
		int repeat = 0;
		// 转换文件格式
		File convertFile = FileUtil.convertMultipartFileToFile(file);
		// 检查上传文件是否符合要求
		if (excelService.checkExcelFormat(convertFile)) {
			Map<String, Object> importInfo = excelService.importExcel(convertFile, loginUser.getDepartNum());
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> errorImportInfo = (List<Map<String, Object>>) importInfo.get("errorValiable");
			if (importInfo != null) {
				// 执行插入操作后的返回结果
				total = (int) importInfo.get("total");
				available = (int) importInfo.get("available");
				importSuccess = (int) importInfo.get("importSuccess");
				repeat = available - importSuccess;
				response.setResponseTotal(total);
				response.setObjectInfo("importSuccess", importSuccess);
				response.setObjectInfo("repeat", repeat);
				response.setObjectInfo("errerImportInfo", errorImportInfo);
				response.setObjectInfo("errorCount", errorImportInfo.size());
				session.setAttribute("errorImportInfo", errorImportInfo);
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			} else {
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			}
		} else {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			response.setResponseMsg("errorFormal");
		}
		return response.generateResponse();

	}

	/**
	 * 
	 * @Title: inLoadToAsset
	 * @Description:将上传到临时表中的数据通过处理后转存到资产表中
	 * @param: request
	 */
	@RequestMapping(value = "inLoadToAsset", method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "Excel表格模块", option = "临时表转存到资产表")
	public Map<String, Object> inLoadToAsset(HttpServletRequest request) {
		// 初始化response
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 获取登陆用户信息
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		try {
			// 处理并转存数据
			Map<String, Object> inLoadInfo = excelService.inLoadToAsset(loginUser.getDepartNum());
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			response.setResponseTotal((int) (inLoadInfo.get("total")));
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: exportErrorAssets @Description: 导出错误文件 @param: @param
	 *         request @param: @param response @return: void @throws
	 */
	@RequestMapping(value = "exportErrorAssets", method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "Excel表格模块", option = "导出错误文件")
	public void exportErrorAssets(HttpServletRequest request, HttpServletResponse response) {
		log.debug("----开始导出错误资产信息的excel表格----");
		excelService.exportErrorAssets(request, response);
	}

	/**
	 * 
	 * @Title: exportExcelEmpPayrollAccount   
	 * @Description: 导出盘点报表   
	 * @param: @param inventoryBatch
	 * @param: @param excel_id excel模板类型
	 * @param: @param request
	 * @param: @param response      
	 * @return: void      
	 * @throws
	 */
	@RequestMapping("/exportExcelEmpPayrollAccount")
	@MethodLog(name = "Excel表格模块", option = "导出盘点报表")
	public void exportExcelEmpPayrollAccount(String inventoryBatch, Integer excel_id, HttpServletRequest request,
			HttpServletResponse response) {
		log.debug("----开始导出excel表格----");
		excelService.downLoadInventoryExcel(inventoryBatch, excel_id, request, response);
	}

}
