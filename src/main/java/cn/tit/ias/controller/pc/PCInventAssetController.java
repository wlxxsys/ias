package cn.tit.ias.controller.pc;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Date; //获取系统时间

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.Put;
import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.ibatis.annotations.Param;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.org.apache.bcel.internal.generic.NEW;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.DicProfitLossInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.service.DepartInfoService;
import cn.tit.ias.service.DicProfitLossInfoService;
import cn.tit.ias.service.StoreInfoService;
import cn.tit.ias.util.controllerresponse.ControllerResponse;
import cn.tit.ias.util.controllerresponse.ControllerResponseFactory;
import javafx.scene.control.Alert;
import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * 
 * 
 * @Description:TODO(这里用一句话描述这个类的作用)
 * @author: chenshaolei
 * @date: 2019年8月6日 下午8:04:29
 *
 */
@Controller
@RequestMapping("/pcInventAsset")
public class PCInventAssetController {
	@Autowired
	private StoreInfoService storeinfoservice;
	@Autowired
	private DicProfitLossInfoService dicprofitlossinfoService;
	@Autowired
	private DepartInfoService departinfoservice;
	@Autowired
	private AssetInfoService assetInfoService;

	/**获取批次号开关*/
	@RequestMapping(value = "getSwitch", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getSwitch(HttpServletRequest request,@Param("depart") String depart){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		String msg="";
		if(depart.equals("")) {
			HttpSession session = request.getSession();
			UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
			String DepartNum = "";
			if(loginUser.getDepartNum()==null) {
				DepartInfo departObject=new DepartInfo();
				try {
					DepartNum=departObject.getDepartNum();
				} catch (Exception e) {
					//msg="无";
				}
				msg="无";
			}else {
				DepartNum=loginUser.getDepartNum();
				//获取部门最大批次号
				String MaxDepartNum=assetInfoService.getMaxBatch(DepartNum);
				try {
					if(assetInfoService.getSwitch(MaxDepartNum)!=null) {
						msg="开启中";
					}else {
						msg="已结束";
					}
				}catch (Exception e) {
					msg="已结束";
				}
			}
		}else {
			if(depart.equals("无")) {
				msg="无";
			}else {
				//获取部门最大批次号
				System.out.println("获取部门最大批次号"+depart);
				String MaxDepartNum="";
				try {
					MaxDepartNum=assetInfoService.getMaxBatch(departinfoservice.getDepartByName(depart).getDepartNum());
					if(MaxDepartNum.equals(null)) {
						MaxDepartNum="";
					}else {
						;
					}
				}catch (Exception e) {
					System.out.println("catch");
					MaxDepartNum="";
				}
				if(!MaxDepartNum.equals("")) {
					try {
						if(assetInfoService.getSwitch(MaxDepartNum)!=null) {
							msg="开启中";
						}else {
							msg="已结束";
						}
					} catch (Exception e) {
						msg="已结束";
					}
				}else {
					msg="无";
				}
			}
		}
		response.setResponseMsg(msg);
		return  response.generateResponse();
	}
	/**关闭批次号开关*/
	@RequestMapping(value = "closeSwitch", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> closeSwitch(@Param("batch") String batch){
		ControllerResponse response = ControllerResponseFactory.newInstance();
		System.out.println(batch);
		String result=ControllerResponse.RESPONSE_RESULT_ERROR;
		String msg="";
		Map<String,Object>batchInfo = new HashMap<>();
		try {
			assetInfoService.closeSwitch(batch);
			batchInfo.put("inventoryBatch", batch);
			batchInfo.put("runStatus",0);
			assetInfoService.updateInventoryBatchInfo(batchInfo);
			result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
			msg="关闭成功";
		} catch (Exception e) {
			result=ControllerResponse.RESPONSE_RESULT_ERROR;
			msg="关闭失败";
		}
		response.setResponseMsg(msg);
		response.setResponseResult(result);
		return  response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: listStatus @Description: 获取所有盘点状态名称 @param: @return @return:
	 * Map<String,Object> @throws
	 */
	@RequestMapping(value = "listStatus", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listStatus() {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<Map<String, Object>> listStatus = new ArrayList<Map<String, Object>>();
		listStatus = assetInfoService.listInventoryStatusObj();// 获取所有的盘点状态对象
		List<String> statusName = new ArrayList<String>();
		for (int i = 0; i < listStatus.size(); i++) {
			statusName.add(listStatus.get(i).get("inventoryStatusName").toString());
		}
		response.setResponseData(statusName);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listDicProfitLoss @Description:
	 * 获取所有损益类型名称 @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "listDicProfitLoss", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listDicProfitLoss() {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		List<DicProfitLossInfo> listdicprofitloss = new ArrayList<DicProfitLossInfo>();
		listdicprofitloss = dicprofitlossinfoService.listAllObject(); // 获取所有损益类型
		List<String> dicName = new ArrayList<String>();
		for (int i = 0; i < listdicprofitloss.size(); i++) {
			dicName.add(listdicprofitloss.get(i).getDicProfitLossName());
		}
		response.setResponseData(dicName);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listDepartStore @Description:
	 * 获取部门的所有存放地名称 @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "listDepartStore", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listDepartStore(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		String DepartNum = loginUser.getDepartNum();
		List<StoreInfo> store = new ArrayList<StoreInfo>();
		store = storeinfoservice.listStoreByDepart(DepartNum);
		List<String> storename = new ArrayList<String>();
		for (int i = 0; i < store.size(); i++) {
			storename.add(store.get(i).getStoreName());
		}
		response.setResponseData(storename);
		return response.generateResponse();
	}
	
	/**
	 * 
	 * @Title: listStoreName @Description:
	 * 获取所有存放地名称 @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "listStoreName", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listStoreName(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		//获取登陆者的学校
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		String DepartNum = loginUser.getDepartNum();
		List<StoreInfo> ListStore=new ArrayList<StoreInfo>();
		try {
			ListStore=storeinfoservice.listStoreByCollege(departinfoservice.getObject(DepartNum).getCollegeNum());
		} catch (Exception e) {
			ListStore=storeinfoservice.listAllObject();
		}
		
		List<String> storename = new ArrayList<String>();
		for (int i = 0; i < ListStore.size(); i++) {
			storename.add(ListStore.get(i).getStoreName());
		}
		response.setResponseData(storename);
		return response.generateResponse();
	}
	/**
	 * 
	 * @Title: listDepart @Description: 获取所有部门的名称 @param: @return @return:
	 * Map<String,Object> @throws
	 */
	@RequestMapping(value = "listDepart", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listDepart() {
		ControllerResponse response = ControllerResponseFactory.newInstance();

		List<DepartInfo> depart = departinfoservice.listAllObject();
		List<String> departName = new ArrayList<String>();
		for (int i = 0; i < depart.size(); i++) {
			departName.add(depart.get(i).getDepartName());
		}
		response.setResponseData(departName);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: listBatchByDepart   
	 * @Description: 根据部门查看盘点批次的信息  
	 * @param: @param request
	 * @param: @param depart 选择的部门
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value="listBatchByDepart",method=RequestMethod.GET)
	@ResponseBody
	public Map<String, Object>listBatchByDepart(HttpServletRequest request,
			@RequestParam("departName") String depart, @RequestParam("limit")Integer limit, 
			@RequestParam("offset")Integer offset){
		
		ControllerResponse response = ControllerResponseFactory.newInstance();
		// 获取用户登陆用户信息
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		String departNum = "";
		// 判断用户角色，部门为null的是超级管理员，否则为管理员
		if(loginUser.getDepartNum()==null) {
			System.out.println("超级管理员选择的部门："+depart);
			DepartInfo departObject=departinfoservice.getDepartByName(depart);
			//System.out.println("超级管理员选择的部门："+departObject);
			try {
				departNum=departObject.getDepartNum();
			} catch (Exception e) {
				System.out.println("超级管理员选择部门异常");
			}
		}else {
			departNum=loginUser.getDepartNum();
		}
		
		// 存放查询结果
		List<Map<String, Object>> batchs = new ArrayList<>();
		try {
			System.out.println("用户所在部门"+departNum);
			batchs = (List<Map<String, Object>>) assetInfoService.listInventoryBatchInfoByDepart(departNum,offset,limit).get("data");
			System.out.println(batchs);
			response.setResponseResult(response.RESPONSE_RESULT_SUCCESS);
			response.setResponseTotal((Integer)assetInfoService.listInventoryBatchInfoByDepart(departNum,offset,limit).get("total"));
			response.setObjectInfo("rows", batchs);
		} catch (Exception e) {
			response.setResponseResult(response.RESPONSE_RESULT_ERROR);
			response.setObjectInfo("rows", null);
			response.setResponseTotal(0);
		}
		return response.generateResponse();
	}
	/**
	 * 
	 * @Title: listDepartBatch @Description:
	 * 查询部门所有盘点批次号 @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "listDepartBatch", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> listDepartBatch(HttpServletRequest request,@Param("depart") String depart) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		String DepartNum = "";
		if(loginUser.getDepartNum()==null) {
			DepartInfo departObject=departinfoservice.getDepartByName(depart);
			try {
				DepartNum=departObject.getDepartNum();
			} catch (Exception e) {
				
			}
			
		}else {
			DepartNum=loginUser.getDepartNum();
		}
		List<String> AllBatch=new ArrayList<String>();
		AllBatch = assetInfoService.getAllBatchByDepart(DepartNum);
		List<String> TrueBatch = new ArrayList<String>();
		// 判断是不是本部门的批次
		for (int i = 0; i < AllBatch.size(); i++) {
			// 截取批次号里的部门编号
			String getdepartbybatch = (String) AllBatch.get(i).subSequence(4, 6);
			int depart_num = Integer.parseInt(getdepartbybatch);
			String transdepartnum = String.valueOf(depart_num);
			// 本部门的放到List中
			if (transdepartnum.equals(DepartNum)) {
				TrueBatch.add(AllBatch.get(i));
			} else {
				;
			}
		}
		response.setResponseData(TrueBatch);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: OpenNewInventory @Description: 点击开始新的盘点批次 @param: @param
	 * inputBatch @param: @param
	 * inventoryBatch,assetNum[] @param: @return @return:
	 * Map<String,Object> @throws
	 */

	@RequestMapping(value = "OpenNewInventory", method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "盘点模块",option = "创建新的盘点批次")
	public Map<String, Object> OpenNewInventory(HttpServletRequest request) {
		ControllerResponse response = ControllerResponseFactory.newInstance();

		HttpSession session = request.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		List<Integer> TrueBatch = new ArrayList<Integer>();
		String DepartNum = loginUser.getDepartNum();
		DecimalFormat zero = new DecimalFormat("00");
		String inventorydepart = zero.format(Integer.parseInt(DepartNum));
		String Batch = "";
		String result="";
		String msg="";
		// 判断该部门是否有未关闭的盘点批次
		Map<String, Object>conditionSwitch = new HashMap<>();
		conditionSwitch.put("departNum", DepartNum);
		try {
			Map<String, Object> runInventoryBatch = assetInfoService.getSwitch_v1(conditionSwitch);
			if(runInventoryBatch != null){
				response.setResponseMsg("有未结束的盘点任务，不允许创建新的盘点批次");
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
				return response.generateResponse();
			}
		} catch (Exception e) {
			response.setResponseMsg("获取盘点批次运行状态异常");
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
			return response.generateResponse();
		}
		
		// 获取该部门的所有批次号
		List<String> AllBatch = assetInfoService.getAllBatchByDepart(DepartNum);
		
		try {
			AllBatch = assetInfoService.getAllBatchByDepart(DepartNum);
		} catch (Exception e) {
			AllBatch = null;
		}
		// 判断是不是本部门的批次
		for (int i = 0; i < AllBatch.size(); i++) {
			// 截取批次号里的部门编号
			String getdepartbybatch = (String) AllBatch.get(i).subSequence(4, 6);
			int depart_num = Integer.parseInt(getdepartbybatch);
			String transdepartnum = String.valueOf(depart_num);
			// 本部门的放到List中
			if (transdepartnum.equals(DepartNum)) {
				int transbatch = Integer.parseInt(AllBatch.get(i));
				TrueBatch.add(transbatch);
			} else {
				;
			}
		}
		
		// 找出这个部门批次号的最大值
		if (TrueBatch.size() != 0) {
			int MaxBatch = Collections.max(TrueBatch);
			System.out.println("maxbatch:"+MaxBatch);
			Batch = String.valueOf(MaxBatch);
		}
		String inventoryBatch = "";
		Date date = new Date();
		String nowyear = String.format("%tY", date);
		try {
			if (Batch == "") {
				inventoryBatch = nowyear + inventorydepart + "0001";

			} else {
				String oldyear = Batch.substring(0, 4);
				if (nowyear.equals(oldyear)) {
					int NewBatch = 0;
					NewBatch = Integer.parseInt(Batch) + 1;
					inventoryBatch = String.valueOf(NewBatch);
				} else {
					inventoryBatch = nowyear + inventorydepart + "0001";
				}
			}
			
			// 查找条件
			AssetInfo condition = new AssetInfo();
			condition.setDepartNum(DepartNum);
			List<AssetInfo> listasset = assetInfoService.listObjectByCondition(condition);
			Map<String, Object>batchInfo = new HashMap<>();
			// 创建新批次的记录
			if(listasset.size()!=0){
				assetInfoService.createBatchByBranch(inventoryBatch, DepartNum,listasset);
				batchInfo.put("departNum",DepartNum);
				batchInfo.put("inventoryBatch",inventoryBatch);
				batchInfo.put("runStatus",1);
				batchInfo.put("createUser",loginUser.getUserCount());
				batchInfo.put("createTime",new Date());
				assetInfoService.addInventoryBatchInfo(batchInfo);
				result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
				msg="开始新的盘点批次成功";
			}else {
				result=ControllerResponse.RESPONSE_RESULT_ERROR;
				msg="开始新的盘点批次失败——资产表为空";
			}
		} catch (Exception e) {
			msg="开始新的盘点批次失败";
			result=ControllerResponse.RESPONSE_RESULT_ERROR;
		}
		response.setResponseMsg(msg);
		response.setResponseResult(result);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: getAssetByBatch @Description: 批次号获取盘点信息 @param: @return @return:
	 * Map<String,Object> @throws
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getAssetByBatch", method = RequestMethod.GET)
	@ResponseBody
	//@MethodLog(name = "盘点模块",option = "批次号获取盘点信息")
	public Map<String, Object> getAssetByBatch(@RequestParam("offset") int offset, @RequestParam("limit") int limit,
			@RequestParam("batch") String Batch) {
		//,@RequestParam("keyword")String keyWord,@RequestParam("repositoryBelong")String searchValue
		ControllerResponse response = ControllerResponseFactory.newInstance();
		String result = ControllerResponse.RESPONSE_RESULT_ERROR;
		int total = 0;
		List<Map<String, Object>> listFinalResult = new ArrayList<>();
		Map<String, Object> queryResult = null;

		List<Map<String, Object>> rows = null;
		String date = "";
		Map<String, Object> Status = new HashMap<String, Object>();
		int StatusId = 0;
		if (Batch.equals("")) {
			System.out.println("batch is null");
		} else {
			queryResult = assetInfoService.getAssetByBatch(offset, limit, Batch);
			System.out.println(Batch);
		}
		if (queryResult != null) {
			rows = (List<Map<String, Object>>) queryResult.get("data");
			Iterator<Map<String, Object>> iterator = rows.iterator();
			while (iterator.hasNext()) {
				Map<String, Object> finalResult = new HashMap<>();
				Map<String, Object> row = iterator.next();

				AssetInfo asset = new AssetInfo();
				/** 异常处理---被删情况下只取盘点表中的信息 */
				try {
					asset = assetInfoService.getObject(row.get("assetNum").toString());
					//封装条件
					Map<String,Object> Conditions=new HashMap<String,Object>();
					Conditions.put("assetNum", asset.getAssetNum());
					Conditions.put("inventoryBatch", Batch);
					//获取inventory对象
					Map<String,Object> inventory=new HashMap<String,Object>();
					inventory=assetInfoService.getInventoryObjByCondition(Conditions);
					
					date = inventory.get("inventoryDate").toString();
					try {
						StatusId = (int) inventory.get("inventoryStatusId");
					} catch (Exception e) {
						StatusId = -1;
					}
					if (StatusId != -1) {
						Status = assetInfoService.getInventoryStatusObjByInventoryStatusId(StatusId);
					} else {
						Status = null;
					}
					finalResult.put("goodsID", asset.getAssetNum());
					finalResult.put("goodsName", asset.getAssetName());
					finalResult.put("goodsPerson", inventory.get("assetTakePeople"));
					finalResult.put("goodsDepart",
							departinfoservice
									.getObject((String)inventory.get("departNum"))
									.getDepartName());
					try {
						finalResult.put("goodsType",
								dicprofitlossinfoService.getObject(
										(int)inventory.get("dicProfitLossNum"))
										.getDicProfitLossName());
					} catch (Exception e) {
						finalResult.put("goodsType", "无");
					}
					try {
						finalResult.put("goodsAddr",
								storeinfoservice
										.getObject((int)inventory.get("storeNum"))
										.getStoreName());
					} catch (Exception e) {
						finalResult.put("goodsAddr", "无");
					}
					/*扫描地*/
					try {
						finalResult.put("spanAddr", storeinfoservice.getObject((int)inventory.get("spanAdress")).getStoreName());
					}catch(Exception e1) {
						finalResult.put("spanAddr", "空");
					}
					if (Status != null) {
						try {
							finalResult.put("goodsStatus", Status.get("inventoryStatusName"));
						} catch (Exception e) {
							finalResult.put("goodsStatus", "状态错误");
						}
					} else {
						/* 盘点状态id获取对象失败 */
					}
					finalResult.put("goodsDate", date);
					finalResult.put("goodsBatch", Batch);
					listFinalResult.add(finalResult);
				} catch (Exception e) {
					/** 资产表中的信息已被删除 */
					//封装条件
					Map<String,Object> Conditions=new HashMap<String,Object>();
					Conditions.put("assetNum", row.get("assetNum"));
					Conditions.put("inventoryBatch", Batch);
					//获取inventory对象
					Map<String,Object> inventory=new HashMap<String,Object>();
					inventory=assetInfoService.getInventoryObjByCondition(Conditions);
					System.out.println(inventory);
					try {
						date = inventory.get("inventoryDate").toString();
					}catch(Exception e1) {
						date="获取时间-失败";
					}
					try {
						StatusId = (int) inventory.get("inventoryStatusId");
					} catch (Exception e1) {
						StatusId = -1;
					}
					if (StatusId != -1) {
						Status = assetInfoService.getInventoryStatusObjByInventoryStatusId(StatusId);
					} else {
						Status = null;
					}
					finalResult.put("goodsID", row.get("assetNum").toString());
					finalResult.put("goodsName", "已移除");
					finalResult.put("goodsPerson", row.get("assetTakePeople"));
					finalResult.put("goodsDepart",
							departinfoservice.getObject(row.get("departNum").toString()).getDepartName());
					try {
						finalResult.put("goodsType",
								dicprofitlossinfoService
										.getObject(Integer.parseInt(row.get("dicProfitLossNum").toString()))
										.getDicProfitLossName());
					} catch (Exception e1) {
						finalResult.put("goodsType", "无");
					}
					try {
						finalResult.put("goodsAddr", storeinfoservice
								.getObject(Integer.parseInt(row.get("storeNum").toString())).getStoreName());
					} catch (Exception e1) {
						finalResult.put("goodsAddr", "无");
					}
					/*扫描地*/
					try {
						finalResult.put("spanAddr", storeinfoservice.getObject((int)inventory.get("spanAdress")).getStoreName());
					}catch(Exception e1) {
						finalResult.put("spanAddr", "空");
					}
					if (Status != null) {
						try {
							finalResult.put("goodsStatus", Status.get("inventoryStatusName"));
						} catch (Exception e1) {
							finalResult.put("goodsStatus", "状态错误");
						}
					} else {
						/* 盘点状态id获取对象失败 */
					}
					finalResult.put("goodsDate", date);
					finalResult.put("goodsBatch", row.get("inventoryBatch"));
					listFinalResult.add(finalResult);
				}
			}
			total = (int) queryResult.get("total");
		}
		result = ControllerResponse.RESPONSE_RESULT_SUCCESS;
		response.setResponseResult(result);
		response.setObjectInfo("rows", listFinalResult);
		response.setResponseTotal(total);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: deleteinventory @Description: 删除一条盘点信息 @param: @param
	 * goodsID @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "deleteinventory", method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "盘点模块",option = "删除一条盘点信息")
	public Map<String, Object> deleteinventory(@RequestParam("goodsID") String goodsID,
			@RequestParam("goodsBatch") String goodsBatch) {
		/** 初始化response */
		ControllerResponse response = ControllerResponseFactory.newInstance();
		try {
			int deleteResult = assetInfoService.deleteinventory(goodsID, goodsBatch);
			if (deleteResult == 1) {
				/** 删除操作成功 */
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
			} else
				/** 删除操作失败 */
				response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: Batchdeleteinventory @Description:
	 * 批量删除盘点信息 @param: @param:String[] goodsID @param: @return @return:
	 * Map<String,Object> @throws
	 */
	@RequestMapping(value = "Batchdeleteinventory", method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "盘点模块",option = "批量删除盘点信息")
	public Map<String, Object> Batchdeleteinventory(@RequestParam("goodsID") String[] goodsID,
			@RequestParam("goodsBatch") String goodsBatch) {
		/** 初始化response */
		System.out.println(goodsID[0]);
		ControllerResponse response = ControllerResponseFactory.newInstance();
		try {
			for (int i = 0; i < goodsID.length; i++) {
				System.out.println(goodsID[i] + "," + goodsBatch);
				assetInfoService.deleteinventory(goodsID[i], goodsBatch);
			}
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		} catch (Exception e) {
			response.setResponseResult(ControllerResponse.RESPONSE_RESULT_ERROR);
		}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: deleteInventoryBatch   
	 * @Description: 删除一条盘点批次信息  
	 * @param: @param batch 盘点批次编号
	 * @param: @return      
	 * @return: Map<String,Object>      
	 * @throws
	 */
	@RequestMapping(value = "deleteInventoryBatch", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object>deleteInventoryBatch(@RequestParam("batch") String batch){
		/** 初始化response*/
		ControllerResponse response = ControllerResponseFactory.newInstance();
			try {
				assetInfoService.deleteInventoryBatchInfo(batch);
				response.setResponseResult(response.RESPONSE_RESULT_SUCCESS);
			} catch (Exception e) {
				response.setResponseResult(response.RESPONSE_RESULT_ERROR);
			}
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: updateinventory @Description: 修改盘点信息 @param: @param
	 * inventory @param: @return @return: Map<String,Object> @throws
	 */
	@RequestMapping(value = "updateinventory", method = RequestMethod.POST)
	@ResponseBody
	@MethodLog(name = "盘点模块",option = "修改盘点信息")
	public Map<String, Object> updateinventory(@RequestBody Map<String, Object> inventory) {

		ControllerResponse response = ControllerResponseFactory.newInstance();
		/** 接收前端传来的参数 */
		String goodsID = (String) inventory.get("goodsID");
		String goodsName = (String) inventory.get("goodsName");
		String goodsAddr = (String) inventory.get("goodsAddr");
		String goodsPerson = (String) inventory.get("goodsPerson");
		String goodsType = (String) inventory.get("goodsType");
		String goodsStatus = (String) inventory.get("goodsStatus");
		String goodsBatch = (String) inventory.get("goodsBatch");
		/*获取资产信息*/
		AssetInfo oldasset = new AssetInfo();
		try {
			oldasset = assetInfoService.getObject(goodsID);
		} catch (Exception e) {
			oldasset = null;
		}
		/*获取存放地编号*/
		/*获取部门编号*/
		String departNum="";
		int storeNum=-1;
		try {
			storeNum=storeinfoservice.getStoreByName(goodsAddr).getStoreNum();
			/*由存放地获取部门编号*/
			departNum=storeinfoservice.getObject(storeNum).getDepartNum();
		}catch(Exception e) {
			storeNum=-1;
			departNum=null;
		}
		/*获取盘点状态编号*/
		int StatusID=-1;
		try {
			StatusID=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName(goodsStatus).get("inventoryStatusId");
		}catch(Exception e) {
			StatusID=-1;
		}
		/*获取损益类型编号*/
		int dicprofitlossNum=-1;
		try {
			dicprofitlossNum=dicprofitlossinfoService.getDicProfitLossByName(goodsType).getDicProfitLossNum();
		}catch(Exception e) {
			dicprofitlossNum=-1;
		}
		
		if (oldasset != null) {
			/**修改盘点表操作*/
			Map<String,Object> updateinventory=new HashMap<String,Object>();
			updateinventory.put("assetNum", goodsID);
			updateinventory.put("inventoryBatch", goodsBatch);
			if(StatusID!=-1) {
				updateinventory.put("inventoryStatusId", StatusID);
			}
			if(dicprofitlossNum!=-1) {
				updateinventory.put("dicProfitLossNum", dicprofitlossNum);
			}
			if(storeNum!=-1) {
				updateinventory.put("storeNum", storeNum);
			}
			if(departNum!=null) {
				updateinventory.put("departNum", departNum);
			}
			updateinventory.put("assetTakePeople", goodsPerson);
			assetInfoService.updateInventory(updateinventory);
			/** 修改资产表操作 */
			AssetInfo newasset = new AssetInfo();
			newasset.setAssetNum(goodsID);
			newasset.setAssetName(goodsName);
			newasset.setAssetModel(oldasset.getAssetModel());
			newasset.setAssetPrice(oldasset.getAssetPrice());
			newasset.setAssetFactory(oldasset.getAssetFactory());
			newasset.setAssetDocumentNum(oldasset.getAssetDocumentNum());
			newasset.setAssetBuyDate(oldasset.getAssetBuyDate());
			newasset.setAssetTakePeople(goodsPerson); // 修改资产表的领用人信息
			newasset.setAssetRemrk(oldasset.getAssetRemrk());
			try {
				if(storeNum!=-1)
				{
					newasset.setStoreNum(storeNum);// 资产表存放地
					
				}else {
					newasset.setStoreNum(oldasset.getStoreNum());
				}
			} catch (Exception e) {
				newasset.setStoreNum(oldasset.getStoreNum());
			}

			try {
				if(departNum!=null) {
					newasset.setDepartNum(departNum);
				}else {
					newasset.setDepartNum(oldasset.getDepartNum());
				}
			} catch (Exception e) {
				newasset.setDepartNum(oldasset.getDepartNum());
			}
			try {
				newasset.setDicProfitLossNum(
						dicprofitlossinfoService.getDicProfitLossByName(goodsType).getDicProfitLossNum());
			} catch (Exception e) {
				newasset.setDicProfitLossNum(oldasset.getDicProfitLossNum());
			}
			assetInfoService.updateObject(newasset);
		} else {
			System.out.println("资产已被删除");
			/**修改盘点表操作*/
			Map<String,Object> updateinventory=new HashMap<String,Object>();
			updateinventory.put("assetNum", goodsID);
			updateinventory.put("inventoryBatch", goodsBatch);
			if(StatusID!=-1) {
				updateinventory.put("inventoryStatusId", StatusID);
			}
			if(dicprofitlossNum!=-1) {
				updateinventory.put("dicProfitLossNum", dicprofitlossNum);
			}
			if(storeNum!=-1) {
				updateinventory.put("storeNum", storeNum);
			}
			if(departNum!=null) {
				updateinventory.put("departNum", departNum);
			}
			updateinventory.put("assetTakePeople", goodsPerson);
			assetInfoService.updateInventory(updateinventory);
		}
		response.setResponseResult(ControllerResponse.RESPONSE_RESULT_SUCCESS);
		return response.generateResponse();
	}

	/**
	 * 
	 * @Title: BatchupdateAssetLog @Description: 批量修改资产信息 @param: @param
	 * String[] goodsID,String keyword,String editcode @param: @return @return:
	 * Map<String,Object> @throws
	 */
	@RequestMapping(value = "BatchupdateAssetLog", method = RequestMethod.GET)
	@ResponseBody
	@MethodLog(name = "盘点模块",option = "批量修改资产信息")
	public Map<String, Object> BatchupdateAssetLog(@RequestParam("goodsID") String[] goodsID,
			@RequestParam("keyword") String keyword, @RequestParam("editcode") String editcode,
			@RequestParam("goodsBatch") String goodsBatch) {
		ControllerResponse response = ControllerResponseFactory.newInstance();
		System.out.println(goodsBatch);
		String[] tag = { "请选择", "领用人","存放地", "盘点状态", "损益类型" };
		String result=ControllerResponse.RESPONSE_RESULT_ERROR;
		try {
			if (keyword.equals(tag[0])){
				result=ControllerResponse.RESPONSE_RESULT_ERROR;
			} else {
				for (int i = 0; i < goodsID.length; i++) {
					/** 由前端ID获取资产信息 */
					AssetInfo oldasset = new AssetInfo();
					/** 异常处理-资产被删除的情况 */
					try {
						oldasset = assetInfoService.getObject(goodsID[i]);
					} catch (Exception e) {
						oldasset = null;
					}
					if (oldasset != null) {
						AssetInfo newasset = new AssetInfo();
						/**修改盘点表操作*/
						Map<String,Object> updateinventory=new HashMap<String,Object>();
						updateinventory.put("assetNum", goodsID[i]);
						updateinventory.put("inventoryBatch", goodsBatch);
						if (keyword.equals(tag[1])) {
							newasset.setAssetTakePeople(editcode); // 资产表领用人
							updateinventory.put("assetTakePeople", editcode);
							System.out.println("修改领用人");
						} else {
							newasset.setAssetTakePeople(oldasset.getAssetTakePeople());
						}
						if (keyword.equals(tag[2])) {
							newasset.setStoreNum(storeinfoservice.getStoreByName(editcode).getStoreNum()); // 资产表存放地
							updateinventory.put("storeNum", storeinfoservice.getStoreByName(editcode).getStoreNum());
							/*通过存放地修改部门*/
							newasset.setDepartNum(storeinfoservice.getStoreByName(editcode).getDepartNum());// 资产表部门
							updateinventory.put("departNum", storeinfoservice.getStoreByName(editcode).getDepartNum());
						} else {
							newasset.setStoreNum(oldasset.getStoreNum());
							newasset.setDepartNum(oldasset.getDepartNum());
						}
						if (keyword.equals(tag[3])) {
							/*获取盘点状态编号*/
							int StatusID=-1;
							try {
								StatusID=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName(editcode).get("inventoryStatusId");
							}catch(Exception e) {
								StatusID=-1;
							}
							if(StatusID!=-1)
							{
								updateinventory.put("inventoryStatusId", StatusID);
							}else {
								System.out.println("修改盘点状态-失败");
							}
						} else {
							;// 盘点状态
						}
						if (keyword.equals(tag[4])) {
							try {
								newasset.setDicProfitLossNum(dicprofitlossinfoService.getDicProfitLossByName(editcode).getDicProfitLossNum());// 资产表损益类型
								updateinventory.put("dicProfitLossNum", dicprofitlossinfoService.getDicProfitLossByName(editcode).getDicProfitLossNum());
							} catch (Exception e) {
								newasset.setDicProfitLossNum(oldasset.getDicProfitLossNum());
								System.out.println("不存在该损益类型！");
							}
						} else {
							newasset.setDicProfitLossNum(oldasset.getDicProfitLossNum());
						}
						newasset.setAssetNum(oldasset.getAssetNum());
						newasset.setAssetName(oldasset.getAssetName());
						newasset.setAssetModel(oldasset.getAssetModel());
						newasset.setAssetPrice(oldasset.getAssetPrice());
						newasset.setAssetFactory(oldasset.getAssetFactory());
						newasset.setAssetDocumentNum(oldasset.getAssetDocumentNum());
						newasset.setAssetBuyDate(oldasset.getAssetBuyDate());
						newasset.setAssetRemrk(oldasset.getAssetRemrk());
						assetInfoService.updateObject(newasset);	//修改资产表
						assetInfoService.updateInventory(updateinventory);//修改盘点表
					} else {
						/**修改盘点表操作*/
						Map<String,Object> updateinventory=new HashMap<String,Object>();
						updateinventory.put("assetNum", goodsID[i]);
						updateinventory.put("inventoryBatch", goodsBatch);
						if (keyword.equals(tag[1])) {
							updateinventory.put("assetTakePeople", editcode); // 盘点表领用人
						} else {
							;
						}
						if (keyword.equals(tag[2])) {
							updateinventory.put("storeNum", storeinfoservice.getStoreByName(editcode).getStoreNum());// 盘点表存放地
						} else {
							;
						}
						if (keyword.equals(tag[3])) {
							/*获取盘点状态编号*/
							int StatusID=-1;
							try {
								StatusID=(int) assetInfoService.getInventoryStatusObjByInventoryStatusName(editcode).get("inventoryStatusId");//盘点表损益类型
							}catch(Exception e) {
								StatusID=-1;
							}
							if(StatusID!=-1)
							{
								updateinventory.put("inventoryStatusId", StatusID);
							}else {
								System.out.println("修改盘点状态-失败");
							}
						}else {
							;
						}
						if (keyword.equals(tag[4])) {
							try {
								updateinventory.put("dicProfitLossNum", dicprofitlossinfoService.getDicProfitLossByName(editcode).getDicProfitLossNum());
							} catch (Exception e) {
								/*修改损益类型-失败*/
								System.out.println("修改损益类型-失败");
							}
						}else {
							;
						}
						assetInfoService.updateInventory(updateinventory);//修改盘点表
					}
				}
				result=ControllerResponse.RESPONSE_RESULT_SUCCESS;
			}
		} catch (Exception e) {
			/*** 修改失败 */
			result=ControllerResponse.RESPONSE_RESULT_ERROR;
		}
		response.setResponseResult(result);
		return response.generateResponse();
	}
}
