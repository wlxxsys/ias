package cn.tit.ias.service;

import java.util.List;

import cn.tit.ias.entity.Permission;

public interface PermissionService {

	/** 获取角色的权限*/
	List<Permission> listPermissionByRoleId(int roleId);
	/** 比较该地址是否是设定权限*/
	boolean needInterceptor(String requestURL);  
}
