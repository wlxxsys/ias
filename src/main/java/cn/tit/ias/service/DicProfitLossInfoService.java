package cn.tit.ias.service;

import cn.tit.ias.entity.DicProfitLossInfo;
/**
 * 
 *  
 * @Description:DicProfitLossInfo业务逻辑   
 * @author: maotao 
 * @date:   2019年6月12日 下午6:59:28       
 *
 */
public interface DicProfitLossInfoService extends IBaseCRUDService<DicProfitLossInfo, Integer> {
	/** according to name DicProfitLossInfo Object*/
    DicProfitLossInfo getDicProfitLossByName(String dicProfitloss);
}
