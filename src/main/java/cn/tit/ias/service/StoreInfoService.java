package cn.tit.ias.service;

import java.util.List;
import java.util.Map;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
/**
 * 
 *  
 * @Description:StoreInfo的业务逻辑   
 * @author: maotao 
 * @date:   2019年6月12日 下午7:04:02       
 *
 */
public interface StoreInfoService extends IBaseCRUDService<StoreInfo, Integer> {
	/** 通过名称获取storeInfo对象 */
	StoreInfo getStoreByName(String StoreName);

	/** 获取部门里的所有存放地*/
	List<StoreInfo>listStoreByDepart(String departNum);

	/** 模糊查询存放地名称 */
	List<StoreInfo> getStore(String StoreName);
	
	/** 根据条件查询需要的存放地。
	 * condition对象可添加的参数：storeNum，storeName，storeDoorNum，departNum。
	 * 添加参数的方式：eg:conditon.setStoreNum=1。*/
	 Map<String, Object> listStoreObjByCondition(StoreInfo condition);
	
    /** 根据条件分页获取需要的部门信息.<br/>
     * condition 添加的条件：departNum,departName,collegeNum<br/>
     * 添加条件的方式：eg:condition.setDepartNum="1001"
     * */
    Map<String, Object>listStoreObjByConditionPage(int offset,int limit,StoreInfo condition);
	
	/**某学校编号获取的所有存放地*/
	List<StoreInfo>listStoreByCollege(int collegeNum);
}
