package cn.tit.ias.service;

import java.io.Serializable;
import java.util.List;

import cn.tit.ias.entity.CollegeInfo;

public interface IBaseCRUDService <E,PK extends Serializable>{
	/**
	 * 
	 * @Title: addObject   
	 * @Description: 添加   
	 * @param: @param entity
	 * @param: @return      
	 * @return: boolean      
	 * @throws
	 */
	int addObject(E entity);
	
	/**
	 * 
	 * @Title: deleteObject   
	 * @Description: 删除   
	 * @param: @param id
	 * @param: @return      
	 * @return: boolean      
	 * @throws
	 */
	int deleteObject(Serializable id);
	
	/**
	 * 
	 * @Title: updateObject   
	 * @Description: 修改  
	 * @param: @param entity
	 * @param: @return      
	 * @return: boolean      
	 * @throws
	 */
	int updateObject(E entity);
	
	/**
	 * 
	 * @Title: getObject   
	 * @Description: 获取符合条件的单条数据   
	 * @param: @param id
	 * @param: @return      
	 * @return: E      
	 * @throws
	 */
	E getObject(Serializable id);
	
	/**
	 * 
	 * @Title: listAllObject   
	 * @Description: 获取符合条件参数的所有数据   
	 * @param: @return      
	 * @return: List<E>      
	 * @throws
	 */
	List<E> listAllObject(Object ...param);

	
}
