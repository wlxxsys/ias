package cn.tit.ias.service;

import java.util.List;
import java.util.Map;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
/**
 * 
 *  
 * @Description:DepartInfo业务逻辑   
 * @author: maotao 
 * @date:   2019年6月12日 下午6:57:23       
 *
 */
public interface DepartInfoService extends IBaseCRUDService<DepartInfo, String> {
	/** 按名称获取部门对象*/
    DepartInfo getDepartByName(String departName);
    
    /** 获取某校区所有的部门数目*/
    int getTotalDepartNum(int collegeNum);
    
    /** 根据条件获取需要的部门信息.<br/>
     * condition 添加的条件：departNum,departName,collegeNum<br/>
     * 添加条件的方式：eg:condition.setDepartNum="1001"
     * */
    Map<String, Object>listDepartObjByCondition(DepartInfo condition);
    
    /** 根据条件分页获取需要的部门信息.<br/>
     * condition 添加的条件：departNum,departName,collegeNum<br/>
     * 添加条件的方式：eg:condition.setDepartNum="1001"
     * */
    Map<String, Object>listDepartObjByConditionPage(int offset,int limit,DepartInfo condition);
}
