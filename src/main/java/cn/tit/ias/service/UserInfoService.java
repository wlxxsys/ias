package cn.tit.ias.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.server.ServerHttpResponse;

import cn.tit.ias.entity.UserInfo;
/**
 * 
 *  
 * @Description:UserInfo业务逻辑   
 * @author: maotao 
 * @date:   2019年6月12日 下午7:07:18       
 *
 */
public interface UserInfoService extends IBaseCRUDService<UserInfo, String> {
	/** 按用户名称获取UserInfo对象*/
	UserInfo getUserByUserName(String userName);
	/** 验证用户登陆*/
	String ValiteUser(UserInfo user);	
	/** 分页查询用户*/
	Map<String, Object> listObjectByPage(int offset,int limit,String departNum); 
    /** 获取部门中所有用户*/
	List<UserInfo> listAllObjectByDepart(String departNum);
	/** 修改密码*/
	public Map<String, Object> passwordModify(String userCount, Map<String, Object> passwordInfo);
	
}
