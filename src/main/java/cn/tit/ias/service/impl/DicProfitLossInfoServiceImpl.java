package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tit.ias.entity.DicProfitLossInfo;
import cn.tit.ias.mapper.DicProfitLossInfoMapper;
import cn.tit.ias.service.DicProfitLossInfoService;

@Service
public class DicProfitLossInfoServiceImpl implements DicProfitLossInfoService{
	@Autowired
	DicProfitLossInfoMapper dicprofitLossInfoMapper;
	@Override
	public int addObject(DicProfitLossInfo entity) {
		return dicprofitLossInfoMapper.addObject(entity);
	}

	@Override
	public int deleteObject(Serializable id) {
		return dicprofitLossInfoMapper.deleteObject(id);
	}

	@Override
	public int updateObject(DicProfitLossInfo entity) {
		return dicprofitLossInfoMapper.updateObject(entity);
	}

	@Override
	public DicProfitLossInfo getObject(Serializable id) {
		return dicprofitLossInfoMapper.getObject(id);
	}

	@Override
	public List<DicProfitLossInfo> listAllObject(Object... param) {
		return dicprofitLossInfoMapper.listAllObject();
	}

	@Override
	public DicProfitLossInfo getDicProfitLossByName(String dicProfitloss) {
		return dicprofitLossInfoMapper.getDicProfitLossByName(dicProfitloss);
	}

}
