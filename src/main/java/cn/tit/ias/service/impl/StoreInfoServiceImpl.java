package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.mapper.DepartInfoMapper;
import cn.tit.ias.mapper.StoreInfoMapper;
import cn.tit.ias.service.StoreInfoService;

@Service
public class StoreInfoServiceImpl implements StoreInfoService{
	@Autowired
	StoreInfoMapper storeinfoMapper;
	@Autowired
	DepartInfoMapper departinfoMapper;
	
	@Override
	public int addObject(StoreInfo entity) {
		int maxstorenum=-1;
		try {
			maxstorenum=storeinfoMapper.getMaxStoreNum()+1;
		} catch (Exception e) {
			maxstorenum=1;
		}
		entity.setStoreNum(maxstorenum);
		return storeinfoMapper.addObject(entity);
	}

	@Override
	public int deleteObject(Serializable id) {
		return storeinfoMapper.deleteObject(id);
	}

	@Override
	public int updateObject(StoreInfo entity) {
		return storeinfoMapper.updateObject(entity);
	}

	@Override
	public StoreInfo getObject(Serializable id) {
		return storeinfoMapper.getObject(id);
	}

	@Override
	public List<StoreInfo> listAllObject(Object... param) {
		return storeinfoMapper.listAllObject();
	}

	@Override
	public StoreInfo getStoreByName(String StoreName) {
		return storeinfoMapper.getStoreByName(StoreName);
	}

	@Override
	public List<StoreInfo> listStoreByDepart(String departNum) {
		
		return storeinfoMapper.listStoreByDepart(departNum);
	}
	
	@Override
	public List<StoreInfo> getStore(String StoreName) {
		return storeinfoMapper.getStore(StoreName);
	}

	/**某学校编号获取的所有存放地*/
	@Override
	public List<StoreInfo> listStoreByCollege(int collegeNum) {
		//通过学校编号获取所有部门
		DepartInfo getdepartbycollege=new DepartInfo();
		List<DepartInfo> listDepart=new ArrayList<DepartInfo>();
		getdepartbycollege.setCollegeNum(collegeNum);
		listDepart=departinfoMapper.listDepartObjByCondition(getdepartbycollege);
		List<String> listdepart=new ArrayList<String>();
		for(int i=0;i<listDepart.size();i++)
		{
			listdepart.add(listDepart.get(i).getDepartNum());
		}
		//通过所有部门获取所有存放地
		List<StoreInfo> listStore=new ArrayList<StoreInfo>();
		for(int j=0;j<listdepart.size();j++)
		{
			StoreInfo getstorebydepart=new StoreInfo();
			getstorebydepart.setDepartNum(listdepart.get(j));
			listStore.addAll(storeinfoMapper.listStoreObjByCondition(getstorebydepart));
		}
		return listStore;
	}

	@Override
	public Map<String, Object> listStoreObjByConditionPage(int offset, int limit, StoreInfo condition) {
		// 结果map
				Map<String, Object> resultSet = new HashMap<>();
				List<StoreInfo> list = null;
				int total = 0;
				boolean isPagination = true;
				// 判断分页条件是否正确
				if(offset < 0 || limit <0)
					isPagination = false;
				// 分页条件正确
				if(isPagination){
					// 添加分页条件,分页查询
					PageHelper.offsetPage(offset, limit);
					list = storeinfoMapper.listStoreObjByCondition(condition);
					// 判断查询结果是否为空
					if(list != null){
						PageInfo<StoreInfo> deInfo = new PageInfo<>(list);
						total = (int) deInfo.getTotal();
					}else{
						list = new ArrayList<>();
					}
				}else {
					// 条件正确不用分页
					list = storeinfoMapper.listStoreObjByCondition(condition);
					if(list != null){
						total = list.size();
					}else{
						list = new ArrayList<>();
					}
				}
				resultSet.put("data", list);
				resultSet.put("total", total);
				return resultSet;
	}

	@Override
	public Map<String, Object> listStoreObjByCondition(StoreInfo condition) {
		Map<String, Object> resultSet = new HashMap<>();
		List<StoreInfo> list = storeinfoMapper.listStoreObjByCondition(condition);
	    resultSet.put("data",list);
	    resultSet.put("total", list.size());
		return resultSet;
	}
}
