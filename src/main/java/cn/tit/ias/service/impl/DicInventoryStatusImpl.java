package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tit.ias.entity.DicInventoryStatus;
import cn.tit.ias.mapper.DicInventoryStatusMapper;
import cn.tit.ias.service.DicInventoryStatusService;
@Service
public class DicInventoryStatusImpl implements DicInventoryStatusService{

	@Autowired
	DicInventoryStatusMapper inventoryStatusMapper;
	@Override
	public int addObject(DicInventoryStatus entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteObject(Serializable id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateObject(DicInventoryStatus entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public DicInventoryStatus getObject(Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DicInventoryStatus> listAllObject(Object... param) {
		
		return inventoryStatusMapper.listAllObject();
	}

	@Override
	public DicInventoryStatus getDicInventoryStatusByName(String inventoryStatusName) {
		
		return inventoryStatusMapper.getDicInventoryStatusByName(inventoryStatusName);
	}

	
	
}
