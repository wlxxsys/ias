package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.tools.ant.taskdefs.MacroDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.DepartInfoMapper;
import cn.tit.ias.service.DepartInfoService;

@Service
public class DepartInfoServiceImpl implements DepartInfoService{
	@Autowired
	DepartInfoMapper departinfoMapper;
	@Override
	public int addObject(DepartInfo entity) {
		String departMaxNum ="";
		try {
			departMaxNum=departinfoMapper.getMaxDepartNum();
		} catch (Exception e) {
			departMaxNum="0";
		}
		int incrementNum = Integer.valueOf(departMaxNum)+1;
		entity.setDepartNum(String.valueOf(incrementNum));
		return departinfoMapper.addObject(entity);
	}
	@Override
	public int deleteObject(Serializable id) {
		return departinfoMapper.deleteObject(id);
	}
	@Override
	public int updateObject(DepartInfo entity) {
		return departinfoMapper.updateObject(entity);
	}
	@Override
	public DepartInfo getObject(Serializable id) {
		return departinfoMapper.getObject(id);
	}
	@Override
	public List<DepartInfo> listAllObject(Object... param) {
		return departinfoMapper.listAllObject();
	}
	@Override
	public DepartInfo getDepartByName(String departName) {
		return departinfoMapper.getDepartByName(departName);
	}
	@Override
	public int getTotalDepartNum(int collegeNum) {
		return departinfoMapper.getTotalDepartNum(collegeNum);
	}
	@Override
	public Map<String, Object> listDepartObjByCondition(DepartInfo condition) {
		Map<String, Object> resultSet = new HashMap<>();
		List<DepartInfo> list = departinfoMapper.listDepartObjByCondition(condition);
	    resultSet.put("data",list);
	    resultSet.put("total", list.size());
		return resultSet;
	}
	@Override
	public Map<String, Object> listDepartObjByConditionPage(int offset,int limit,DepartInfo condition) {
		// 结果map
		Map<String, Object> resultSet = new HashMap<>();
		List<DepartInfo> list = null;
		int total = 0;
		boolean isPagination = true;
		// 判断分页条件是否正确
		if(offset < 0 || limit <0)
			isPagination = false;
		// 分页条件正确
		if(isPagination){
			// 添加分页条件,分页查询
			PageHelper.offsetPage(offset, limit);
			list = departinfoMapper.listDepartObjByCondition(condition);
			// 判断查询结果是否为空
			if(list != null){
				PageInfo<DepartInfo> deInfo = new PageInfo<>(list);
				total = (int) deInfo.getTotal();
			}else{
				list = new ArrayList<>();
			}
		}else {
			// 条件正确不用分页
			list = departinfoMapper.listDepartObjByCondition(condition);
			if(list != null){
				total = list.size();
			}else{
				list = new ArrayList<>();
			}
		}
		resultSet.put("data", list);
		resultSet.put("total", total);
		return resultSet;
	}
	
}

