package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tit.ias.entity.DicUserRoleInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.DicUserRoleInfoMapper;
import cn.tit.ias.service.DicUserRoleInfoService;

/**
 * 
 *  
 * @Description:用户角色service的实现类   
 * @author: maotao 
 * @date:   2019年7月4日 上午10:17:46       
 *
 */
@Service
public class DicUserRoleInfoServiceImpl implements DicUserRoleInfoService{
    @Autowired
	private DicUserRoleInfoMapper userRoleMapper;
    
	@Override
	public int addObject(DicUserRoleInfo entity) {
		return userRoleMapper.addObject(entity);
	}

	@Override
	public int deleteObject(Serializable id) {
		return userRoleMapper.deleteObject(id);
	}

	@Override
	public int updateObject(DicUserRoleInfo entity) {
		return userRoleMapper.updateObject(entity);
	}



	@Override
	public DicUserRoleInfo getObject(Serializable id) {
		return userRoleMapper.getObject(id);
	}

	@Override
	public List<DicUserRoleInfo> listAllObject(Object... param) {
		/** 获取传的参数*/
		List<Object> pp = new ArrayList<>();
		for(Object paramm:param){
			pp.add(paramm);
		}
		return userRoleMapper.listAllObject();
	}

	@Override
	public DicUserRoleInfo getUserByName(String userRoleName) {
		
		return userRoleMapper.getUserByName(userRoleName);
	}

	

	


}
