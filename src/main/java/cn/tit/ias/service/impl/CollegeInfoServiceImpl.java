package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.mapper.CollegeInfoMapper;
import cn.tit.ias.service.CollegeInfoService;
@Service
public class CollegeInfoServiceImpl implements CollegeInfoService{

	@Autowired
	CollegeInfoMapper collegeInfoMapper;
	@Override
	public CollegeInfo getCollegeByName(String collegeName) {
		return collegeInfoMapper.getCollegeByName(collegeName);
	}
	
	@Override
	public int addObject(CollegeInfo entity) {
		int maxstorenum=-1;
		try {
			maxstorenum=collegeInfoMapper.getMaxCollegeNum()+1;
		} catch (Exception e) {
			maxstorenum=10001;
		}
		entity.setCollegeNum(maxstorenum);
		 return collegeInfoMapper.addObject(entity);
	}
	@Override
	public int deleteObject(Serializable id) {
		return collegeInfoMapper.deleteObject(id);
	}
	@Override
	public int updateObject(CollegeInfo entity) {
		return collegeInfoMapper.updateObject(entity);
	}
	@Override
	public CollegeInfo getObject(Serializable id) {
		return collegeInfoMapper.getObject(id);
	}



	@Override
	public List<CollegeInfo> listAllObject(Object... param) {
		
		return collegeInfoMapper.listAllObject();
	}

	@Override
	public Map<String, Object> listCollegeObjByConditionPage(int offset, int limit, CollegeInfo condition) {
		// 结果map
				Map<String, Object> resultSet = new HashMap<>();
				List<CollegeInfo> list = null;
				int total = 0;
				boolean isPagination = true;
				// 判断分页条件是否正确
				if(offset < 0 || limit <0)
					isPagination = false;
				// 分页条件正确
				if(isPagination){
					// 添加分页条件,分页查询
					PageHelper.offsetPage(offset, limit);
					list = collegeInfoMapper.listCollegeObjByCondition(condition);
					// 判断查询结果是否为空
					if(list != null){
						PageInfo<CollegeInfo> deInfo = new PageInfo<>(list);
						total = (int) deInfo.getTotal();
					}else{
						list = new ArrayList<>();
					}
				}else {
					// 条件正确不用分页
					list = collegeInfoMapper.listCollegeObjByCondition(condition);
					if(list != null){
						total = list.size();
					}else{
						list = new ArrayList<>();
					}
				}
				resultSet.put("data", list);
				resultSet.put("total", total);
				return resultSet;
	}

	@Override
	public Map<String, Object> listCollegeObjByCondition(CollegeInfo condition) {
		Map<String, Object> resultSet = new HashMap<>();
		List<CollegeInfo> list = collegeInfoMapper.listCollegeObjByCondition(condition);
	    resultSet.put("data",list);
	    resultSet.put("total", list.size());
		return resultSet;
	}
	

}
