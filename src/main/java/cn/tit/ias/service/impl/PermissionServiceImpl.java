package cn.tit.ias.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.tit.ias.entity.Permission;
import cn.tit.ias.mapper.PermissionMapper;
import cn.tit.ias.service.PermissionService;
@Service
public class PermissionServiceImpl implements PermissionService{

	@Autowired
	PermissionMapper permissionMapper;
	@Override
	public List<Permission> listPermissionByRoleId(int roleId) {
		
		return permissionMapper.listPermissionByRoleId(roleId);
	}

	@Override
	public boolean needInterceptor(String requestURL) {
		List<Permission>list = permissionMapper.listPermission();
		for (Permission permission : list) {
			if(permission.getUrl().equals(requestURL))
				return true;
		}
		return false;
	}

}
