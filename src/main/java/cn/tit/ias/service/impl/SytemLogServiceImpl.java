package cn.tit.ias.service.impl;

import java.awt.Robot;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.tools.ant.taskdefs.MacroDef;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.SystemLogInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.SystemLogInfoMapper;
import cn.tit.ias.service.SystemLogService;

/**
 * 
 *  
 * @Description:用户操作日志service实现   
 * @author: maotao 
 * @date:   2019年9月11日 下午10:17:16       
 *
 */
@Service
public class SytemLogServiceImpl implements SystemLogService{
	@Autowired
	SystemLogInfoMapper systemLogmapper;
	@Override
	public int deleteByPrimaryKey(Integer id) {
		
		return systemLogmapper.deleteUserOperationRecordByPrimaryKey(id);
	}

	@Override
	public int insert(SystemLogInfo record) {
		
		return systemLogmapper.insertUserOperationRecord(record);
	}

	@Override
	public int deleteUserOperationRecordByDepartNum(String departNum) {
		
		return systemLogmapper.deleteUserOperationRecordByDepartNum(departNum);
	}


	@Override
	public int insertLoginRecord(Map<String, Object> loginRecord) {
		
		return systemLogmapper.insertLoginRecord(loginRecord);
	}

	@Override
	public int deleteLoginRecordByDepartNum(String departNum) {
	
		return systemLogmapper.deleteLoginRecordByDepartNum(departNum);
	}


	@Override
	public Map<String, Object> getLoginRecordByConditionPage(int offset, int limit,Map<String, Object> condition) {
		Map<String, Object> resultSet = new HashMap<>();
		List<Map<String, Object>> record = null;
		int total = 0;
		boolean isPagination = true;
		if (offset < 0 || limit < 0)
			isPagination = false;
		try {
			if (isPagination) {
				PageHelper.offsetPage(offset, limit);
				record = systemLogmapper.getLoginRecordByCondition(condition);
				if (record != null) {
					PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(record);
					total = (int) pageInfo.getTotal();
					System.out.println(total);
				} else {
					record = new ArrayList<>();
				}
			} else {
				record = systemLogmapper.getLoginRecordByCondition(condition);
				if (record != null) {
					total = record.size();
				} else {
					record = new ArrayList<>();
				}
			}
		} catch (Exception e) {

		}
		List<Map<String, Object>> list = new ArrayList<>();
		// 格式化日期并转换成字符串，转换登陆类型为汉字
		for (Map<String, Object> map : record) {
			Date accessTime = (Date) map.get("accessTime");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			map.put("accessTime",sdf.format(accessTime));
			String accessType = (String) map.get("accessType");
			if(accessType.equals("login"))
				accessType = "登入";
			if(accessType.equals("logout"))
				accessType = "登出";
			map.put("accessType", accessType);
			list.add(map);
		}
		resultSet.put("data",list);
		resultSet.put("total", total);
		return resultSet;
	}

	@Override
	public Map<String, Object> getUserOperationByConditionPage(int offset, int limit, Map<String, Object> condition) {
		Map<String, Object> resultSet = new HashMap<>();
		List<Map<String, Object>> record = null;
		int total = 0;
		boolean isPagination = true;
		if (offset < 0 || limit < 0)
			isPagination = false;
		try {
			if (isPagination) {
				PageHelper.offsetPage(offset, limit);
				record = systemLogmapper.getUserOperationRecordByCondition(condition);
				if (record != null) {
					PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(record);
					total = (int) pageInfo.getTotal();
					System.out.println(total);
				} else {
					record = new ArrayList<>();
				}
			} else {
				record = systemLogmapper.getUserOperationRecordByCondition(condition);
				if (record != null) {
					total = record.size();
				} else {
					record = new ArrayList<>();
				}
			}
		} catch (Exception e) {

		}
		List<Map<String, Object>> list = new ArrayList<>();
		// 格式化日期并转换成字符串，转换登陆类型未汉字
		for (Map<String , Object> systemLogInfo : record) {
			Map<String, Object> map = new HashMap<>();
			Date temptime = (Date) systemLogInfo.get("createTime");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			map.put("id", systemLogInfo.get("id"));
			map.put("userCount",systemLogInfo.get("userCount"));
			map.put("createTime",sdf.format(temptime));
			map.put("operation",(String)systemLogInfo.get("operation"));
			map.put("ip",systemLogInfo.get("ip"));
			map.put("userName", systemLogInfo.get("userName"));
			map.put("userRole", systemLogInfo.get("userRole"));
			list.add(map);
		}
		resultSet.put("data",list);
		resultSet.put("total", total);
		return resultSet;
	}


}
