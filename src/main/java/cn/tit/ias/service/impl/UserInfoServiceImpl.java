package cn.tit.ias.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aspectj.apache.bcel.generic.ReturnaddressType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageInterceptor;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.UserInfoMapper;
import cn.tit.ias.service.UserInfoService;

/**
 * 
 * 
 * @Description:userInfoService实现类
 * @author: maotao
 * @date: 2019年7月4日 下午4:39:48
 *
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {
	@Autowired
	UserInfoMapper userMapper;

	/** 添加用户 */
	@Override
	public int addObject(UserInfo entity) {
		return userMapper.addObject(entity);
	}

	/** 删除用户 */
	@Override
	public int deleteObject(Serializable id) {
		return userMapper.deleteObject(id);
	}

	/** 修改用户 */
	@Override
	public int updateObject(UserInfo entity) {
		return userMapper.updateObject(entity);
	}

	/** 获取一个指定用户 */
	@Override
	public UserInfo getObject(Serializable id) {
		return userMapper.getObject(id);
	}

	@Override
	/** 根据条件获取用户 */
	public List<UserInfo> listAllObject(Object... param) {
		if (param == null) {
			return userMapper.listAllObject();
		}
		String departNum = null;
		for (Object par : param) {
			departNum = (String) par;
		}
		return userMapper.listUserByDepartNum(departNum);
	}

	@Override
	/** 按用户名称获取用户 */
	public UserInfo getUserByUserName(String userName) {
		return userMapper.getUserByUserName(userName);
	}

	/** 验证用户是否存在 */
	@Override
	public String ValiteUser(UserInfo user) {
		try {
			UserInfo dbuser = userMapper.getObject(user.getUserCount());
			if (dbuser.getUserPassward().equals(user.getUserPassward())) {
				return "pass";
			} else {
				return "passwordError";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "noExistUser";
		}
	}

	/**
	 * 获取部门所有用户
	 * <p>
	 * Title: listObjectByPage
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param offset
	 *            开始页
	 * @param limit
	 *            每一页的容纳数据量
	 * @param departNum
	 *            部门编号
	 * @return 封装的数据集
	 * @see cn.tit.ias.service.UserInfoService#listObjectByPage(int, int,
	 *      java.lang.String)
	 */
	@Override
	public Map<String, Object> listObjectByPage(int offset, int limit, String departNum) {

		/** 初始化结果集 */
		Map<String, Object> resultSet = new HashMap<>();
		List<UserInfo> users = null;
		System.out.println("部门编号" + departNum);
		int total = 0;
		// 分页的标识符
		boolean isPagination = true;
		// 验证
		if (offset < 0 || limit < 0)
			isPagination = false;
		// query
		try {
			if (isPagination) {
				// 开始分页
				PageHelper.offsetPage(offset, limit);
				System.out.println(offset + "," + limit);
				users = userMapper.listUserByDepartNum(departNum);
				System.out.println(users);
				if (users != null) {
					// 使用pageInfo对查出的结果进行包装
					PageInfo<UserInfo> pageInfo = new PageInfo<>(users);
					total = (int) pageInfo.getTotal();

					// System.out.println("分页总数："+total);
					// System.out.println(pageInfo.toString());
				} else {
					// 空列表
					// System.out.println("空列表");
					users = new ArrayList<>();
				}
			} else {
				users = userMapper.listUserByDepartNum(departNum);
				if (users != null) {
					total = users.size();
				} else {
					users = new ArrayList<>();
				}
			}

		} catch (Exception e) {

		}
		// System.out.println(users);
		resultSet.put("data", users);
		resultSet.put("total", total);
		return resultSet;
	}

	@Override
	public List<UserInfo> listAllObjectByDepart(String departNum) {

		return userMapper.listUserByDepartNum(departNum);
	}

	@Override
	public Map<String, Object> passwordModify(String userCount, Map<String, Object> passwordInfo) {
		Map<String, Object> resultSet = new HashMap<>();
		if (passwordInfo == null)
			resultSet.put("result", "error");

		// 获取更改密码信息
		String rePassword = (String) passwordInfo.get("rePassword");
		String newPassword = (String) passwordInfo.get("newPassword");
		String oldPassword = (String) passwordInfo.get("oldPassword");
		if (rePassword == null || newPassword == null || oldPassword == null){
			resultSet.put("msg", "不能输入空值");
			return resultSet;
		}
		try {
			// 获取用户的账户信息
			UserInfo user = userMapper.getObject(userCount);
			// 用户不存在
			if(user == null){
				resultSet.put("msg","该用户不存在");
				resultSet.put("result","error");
				return resultSet;
			}
				
			// 新密码一致性验证
			if (!newPassword.equals(rePassword)){
				resultSet.put("msg", "passwordUnmatched");
				resultSet.put("result","error");
				return resultSet;
			}
				
			// 输入的原密码错误
			if(!oldPassword.equals(user.getUserPassward())){
				resultSet.put("msg", "passwordError");
				resultSet.put("result","error");
				return resultSet;
			}
				
			// 更新数据库	
			user.setUserPassward(newPassword);
			userMapper.updateObject(user);
			System.out.println("success");
			resultSet.put("result", "success");
		} catch (Exception e) {
			resultSet.put("msg","error");
			resultSet.put("result","error");
		}
		return resultSet;
	}

}
