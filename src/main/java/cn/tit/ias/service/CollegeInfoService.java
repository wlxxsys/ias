package cn.tit.ias.service;

import java.util.Map;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.entity.DepartInfo;
/**
 * 
 *  
 * @Description:CollegeInfo业务逻辑   
 * @author: maotao 
 * @date:   2019年6月12日 下午6:56:53       
 *
 */
public interface CollegeInfoService extends IBaseCRUDService<CollegeInfo, Integer>{
    public CollegeInfo getCollegeByName(String collegeName);    
    
    /** 根据条件分页获取需要的部门信息*/
    Map<String, Object>listCollegeObjByConditionPage(int offset,int limit,CollegeInfo condition);

	public Map<String,Object> listCollegeObjByCondition(CollegeInfo condition);
}
