package cn.tit.ias.service;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.multipart.MultipartFile;
/**
 * 
 *  
 * @Description:处理Excel接口   
 * @author: maotao 
 * @date:   2019年8月3日 下午12:11:28       
 *
 */
public interface ExcelService {

	/** 导入资产信息表*/
	Map<String, Object> importExcel(File file,String departNum);
	/** 将部门中导入的资产处理转存到资产表中*/
	Map<String, Object> inLoadToAsset(String departNum);
	/** 检查上传文件格式
	 * @throws InvalidFormatException 
	 * @throws EncryptedDocumentException 
	 * @throws IOException 
	 * @throws FileNotFoundException */
	boolean checkExcelFormat(File file) throws EncryptedDocumentException, InvalidFormatException, FileNotFoundException, IOException;
	
	/**
	 * 
	 * @Title: exportErrorAssets   
	 * @Description: 导出上传的错误资产信息   
	 * @param: @param request
	 * @param: @param response      
	 * @return: void      
	 * @throws
	 */
	void exportErrorAssets(HttpServletRequest request, HttpServletResponse response);
	/**
	 * 
	 * @Title: downLoadInventoryExcel   
	 * @Description: 导出盘点报表 
	 * @param: @param inventoryBatch 导出条件：盘点批次号
	 * @param: @param excel_id  excel模板类型
	 * @param: @param request 
	 * @param: @param response      
	 * @return: void      
	 * @throws
	 */
	void downLoadInventoryExcel(String inventoryBatch, Integer excel_id, HttpServletRequest request,
			HttpServletResponse response);
}
