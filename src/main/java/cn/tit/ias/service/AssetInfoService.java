package cn.tit.ias.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.annotations.Param;
import org.apache.xmlbeans.impl.jam.mutable.MPackage;

import com.alibaba.druid.sql.ast.statement.SQLForeignKeyImpl.On;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.UserInfo;
/**
 * 
 *  
 * @Description:AssetInfo业务逻辑   
 * @author: maotao 
 * @date:   2019年6月12日 下午7:09:16       
 *
 */
public interface AssetInfoService extends IBaseCRUDService<AssetInfo, String> {
	/******************** 盘点状态service  *************************/
	/** 获取所有盘点状态对象*/
    List<Map<String, Object>>listInventoryStatusObj();
	/** 根据盘点状态id获取盘点状态对象*/
	Map<String, Object>getInventoryStatusObjByInventoryStatusId(int id);
	/** 根据盘点状态name获取盘点状态对象*/
	Map<String, Object>getInventoryStatusObjByInventoryStatusName(String name);
	
	/******************* ias_asset表service ************************/
	/** 获取存放地所有资产的编号*/
	List<String> listAssetInfoNumsByStore(int storeNum);
	
	/** 根据条件查询需要的资产信息。<br/>
	 * 
	 * assetInfo可以添加的条件：assetNum，assetName，assetFactory，assetDocumentNum，assetBuyDate
	 * ，assetTakePeople，storeNum，dicProfitLossNum，departNum。<br/>
	 * 添加条件的方式：eg：assetInfo.setAssetNum="100010"
	 * */
	List<AssetInfo> listObjectByCondition(AssetInfo assetInfo);
	
	/** 根据条件分页查询需要的资产信息。<br/>
	 * offset参数：分页的起始页；limit参数：分页大小。<br/>
	 * assetInfo可以添加的条件：assetNum，assetName，assetFactory，assetDocumentNum，assetBuyDate
	 * ，assetTakePeople，storeNum，dicProfitLossNum，departNum。<br/>
	 * 添加条件的方式：eg：assetInfo.setAssetNum="100010"
	 * */
	Map<String, Object> listObjectByConditionByPage(int offset, int limit,AssetInfo assetInfo);
	/** 根据条件获取资产的个数*/
	int getCountByConditon(AssetInfo assetInfo);
	/******************* ias_import_asset表service ************************/
	
	/** 封装要导出的资产信息*/
	List<List<Object>> packageExportData(List<AssetInfo> assets);
	
	/** 获取要导出的资产采用easyExcel*/
	void exportAssetInfoByCondition(String key,String value,UserInfo loginUser,Integer excel_id,HttpServletRequest request,HttpServletResponse response);
	
	/********************inventory表 service ***************************/

	/**删除一条盘点信息*/	
	public int deleteinventory(String assetnum,String Batch);
	
	/**
	 * 修改一条盘点记录，参数可以有多个，有几个就都封装到Map<String,Object>中。<br/>
	 * 可以添加的参数：inventoryBatch，assetNum，inventoryStatusId，userCount，
	 * departNum，assetTakePeople，spanAdress，storeNum，dicProfitLossNum。<br/>
	 * inventoryBatch，assetNum是必选的参数，用来作为修改盘点记录的where条件。<br/>
	 * inventoryDate是不允许添加的参数，因为该参数该参数已经默认添加为当前时间，调用该方法时，不用自己在添加条件。
	 * */
	int updateInventory(Map<String, Object>inventory);
	
	/** 根据条件查询一条盘点记录的某个字段。<br/>
	 * condition是Map<String,Object>对象.<br/>
	 * condition可以添加的条件：盘点状态编号inventoryStatusId，盘点日期inventoryDate，
	 * 盘点用户账号userCount，扫描地spanAdress，原存放地storeNum，损益类型编号dicProfitLossNum 作为要查询的字段（添加方式：eg:condition.put("inventoryStatusId","inventoryStatusId");）<br/>
	 * condition必须添加的条件：盘点批次号inventoryBatch，资产编号assetNum作为查询的wehre条件；<br/>
	 * 必需要添加条件的方式：condition.put("inventoryBatch","2019000001"),condition.put("assetNum", "11001056");
	 * */
	Object getInventoryParameterByCondition(Map<String, Object> condition);
	
	/** 根据条件查询一条盘点记录对象<br/>
	 * condition是Map<String,Object>对象.<br/>
	 * condition必须添加的条件：盘点批次号inventoryBatch，资产编号assetNum作为查询的wehre条件；<br/>
	 * 必需要添加条件的方式：condition.put("inventoryBatch","2019000001"),condition.put("assetNum", "11001056");
	 * */
	Map<String, Object> getInventoryObjByCondition(Map<String, Object> condition);
    
	/**部门编号获取该部门的最大批次号*/	
	public String getMaxBatch(String departNum);	
	
	/**获取部门的所有批次号*/	
	List<String> getAllBatchByDepart(String departNum);	
	
	/**创建批次号*/	
	public void createBatch(String Batch,String assetNum);	

	/**创建批次号批量操作*/
	public void createBatchByBranch(String batch,String departNum,List<AssetInfo>assetInfos);
	
	/**按盘点日期获取资产*/
	//	List<AssetInfo> listAssetLogByDate(String date);	
	
	/**按盘点批次获取盘点信息*/
	public Map<String,Object> getAssetByBatch(int offset,int limit,String batch);
	
	/**按盘点批次获取盘点信息 更新版本v1*/
	public Map<String,Object> getAssetByBatch_v1(int offset,int limit,String batch);

	/**获取存放地某批次的所有资产编号*/
	List<String> listAssetNumByBatchStoreNum(String Batch,int StoreNum);
	
	/**针对部门异常-通过批次号和扫描地编号获取所有部门异常盘点信息*/
	List<String> listAssetNumByBatchSpanAdress(String Batch,int spanAdress);

	/**获取批次号开关*/
	Map<String, Object> getSwitch(String Batch);
	
	/** 获取批次号开关新版本
	 * @param Map<String, Object> condition 查询条件的  key:inventoryBatch,departNum
	 * */
	Map<String, Object> getSwitch_v1(Map<String, Object> condition);
	/**删除批次号开关(关闭)*/
	int closeSwitch(String Batch);
	
	/** 根据条件获取盘点资产个数。旧版本<br/>
	 *  condition是Map<String,Object>对象。<br/>
	 * condition 添加的条件：盘点批次号inventoryBatch 盘点状态名称inventoryStatusName<br/>
	 * eg:condition.put("inventoryBatch","2019000001");
	 * 返回值：int
	 * */
	int listInventoryAssetNumberByCondition(Map<String,Object>condition);

	/** 根据条件获取盘点资产个数。新版本<br/>
	 * 1.condition是Map<String,Object>对象。<br/>
	 * 2.condition必须添加的条件：盘点批次号inventoryBatch<br/>
	 * 3.condition可以添加的条件参数：<br>
	 * 部门编号departNum,存放地编号：storeNum,损益类型编号：dicProfitLossNum,<br>
	 * 资产编号assetNum,盘点状态编号inventoryStatusId,盘点日期inventoryDate,<br>
	 * 盘点用户账号userCount,领用人assetTakePeople,扫描地spanAdress。<br>
	 * 4.eg:condition.put("inventoryBatch","2019000001");
	 * 返回值：int
	 * */
	int listInventoryAssetNumberByCondition_v1(Map<String,Object>condition);
	
	/********************inventory_batch表 service ***************************/
	/**根据部门查询盘点批次信息*/
	Map<String, Object> listInventoryBatchInfoByDepart(String departNum,Integer offset,Integer limit);
	/**根据批次查询盘点批次信息*/
	Map<String, Object> getInventoryBatchInfoByBatch(String departNum);
	/**添加一条盘点批次  盘点批次的状态：0代表结束；1代表正在运行*/
	int addInventoryBatchInfo(Map<String, Object> batchInfo);
	/**修改盘点批次的状态  map参数：inventoryBatch,runStatus*/
	int updateInventoryBatchInfo(Map<String, Object> batchInfo);
	/**删除一条盘点批次信息*/
	int deleteInventoryBatchInfo(String Batch);
}
