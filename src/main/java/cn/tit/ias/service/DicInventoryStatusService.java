package cn.tit.ias.service;

import cn.tit.ias.entity.DicInventoryStatus;

public interface DicInventoryStatusService extends IBaseCRUDService<DicInventoryStatus,Integer>{

	/** 根据名称获取盘点状态名称*/
	DicInventoryStatus getDicInventoryStatusByName(String inventoryStatusName);
}
