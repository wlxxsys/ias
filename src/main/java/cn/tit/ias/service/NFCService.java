package cn.tit.ias.service;
/**
 * 
 *  
 * @Description:NFC外部设备业务逻辑   
 * @author: maotao 
 * @date:   2019年6月13日 上午10:51:06       
 *
 */
public interface NFCService {
    /** 连接NFC外部设备*/
	int connetNFCDevice();
	
	/** 读取NFC标签数据*/
	void readData();
	
	/** 写入NFC标签数据*/
	void writeData();
}
