package cn.tit.ias.service;

import java.util.List;
import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Service;

import cn.tit.ias.aop.MethodLog;
import cn.tit.ias.entity.SystemLogInfo;

/**
 * 
 * 
 * @Description:系统操作日志Service
 * @author: maotao
 * @date: 2019年9月11日 上午11:45:59
 *
 */

public interface SystemLogService {
	/** 按编号删除用户操作记录 */
	int deleteByPrimaryKey(Integer id);

	/** 添加用户操作记录 */
	int insert(SystemLogInfo record);

	/** 按部门删除用户操作记录 */
	int deleteUserOperationRecordByDepartNum(String departNum);

	/** 添加用户登陆记录 */
	int insertLoginRecord(Map<String, Object> loginRecord);

	/** 按部门删除用户登陆记录 */
	int deleteLoginRecordByDepartNum(String departNum);

	/** 按条件获取登陆记录*/
	Map<String, Object> getLoginRecordByConditionPage(int offset, int limit,Map<String, Object>condition);
	
	/** 按条件获取用户操作记录*/
	Map<String, Object> getUserOperationByConditionPage(int offset, int limit,Map<String, Object>condition);
}

