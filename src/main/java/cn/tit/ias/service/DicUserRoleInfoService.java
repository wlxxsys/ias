package cn.tit.ias.service;

import java.util.List;

import cn.tit.ias.entity.DicUserRoleInfo;
import cn.tit.ias.entity.UserInfo;

/***
 * 
 * 
 * @Description: DicUserRoleInfo业务逻辑
 * @author: maotao
 * @date: 2019年6月12日 下午7:01:21
 *
 */
public interface DicUserRoleInfoService extends IBaseCRUDService<DicUserRoleInfo, Integer> {
	/** 按名称获取角色*/
	DicUserRoleInfo getUserByName(String userRoleName);
	/** 获取部门中某种角色的所有管理员 */
	//List<UserInfo> listAllUserOfRoleInDepart(String departNum);

	/** 获取部门某种角色的管理员个数 */
	//int getTotalUserNum(String departNum);
}
