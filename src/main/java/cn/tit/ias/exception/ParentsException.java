package cn.tit.ias.exception;

/***
 * 
 * 
 * @Description:业务层异常父类
 * @author: maotao
 * @date: 2019年6月14日 上午11:28:07
 *
 */
public class ParentsException extends Exception {
	/** 异常信息 */
	private String exeMessage;
	
	public ParentsException(){}

	public ParentsException(Exception e) {
		super(e);
	}

	public ParentsException(Exception e, String excMessage) {
		super(e);
		this.exeMessage = excMessage;
	}
	
	public ParentsException(String exeMessage){
		this.exeMessage = exeMessage;
	}

	public String getExeMessage() {
		return exeMessage;
	}

	public void setExeMessage(String exeMessage) {
		this.exeMessage = exeMessage;
	}
	

}
