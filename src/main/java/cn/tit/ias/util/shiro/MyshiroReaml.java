package cn.tit.ias.util.shiro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hslf.dev.UserEditAndPersistListing;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import cn.tit.ias.entity.DicUserRoleInfo;
import cn.tit.ias.entity.Permission;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.DicUserRoleInfoMapper;
import cn.tit.ias.mapper.PermissionMapper;
import cn.tit.ias.service.UserInfoService;
/**
 * 
 *  
 * @Description:shirReaml 身份验证，权限验证 
 * @author: maotao 
 * @date:   2019年9月20日 下午6:30:23       
 *
 */
public class MyshiroReaml extends AuthorizingRealm {

	@Autowired
	UserInfoService userInfoService;

	@Autowired
	PermissionMapper permissionMapper;
	
	@Autowired
	DicUserRoleInfoMapper roleMapper;
	/**
	 * 
	 * <p>
	 * Title: doGetAuthorizationInfo
	 * </p>
	 * <p>
	 * Description:验证权限
	 * </p>
	 * 
	 * @param arg0
	 * @return
	 * @see org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection token) {
		// 授权
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		Subject subject = SecurityUtils.getSubject();
		//System.out.println(subject);
		Session session = subject.getSession();
		UserInfo loginUser = (UserInfo) session.getAttribute("loginUser");
		// 创建存放用户角色的 Set
        Set<String> roles = new HashSet<>();
		// 获取登陆用户的角色
		int roleId = loginUser.getUserRoleId();
		String role = roleMapper.getObject(roleId).getUserRoleName();
		roles.add(role);
		// 获取用户权限
		List<Permission> permissions = permissionMapper.listPermissionByRoleId(roleId);
		List<String> permissionsColl = new ArrayList<>();
		for (Permission permission : permissions) {
			permissionsColl.add(permission.getUrl());
		}
		
		// 返回登陆用户的角色和权限
		authorizationInfo.addStringPermissions(permissionsColl);
		authorizationInfo.addRoles(roles);
		session.setAttribute("loginUserRole",role);
		return authorizationInfo;
	}

	/**
	 * 
	 * <p>
	 * Title: doGetAuthenticationInfo
	 * </p>
	 * <p>
	 * Description: 认证身份
	 * </p>
	 * 
	 * @param token
	 * @return
	 * @throws AuthenticationException
	 * @see org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		// 获取账号
		String userCount = (String) token.getPrincipal();
		UserInfo userInfo = userInfoService.getObject(userCount);
		if (userInfo == null) {
			System.out.println("认证：当前登陆的用户不存在");
			throw new UnknownAccountException();
		}
		String password = userInfo.getUserPassward();
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		String checkCode = (String) session.getAttribute("checkCode");
		
		// 交给AuthenticationRealm使用CredentialsMatcher进行密码匹配
		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(userCount, password, getName());
		
		session.setAttribute("loginUser", userInfo);
		return authenticationInfo;
	}

}
