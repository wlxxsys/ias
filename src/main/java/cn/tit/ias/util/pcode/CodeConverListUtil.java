package cn.tit.ias.util.pcode;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CodeConverListUtil {
	 public static final Logger log = LoggerFactory.getLogger(CodeConverListUtil.class);
	 
	 public CodeConverListUtil(){
	 }
	 
	 public static List<?> convert(List<?> list){
		 long startTime = System.currentTimeMillis();
		 log.debug("-------码表转换开始------");
		 
		 
		 
		 long endTime = System.currentTimeMillis();
		 long times = endTime - startTime;
		 log.debug("------码表转换结束："+endTime);
		 if (times > 1000L) {
             log.info("---------码表转换耗时：------" + times / 1000L + "s");
         } else {
             log.info("---------码表转换耗时：------" + times + "ms");
         }
		return list;
		 
	 }
}
