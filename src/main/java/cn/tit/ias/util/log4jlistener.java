package cn.tit.ias.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class log4jlistener implements ServletContextListener{

	public static final String log4jdirkey = "log4jdir";
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.getProperties().remove(log4jdirkey);
		
	}

	@Override
	public void contextInitialized(ServletContextEvent servletcontextevent) {
		String log4jdir = servletcontextevent.getServletContext().getRealPath("/");
		System.out.println("log4jdir:"+log4jdir);
		System.setProperty(log4jdirkey, log4jdir);
		
	}

}
