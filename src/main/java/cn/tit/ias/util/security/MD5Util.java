package cn.tit.ias.util.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *  
 *  
 * @Description:MD5   
 * @author: maotao 
 * @date:   2019年7月6日 下午9:56:41       
 *
 */
public class MD5Util {
    
	/**
	 * 
	 * @Title: MD5   
	 * @Description: 使用MD5算法对字符串进行处理   
	 * @param: @param plainString 需要处理的字符串
	 * @param: @return      
	 * @return: String      返回处理后的字符串
	 * @throws
	 */
	public static String MD5(String plainString) {
		
		if(plainString != null) {
			try {
				//创建具有指定算法名称的信息摘要 
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                messageDigest.update(plainString.getBytes());
                //使用指定的字节数组对摘要进行最后更新，然后完成摘要计算 
                byte[] byteData = messageDigest.digest();

                StringBuilder hexString = new StringBuilder();
                for (byte aByteData : byteData) {
                    String hex = Integer.toHexString(0xff & aByteData);
                    if (hex.length() == 1)
                        hexString.append('0');
                    hexString.append(hex);
                }
                return hexString.toString();
            } catch (NoSuchAlgorithmException e) {/* log */}
        }
		
		return "";
	}
}
