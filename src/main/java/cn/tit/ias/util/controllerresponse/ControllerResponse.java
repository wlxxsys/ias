package cn.tit.ias.util.controllerresponse;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;


/**
 * 
 *  
 * @Description:controller返回的信息载体response   
 * @author: maotao 
 * @date:   2019年7月6日 下午12:03:02       
 *
 */
public class ControllerResponse {
	public static final String RESPONSE_RESULT_SUCCESS = "success";
    public static final String RESPONSE_RESULT_ERROR = "error";

    // response 中可能使用的值
    private static final String RESPONSE_RESULT = "result";
    private static final String RESPONSE_MSG = "msg";
    private static final String RESPONSE_DATA = "data";
    private static final String RESPONSE_TOTAL = "total";
    // 存放响应中的信息
    private Map<String,Object> responseContent;

    // Constructor
    ControllerResponse() {
        this.responseContent = new HashedMap(10);
    }
    
    /**
     * 
     * @Title: setResponseResult   
     * @Description:  设置response 的状态
     * @param: @param result    response 的状态，值为 success 或 error  
     * @return: void      
     * @throws
     */
    public void setResponseResult(String result) {
    	this.responseContent.put(ControllerResponse.RESPONSE_RESULT, result);
    }
    
    /**
     * 
     * @Title: setResponseMsg   
     * @Description: 设置 response 的附加信息
     * @param: @param msg  response  的附加信息
     * @return: void      
     * @throws
     */
    public void setResponseMsg(String msg) {
    	this.responseContent.put(ControllerResponse.RESPONSE_MSG, msg);
    }
    
    /**
     * 
     * @Title: setResponseData   
     * @Description:   设置response中携带的数据
     * @param: @param data  携带的数据
     * @return: void      
     * @throws
     */
    public void setResponseData(Object data) {
    	this.responseContent.put(ControllerResponse.RESPONSE_DATA, data);
    	
    }
    
    /**
     * 
     * @Title: setResponseTotal   
     * @Description:           设置response中携带数据的数量，与RESPONSE_DATA配合使用  
     * @param: @param tatal    携带数据的数量
     * @return: void      
     * @throws
     */
    public void setResponseTotal(long tatal) {
    	this.responseContent.put(RESPONSE_TOTAL, tatal);
    }
    
    /**
     * 
     * @Title: setObjectInfo   
     * @Description:         设置response的自定义信息   
     * @param: @param key    自定义信息的key
     * @param: @param value  自定义信息的值
     * @return: void      
     * @throws
     */
    public void setObjectInfo(String key,Object value) {
    	this.responseContent.put(key, value);
    }
    
    /**
     * 
     * @Title: generateResponse   
     * @Description:   生成response
     * @param: @return      
     * @return: Map<String,Object>     代表response的一个map对象 
     * @throws
     */
    public Map<String,Object> generateResponse() {
    	return this.responseContent;
    }
}
