package cn.tit.ias.util.controllerresponse;
/***
 * 
 *  
 * @Description: ControllerResponse 工厂类
 * @author: maotao 
 * @date:   2019年7月6日 下午12:25:41       
 *
 */
public class ControllerResponseFactory {

	/**
	 * 
	 * @Title: newInstance   
	 * @Description: 实例化一个ControllerResponse对象   
	 * @param: @return    
	 * @return: ControllerResponse  实例化对象      
	 * @throws
	 */
	public static ControllerResponse newInstance() {
		return new ControllerResponse();
	}
}
