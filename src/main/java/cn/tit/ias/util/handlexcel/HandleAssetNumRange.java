package cn.tit.ias.util.handlexcel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.Count;

import cn.tit.ias.entity.AssetInfo;

/**
 * 
 * @Description:处理导入的资产表中的资产编号
 * @author: maotao
 * @date: 2019年8月2日 下午6:19:56
 */
public class HandleAssetNumRange {
	

	private static Map<String, Object> profitLossType = new HashMap<String, Object>();
	/**
	 * 
	 * @Title: handleExcel
	 * @Description: 将导入资产表 中一条资产编号范围展开为多条
	 * @param: @param assets 
	 * @return: List<AssetInfo> 处理后的所有资产
	 */
	public static List<AssetInfo> handleExcel(List<Map<String, Object>> assets,Map<String, Object>profitLossTypes) {
		profitLossType = profitLossTypes;
		// 存放所有的处理结果
		List<AssetInfo> resultAssets = new ArrayList<>();
		// 创建list的迭代器
		Iterator<Map<String, Object>> iterator = assets.iterator();
		// 遍历每一条资产信息
		Map<String, Object> asset;
		while (iterator.hasNext()) {
			String imAssetRange ="";
			asset = iterator.next();
			resultAssets.addAll(handleAssetNumRanges(asset));
			//System.out.println(resultAssets);
		}
		return resultAssets;
	}

	/**
	 * 处理资产编号，组成新的资产信息
	 * @param map
	 * @return
	 */
	public static List<AssetInfo> handleAssetNumRanges(Map<String, Object> asset) {
		
		// 是否有字母
		boolean isLetter = false;
		// 若有字母，字母的位置
		List<Integer> index = new ArrayList<>();
		// 获取资产编号范围
		String assetRange = (String) asset.get("imAssetRange");
		// 判断资产编号范围，是否为一个资产，大于9就多条资产
		if (assetRange.length() > 9) {
			// 判断资产编号中是否有字母
			for (int k = 0; k < assetRange.length(); k++) {
				if (Character.isLetter(assetRange.charAt(k))) {
					isLetter = true;
					index.add(k);
				}
			}
			if (isLetter) {
				// 有字母
				return hasLetter(assetRange, asset, index);
			} else {
				// 没有字母
				return noLetter(assetRange, asset);
			}
		} else {
			// 资产编号范围内只有一条资产
			return oneAsset(assetRange, asset);
		}

	}

	/**
	 * 
	 * @Title: oneAsset
	 * @Description: 处理只有资产编号范围中只有一个资产的
	 * @return: List<AssetInfo>
	 */
	public static List<AssetInfo> oneAsset(String assetRange, Map<String, Object> asset) {
		List<AssetInfo> listNewAssets = new ArrayList<>();
		listNewAssets.add(packageObject(assetRange, asset));
		return listNewAssets;
	}

	/**
	 * 
	 * @Title: hasLetter   
	 * @Description: 资产编号中有字母的处理   
	 * @param: @param assetRange 资产编号范围
	 * @param: @param asset 资产信息 存在对应的map对象
	 * @param: @param index 存放字母在资产编号范围中位置的集合
	 * @return: List<AssetInfo>  该编号范围内的所有资产    
	 * @throws
	 */
	public static List<AssetInfo> hasLetter(String assetRange, Map<String, Object> asset, List<Integer> index) {
		// 提取资产编号范围中的字母
		String assetLetters = assetRange.replaceAll("[^(a-zA-Z)]", "");
		// 提取资产编号范围中的数字
		String assetNumber = assetRange.replaceAll("[^(0-9)]", "");
		// 末尾编号的数字字符
		String lastAssetNumber = assetNumber.substring(assetNumber.length() / 2, assetNumber.length());
		// 起始编号的数字字符
		String firAssetNumber = assetNumber.substring(0, assetNumber.length() / 2);
		// 提取资产编号中一半的字母（字母有相同的）
		String assetLetter = assetLetters.substring(0, assetLetters.length() / 2);
		// 计数器（记录其实位置为0的个数）
		int count = 0;
		// 判断数字字符起始位置是否有0
		for (int i = 0; i < assetNumber.length(); i++) {
			if (assetNumber.charAt(i) == '0') {
				count++;
				continue;
			}
			break;
		}
		int first = Integer.parseInt(firAssetNumber);
		int last = Integer.parseInt(lastAssetNumber);
		List<AssetInfo> listNewAssets = new ArrayList<>();
		for (int j = first; j <= last; j++) {
			// 补零字符串
			String zeroFill = "";
			if (count > 0) {
				zeroFill = Integer.toBinaryString(1 << count - 1).replace('1', '0');
			}
			// 拼接资产编号的字符和数字
			StringBuffer jointString = new StringBuffer(zeroFill + Integer.toString(j));
			for (int i = 0; i < assetLetter.length(); i++) {
				jointString.insert((int) index.get(i), assetLetter.charAt(i));
			}
			listNewAssets.add(packageObject(jointString.toString(), asset));
		}
		return listNewAssets;
	}

	
    /**
     * 
     * @Title: noLetter   
     * @Description: 没有字母的资产编号范围的处理   
     * @param: @param assetRange 资产编号范围
     * @param: @param asset 资产对象信息对应的map对象
     * @return: List<AssetInfo> 该编号范围内的所有资产  
     * @throws
     */
	public static List<AssetInfo> noLetter(String assetRange, Map<String, Object> asset) {
		// 其实编号
		int first = Integer.parseInt(assetRange.substring(0, (assetRange.length() - 1) / 2));
		// 末尾编号
		int last = Integer.parseInt(assetRange.substring((assetRange.length() + 1) / 2, assetRange.length()));
		// 存放所有资产对象
		List<AssetInfo> listNewAssets = new ArrayList<>();
		for (int j = first; j <= last; j++) {
			// 封装资产对象
			listNewAssets.add(packageObject(Integer.toString(j), asset));
		}
		return listNewAssets;
	}
	
	/**
	 * @Title: packageObject   
	 * @Description: 封装资产对象   
	 * @param: @param assetNum 要封装的资产编号
	 * @param: @param asset 资产对象信息对应的map对象
	 * @return: AssetInfo    资产对象  
	 * @throws
	 */
	public static AssetInfo packageObject(String assetNum,Map<String, Object> asset) {
		// 用来临时存放处理后的资产
		AssetInfo newAsset = new AssetInfo();
		// 处理后的资产
		newAsset.setAssetNum(assetNum);
		newAsset.setAssetName((String) asset.get("imAssetName"));
		newAsset.setAssetModel((String) asset.get("imAssetModel"));
		newAsset.setAssetDocumentNum((String) asset.get("imDocumentNum"));
		newAsset.setAssetPrice((float) asset.get("imAssetPrice"));
		newAsset.setAssetFactory((String) asset.get("imFactory"));
		newAsset.setAssetBuyDate((String) asset.get("imBuyDate"));
		newAsset.setAssetTakePeople((String) asset.get("imTakePeople"));
		newAsset.setAssetRemrk((String) asset.get("imRemark"));
		String dicProfitLossName;
		if(asset.get("imProfitLoss") == null){
			dicProfitLossName ="无";
		}else{
			dicProfitLossName = (String)asset.get("imProfitLoss");
		}
		newAsset.setDicProfitLossNum((Integer)profitLossType.get(dicProfitLossName));
		newAsset.setDepartNum((String)asset.get("departNum"));
		return newAsset;
	}
}
