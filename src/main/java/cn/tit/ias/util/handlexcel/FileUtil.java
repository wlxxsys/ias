package cn.tit.ias.util.handlexcel;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;
/**
 * 
 *  
 * @Description: 文件操作工具类  
 * @author: maotao 
 * @date:   2019年7月31日 下午6:03:45       
 *
 */
public class FileUtil {

	/**
	 * 
	 * @Title: convertMultipartFileToFile   
	 * @Description: 将 org.springframework.web.multipart.MultipartFile 类型的文件转换为 java.io.File 类型的文件
	 * @param: @param multipartFile 上传文件
	 *  
	 * @param: @throws IOException      
	 * @return: File   java.io.File类型文件
	 * @throws
	 */
	public static File convertMultipartFileToFile(MultipartFile multipartFile) throws IOException {
		//初始化io.file类型对象
		File convertedFile = new File(multipartFile.getOriginalFilename());
		//转换文件类型
		multipartFile.transferTo(convertedFile);
		return convertedFile;
	}
}
