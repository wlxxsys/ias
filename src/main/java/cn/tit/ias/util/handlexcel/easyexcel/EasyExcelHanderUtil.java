package cn.tit.ias.util.handlexcel.easyexcel;

import java.io.File;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
/**
 * 
 * 
 * @Description:EasyExcel操作文件
 * @author: maotao
 * @date: 2020年4月18日 上午9:04:45
 *
 */
public class EasyExcelHanderUtil {

	/**
	 * 
	 * @Title: templeteFillExport @Description: 通过xlsx模板填充导出 @param: @return:
	 * void @throws
	 */
	public static void templateFillExport(String templateFlilePath, String exportClientPath, Map<String, Object> data) {

		String fileName = exportClientPath +File.separator + System.currentTimeMillis() + ".xlsx";
		System.out.println(fileName);
		EasyExcel.write(fileName).withTemplate(templateFlilePath).sheet().doFill(data);

	}
	
	/**
	 * 
	 * @Title: ExcelTemplate   
	 * @Description: 模板填充xlsx,并 提供下载
	 * @param: @param excel_id 需要选择的excel类型编号  1：盘点报表 2：资产信息 3：异常资产信息
	 * @param: @param list 需要填充的数据源
	 * @param: @param map 需要额外填充的参数
	 * @param: @param request 
	 * @param: @param response      
	 * @return: void      
	 * @throws
	 */
	public static void ExcelTemplate(String templateFileName,Integer excel_id,List list, Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) {
		String name = "";
		switch (excel_id) {
		case 1:
			name = "盘点报表.xlsx";
			break;
		case 2:
			name = "资产信息.xlsx";
			break;
		case 3:
			name = "异常资产信息.xlsx";
			break;
		}
		
		//String templateFileName = request.getSession().getServletContext().getRealPath("/") + "\\file\\download\\emp.xlsx";
		ExcelWriter excelWriter = null;
		try {
			String s = name.replaceAll(".xlsx", "");
			String fileName = s + System.currentTimeMillis();// 导出的文件名称
			response.setContentType("application/vnd.ms-excel");
			response.setCharacterEncoding("utf-8");
			// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
			String filename = URLEncoder.encode(fileName, "utf-8");
			response.setHeader("Content-disposition", "attachment;filename=" + filename + ".xls");
			// 使用response.getOutputStream()下载,并使用项目下的模板填充
			excelWriter = EasyExcel.write(response.getOutputStream()).withTemplate(templateFileName).build();
			WriteSheet writeSheet = EasyExcel.writerSheet().build();
			if (map != null) {
				excelWriter.fill(map, writeSheet);// 存入map
			}
			System.out.println(list);
			if (list != null) {
				excelWriter.fill(list, writeSheet);// 存入list
			}
			excelWriter.finish();
		} catch (Exception e) {
			// 重置response
			response.reset();
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			try {
				throw new Exception("导出excel表格失败!", e);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public static void listFill(String templateFileName,Integer excel_id,List list, Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) {
        // 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
        // 填充list 的时候还要注意 模板中{.} 多了个点 表示list
        // 方案1 一下子全部放到内存里面 并填充
		
		String name = "";
		switch (excel_id) {
		case 1:
			name = "盘点报表.xlsx";
			break;
		case 2:
			name = "资产信息.xlsx";
			break;
		case 3:
			name = "异常资产信息.xlsx";
			break;
		}
		try {
			String s = name.replaceAll(".xlsx", "");
			String fileName = s + System.currentTimeMillis();// 导出的文件名称
			response.setContentType("application/vnd.ms-excel");
			response.setCharacterEncoding("utf-8");
			// 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
			String filename = URLEncoder.encode(fileName, "utf-8");
			response.setHeader("Content-disposition", "attachment;filename=" + filename + ".xls");
	        // 这里 会填充到第一个sheet， 然后文件流会自动关闭
	        EasyExcel.write(response.getOutputStream()).withTemplate(templateFileName).sheet().doFill(list);
		} catch (Exception e) {
		}
		
    }
	
}
