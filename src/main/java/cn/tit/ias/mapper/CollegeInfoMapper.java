package cn.tit.ias.mapper;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.entity.DepartInfo;

import java.util.List;
/**
 * 
 *  
 * @Description:CollegeInfo的dao层   
 * @author: maotao 
 * @date:   2019年6月12日 下午7:02:07       
 *
 */
public interface CollegeInfoMapper extends BaseMapper<CollegeInfo, Integer>{
	/** 按名称获取校区的对象*/
    CollegeInfo getCollegeByName(String collegeName);
    
    /** 根据条件获取需要的学校信息*/
    List<CollegeInfo>listCollegeObjByCondition(CollegeInfo condition);

    /**获取最大的学校编号*/
	int getMaxCollegeNum();
}