package cn.tit.ias.mapper;

import cn.tit.ias.entity.AssetInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @Description:AssetInfo的dao层
 * @author: maotao
 * @date: 2019年6月6日 下午9:48:20
 *
 */
public interface AssetInfoMapper extends BaseMapper<AssetInfo, String> {
	/****************** 盘点状态表mapper **************************/

	/** 查找所有盘点状态 */
	List<Map<String, Object>> listInventoryStatusObj();

	/** 根据id查找盘点状态 */
	Map<String, Object> getInventoryStatusObjByInventoryStatusId(int id);

	/** 根据名称查找盘点状态 */
	Map<String, Object> getInventoryStatusObjByInventoryStatusName(String name);

	/******************* ias_asset表mapper ************************/

	/** 根据条件查询需要的资产信息。<br/>
	 * 
	 * assetInfo可以添加的条件：assetNum，assetName，assetFactory，assetDocumentNum，assetBuyDate
	 * ，assetTakePeople，storeNum，dicProfitLossNum，departNum。<br/>
	 * 添加条件的方式：eg：assetInfo.setAssetNum="100010"
	 * */
	List<AssetInfo> listObjectByCondition(AssetInfo assetInfo);

	/** 批量插入数据到资产表 */
	int addObjectByBranch(List<AssetInfo> assets);

	/** 获取存放地的所有资产编号 */
	List<String> listAssetInfoNumsByStore(int storeNum);
    /** 根据条件获取资产的个数*/
	int getCountByConditon(AssetInfo assetInfo);
	/******************* ias_import_asset表mapper ************************/

	/** 导入资产到临时表中 */
	int importAssetToTemportaryTable(Map<String, Object> list);

	/** 导入资产表到临时表的copy中 */
	//int importAssetToTemportaryTable_copy(Map<String, Object> list);

	/** 读取临时表中数据 */
	List<Map<String, Object>> getTemportaryTable(@Param("departNum") String departNum);

	/** 清空临时表*/
	int clearImportAssetTemportaryTable(@Param("departNum") String departNum);
	/******************* inventory表mapper ************************/

	/** 删除一条盘点信息 */
	int deleteinventory(@Param("assetNum") String assetnum, @Param("Batch") String Batch);

	/** 修改一条盘点记录 */
	int updateInventory(Map<String, Object> inventory);

	/** 根据条件查询一条盘点记录的某个字段。<br/>
	 * condition是Map<String,Object>对象.<br/>
	 * condition可以添加的条件：盘点状态编号inventoryStatusId，盘点日期inventoryDate，
	 * 盘点用户账号userCount，扫描地spanAdress，原存放地storeNum，损益类型编号dicProfitLossNum 作为要查询的字段（添加方式：eg:condition.put("inventoryStatusId","inventoryStatusId");）<br/>
	 * condition必须添加的条件：盘点批次号inventoryBatch，资产编号assetNum作为查询的wehre条件；<br/>
	 * 必需要添加条件的方式：condition.put("inventoryBatch","2019000001"),condition.put("assetNum", "11001056");
	 * */
	Object getInventoryParameterByCondition(Map<String, Object> condition);

	/** 根据条件查询一条盘点记录对象<br/>
	 * condition是Map<String,Object>对象.<br/>
	 * condition必须添加的条件：盘点批次号inventoryBatch，资产编号assetNum作为查询的wehre条件；<br/>
	 * 必需要添加条件的方式：condition.put("inventoryBatch","2019000001"),condition.put("assetNum", "11001056");
	 * */
	Map<String, Object> getInventoryObjByCondition(Map<String, Object> condition);
	/** 部门编号获取该部门的最大批次号 */
	String getMaxBatch(String departNum);

	/** 获取部门的所有批次号 */
	List<String> getAllBatchByDepart(String departNum);

	/** 创建批次号批量操作 */
	void createBatchBybranch(List<Map<String, Object>> assets);

	/** 按盘点批次获取所有资产盘点信息 */
	List<Map<String, Object>> getAssetByBatch(String Batch, int currPage, int pageSize);

	/** 按盘点批次获取所有资产盘点信息  更新版本 */
	List<Map<String, Object>> getAssetByBatch_v1(String Batch);
	/** 获取按盘点批次获取所有资产盘点信息的个数 */
	int getAssetByBachCount(String Batch);
	/** 按盘点日期获取资产 */
	// List<AssetInfo> listAssetLogByDate(String date);
	
	/**获取存放地某存放地的所有资产编号*/
	List<String> listAssetNumByBatchStoreNum(String Batch,int StoreNum);
	
	/**针对部门异常-通过批次号和扫描地编号获取所有部门异常盘点信息*/
	List<String> listAssetNumByBatchSpanAdress(String Batch,int spanAdress);

	/**添加批次号开关（开启）*/
	int createSwitch(@Param("Batch") String Batch,@Param("departNum")String departNum);
	/**获取批次号开关*/
	Map<String, Object> getSwitch(@Param("Batch")String Batch);
	/** 获取批次号开关新版本
	 * @param Map<String, Object> condition 查询条件的  key:inventoryBatch,departNum
	 * */
	Map<String, Object> getSwitch_v1(Map<String, Object> condition);
	/**删除批次号开关(关闭)*/
	int closeSwitch(@Param("Batch") String Batch);
	/** 根据条件获取盘点资产个数。旧版本<br/>
	 *  condition是Map<String,Object>对象。<br/>
	 * condition 添加的条件：盘点批次号inventoryBatch 盘点状态名称inventoryStatusName<br/>
	 * eg:condition.put("inventoryBatch","2019000001");
	 * 返回值：int
	 * */
	int listInventoryAssetNumberByCondition(Map<String, Object> condition);
	/** 根据条件获取盘点资产个数。新版本<br/>
	 * 1.condition是Map<String,Object>对象。<br/>
	 * 2.condition必须添加的条件：盘点批次号inventoryBatch<br/>
	 * 3.condition可以添加的条件参数：<br>
	 * 部门编号departNum,存放地编号：storeNum,损益类型编号：dicProfitLossNum,<br>
	 * 资产编号assetNum,盘点状态编号inventoryStatusId,盘点日期inventoryDate,<br>
	 * 盘点用户账号userCount,领用人assetTakePeople,扫描地spanAdress。<br>
	 * 4.eg:condition.put("inventoryBatch","2019000001");
	 * 返回值：int
	 * */
	int listInventoryAssetNumberByCondition_v1(Map<String, Object> condition);
	/******************* inventory_batch表mapper ************************/
    /**根据部门查询盘点批次*/
	List<Map<String, Object>> listInventoryBatchInfoByDepart(String departNum);
	/**根据批次查询盘点批次信息*/
	Map<String, Object> getInventoryBatchInfoByBatch(String batch);
	/**添加一个盘点批次  盘点批次的状态：0代表结束；1代表正在运行*/
	int addInventoryBatchInfo(Map<String, Object> batchInfo);
	/**修改盘点批次信息  map参数：inventoryBatch,runStatus*/
	int updateInventoryBatchInfo(Map<String, Object> batchInfo);
	/**删除一个盘点批次*/
	int deleteInventoryBatchInfo(@Param("Batch") String Batch);
	
}