package cn.tit.ias.mapper;

import java.util.List;
import java.util.Map;

import cn.tit.ias.entity.SystemLogInfo;

/**
 * 
 * 
 * @Description:用户操作日志mapper
 * @author: maotao
 * @date: 2019年9月11日 下午10:17:43
 *
 */
public interface SystemLogInfoMapper {
	/** 添加用户操作记录 */
	int insertUserOperationRecord(SystemLogInfo record);

	/** 按编号删除用户操作记录 */
	int deleteUserOperationRecordByPrimaryKey(Integer id);

	/** 按部门删除用户操作记录 */
	int deleteUserOperationRecordByDepartNum(String departNum);


	/** 按条件获取用户操作记录 */
	List<Map<String, Object>> getUserOperationRecordByCondition(Map<String, Object>condition);

	/** 添加用户登陆记录 */
	int insertLoginRecord(Map<String, Object> loginRecord);

	/** 按部门删除用户登陆记录 */
	int deleteLoginRecordByDepartNum(String departNum);

	/** 按条件获取登陆记录 */
	List<Map<String, Object>> getLoginRecordByCondition(Map<String, Object> condition);

}