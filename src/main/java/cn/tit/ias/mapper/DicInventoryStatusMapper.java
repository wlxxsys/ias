package cn.tit.ias.mapper;

import cn.tit.ias.entity.DicInventoryStatus;
import cn.tit.ias.entity.DicInventoryStatusExample;

public interface DicInventoryStatusMapper extends BaseMapper<DicInventoryStatus, Integer>{
    
	/** according to name DicProfitLossInfo Object*/
    DicInventoryStatus getDicInventoryStatusByName(String inventoryStatusName);
}