package cn.tit.ias.mapper;

import java.util.List;

import cn.tit.ias.entity.DicProfitLossInfo;

/**
 * 
 *  
 * @Description:DicProfitLossInfo的dao层   
 * @author: maotao 
 * @date:   2019年6月6日 下午12:24:42       
 *
 */
public interface DicProfitLossInfoMapper extends BaseMapper<DicProfitLossInfo, Integer> {
   
	/** according to name DicProfitLossInfo Object*/
    
	DicProfitLossInfo getDicProfitLossByName(String dicProfitloss);
}