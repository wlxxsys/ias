package cn.tit.ias.mapper;

import cn.tit.ias.entity.DicUserRoleInfo;
import cn.tit.ias.entity.DicUserRoleInfoExample;
import cn.tit.ias.entity.UserInfo;

import java.util.List;

/**
 * 
 * 
 * @Description: DicUserRoleInfo的dao层
 * @author: maotao
 * @date: 2019年6月6日 下午12:03:24
 *
 */
public interface DicUserRoleInfoMapper extends BaseMapper<DicUserRoleInfo, Integer> {
	/** 按名称获取角色对象*/
	DicUserRoleInfo getUserByName(String suerRoleName);
	/** 获取部门中某种角色的所有管理员 */
	//List<UserInfo> listAllUserOfRoleInDepart(String departNum);

	/** 获取部门某种角色的管理员个数 */
	//int getTotalUserNum(String departNum);
}