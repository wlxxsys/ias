package cn.tit.ias.mapper;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.StoreInfoExample;
import java.util.List;

/**
 * 
 * 
 * @Description:StoreInfo的dao层
 * @author: maotao
 * @date: 2019年6月6日 下午12:04:07
 *
 */
public interface StoreInfoMapper extends BaseMapper<StoreInfo, Integer> {
	/** 通过名称获取storeInfo对象 */
	StoreInfo getStoreByName(String StoreName);

	/** 获取部门所有的存放地*/
	List<StoreInfo>listStoreByDepart(String departNum);
	
	/** 模糊查询存放地名称 */
	List<StoreInfo> getStore(String StoreName);
	
	/** 根据条件查询需要的存放地。
	 * condition对象可添加的参数：storeNum，storeName，storeDoorNum，departNum。
	 * 添加参数的方式：eg:conditon.setStoreNum=1。*/
	List<StoreInfo> listStoreObjByCondition(StoreInfo condition);
	
	/**获取存放地最大编号*/
	int getMaxStoreNum();

	
}