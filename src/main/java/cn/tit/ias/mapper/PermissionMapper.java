package cn.tit.ias.mapper;

import cn.tit.ias.entity.Permission;
import java.util.List;

public interface PermissionMapper {
	/** 获取角色的权限*/
	List<Permission> listPermissionByRoleId(int roleId);
	/** 获取所有权限*/
	List<Permission> listPermission();
}