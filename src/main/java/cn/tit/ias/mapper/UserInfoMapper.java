package cn.tit.ias.mapper;

import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.entity.UserInfoExample;
import java.util.List;

public interface UserInfoMapper extends BaseMapper<UserInfo, String>{
    /** 按用户名称获取UserInfo对象*/
	UserInfo getUserByUserName(String userName);
	
	/** 获取部门某种角色的管理员个数 */
	int getTotalUserNum(String departNum);
	/** 获取部门中所有管理员*/
	List<UserInfo> listUserByDepartNum(String departNum);
    
	
}