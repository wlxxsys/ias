package cn.tit.ias.mapper;

import java.util.List;
import java.util.Map;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.entity.UserInfo;

/**
 * 
 * 
 * @Description:departInfo的dao层
 * @author: maotao
 * @date: 2019年6月5日 下午9:27:41
 *
 */
public interface DepartInfoMapper extends BaseMapper<DepartInfo, String> {
	/** 按名称获取部门对象 */
	DepartInfo getDepartByName(String departName);
	
	/** 获取某校区所有的部门数目*/
    int getTotalDepartNum(int collegeNum);
    
    /** 根据条件获取需要的部门信息.<br/>
     * condition 添加的条件：departNum,departName,collegeNum<br/>
     * 添加条件的方式：eg:condition.setDepartNum="1"
     * */
    List<DepartInfo>listDepartObjByCondition(DepartInfo condition);
    
    /** 获取编号最大的部门编号，为了实现varchar类型的部门编号自增，*/
    String getMaxDepartNum();
}