package cn.tit.ias.entity;

import java.util.List;

public class DepartInfo {
    private String departNum;

    private String departName;

    private String departRemark;

    private Integer collegeNum;
    
    private List<UserInfo> users;
    
    private List<StoreInfo> stores;
    
    private List<AssetInfo> assets;
    
    
    public List<UserInfo> getUsers() {
		return users;
	}

	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}

	public List<StoreInfo> getStores() {
		return stores;
	}

	public void setStores(List<StoreInfo> stores) {
		this.stores = stores;
	}

	public List<AssetInfo> getAssets() {
		return assets;
	}

	public void setAssets(List<AssetInfo> assets) {
		this.assets = assets;
	}

	public String getDepartNum() {
        return departNum;
    }

    public void setDepartNum(String departNum) {
        this.departNum = departNum == null ? null : departNum.trim();
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName == null ? null : departName.trim();
    }

    public String getDepartRemark() {
        return departRemark;
    }

    public void setDepartRemark(String departRemark) {
        this.departRemark = departRemark == null ? null : departRemark.trim();
    }

    public Integer getCollegeNum() {
        return collegeNum;
    }

    public void setCollegeNum(Integer collegeNum) {
        this.collegeNum = collegeNum;
    }

	@Override
	public String toString() {
		return "DepartInfo [departNum=" + departNum + ", departName=" + departName + ", departRemark=" + departRemark
				+ ", collegeNum=" + collegeNum + ", users=" + users + ", stores=" + stores + ", assets=" + assets + "]";
	}

	
    
    
}