package cn.tit.ias.entity;

import java.util.List;

public class StoreInfo {
    private Integer storeNum;

    private String storeName;

    private String storeDoorNum;

    private String departNum;
    /** 非数据库字段  是存放地资产的集合*/
    private List<AssetInfo> assets;
    
    public List<AssetInfo> getAssets() {
		return assets;
	}

	public void setAssets(List<AssetInfo> assets) {
		this.assets = assets;
	}

	public Integer getStoreNum() {
        return storeNum;
    }

    public void setStoreNum(Integer storeNum) {
        this.storeNum = storeNum;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName == null ? null : storeName.trim();
    }

    public String getStoreDoorNum() {
        return storeDoorNum;
    }

    public void setStoreDoorNum(String storeDoorNum) {
        this.storeDoorNum = storeDoorNum == null ? null : storeDoorNum.trim();
    }

    public String getDepartNum() {
        return departNum;
    }

    public void setDepartNum(String departNum) {
        this.departNum = departNum == null ? null : departNum.trim();
    }

	@Override
	public String toString() {
		return "StoreInfo [storeNum=" + storeNum + ", storeName=" + storeName + ", storeDoorNum=" + storeDoorNum
				+ ", departNum=" + departNum + ", assets=" + assets + "]";
	}

	
    
    
}