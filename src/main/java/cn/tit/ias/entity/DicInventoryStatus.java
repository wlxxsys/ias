package cn.tit.ias.entity;

public class DicInventoryStatus {
    private Integer inventoryStatusId;

    private String inventoryStatusName;

    public Integer getInventoryStatusId() {
        return inventoryStatusId;
    }

    public void setInventoryStatusId(Integer inventoryStatusId) {
        this.inventoryStatusId = inventoryStatusId;
    }

    public String getInventoryStatusName() {
        return inventoryStatusName;
    }

    public void setInventoryStatusName(String inventoryStatusName) {
        this.inventoryStatusName = inventoryStatusName == null ? null : inventoryStatusName.trim();
    }
}