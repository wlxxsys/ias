package cn.tit.ias.entity;

import java.util.List;

public class DicUserRoleInfo {
    private Integer userRoleId;

    private String userRoleName;
    
    private List<UserInfo> users;
    
    public List<UserInfo> getUsers() {
		return users;
	}

	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}

	public Integer getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName == null ? null : userRoleName.trim();
    }

	@Override
	public String toString() {
		return "DicUserRoleInfo [userRoleId=" + userRoleId + ", userRoleName=" + userRoleName + ", users=" + users
				+ "]";
	}

    
    
}