package cn.tit.ias.entity;

import java.util.List;

public class CollegeInfo {
    private Integer collegeNum;

    private String collegeName;
    
    private List<DepartInfo> departInfos;
    
    public List<DepartInfo> getDepartInfos() {
		return departInfos;
	}


	public Integer getCollegeNum() {
        return collegeNum;
    }

    public void setCollegeNum(Integer collegeNum) {
        this.collegeNum = collegeNum;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName == null ? null : collegeName.trim();
    }

	@Override
	public String toString() {
		return "CollegeInfo [collegeNum=" + collegeNum + ", collegeName=" + collegeName + "]";
	}
    
}