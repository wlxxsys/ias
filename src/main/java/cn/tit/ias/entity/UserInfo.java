package cn.tit.ias.entity;

public class UserInfo {
    private String userCount;

    private String userPassward;

    private String userName;

    private Integer userRoleId;

    private String departNum;
    
    private DicUserRoleInfo userRoleInfo;
    
    public DicUserRoleInfo getUserRoleInfo() {
		return userRoleInfo;
	}

	public void setUserRoleInfo(DicUserRoleInfo userRoleInfo) {
		this.userRoleInfo = userRoleInfo;
	}

	public String getUserCount() {
        return userCount;
    }

    public void setUserCount(String userCount) {
        this.userCount = userCount == null ? null : userCount.trim();
    }

    public String getUserPassward() {
        return userPassward;
    }

    public void setUserPassward(String userPassward) {
        this.userPassward = userPassward == null ? null : userPassward.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getDepartNum() {
        return departNum;
    }

    public void setDepartNum(String departNum) {
        this.departNum = departNum == null ? null : departNum.trim();
    }

	@Override
	public String toString() {
		return "UserInfo [userCount=" + userCount + ", userPassward=" + userPassward + ", userName=" + userName
				+ ", userRoleId=" + userRoleId + ", departNum=" + departNum + ", userRoleInfo=" + userRoleInfo + "]";
	}

	
	
    
    
}