package cn.tit.ias.entity;

public class InventoryEasyExcelModelInfo {

	private Integer id;
	private String inventoryBatch;
	private String assetNum;
	private String inventoryStatus;
	private String inventoryStatusName;
	private String inventoryDate;
	private String userCount;
	private String departName;
	private String assetTakePeople;
	private String spanAdress;
	private String storeName;
	private String dicProfitLossName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInventoryBatch() {
		return inventoryBatch;
	}
	public void setInventoryBatch(String inventoryBatch) {
		this.inventoryBatch = inventoryBatch;
	}
	public String getAssetNum() {
		return assetNum;
	}
	public void setAssetNum(String assetNum) {
		this.assetNum = assetNum;
	}
	public String getInventoryStatus() {
		return inventoryStatus;
	}
	public void setInventoryStatus(String inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}
	public String getInventoryStatusName() {
		return inventoryStatusName;
	}
	public void setInventoryStatusName(String inventoryStatusName) {
		this.inventoryStatusName = inventoryStatusName;
	}
	public String getInventoryDate() {
		return inventoryDate;
	}
	public void setInventoryDate(String inventoryDate) {
		this.inventoryDate = inventoryDate;
	}
	public String getUserCount() {
		return userCount;
	}
	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}
	public String getDepartName() {
		return departName;
	}
	public void setDepartName(String departName) {
		this.departName = departName;
	}
	public String getAssetTakePeople() {
		return assetTakePeople;
	}
	public void setAssetTakePeople(String assetTakePeople) {
		this.assetTakePeople = assetTakePeople;
	}
	public String getSpanAdress() {
		return spanAdress;
	}
	public void setSpanAdress(String spanAdress) {
		this.spanAdress = spanAdress;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getDicProfitLossName() {
		return dicProfitLossName;
	}
	public void setDicProfitLossName(String dicProfitLossName) {
		this.dicProfitLossName = dicProfitLossName;
	}
	@Override
	public String toString() {
		return "EasyExcelModelInfo [id=" + id + ", inventoryBatch=" + inventoryBatch + ", assetNum=" + assetNum
				+ ", inventoryStatus=" + inventoryStatus + ", inventoryStatusName=" + inventoryStatusName
				+ ", inventoryDate=" + inventoryDate + ", userCount=" + userCount + ", departName=" + departName
				+ ", assetTakePeople=" + assetTakePeople + ", spanAdress=" + spanAdress + ", storeName=" + storeName
				+ ", dicProfitLossName=" + dicProfitLossName + "]";
	}
	
	
	
}
