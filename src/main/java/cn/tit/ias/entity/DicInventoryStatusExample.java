package cn.tit.ias.entity;

import java.util.ArrayList;
import java.util.List;

public class DicInventoryStatusExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DicInventoryStatusExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andInventoryStatusIdIsNull() {
            addCriterion("inventoryStatusId is null");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdIsNotNull() {
            addCriterion("inventoryStatusId is not null");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdEqualTo(Integer value) {
            addCriterion("inventoryStatusId =", value, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdNotEqualTo(Integer value) {
            addCriterion("inventoryStatusId <>", value, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdGreaterThan(Integer value) {
            addCriterion("inventoryStatusId >", value, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inventoryStatusId >=", value, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdLessThan(Integer value) {
            addCriterion("inventoryStatusId <", value, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("inventoryStatusId <=", value, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdIn(List<Integer> values) {
            addCriterion("inventoryStatusId in", values, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdNotIn(List<Integer> values) {
            addCriterion("inventoryStatusId not in", values, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("inventoryStatusId between", value1, value2, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inventoryStatusId not between", value1, value2, "inventoryStatusId");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameIsNull() {
            addCriterion("inventoryStatusName is null");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameIsNotNull() {
            addCriterion("inventoryStatusName is not null");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameEqualTo(String value) {
            addCriterion("inventoryStatusName =", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameNotEqualTo(String value) {
            addCriterion("inventoryStatusName <>", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameGreaterThan(String value) {
            addCriterion("inventoryStatusName >", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameGreaterThanOrEqualTo(String value) {
            addCriterion("inventoryStatusName >=", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameLessThan(String value) {
            addCriterion("inventoryStatusName <", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameLessThanOrEqualTo(String value) {
            addCriterion("inventoryStatusName <=", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameLike(String value) {
            addCriterion("inventoryStatusName like", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameNotLike(String value) {
            addCriterion("inventoryStatusName not like", value, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameIn(List<String> values) {
            addCriterion("inventoryStatusName in", values, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameNotIn(List<String> values) {
            addCriterion("inventoryStatusName not in", values, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameBetween(String value1, String value2) {
            addCriterion("inventoryStatusName between", value1, value2, "inventoryStatusName");
            return (Criteria) this;
        }

        public Criteria andInventoryStatusNameNotBetween(String value1, String value2) {
            addCriterion("inventoryStatusName not between", value1, value2, "inventoryStatusName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}