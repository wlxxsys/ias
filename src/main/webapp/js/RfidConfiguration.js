	BlockID = "1"; 
	Key ="FFFFFFFFFFFF";

	// rfid设备回调函数
	rfidreader.onResult(function(resultdata){
				switch(resultdata.FunctionID){
					case 14:
			        document.getElementById("CloudReaderVer").value = resultdata.strData;
			        break;
					case 5:
					// 获取卡号
					document.getElementById("CardNo").value = resultdata.CardNo;
					// 转换数据格式为16进制并显示读取数据
					if(resultdata.Result>0){
			            var data = {
								datastr : resultdata.strData
						}
						$.ajax({
							url : "PCHanderTag/handleReadData",
							type : "post",
							dataType : "json",
							data : data,
							async : false,
					    	success : function(response){
					    		//alert("读取的转换数据"+response.data);
					    		document.getElementById("DataRead").value=response.data;	
					    	},
					    	error : function() {
					    		alert("写入数据失败");
					    	}
						})
					}else{   // 异常信息提示
						document.getElementById("DataRead").value="";
						//alert(GetErrStr(resultdata.Result));
						$('#not_connect_nfc').modal('show');
						$('#dateId').val(GetErrStr(resultdata.Result));
					}
					break;
					case 6:
					document.getElementById("CardNo").value = resultdata.CardNo;
					if(resultdata.Result>0){
						alert("写入成功");
					}else{
						$('#not_connect_nfc').modal('show');
						$('#dateId').val("写入失败，错误：" +GetErrStr(resultdata.Result));
					}
					break;
			    }
	});
	         
	// 读数据
	function ReadBlock(){
		rfidreader.KeyMode=0;
		rfidreader.KeyStringMode=0;
		rfidreader.KeyString=Key;
		rfidreader.Repeat=0
		rfidreader.M1ReadBlock(BlockID, 0);
	}
			
	// 写数据
	function WriteBlock(){
		var Data;
		var data = {
				datastr : document.getElementById("DataWrite").value
		}
	    $.ajax({
	    	url : "PCHanderTag/handleWriteData",
	    	type : "post",
	    	dataType : "json",
	    	data : data,
	    	async : false,
	    	success : function(response){
	    		Data=response.data;	
	    		document.getElementById("DataRead").value = response.writeDataStr;
	    	},
	    	error : function() {
	    		alert("写入数据失败");
	    	}
	    	
	    });
		rfidreader.KeyMode=0;
		rfidreader.KeyStringMode=0;
		rfidreader.KeyString=Key;
		rfidreader.Repeat=0;
		rfidreader.M1WriteBlock(BlockID, Data,0)						
	}
			