/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50026
Source Host           : 127.0.0.1:3306
Source Database       : ias

Target Server Type    : MYSQL
Target Server Version : 50026
File Encoding         : 65001

Date: 2020-06-03 11:08:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ias_asset
-- ----------------------------
DROP TABLE IF EXISTS `ias_asset`;
CREATE TABLE `ias_asset` (
  `assetNum` varchar(10) NOT NULL,
  `assetName` varchar(255) default NULL,
  `assetModel` varchar(255) default NULL,
  `assetPrice` float default NULL,
  `assetFactory` varchar(20) default NULL,
  `assetDocumentNum` varchar(20) default NULL,
  `assetBuyDate` varchar(20) default NULL,
  `assetTakePeople` varchar(10) default NULL,
  `assetRemrk` varchar(255) default NULL,
  `storeNum` int(11) default NULL,
  `dicProfitLossNum` int(11) default NULL,
  `departNum` varchar(8) NOT NULL,
  PRIMARY KEY  (`assetNum`),
  KEY `FK_belong` (`dicProfitLossNum`),
  KEY `FK_depart_pro` (`departNum`),
  KEY `FK_save` (`storeNum`),
  CONSTRAINT `FK_belong` FOREIGN KEY (`dicProfitLossNum`) REFERENCES `ias_dic_profit_loss` (`dicProfitLossNum`),
  CONSTRAINT `FK_depart_pro` FOREIGN KEY (`departNum`) REFERENCES `ias_depart` (`departNum`),
  CONSTRAINT `FK_save` FOREIGN KEY (`storeNum`) REFERENCES `ias_store` (`storeNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_college
-- ----------------------------
DROP TABLE IF EXISTS `ias_college`;
CREATE TABLE `ias_college` (
  `collegeNum` int(11) NOT NULL auto_increment,
  `collegeName` varchar(10) default NULL,
  PRIMARY KEY  (`collegeNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_depart
-- ----------------------------
DROP TABLE IF EXISTS `ias_depart`;
CREATE TABLE `ias_depart` (
  `departNum` varchar(8) NOT NULL,
  `departName` varchar(10) default NULL,
  `departRemark` varchar(50) default NULL,
  `collegeNum` int(11) NOT NULL,
  PRIMARY KEY  (`departNum`),
  KEY `FK_have` (`collegeNum`),
  CONSTRAINT `FK_have` FOREIGN KEY (`collegeNum`) REFERENCES `ias_college` (`collegeNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_dic_profit_loss
-- ----------------------------
DROP TABLE IF EXISTS `ias_dic_profit_loss`;
CREATE TABLE `ias_dic_profit_loss` (
  `dicProfitLossNum` int(11) NOT NULL,
  `dicProfitLossName` varchar(10) default NULL,
  PRIMARY KEY  (`dicProfitLossNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_import_asset
-- ----------------------------
DROP TABLE IF EXISTS `ias_import_asset`;
CREATE TABLE `ias_import_asset` (
  `imAssetRange` varchar(50) NOT NULL,
  `departNum` varchar(8) NOT NULL,
  `imUnitName` varchar(255) default NULL,
  `imDocumentNum` varchar(50) default NULL,
  `imAssetName` varchar(255) default NULL,
  `imAssetModel` varchar(255) default NULL,
  `imAssetPrice` float default NULL,
  `imFactory` varchar(50) default NULL,
  `imBuyDate` varchar(20) default NULL,
  `imTakePeople` varchar(10) default NULL,
  `imRemark` varchar(255) default NULL,
  `imProfitLoss` varchar(10) default NULL,
  PRIMARY KEY  (`imAssetRange`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_import_asset_copy
-- ----------------------------
DROP TABLE IF EXISTS `ias_import_asset_copy`;
CREATE TABLE `ias_import_asset_copy` (
  `imId` int(11) NOT NULL auto_increment,
  `imUnitName` varchar(255) default NULL,
  `imAssetRange` varchar(50) default NULL,
  `imDocumentNum` varchar(50) default NULL,
  `imAssetName` varchar(255) default NULL,
  `imAssetModel` varchar(255) default NULL,
  `imPurchaseNum` int(11) default NULL,
  `imAssetPrice` float default NULL,
  `imAssetTotalPrice` float default NULL,
  `imFactory` varchar(50) default NULL,
  `imBuyDate` varchar(20) default NULL,
  `imTakePeople` varchar(10) default NULL,
  `imRemark` varchar(255) default NULL,
  `imAbsoluteQuantity` int(11) default NULL,
  `imAssetStore` varchar(255) default NULL,
  `imProfitLoss` varchar(10) default NULL,
  PRIMARY KEY  (`imId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_inventory_batch
-- ----------------------------
DROP TABLE IF EXISTS `ias_inventory_batch`;
CREATE TABLE `ias_inventory_batch` (
  `departNum` varchar(8) NOT NULL,
  `inventoryBatch` varchar(10) NOT NULL,
  `runStatus` int(10) NOT NULL,
  `createUser` varchar(10) NOT NULL,
  `createTime` date NOT NULL,
  PRIMARY KEY  (`inventoryBatch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_inventory_record
-- ----------------------------
DROP TABLE IF EXISTS `ias_inventory_record`;
CREATE TABLE `ias_inventory_record` (
  `inventoryBatch` varchar(10) NOT NULL,
  `assetNum` varchar(11) NOT NULL,
  `inventoryStatusId` int(4) NOT NULL,
  `inventoryDate` date default NULL,
  `userCount` varchar(10) default NULL,
  `departNum` varchar(8) default NULL,
  `assetTakePeople` varchar(10) default NULL,
  `spanAdress` int(11) default NULL,
  `storeNum` int(11) default NULL,
  `dicProfitLossNum` int(11) default NULL,
  KEY `FK_inventory` (`assetNum`),
  KEY `FK_inventory2` (`userCount`),
  KEY `FK_inventory3` (`departNum`),
  CONSTRAINT `FK_inventory3` FOREIGN KEY (`departNum`) REFERENCES `ias_depart` (`departNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_inventory_status
-- ----------------------------
DROP TABLE IF EXISTS `ias_inventory_status`;
CREATE TABLE `ias_inventory_status` (
  `inventoryStatusId` int(11) NOT NULL,
  `inventoryStatusName` varchar(10) default NULL,
  PRIMARY KEY  (`inventoryStatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_inventory_switch
-- ----------------------------
DROP TABLE IF EXISTS `ias_inventory_switch`;
CREATE TABLE `ias_inventory_switch` (
  `inventoryBatch` varchar(10) NOT NULL,
  `departNum` varchar(8) NOT NULL,
  PRIMARY KEY  (`inventoryBatch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_store
-- ----------------------------
DROP TABLE IF EXISTS `ias_store`;
CREATE TABLE `ias_store` (
  `storeNum` int(11) NOT NULL,
  `storeName` varchar(10) default NULL,
  `storeDoorNum` varchar(10) default NULL,
  `departNum` varchar(8) default NULL,
  PRIMARY KEY  (`storeNum`),
  KEY `FK_exist` (`departNum`),
  CONSTRAINT `FK_exist` FOREIGN KEY (`departNum`) REFERENCES `ias_depart` (`departNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_user
-- ----------------------------
DROP TABLE IF EXISTS `ias_user`;
CREATE TABLE `ias_user` (
  `userCount` varchar(10) NOT NULL,
  `userPassward` varchar(40) NOT NULL,
  `userName` varchar(10) default NULL,
  `userRoleId` int(11) NOT NULL,
  `departNum` varchar(8) default NULL,
  PRIMARY KEY  (`userCount`),
  KEY `FK_apply` (`departNum`),
  KEY `FK_include` (`userRoleId`),
  CONSTRAINT `FK_apply` FOREIGN KEY (`departNum`) REFERENCES `ias_depart` (`departNum`),
  CONSTRAINT `FK_include` FOREIGN KEY (`userRoleId`) REFERENCES `ias_user_role` (`userRoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_user_access_log
-- ----------------------------
DROP TABLE IF EXISTS `ias_user_access_log`;
CREATE TABLE `ias_user_access_log` (
  `loginRecordId` int(11) NOT NULL auto_increment,
  `userCount` varchar(10) default NULL,
  `userName` varchar(10) default NULL,
  `accessType` varchar(10) default NULL,
  `accessTime` timestamp NULL default NULL on update CURRENT_TIMESTAMP,
  `accessIp` varchar(50) default NULL,
  `departNum` varchar(10) default NULL,
  PRIMARY KEY  (`loginRecordId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `ias_user_permission`;
CREATE TABLE `ias_user_permission` (
  `perId` int(11) NOT NULL auto_increment,
  `url` varchar(255) NOT NULL,
  `perName` varchar(11) default NULL,
  `description` varchar(50) default NULL,
  PRIMARY KEY  (`perId`),
  KEY `perId` (`perId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ias_user_role`;
CREATE TABLE `ias_user_role` (
  `userRoleId` int(11) NOT NULL,
  `userRoleName` varchar(10) default NULL,
  PRIMARY KEY  (`userRoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_user_role_permisson
-- ----------------------------
DROP TABLE IF EXISTS `ias_user_role_permisson`;
CREATE TABLE `ias_user_role_permisson` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY  (`role_id`,`permission_id`),
  KEY `permission_id` USING BTREE (`permission_id`),
  CONSTRAINT `ias_user_role_permisson_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `ias_user_role` (`userRoleId`),
  CONSTRAINT `ias_user_role_permisson_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `ias_user_permission` (`perId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ias_user_system_log
-- ----------------------------
DROP TABLE IF EXISTS `ias_user_system_log`;
CREATE TABLE `ias_user_system_log` (
  `id` int(11) NOT NULL auto_increment,
  `userCount` varchar(10) default NULL,
  `createTime` timestamp NULL default NULL on update CURRENT_TIMESTAMP,
  `operation` varchar(50) default NULL,
  `content` varchar(50) default NULL,
  `ip` varchar(255) default NULL,
  `userName` varchar(10) default NULL,
  `departNum` varchar(10) default NULL,
  `userRole` varchar(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Procedure structure for pro_asset_addObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_addObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_addObject`(in assetnum varchar(8),in assetname varchar(10),in assetmodel varchar(50),in assetprice REAL,in assetfactory varchar(20),in assetdocumentnum varchar(20),in asset_BuyDate varchar(20),in assettakepeople varchar(10),in assetremrk varchar(50),in storenum int,in dicprofitlossnum int,in departnum varchar(8))
BEGIN
insert into ias_asset
(assetNum,assetName,assetModel,assetPrice,assetFactory,assetDocumentNum,assetBuyDate,assetTakePeople,assetRemrk,storeNum,dicProfitLossNum,departNum)
values(assetnum,assetname,assetmodel,assetprice,assetfactory,assetdocumentnum,asset_BuyDate,assettakepeople,assetremrk,storenum,dicprofitlossnum,departnum);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_createInventoreyBach
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_createInventoreyBach`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_createInventoreyBach`(IN Batch VARCHAR(10),in asset_num VARCHAR(8),in depart_num VARCHAR(8))
BEGIN
INSERT INTO inventory
(inventoryBatch,assetNum,inventory.inventoryStatus,inventory.inventoryDate,departNum)
VALUES(Batch,asset_num,"0","0001-01-01",depart_num);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_deleteinventory
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_deleteinventory`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_deleteinventory`(in asset_num varchar(8),in batch VARCHAR(10))
BEGIN
DELETE
FROM inventory
WHERE inventory.assetNum=asset_num AND inventory.inventoryBatch=batch;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_deleteObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_deleteObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_deleteObject`(in num varchar(8))
BEGIN
DELETE FROM ias_asset
WHERE assetNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getAllBatchByDepart
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getAllBatchByDepart`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getAllBatchByDepart`(in depart_num VARCHAR(8))
BEGIN
SELECT inventoryBatch 
FROM inventory  
WHERE inventory.departNum=depart_num
GROUP BY inventoryBatch;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getAssetByBatch
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getAssetByBatch`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getAssetByBatch`(in batch VARCHAR(10))
BEGIN
SELECT *
FROM inventory
WHERE inventory.inventoryBatch=batch;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getinventoryDate
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getinventoryDate`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getinventoryDate`(in asset_num varchar(8),in batch VARCHAR(10))
BEGIN
SELECT inventory.inventoryDate
FROM inventory
WHERE inventory.assetNum=asset_num AND inventoryBatch=batch;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getinventoryStatus
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getinventoryStatus`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getinventoryStatus`(in asset_num varchar(8),in batch VARCHAR(10))
BEGIN
SELECT inventory.inventoryStatus
FROM inventory
WHERE inventory.assetNum=asset_num AND inventory.inventoryBatch=batch;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getinventoryuserCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getinventoryuserCount`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getinventoryuserCount`(in asset_num varchar(8))
BEGIN
SELECT inventory.userCount
FROM inventory
WHERE inventory.assetNum=(SELECT ias_asset.assetNum
													FROM ias_asset
													WHERE ias_asset.assetNum=asset_num
													);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getMaxBatch
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getMaxBatch`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getMaxBatch`(IN depart_num varchar(8))
BEGIN
SELECT MAX(inventory.inventoryBatch)
FROM inventory
WHERE inventory.departNum=depart_num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_getObject`(in num VARCHAR(8))
BEGIN
SELECT * FROM ias_asset
WHERE assetNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_listAssetInfoNumsByStore
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_listAssetInfoNumsByStore`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_listAssetInfoNumsByStore`(in num int)
select assetNum
FROM ias_asset
WHERE storeNum=num
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_listObject`()
BEGIN
SELECT * FROM ias_asset;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_listObject1
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_listObject1`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_listObject1`(in depart_num varchar(8))
BEGIN
SELECT * from ias_asset
where departNum=depart_num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_updateinventory
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_updateinventory`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_updateinventory`(IN asset_num VARCHAR(8),IN inventory_Batch varchar(10),IN inventory_status TINYINT)
BEGIN
UPDATE inventory
SET inventory.inventoryStatus=inventory_status
WHERE inventory.assetNum=asset_num AND inventory.inventoryBatch=inventory_Batch;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_asset_updateObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_asset_updateObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_asset_updateObject`(in num varchar(8),in asset_name varchar(10),in asset_model varchar(50),in asset_price FLOAT,in asset_factory varchar(20),in asset_documentnum varchar(20),in asset_buydate VARCHAR(20),in asset_takepeople varchar(10),in asset_remrk varchar(50),in asset_storenum int(11),in dicprofitloss_num int(11),in depart_num varchar(8))
begin
update ias_asset
set assetName=asset_name,assetModel=asset_model,assetPrice=asset_price,assetFactory=asset_factory,assetDocumentNum=asset_documentnum,assetBuyDate=asset_buydate,assetTakePeople=asset_takepeople,assetRemrk=asset_remrk,storeNum=asset_storenum,dicProfitLossNum=dicprofitloss_num,departNum=depart_num
where assetNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_college_addObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_college_addObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_college_addObject`(in name varchar(10))
begin
insert into ias_college
(collegeName)
values(name);
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_college_deleteObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_college_deleteObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_college_deleteObject`(in num int)
begin
delete from ias_college
where collegeNum=num;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_college_getCollegeByName
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_college_getCollegeByName`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_college_getCollegeByName`(in name varchar(10))
begin
select * from ias_college
where collegeName=name;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_college_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_college_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_college_getObject`(in num int)
begin
select * from ias_college
where collegeNum=num;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_college_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_college_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_college_listObject`()
begin
select * from ias_college;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_college_updateObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_college_updateObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_college_updateObject`(in num int,in name varchar(10))
begin
update ias_college
set collegeName=name
where collegeNum=num;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_depart_addObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_depart_addObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_depart_addObject`(in departnum VARCHAR(8),in NAME VARCHAR(10),in remark VARCHAR(50),in collegenum INTEGER(11))
BEGIN
insert into ias_depart
(departNum,departName,departRemark,collegeNum)
values(departnum,NAME,remark,collegenum);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_depart_deleteObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_depart_deleteObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_depart_deleteObject`(in num VARCHAR(8))
BEGIN
delete from ias_depart
where departNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_depart_getDepartByName
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_depart_getDepartByName`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_depart_getDepartByName`(in name varchar(10))
BEGIN
select * from ias_depart
where departName=name;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_depart_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_depart_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_depart_getObject`(in num VARCHAR(8))
BEGIN
select * from ias_depart
where departNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_depart_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_depart_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_depart_listObject`()
BEGIN
select * from ias_depart;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_depart_updateObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_depart_updateObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_depart_updateObject`(in num VARCHAR(8),in name VARCHAR(10),in remark VARCHAR(50))
begin
update ias_depart
set departName=name,departRemark=remark
where departNum=num;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_dic_profit_loss_addObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_dic_profit_loss_addObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_dic_profit_loss_addObject`(in num INT(11),in name VARCHAR(10))
BEGIN
insert into ias_dic_profit_loss
(dicProfitLossNum,dicProfitLossName)
values(num,name);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_dic_profit_loss_deleteObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_dic_profit_loss_deleteObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_dic_profit_loss_deleteObject`(in num INTEGER(11))
BEGIN
delete from ias_dic_profit_loss
where dicProfitLossNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_dic_profit_loss_getDicProfitLossByName
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_dic_profit_loss_getDicProfitLossByName`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_dic_profit_loss_getDicProfitLossByName`(in name VARCHAR(10))
BEGIN
select * from ias_dic_profit_loss
where dicProfitLossName=name;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_dic_profit_loss_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_dic_profit_loss_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_dic_profit_loss_getObject`(in num INTEGER(11))
BEGIN
select * from ias_dic_profit_loss
where dicProfitLossNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_dic_profit_loss_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_dic_profit_loss_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_dic_profit_loss_listObject`()
BEGIN
select * from ias_dic_profit_loss;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_dic_profit_loss_updateObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_dic_profit_loss_updateObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_dic_profit_loss_updateObject`(in num int(11),in name varchar(10))
BEGIN
update ias_dic_profit_loss
set dicProfitLossName=name
where dicProfitLossNum=num;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_import_asset_getTemportaryTable
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_import_asset_getTemportaryTable`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_import_asset_getTemportaryTable`(in num VARCHAR(8))
select departNum,imAssetRange,imAssetRange,imDocumentNum,imAssetName,imAssetModel,imAssetPrice,imFactory,imBuyDate,imTakePeople,imRemark,imProfitLoss
from ias_import_asset
where departNum=num
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_addObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_addObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_addObject`(in storenum INT(11),in name VARCHAR(10),in doornum varchar(10),in departnum varchar(8))
BEGIN
INSERT into ias_store
(storeNum,storeName,storeDoorNum,departNum)
VALUES(storenum,name,doornum,departnum);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_deleteObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_deleteObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_deleteObject`(in num int)
BEGIN
DELETE FROM ias_store
where storeNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_getObject`(in num int)
BEGIN
select * from ias_store
where storeNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_getStoreByName
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_getStoreByName`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_getStoreByName`(in name VARCHAR(10))
BEGIN
SELECT * from ias_store
WHERE storeName=name;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_listObject`()
BEGIN
SELECT * FROM ias_store;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_listStoresByDepart
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_listStoresByDepart`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_listStoresByDepart`(in num VARCHAR(8))
select * 
FROM ias_store
where departNum=num
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_store_updateObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_store_updateObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_store_updateObject`(in num int,in name varchar(10),in doornum varchar(10),in departnum varchar(8))
BEGIN
UPDATE ias_store
set storeName=name,storeDoorNum=doornum,departNum=departnum
WHERE storeNum=num;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_userRole_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_userRole_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_userRole_getObject`(in id INT)
SELECT *
FROM ias_user_role
WHERE userRoleId=id
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_userRole_getUserByName
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_userRole_getUserByName`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_userRole_getUserByName`(IN rolename VARCHAR(10))
SELECT * FROM ias_user_role
WHERE userRoleName=rolename
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_userRole_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_userRole_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_userRole_listObject`()
begin
select * from ias_user_role;
end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_user_addObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_user_addObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_user_addObject`(in ucount VARCHAR(10),in upassward VARCHAR(10),in uname VARCHAR(10),in uroleId INT ,in dnum VARCHAR(10))
INSERT into ias_user
VALUES(ucount,upassward,uname,uroleId,dnum)
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_user_deleteObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_user_deleteObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_user_deleteObject`(in ucount VARCHAR(10))
delete FROM ias_user
WHERE userCount=ucount
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_user_getObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_user_getObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_user_getObject`(in count VARCHAR(10))
SELECT * from ias_user
where userCount=count
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_user_getUserByUserName
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_user_getUserByUserName`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_user_getUserByUserName`(in name VARCHAR(10))
SELECT * from ias_user
where userName=name
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_user_listObject
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_user_listObject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_user_listObject`()
select * from ias_user
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for pro_user_listObject1
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_user_listObject1`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_user_listObject1`(in departunm VARCHAR(10))
select * from ias_user
where departNum=departnum
;;
DELIMITER ;
