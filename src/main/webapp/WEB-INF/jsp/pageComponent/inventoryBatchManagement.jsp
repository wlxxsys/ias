<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Hello, Bootstrap Table!</title>
<div class="panel panel-default">
	<ol class="breadcrumb">
		<li>盘点批次管理</li>
	</ol>
	<!-- 内容面板 -->
	<div class="panel-body">
		<!-- 头部按钮和下拉框组件 -->
		<div class="row">
			<div class="col-md-2 col-sm-2">
				<shiro:hasRole name="管理员">
					<button id="create_inventory_btn" class="btn btn-success">
						<span>开始新的盘点批次</span>
					</button>
				</shiro:hasRole>
			</div>
			<shiro:hasRole name="超级管理员">
			<div class="col-md-3 col-sm-3">
				<select class="selectpicker" title="学校" id="college"
					onchange="initDepart()">
				</select>
			</div>
			<div class="col-md-3 col-sm-3">
				<select class="selectpicker" title="部门" id="depart">
				</select>
			</div>
			
			<div class="col-md-2 col-sm-2">
				<button id="search_btn" class="btn btn-success">
					<span>查询</span>
				</button>
			</div>
			</shiro:hasRole>

		</div>
		<!-- 查询表格组件 -->
		<div class="row" style="margin-top: 15px">
			<table id="table"></table>
		</div>

	</div>
</div>

<!-- 关闭开关提示模态框 -->
<div class="modal fade" id="closeWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认结束此次盘点</h3>
						<p>(注意：一旦结束此次盘点，将不能恢复)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="close_confirm">
					<span>确认结束</span>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 删除提示模态框 -->
<div class="modal fade" id="deleteWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认删除该条库存信息</h3>
						<p>(注意：一旦删除该条库存信息，将不能恢复)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger delete_confirm_class" type="button"
					id="delete_confirm">
					<span>确认删除</span>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 关闭开关提示模态框 -->
<div class="modal fade" id="closeWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认结束此次盘点</h3>
						<p>(注意：一旦结束此次盘点，将不能恢复)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="close_confirm">
					<span>确认结束</span>
				</button>
			</div>
		</div>
	</div>
</div>

</div>
<!-- 下拉框相关js -->
<script type="text/javascript">
	/* 动态生成college的select选项 */
	function initCollege() {
		var selectObj = $("#college");
		$.ajax({
			url : "PCCollegeHandler/listCollege_v1",
			type : "GET",
			success : function(response) {
				if (response.result == "success") {
					var configs = response.data;
					for ( var i in configs) {
						var config = configs[i];
						var optionValue = config.collegeNum;
						var optionText = config.collegeName;
						selectObj.append(new Option(optionText, optionValue));
					}
					// 刷新select
					selectObj.selectpicker('refresh');
				} else {
					console.log(response.msg);
				}
			},
			error : function() {
				console.log("服务器请求失败");
			}

		})
	}

	/* 根据选择学校动态生成部门下拉框 */
	function initDepart() {
		// 清空部门下拉框
		$("#depart").find("option:not(:first)").remove();
		$("#depart").selectpicker('refresh');
		// 重新获取部门下拉框数据 --- 根据选择的学校
		var collegeSelect = $("#college").val();
		var selectObj = $("#depart");
		var data = {
			collegeNum : collegeSelect
		}
		$.ajax({
			url : "PCDepartInfoHandler/listDepart_v1",
			type : "GET",
			dataType : "json",
			data : data,
			success : function(response) {
				if (response.result == "success") {
					var configs = response.data;
					for ( var i in configs) {
						var config = configs[i];
						var optionValue = config.departNum;
						var optionText = config.departName;
						selectObj.append(new Option(optionText, optionValue));
					}
					// 刷新select
					selectObj.selectpicker('refresh');
				} else {
					console.log(response.msg);
				}
			},
			error : function() {
				console.log("服务器请求失败");
			}

		})
	}

	function searchButton(){
		$("#search_btn").click(function(){
			tableRefresh();
		});
	}
	$(function() {
		/* 初始化动态加载学校 */
		initCollege();
		searchButton();
		// 初始化 下拉框组件
		$('select').selectpicker();

	})
</script>
<!-- 表格相关js -->
<script>
	var $table = $('#table')
	var $remove = $('#remove')
	var selections = []
	// 表格刷新
	function tableRefresh() {
		$('#table').bootstrapTable('refresh', {
			query : {}
		});
	}

	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			departName : $("#depart option:selected").text()
		}
		console.log(temp.departName);
		return temp;
	}

	// 初始化table
	function initTable() {
		$table.bootstrapTable({
			url : "pcInventAsset/listBatchByDepart",
			dataType : "json",
			method : "get",
			queryParams : queryParams,
			pagination : true,
			sidePagination : 'server',
			pageNumber : 1,//初始化加载第一页
			pageSize : 5,//每页的记录行数
			pageList : [ 5, 10, 25, 50, 100 ],//可供选择的每页的行数
			locale : "zh-CN",
			//refresh : "true",
			columns : [ {
				title : '序号',
				field : 'id',
				align : 'center',
				formatter : function(value, row, index) {
					return index + 1;
				}
			}, {
				field : 'inventoryBatch',
				title : '盘点批次号',
				//sortable : true,
				align : 'center',
			}, {
				field : 'runStatus',
				title : '运行状态',
				align : 'center',
				events : window.operateEventRunStatus,
				formatter : operateFormatterRunStatus
			}, {
				field : 'createUser',
				title : '创建者',
				align : 'center',
			}, {
				field : 'createTime',
				title : '创建时间',
				align : 'center',
			}, {
				field : 'operate',
				title : '操作',
				align : 'center',
				clickToSelect : false,
				events : window.operateEvents,
				formatter : operateFormatter
			} ]
		})
	}

	// 创建新的盘点批次按钮点击事件
	$('#create_inventory_btn').click(function() {
		$.ajax({
			type : "GET",
			url : 'pcInventAsset/OpenNewInventory',
			success : function(response) {
				var type;
				var msg;
				var append = '';
				if (response.result == "success") {
					type = "success";
					msg = response.msg;
				} else if (response.result == "error") {
					type = "error";
					msg = response.msg;
				}
				showMsg(type, msg, append);
				tableRefresh();
			},
			error : function(xhr, textStatus, errorThrown) {
				handleAjaxError(xhr.status);
			}
		});
	})

	function responseHandler(res) {
		$.each(res.rows, function(i, row) {
			row.state = $.inArray(row.id, selections) !== -1
		})
		return res
	}

	// 操作添加按钮的方法
	function operateFormatter(value, row, index) {
		// 运行状态允许查看，结束状态不允许查看
		var s = "";
		if (row.runStatus == "运行") {
			s = '<button class="btn btn-info btn-sm like" id='+row.inventoryBatch+"see"+'><span>审查</span></button>';
		} else {
			s = '<button class="btn btn-sm like" id='+row.inventoryBatch+"see"+'><span>查看</span></button>';
		}

		var d = '<button class="btn btn-danger btn-sm remove"><span>删除</span></button>';
		var x = '<button class="btn btn-info btn-sm text"><span>盘点报表</span></button>';
		var fun = '';
		return s + ' ' + d + ' ' + x;
	}

	//操作的点击事件
	window.operateEvents = {
		// 查看盘点批次
		'click .like' : function(e, value, row, index) {
			delay(function() {
				$('#panel').load("pageForward/inventoryGoods",{
					"inventoryBatch" : row.inventoryBatch,
					"status" : row.runStatus
				});
			}, 500);
		},
		// 删除盘点批次
		'click .remove' : function(e, value, row, index) {
			$('#deleteWarning_modal').modal('show');
			$("#delete_confirm").click(function() {
				var data = {
					batch : row.inventoryBatch
				};
				$.ajax({
					type : "GET",
					url : "pcInventAsset/deleteInventoryBatch",
					dataType : "json",
					contentType : "application/json",
					data : data,
					success : function(response) {
						$('#deleteWarning_modal').modal("hide");
						var type;
						var msg;
						var append = '';
						if (response.result == "success") {
							type = "success";
							msg = "库存信息删除成功";
						} else {
							type = "error";
							msg = "库存信息删除失败";
						}
						showMsg(type, msg, append);
					},
					error : function(xhr, textStatus, errorThrown) {
						$('#deleteWarning_modal').modal("hide");
						handleAjaxError(xhr.status);
					}
				});
				$('#deleteWarning_modal').modal('hide');
				$table.bootstrapTable('remove', {
					field : 'inventoryBatch',
					values : [ row.inventoryBatch ]
				});
			})
		},
		// 盘点报表
		'click .text' : function(e, value, row, index) {
			/* var myParams = {
					inventoryBatch : row.inventoryBatch
			};
			console.log("inventoryBatch："+myParams); */
			delay(function() {
				// 跳转echart.jsp页面，并传递参数
				$('#panel').load("pageForward/echarts", {
					"inventoryBatch" : row.inventoryBatch,
					"departNum" : $("#depart option:selected").val()
				});
			}, 500);
		}

	}

	// 运行状态添加按钮的方法
	function operateFormatterRunStatus(value, row, index) {
		if (row.runStatus == "运行") {
			return '<button type="button" class="btn btn-info run_status"  id=' + row.inventoryBatch + '>'
					+ row.runStatus + '</button>';
		} else {
			return '<button type="button" class="btn run_status" disabled="true" id='+row.inventoryBatch + '>'
					+ row.runStatus + '</button>';
		}

	}
	// 运行状态按钮的的点击事件
	window.operateEventRunStatus = {
		'click .run_status' : function(e, value, row, index) {
			$('#closeWarning_modal').modal("show");
			$('#close_confirm')
					.click(
							function() {
								if (value == "运行") {

									$('#' + row.inventoryBatch).removeClass(
											'btn-info');
									$('#' + row.inventoryBatch).attr(
											"disabled", "true");
									$('#' + row.inventoryBatch).text("结束");
									$('#' + row.inventoryBatch + "see")
											.removeClass('btn-info');
									$('#' + row.inventoryBatch + "see").text(
											"查看");
									data = {
										batch : row.inventoryBatch
									};
									$
											.ajax({
												type : "GET",
												url : 'pcInventAsset/closeSwitch',
												data : data,
												success : function(response) {
													if (response.result == "success") {
														$('#closeWarning_modal')
																.modal("hide");
														showMsg("success",
																"结束此次盘点成功", '');
													} else {
														$('#closeWarning_modal')
																.modal("hide");
														showMsg("error",
																"结束此次盘点失败", '');
													}
												},
												error : function(xhr,
														textStatus, errorThrown) {
													$('#closeWarning_modal')
															.modal("hide");
													$('#closeWarning_modal')
															.modal("hide");
													showMsg("error",
															"结束此次盘点失败", '');
													//handleAjaxError(xhr.status);
												}
											})
								}
							})
		}
	}
	$(function() {
		tableRefresh();
		initTable();
		// 初始化 下拉框组件
		$('select').selectpicker();
	})
</script>