<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<script>
	$(function() {
		bootstrapValidatorInit();
		//update_but();
	});

	function bootstrapValidatorInit(){
		$('#form').bootstrapValidator({
			message:'This value is not valid',
			feedbackIcons:{
				valid:'glyphicon glyphicon-ok',
				invalid:'glyphicon glyphicon-remove',
				validating:'glyphicon glyphicon-refresh'
			},
			excluded: [':disabled'],
			fields:{// 字段验证
				oldPassword:{// 原密码
					validators:{
						notEmpty:{
							message:'输入不能为空'
						},
						callback:{}
					}
				},
				newPassword:{// 新密码
					validators:{
						notEmpty:{
							message:'输入不能为空'
						},
						stringLength:{
							min:6,    
							max:16,
							message:'密码长度为6~16位'
						},
						callback:{}
					}
				},
				newPassword_re:{// 重复新密码
					validators:{
						notEmpty:{
							message:'输入不能为空'
						},
						identical:{
							field:'newPassword',
							message:'两次密码不一致'
						}
					}
				}
			}
		})
		.on('success.form.bv',function(e){
			// 禁用默认表单提交
			e.preventDefault();
			
			// 获取 form 实例
			var $form = $(e.target);
			// 获取 bootstrapValidator 实例
			var bv = $form.data('bootstrapValidator');

			var userID = $('#userID').html();
			var oldPassword = $('#oldPassword').val();
			var newPassword = $('#newPassword').val();
			var rePassword = $('#newPassword_re').val();

			oldPassword = passwordEncrying(oldPassword);
			newPassword = passwordEncrying(newPassword);
			rePassword = passwordEncrying(rePassword);
			var data = {
					"oldPassword" : oldPassword,
					"newPassword" : newPassword,
					"rePassword" : rePassword
				}

			// 将数据通过 AJAX 发送到后端
			$.ajax({
				type: "POST",
				url:"passwordModify",
				dataType:"json",
				contentType:"application/json",
				data:JSON.stringify(data),
				success:function(response){
					// 接收并处理后端返回的响应e'd'
					if(response.result == "error"){
						var errorMessage;
						if(response.msg == "passwordError"){
							errorMessage = "密码错误";
							field = "oldPassword"
						}else if(response.msg == "passwordUnmatched"){
							errorMessage = "密码不一致";
							field = "newPassword"
						}

						$("#oldPassword").val("");
						$("#newPassword").val("");
						$("#newPassword_re").val("");
						bv.updateMessage(field,'callback',errorMessage);
						bv.updateStatus(field,'INVALID','callback');
					}else{
						// 否则更新成功，弹出模态框并清空表单
						showMsg('success', '密码修改成功', '')
						$('#reset').trigger("click");
						$('#form').bootstrapValidator("resetForm",true); 
					}
					
				},
				error:function(xhr, textStatus, errorThrown){
					// handler error
					handleAjaxError(xhr.status);
				}
			});
		})
	}

	// 密码加密模块
	function passwordEncrying(password){
		var str1 = $.md5(password);
		//var str2 = $.md5(str1 + userID);
		return str1;
	}

	
	//修改密码
	/*修改密码按钮*/
	function update_but(){
		$("#update_but").click(function(){
			var oldpassword=$("#oldPassword").val();
			var newpassword=$("#newPassword").val();
			var truepassword=$("#newPassword_re").val();
			//验证两次密码是否一致
			if(newpassword==truepassword){
				//两次密码一致
				//验证密码强度等级
				var Grade=checkPass(truepassword);
				if(Grade==3)
				{	
					//符合密码强度要求
					var data={
							oldpassword : oldpassword,
							newpassword : truepassword
					}
					$.ajax({
						url : "Forced",
						type : "post",
						dataType : "json",
						contentType : "application/json",
						data : JSON.stringify(data),
						success : function(response) {
							var result=response.result;
							var msg=response.msg;
							alert(msg);
							if(result == 'error'){
								//修改失败
								if(msg=="登陆状态异常，请重新登陆"){
									alert(msg);
									window.location.href="./index.jsp";
								}else{
									alert(msg);
								}
							}else{
								//修改成功
								alert(msg+",点击确定返回登陆");
								try{
									window.location.href="./index.jsp";
								}catch(e){
									alert("跳转失败");
								}
								
							}
						},
						error : function(response) {
							alert("请求失败");
						}
					});
				}else if(Grade==-1){
					alert("密码长度必须大于8位且小于20位");
				}else{
					if(Grade==0){
						alert("密码长度必须大于8位且小于20位");
					}else{
						alert("密码必须同时包含：字母、数字、符号");
					}
				}
			}else{
				alert("两次密码不一致！");
			}
		})
	}
	//验证密码复杂度
	function checkPass(password){
         if(password.length < 8){
            return 0;
         }
        if(password.length >20)
        {
        	 return -1;
        }
        var grade= 0;
        if(password.match(/([a-z])+/)){
        	grade++;
       	 }
        if(password.match(/([0-9])+/)){
       		grade++;
        }   
        if(password.match(/([A-Z])+/)){ 
       		grade++;
         }
        if(password.match(/[^a-zA-Z0-9]+/)){
         	grade++;
        }
        return grade;
    }
</script>
<!-- 修改密码面板 -->
<div class="panel panel-default">
	<!-- 面包屑 -->
	<ol class="breadcrumb">
		<li>修改密码</li>
	</ol>

	<div class="panel-body">
		<!--  修改密码主体部分 -->
		<div class="row">
			<div class="col-md-4 col-sm-2"></div>
			<div class="col-md-4 col-sm-8">

				<form action="" method="post" class="form-horizontal" style=""
					role="form" id="form">
					
					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4"> 用户ID: </label>
						<div class="col-md-8 col-sm-8">
							<span class="hidden" id="userID">${sessionScope.loginUser.userCount }</span>
							<p class="form-control-static">${sessionScope.loginUser.userCount }</p>
						</div>
					</div>

					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4"> 输入原密码: </label>
						<div class="col-md-8 col-sm-8">
							<input type="password" class="form-control" id="oldPassword"
								name="oldPassword">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4"> 输入新密码: </label>
						<div class="col-md-8 col-sm-8">
							<input type="password" class="form-control" id="newPassword"
								name="newPassword">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4"> 确认新密码: </label>
						<div class="col-md-8 col-sm-8 has-feedback">
							<input type="password" class="form-control" id="newPassword_re"
								name="newPassword_re">
						</div>
					</div>

					<div>
						<div class="col-md-4 col-sm-4"></div>
						<div class="col-md-4 col-sm-4">
							<button type="submit" id="update_but" class="btn btn-success">
								&nbsp;&nbsp;&nbsp;&nbsp;确认修改&nbsp;&nbsp;&nbsp;&nbsp;</button>
						</div>
						<div class="col-md-4 col-sm-4"></div>
					</div>
					<input id="reset" type="reset" style="display:none">
				</form>

			</div>
			<div class="col-md-4 col-sm-2"></div>
		</div>

		<div class="row">
			<div class="col-md-3 col-sm-1"></div>
			<div class="col-md-6 col-sm-10">
				<div class="alert alert-info" style="margin-top: 50px">
					<p>登录密码修改规则说明：</p>
					<p>1.密码长度为8~20位。</p>
					<p>2.至少包含数字、字母、特殊符号,字母区分大小写。</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-1"></div>
		</div>
	</div>
</div>
