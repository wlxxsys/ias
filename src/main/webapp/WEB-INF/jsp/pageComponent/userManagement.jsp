<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<script>
	var search_key = "";
	var selectID;
	var selector_college_name = "";// 选择的学校名称
	var selector_depart_name = "";//选择的部门名称

	$(function() {
		searchAction();
		userListInit();
		bootstrapValidatorInit();
		adduserAction();
		edituserAction();
		deleteuserAction();
		getAllCollege();
		getAllDepart();
	})
	// 获取选择的学校名称
	$(".selector_college").click(function() {
		selector_college_name = $(this).val();
		$(".selector_depart").html("");
		selector_depart_name = "";
		getAllDepart();
	});

	$(".selector_depart").click(function() {
		selector_depart_name = $(this).val();
		search_key = $('#depart_selector').val();
	})

	//获取后端所有学校显示在下拉列表中
	function getAllCollege() {
		var selections = document.getElementsByClassName("selector_college");
		$.ajax({
			type : "GET",
			url : 'PCCollegeHandler/listCollege',
			//async :false,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				for (var i = 0; i < batch.length; i++) {
					for (var j = 0; j < selections.length; j++) {
						var option = document.createElement("option");
						option.value = batch[i];
						option.text = batch[i];
						selections[j].options.add(option);
					}
					;
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}

	//获取登陆者所在学校的所有部门
	function getAllDepart() {
		var data = {
			college : selector_college_name
		}
		$.ajax({
			type : "GET",
			url : 'PCStoreHandler/listDepartName',
			dataType : "json",
			data : data,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				//添加到查询
				var selections = document
						.getElementsByClassName("selector_depart");
				if (batch.length < 1) {
					for (var j = 0; j < selections.length; j++) {
						var option = document.createElement("option");
						option.value = "";
						option.text = "---选择部门---";
						selections[j].options.add(option);
					}
				}

				for (var i = 0; i < batch.length; i++) {
					for (var j = 0; j < selections.length; j++) {
						var option = document.createElement("option");
						option.value = batch[i];
						option.text = batch[i];
						selections[j].options.add(option);
					}
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}

	// 搜索动作
	function searchAction() {
		$('#search_button').click(function() {
			
			tableRefresh();
		})
	}

	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			depart : search_key
		}
		return temp;
	}

	// 表格初始化
	function userListInit() {
		$('#userList')
				.bootstrapTable(
						{
							columns : [
									{
										field : 'userCount',
										title : '用户账号'
									//sortable: true
									},
									{
										field : 'userName',
										title : '用户名称'
									},
									{
										field : 'userRole',
										title : '用户角色权限'
									},

									{
										field : 'operation',
										title : '操作',
										formatter : function(value, row, index) {
											var s = '<button class="btn btn-info btn-sm edit"><span>编辑</span></button>';
											var d = '<button class="btn btn-danger btn-sm delete"><span>删除</span></button>';
											var fun = '';
											return s + ' ' + d;
										},
										events : {
											// 操作列中编辑按钮的动作
											'click .edit' : function(e, value,
													row, index) {
												selectID = row.userCount;
												rowEditOperation(row);
											},
											'click .delete' : function(e,
													value, row, index) {
												selectID = row.userCount;
												$('#deleteWarning_modal')
														.modal('show');
											}
										}
									} ],
							url : 'listUserInfo',
							onLoadError : function(status) {
								handleAjaxError(status);
							},
							method : 'GET',
							queryParams : queryParams,
							sidePagination : "server",//分页方式，服务器端分页
							dataType : 'json',
							pagination : true,//是否开启分页
							pageNumber : 1,//初始化加载第一页
							pageSize : 5,//每页的记录行数
							pageList : [ 5, 10, 25, 50, 100 ],//可供选择的每页的行数
							clickToSelect : true
						//是否启用点击选中行
						});
	}

	// 表格刷新
	function tableRefresh() {
		$('#userList').bootstrapTable('refresh', {
			query : {}
		});
	}

	// 行编辑操作
	function rowEditOperation(row) {
		$('#edit_modal').modal("show");

		// load info
		$('#user_form_edit').bootstrapValidator("resetForm", true);
		$('#user_count_edit').val(row.userCount);
		$('#user_name_edit').val(row.userName);
		$('#user_passward_edit').val(row.userPassward);
		$('#user_role_edit').val(row.userRole);

	}

	// 添加用户模态框数据校验
	function bootstrapValidatorInit() {
		$("#user_form,#user_form_edit").bootstrapValidator({
			live : 'enabled',
			message : 'This is not valid',
			feedbackIcons : {//根据验证结果显示各种图标
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			excluded : [ ':disabled' ],//排除无需验证的控件
			fields : {
				user_count : {
					validators : {
						notEmpty : {
							message : '用户账户不能为空'
						}
					}
				},
				user_name : {
					validators : {
						notEmpty : {
							message : '用户名称不能为空'
						},
						stringLength : {
							min : 2,
							max : 5,
							message : '长度必须为2-5之间'
						}
					}
				}

			}
		})
	}

	// 编辑用户信息
	function edituserAction() {
		$('#edit_modal_submit')
				.click(
						function() {
							$('#user_form_edit').data('bootstrapValidator')
									.validate();
							if (!$('#user_form_edit')
									.data('bootstrapValidator').isValid()) {
								return;
							}

							var data = {
								userCount : selectID,
								userName : $('#user_name_edit').val(),
								userPassward : passwordEncrying($(
										'#user_passward_edit').val()),
								userRole : $('#user_role_edit').val()

							}

							// ajax
							$.ajax({
								type : "POST",
								url : 'updateUserInfo',
								dataType : "json",
								contentType : "application/json",
								data : JSON.stringify(data),
								success : function(response) {
									$('#edit_modal').modal("hide");
									var type;
									var msg;
									var append = ''
									if (response.result == "success") {
										type = "success";
										msg = "客户信息更新成功";
									} else if (response.result == "error") {
										type = "error";
										msg = "客户信息更新失败"
									}
									showMsg(type, msg, append);
									tableRefresh();
								},
								error : function(xhr, textStatus, errorThrown) {
									$('#edit_modal').modal("hide");
									// 处理错误
									handleAjaxError(xhr.status)
								}
							});
						});
	}

	// 刪除客户信息
	function deleteuserAction() {
		$('#delete_confirm').click(function() {
			var data = {
				userCount : selectID
			}

			// ajax
			$.ajax({
				type : "GET",
				url : "deleteUserInfo",
				dataType : "json",
				contentType : "application/json",
				data : data,
				success : function(response) {
					$('#deleteWarning_modal').modal("hide");//弹框插件，hide指的是点击的时候触发关闭模态窗
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {

						type = "success";
						msg = "客户信息删除成功";
					} else {
						type = "error";
						msg = "客户信息删除失败";
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#deleteWarning_modal').modal("hide");
					// handler error
					handleAjaxError(xhr.status)
				}
			})

			$('#deleteWarning_modal').modal('hide');
		})
	}

	// 添加客户信息
	function adduserAction() {
		$('#add_user').click(function() {
			$('#add_modal').modal("show");
		});

		$('#add_modal_submit').click(function() {
			var data = {
				userCount : $('#user_count').val(),
				userName : $('#user_name').val(),
				//userPassward : $('#user_passward').val(),
				userRole : $('#user_role').val(),
				userDepart : $('#add_depart_selector').val()
			}
			// ajax
			$.ajax({
				type : "POST",
				url : "addUserInfo",
				dataType : "json",
				contentType : "application/json; charset=utf-8",
				data : JSON.stringify(data),
				success : function(response) {

					$('#add_modal').modal("hide");
					var msg;
					var type;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = "客户添加成功";
					} else if (response.result == "error") {

						type = "error";
						msg = "客户添加失败";
					} else if (response.result == "未登录用户") {
						type = "error";
						msg = "未登录用户"
					} else if (response.result == "该账户已存在") {
						type = "error";
						msg = "该账户已存在"
					}
					showMsg(type, msg, append);
					tableRefresh();

					// reset
					$('#user_count').val("");
					$('#user_name').val("");
					$('#user_passward').val("");
					$('#user_role').val("");
					$('#user_form').bootstrapValidator("resetForm", true);
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#add_modal').modal("hide");
					// handler error
					handleAjaxError(xhr.status);
				}
			})
		})
	}
	// 密码加密模块
	function passwordEncrying(password) {
		var str1 = $.md5(password);
		//var str2 = $.md5(str1 + userID);
		return str1;
	}
</script>
<div class="panel panel-default">
	<ol class="breadcrumb">
		<li>用户信息管理</li>
	</ol>
	<div class="panel-body">
		<div class="row">
		<shiro:hasRole name="超级管理员">
			<div class="col-md-3 col-sm-3">
				<select style="width: 20;" name="" id="college_selector"
					class="form-control selector_college" border="none" outline="none">
					<option value="">---选择学校---</option>
				</select>
			</div>
		</shiro:hasRole>
		<shiro:hasRole name="超级管理员">
			<div class="col-md-3 col-sm-3">
				<select style="width: 20;" name="" id="depart_selector"
					class="form-control selector_depart" border="none" outline="none">
					<option value="">---选择部门---</option>
				</select>
			</div>
		
			<div class="col-md-6 col-sm-6">
				<div>
					<div class="col-md-2 col-sm-2">
						<button id="search_button" class="btn btn-success">
							<span class="glyphicon glyphicon-search"></span> <span>查询</span>
						</button>
					</div>
				</div>
			</div>
			</shiro:hasRole>
		</div>

		<div class="row" style="margin-top: 25px">
			<div class="col-md-5">
				<button class="btn btn-sm btn-default" id="add_user">
					<span class="glyphicon glyphicon-plus"></span> <span>添加用户</span>
				</button>
			</div>
			<div class="col-md-5"></div>
		</div>

		<div class="row" style="margin-top: 15px">
			<div class="col-md-12">
				<table id="userList" class="table table-striped"></table>
			</div>
		</div>
	</div>
</div>

<!-- 添加用户信息模态框 -->
<div id="add_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">添加用户信息</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="user_form"
							style="margin-top: 25px">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>用户账号：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="user_count"
										name="user_count" placeholder="用户账户">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>用户名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="user_name"
										name="user_name" placeholder="用户名称">
								</div>
							</div>
							<shiro:hasRole name="超级管理员">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>所属学校：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select name="" class="form-control selector_college"
										id="add_college_selector">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>所属部门：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select name="" class="form-control selector_depart"
										id="add_depart_selector">
									</select>
								</div>
							</div>
							</shiro:hasRole>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>角色权限：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select name="" class="form-control" id="user_role">
										<option value="管理员">管理员</option>
										<option value="盘点员">盘点员</option>
									</select>
								</div>
							</div>
					</div>
					</form>
				</div>
				<div class="col-md-1 col-sm-1"></div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn btn-default" type="button" data-dismiss="modal">
				<span>取消</span>
			</button>
			<button class="btn btn-success" type="button" id="add_modal_submit">
				<span>提交</span>
			</button>
		</div>
	</div>
</div>
</div>




<!-- 删除提示模态框 -->
<div class="modal fade" id="deleteWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认删除该条用户信息</h3>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="delete_confirm">
					<span>确认删除</span>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 编辑客户信息模态框 -->
<div id="edit_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">编辑用户信息</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="user_form_edit"
							style="margin-top: 25px">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>用户账号：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" disabled="disabled" class="form-control"
										id="user_count_edit" name="user_count" placeholder="用户账号">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>用户名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="user_name_edit"
										name="user_name" placeholder="用户名称">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>用户密码：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="user_passward_edit"
										name="user_passward" placeholder="用户密码">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>用户角色权限：</span>
								</label>
								<div class="col-md-5 col-sm-5">
									<select name="" class="form-control" id="user_role_edit">
										<option value="管理员">管理员</option>
										<option value="盘点员">盘点员</option>
									</select>
								</div>
							</div>

						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-success" type="button" id="edit_modal_submit">
					<span>确认更改</span>
				</button>
			</div>
		</div>
	</div>
</div>