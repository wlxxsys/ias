<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<head>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/main.css">
</head>
<script>
	$(function() {
		quickAccessInit();
	})
	// 快捷方式
	function quickAccessInit() {
		$('.shortcut').click(function() {
			var url = $(this).attr("name");
			// mLoading遮罩插件 显示loading组件 默认值：加载中...  hide则是隐藏
			$('#panel .panel').mLoading('show');
			delay(function(){
				$('#panel').load(url);
			}, 500);
		})
		
	}
</script>
<!-- 欢迎界面你好 -->
<div class="panel panel-default">
	<!-- 面包屑 -->
	<ol class="breadcrumb">
		<li>主页</li>
	</ol>
<div class="panel-body" style="background-color: #fff">
						<div class="row" style="margin-top: 100px; margin-bottom: 100px">
							<div class="col-md-1"></div>
							<div class="col-md-10" style="text-align: center">
								<div class="col-md-6 col-sm-6">
									<a href="javascript:void(0)" class="thumbnail shortcut" 
									name="pageForward/inventoryBatchManagement"> 
									<img
										src="media/icons/stock_search-512.png" alt="库存查询"
										class="img-rounded link" style="width: 150px; height: 150px;">
										<div class="caption">
											<h3 class="title">盘点记录</h3>
										</div>
									</a>
								</div>
								<shiro:hasRole name="管理员">
 								<div class="col-md-6 col-sm-6">
									<a href="javascript:void(0)" class="thumbnail shortcut" 
									name="pageForward/excelManagement"> 
									<img
										src="media/icons/stock_in-512.png" alt="货物入库"
										class="img-rounded link" style="width: 150px; height: 150px;">
										<div class="caption">
											<h3 class="title">数据迁移</h3>
										</div>
									</a>
								</div>
								</shiro:hasRole>
								<!--<div class="col-md-4 col-sm-4">
									<a href="javascript:void(0)" class="thumbnail shortcut"> <img
										src="media/icons/stock_out-512.png" alt="货物出库"
										class="img-rounded link" style="width: 150px; height: 150px;">
										<div class="caption">
											<h3 class="title">盘点异常</h3>
										</div>
									</a>
								</div> -->
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>
				</div>

	<!-- <div class="panel-body">
		<div class="row">
			<div class="col-lg-3 col-xl-3">
				<div class="card mb-3 widget-content">
					<div class="widget-content-wrapper">
						<div class="widget-content-left">
							<div class="widget-heading">已盘点</div>
							<div class="widget-subheading">Last year expenses</div>
						</div>
						<div class="widget-content-right">
							<div class="widget-numbers text-success">
								<span>1896</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-xl-3">
				<div class="card mb-3 widget-content">
					<div class="widget-content-wrapper">
						<div class="widget-content-left">
							<div class="widget-heading">未盘点</div>
							<div class="widget-subheading">Total Clients Profit</div>
						</div>
						<div class="widget-content-right">
							<div class="widget-numbers text-primary">
								<span>$ 568</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-xl-3">
				<div class="card mb-3 widget-content">
					<div class="widget-content-wrapper">
						<div class="widget-content-left">
							<div class="widget-heading">存放地异常</div>
							<div class="widget-subheading">Total revenue streams</div>
						</div>
						<div class="widget-content-right">
							<div class="widget-numbers text-warning">
								<span>$14M</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-xl-3">
				<div class="card mb-3 widget-content">
					<div class="widget-content-wrapper">
						<div class="widget-content-left">
							<div class="widget-heading">部门异常</div>
							<div class="widget-subheading">People Interested</div>
						</div>
						<div class="widget-content-right">
							<div class="widget-numbers text-danger">
								<span>46%</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="divider mt-0" style="margin-bottom: 30px;"></div>
		<div class="row">
		    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		        <div class="box">
		            <div class="box-header">
		                <i class="ion fa fa-pie-chart"></i>
		                <h3 class="box-title">资产概况</h3>
		            </div>
		            <div class="box-body">
		                <div class="row" id="rowAssetBaseInfo">
		                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="position:relative;">
		                        <div id="pieBasicInfo1" style="width: 100%; height: 200px; -webkit-tap-highlight-color: transparent; user-select: none; position: relative;">
		                        		                        <img src="media/images/img_usering.png" alt="" style="width: 100%; height: 158px;">
		                        <div></div></div>
		                        
		                        <div class="text-layout pieBasicInfoText" style="visibility: visible;">
		                           
		                            <label class="item">
		                                资产数量
		                                <span class="num" id="spanAssetUseCount">0</span>
		                            </label>
		                            <label class="item">
		                                资产金额
		                                <span class="num" id="spanAssetUseAmount"><span style="font-size:12px">￥</span>0</span>
		                            </label>
		                        </div>
		                    </div>
		                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="position:relative;">
		                        <div id="pieBasicInfo2" style="width: 100%; height: 200px; -webkit-tap-highlight-color: transparent; user-select: none; position: relative;">
		                        		                        <img src="media/images/img_no_use.png" alt="" style="width: 100%; height: 158px;">
		                        <div></div></div>		                        <div class="text-layout pieBasicInfoText" style="visibility: visible;">
		                            <label class="item">
		                                资产数量
		                                <span class="num" id="spanAssetFreeCount">0</span>
		                            </label>
		                            <label class="item">
		                                资产金额
		                                <span class="num" id="spanAssetFreeAmount"><span style="font-size:12px">￥</span>0</span>
		                            </label>
		                        </div>
		                    </div>
		                </div> 
		            </div>
		        </div>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		        <div class="box">
		            <div class="box-header">
		                <i class="ion fa fa-pie-chart"></i>
		                <h3 class="box-title">资产状态占比</h3>
		            </div>
		            <div class="box-body">
		                <div class="row" id="rowAssetStatPer" style="visibility: visible;">
		                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		                        <div id="pieAssetStatPer" style="width: 100%; height: 200px; -webkit-tap-highlight-color: transparent; user-select: none; position: relative;" _echarts_instance_="ec_1589973892094"><div style="position: relative; overflow: hidden; width: 305px; height: 200px; padding: 0px; margin: 0px; border-width: 0px; cursor: default;"><canvas data-zr-dom-id="zr_0" width="381" height="250" style="position: absolute; left: 0px; top: 0px; width: 305px; height: 200px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); padding: 0px; margin: 0px; border-width: 0px;"></canvas></div><div style="position: absolute; display: none; border-style: solid; white-space: nowrap; z-index: 9999999; transition: left 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s, top 0.4s cubic-bezier(0.23, 1, 0.32, 1) 0s; background-color: rgba(50, 50, 50, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font: 14px / 21px &quot;Microsoft YaHei&quot;; padding: 5px; left: 87px; top: 39px;">资产状态 <br> : 0 (0%)</div></div>
		                    </div>
		                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		                        <ul class="float-left state-list">
		                            <li>
		                                <label class="title"><i class="dot dot-1"></i>闲置</label>
		                                <span class="num" id="spanStatFreeCount">0</span>
		                            </li>
		                            <li>
		                                <label class="title"><i class="dot dot-2"></i>在用</label>
		                                <span class="num" id="spanStatUseCount">0</span>
		                            </li>
		                            <li>
		                                <label class="title"><i class="dot dot-3"></i>借出</label>
		                                <span class="num" id="spanStatBorrowCount">0</span>
		                            </li>
		                            <li>
		                                <label class="title"><i class="dot dot-4"></i>维修</label>
		                                <span class="num" id="spanStatRepairCount">0</span>
		                            </li>
		                            <li>
		                                <label class="title"><i class="dot dot-5"></i>调拨中</label>
		                                <span class="num" id="spanStatTransferCount">0</span>
		                            </li>
		                            <li>
		                                <label class="title"><i class="dot dot-6"></i>报废</label>
		                                <span class="num" id="spanStatClearCount">0</span>
		                            </li>
		                        </ul>
		                    </div>
		                </div> 
		            </div>
		        </div>
		    </div>
		</div>
	</div>
 --></div>

<script type="text/javascript">
	// 全局加载
	$(function() {
		
		get_inventory();
	})

	// 获取盘点数据
	function get_inventory() {
		$
				.ajax({
					type : 'get',
					//url:'pcInventAsset/listInvetoryAssetsNumber',
					dataType : 'json',
					success : function(response) {
						var inventory_status_name = new Array();
						inventory_status_name = response.totalNames;
						for (var i = 0; i < inventory_status_name.length; i++) {
							if (inventory_status_name[i] == "已盘点") {
								$('#finish_inventory').text(response.data[i]);
							} else if (inventory_status_name[i] == "未盘点") {
								$('#not_finish_inventory').text(
										response.data[i]);
							} else if (inventory_status_name[i] == "存放地异常") {
								$('#store_exception_inventory').text(
										response.data[i]);
							} else if (inventory_status_name[i] == "部门异常") {
								$('#depart_exception_inventory').text(
										response.data[i]);
							}
						}

					}
				})
	}
</script>
<%-- <script src="${pageContext.request.contextPath}/js/main.js"></script> --%>