<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>echarts</title>

</head>
<body>
	<div class="panel panel-default">
		<div class="panel-heading">盘点报表</div>
		<div class="panel-body">
			<form class="form-inline">
				<div class="row">
					<shiro:hasRole name="超级管理员">
						<!-- <div class="col-md-3 col-sm-3">
							<label for="" class="form-label">选学校：</label> <select name=""
								id="college_selector" class="form-control" border="none"
								outline="none">
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<label for="" class="form-label">选部门：</label> <select name=""
								id="depart_selector" class="form-control" border="none"
								outline="none">
								<option value="none">---部门---</option>
							</select>
						</div> -->
						<!-- <div class="col-md-3 col-sm-3">
				<select class="selectpicker" title="学校" id="college"
					onchange="initDepart()">
				</select>
			</div>
			<div class="col-md-3 col-sm-3">
				<select class="selectpicker" title="部门" id="depart">
				</select>
			</div> -->
					</shiro:hasRole>
					<div class="col-md-1 col-sm-1">
						<button type="button" class="btn btn-success" id="search_btn">查询统计图</button>
					</div>
					<div class="col-md-1 col-sm-1">
						<button type="button" class="btn btn-success"
							id="search_btn_excel">导出盘点报表</button>
					</div>
				</div>
			</form>
			<br /> <br /> <input type="hidden" id="inventoryBatchParams"
				value="<%=request.getParameter("inventoryBatch")%>">
				<input type="hidden" id="departParams"
				value="<%=request.getParameter("departNum")%>">
			<div class="row">
				<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
				<div id="main" style="height: 400px"></div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	var selector_college = "";
	var selector_depart = "";
	$(function() {
		searchButton();
		searchFormButton();
	});

	var myChart = echarts.init(document.getElementById('main'));
	//数据加载时间较长时显示动画
	//myChart.showLoading();
	var inventoryBatch = $("#inventoryBatchParams").val();
	var departNum = $("#departParams").val();
	console.log("传递成功：" + inventoryBatch+","+departNum);

	// 点击查询图表按钮
	function searchButton() {
		$("#search_btn").bind("click", function() {
			get_source();
		})
	}
	// 点击导出报表按钮
	function searchFormButton() {
		$("#search_btn_excel").bind("click", function() {
			exportForm();
		})
	}

	//加载后台数据显示在图表中
	function get_source() {
		var data = {
			inventoryBatch : inventoryBatch,
			departNum : departNum
		};
		$.ajax({
			url : 'PCAssetInfoHandler/countEchartsData_v1',
			type : 'GET',
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : data,
			success : function(response) {
				//取消加载动画
				//myChart.hideLoading();
				//var sources = response.data;
				myChart.setOption({
					title : {
						text : response.title,
						x : 'center'
					},
					tooltip : {
						trigger : 'item',
						formatter : "{a} <br/>{b} : {c} ({d}%)"
					},
					legend : {
						orient : 'vertical',
						x : 'left',
						data : response.dicProfitLossNames
					},
					toolbox : {
						show : true,
						feature : {
							mark : {
								show : true
							},
							dataView : {
								show : true,
								readOnly : false
							},
							magicType : {
								show : true,
								type : [ 'pie', 'funnel' ],
								option : {
									funnel : {
										x : '25%',
										width : '50%',
										funnelAlign : 'left',
										max : 1548
									}
								}
							},
							restore : {
								show : true
							},
							saveAsImage : {
								show : true
							}
						}
					},
					calculable : true,
					series : [ {
						name : '访问来源',
						type : 'pie',
						radius : '55%',
						center : [ '50%', '60%' ],
						data : response.data
					} ]

				})

			}
		})
	}

	// 获取导出报表数据
	function exportForm() {
        var url = 'PCHanerExcel/exportExcelEmpPayrollAccount';
        //创建一个a标签进行下载
        var a = document.createElement('a');
        a.href = url+'?inventoryBatch='+inventoryBatch+'&excel_id=1';
        $('body').append(a);  // 修复firefox中无法触发click
        a.click();
        $(a).remove();
	}
	
	/* 动态生成college的select选项 */
	function initCollege() {
		var selectObj = $("#college");
		$.ajax({
			url : "PCCollegeHandler/listCollege_v1",
			type : "GET",
			success : function(response) {
				if (response.result == "success") {
					var configs = response.data;
					for ( var i in configs) {
						var config = configs[i];
						var optionValue = config.collegeNum;
						var optionText = config.collegeName;
						selectObj.append(new Option(optionText, optionValue));
					}
					// 刷新select
					selectObj.selectpicker('refresh');
				} else {
					console.log(response.msg);
				}
			},
			error : function() {
				console.log("服务器请求失败");
			}

		})
	}

	/* 根据选择学校动态生成部门下拉框 */
	function initDepart() {
		// 清空部门下拉框
		$("#depart").find("option:not(:first)").remove();
		$("#depart").selectpicker('refresh');
		// 重新获取部门下拉框数据 --- 根据选择的学校
		var collegeSelect = $("#college").val();
		var selectObj = $("#depart");
		var data = {
			collegeNum : collegeSelect
		}
		$.ajax({
			url : "PCDepartInfoHandler/listDepart_v1",
			type : "GET",
			dataType : "json",
			data : data,
			success : function(response) {
				if (response.result == "success") {
					var configs = response.data;
					for ( var i in configs) {
						var config = configs[i];
						var optionValue = config.departNum;
						var optionText = config.departName;
						selectObj.append(new Option(optionText, optionValue));
					}
					// 刷新select
					selectObj.selectpicker('refresh');
				} else {
					console.log(response.msg);
				}
			},
			error : function() {
				console.log("服务器请求失败");
			}

		})
	}
	$(function() {
		/* 初始化动态加载学校 */
		initCollege();
		// 初始化 下拉框组件
		$('select').selectpicker();

	})
</script>
</html>