<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script>
	var selector_college_name = "";// 选择的学校名称
	// 查询参数
	search_user_id = "0"
	search_start_date = null
	search_end_date = null
	search_depart = null
	$(function() {
		getAllCollege();
		datePickerInit();
		operaterRecordTableInit();
		searchActionInit();

	})

	// 日期选择器初始化
	function datePickerInit() {
		$('.form_date').datetimepicker({
			format : 'yyyy-mm-dd',
			language : 'zh-CN',
			endDate : new Date(),
			weekStart : 1,
			todayBtn : 1,
			autoClose : 1,
			todayHighlight : 1,
			startView : 2,
			forceParse : 0,
			minView : 2
		});
	}

	// 表格初始化
	function operaterRecordTableInit() {
		$('#accessRecordDOS').bootstrapTable({
			columns : [ {
				field : 'id',
				title : '记录ID'
			}, {
				field : 'userCount',
				title : '用户ID'
			}, {
				field : 'userName',
				title : '用户名'
			}, {
				field : 'createTime',
				title : '操作时间'
			}, {
				field : 'ip',
				title : 'IP'
			}, {
				field : 'operation',
				title : '操作'
			}, {
				field : 'userRole',
				title : '用户权限'
			} ],
			url : 'systemLog/getUserOperationLog',
			onLoadError : function(status) {
				handleAjaxError(status);
			},
			method : 'GET',
			queryParams : queryParams,
			sidePagination : "server",
			dataType : 'json',
			pagination : true,
			pageNumber : 1,
			pageSize : 5,
			pageList : [ 5, 10, 25, 50, 100 ],
			clickToSelect : true
		});
	}

	// 表格刷新
	function tableRefresh() {
		$('#accessRecordDOS').bootstrapTable('refresh', {
			query : {}
		});
	}

	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			userID : search_user_id,
			startDate : search_start_date,
			endDate : search_end_date,
			departName : search_depart
		}
		return temp;
	}

	// 查询操作
	function searchActionInit() {
		$('#search_button').click(function() {
			search_user_id = $('#user_id').val();
			search_start_date = $('#start_date').val();
			search_end_date = $('#end_date').val();
			search_depart = $('#depart_selector option:selected').val();
			tableRefresh();
		})
	}

	// 获取选择的学校名称
	$("#college_selector").click(function() {
		selector_college_name = $(this).val();
		$("#depart_selector").html("");
		selector_depart_name = "";
		getAllDepart();
	});
	
	//获取后端所有学校显示在下拉列表中
	function getAllCollege() {
		var selections = document.getElementById("college_selector");
		$.ajax({
			type : "GET",
			url : 'PCCollegeHandler/listCollege',
			//async :false,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				for (var i = 0; i < batch.length; i++) {
					var option = document.createElement("option");
					option.value = batch[i];
					option.text = batch[i];
					selections.options.add(option);
				}
				;

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}

	//获取登陆者所在学校的所有部门
	function getAllDepart() {
		var data = {
			college : selector_college_name
		}
		$.ajax({
			type : "GET",
			url : 'PCStoreHandler/listDepartName',
			dataType : "json",
			//async : false,
			data : data,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				//添加到查询
				var selections = document.getElementById("depart_selector");
				if (batch.length < 1) {
					var option = document.createElement("option");
					option.value = "";
					option.text = "---部门---";
					selections.options.add(option);
				}
				for (var i = 0; i < batch.length; i++) {
					option = document.createElement("option");
					option.value = batch[i];
					option.text = batch[i];
					selections.options.add(option);
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
</script>

</head>
<body>
	<div class="panel panel-default">
		<ol class="breadcrumb">
			<li>系统登陆日志</li>
		</ol>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-3">
					<form action="" class="form-inline">
						<div class="form-group">
							<label class="form-label">用户ID：</label> <input type="text"
								id="user_id" class="form-control" placeholder="指定用户ID"
								style="width: 50%">
						</div>
					</form>
				</div>
				<div class="col-md-3 col-sm-3">
				<form action="" class="form-inline">
					<label for="" class="form-label">选学校：</label> 
					<select name="" id="college_selector" class="form-control" border="none" outline="none">
						<option value="">---学校---</option>
					</select>
					</form>
				</div>
				<div class="col-md-3 col-sm-3">
				<form action="" class="form-inline">
					<label for="" class="form-label">选部门：</label> 
					<select	name="" id="depart_selector" class="form-control" border="none" outline="none">
						<option value="">---部门---</option>
					</select>
					</form>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" id="search_button">
						<span class="glyphicon glyphicon-search"></span> <span>查询</span>
					</button>
				</div>
			</div>
			<div class="row" style="margin-top: 20px">
				<div class="col-md-6">
					<form action="" class="form-inline">
						<label class="form-label">日期范围：</label> <input
							class="form_date form-control" id="start_date" placeholder="起始日期">
						<label class="form-label">&nbsp;&nbsp;-&nbsp;&nbsp;</label> <input
							class="form_date form-control" id="end_date" placeholder="结束日期">
					</form>
				</div>
			</div>
			<div class="row" style="margin-top: 25px">
				<div class="col-md-12">
					<table class="table table-striped" id="accessRecordDOS"></table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>