<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<script>
	var search_keyWord = "";// 编辑关键字
	var search_keyWord_btn = "";// 查询关键字
	var search_value_btn = ""; // 查询输入值
	var search_editcode = "";// 编辑输入或选择值
	var select_batch = $("#inventoryBatchParams").val();
	var selectBatch = $("#inventoryBatchParams").val();
	var inventoryStatus = $("#inventoryStatusParams").val();
	var store = new Array();//保存所有的存放地名称
	var dicprofitloss = new Array();//保存所有的损益类型名称
	var inventorystatus = new Array();//保存所有的盘点状态名称

	$(function() {
		//optionAction();// 下卡框查询选项动作
		optionActionEdit();//下拉编辑选项动作
		//searchAction();// 查询动作
		storageListInit();// 表格初始化
		editStorageAction();// 编辑盘点信息
		deleteStorageAction();// 刪除盘点信息
		exportStorageAction();// 批量操作
		search_dicprofitloss();//获取所有损益类型名称显示在修改处
		search_store();//获取本部门所有的存放地名称显示在修改处
		search_status();//获取所有的盘点状态显示在修改处
		edit_bind();//显示下拉编辑内容
		edit_show_hidden();// 根据条件隐藏编辑菜单

	})
	//根据条件隐藏编辑菜单
	function edit_show_hidden(){
		if(inventoryStatus!="运行"){
			document.getElementById("edit_menu").style.display="none";
		}
		
	}
	
	// 下拉编辑选项动作
	function optionActionEdit() {
		$(".dropOption").click(function() {
			var type = $(this).text();
			$("#edit_type").text(type);
		})
	}
	
	// 下拉框查询选项动作
	function optionAction() {
		$(".dropOption").click(function() {
			var type = $(this).text();
			$("#search_type").text(type);
		})
	};
	// 搜索动作
	function searchAction() {
		$('#search_button').click(function() {
			search_keyWord_btn = $('#search_type').text();
				search_value_btn = $('#search_input_type').val();
				tableRefresh();
		});
	}
	//获取所有的盘点状态显示在修改处
	function search_status() {
		var selections = document.getElementById("storage_Type_edit");
		$.ajax({
			type : "GET",

			url : 'pcInventAsset/listStatus',
			success : function(response) {
				inventorystatus = response.data;
				for (var i = 0; i < inventorystatus.length; i++) {
					option = document.createElement("option");
					option.value = inventorystatus[i];
					option.text = inventorystatus[i];
					selections.options.add(option);
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		})
	}
	//获取所有损益类型名称显示在修改处
	function search_dicprofitloss() {
		var selections = document
				.getElementById("storage_assetDicProfitType_edit");
		$.ajax({
			type : "GET",
			url : 'pcInventAsset/listDicProfitLoss',
			success : function(response) {
				dicprofitloss = response.data;
				for (var i = 0; i < dicprofitloss.length; i++) {
					option = document.createElement("option");
					option.value = dicprofitloss[i];
					option.text = dicprofitloss[i];
					selections.options.add(option);
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		})
	}
	//获取所有的存放地名称显示在修改处
	function search_store() {
		var selections = document.getElementById("storage_assetStore_edit");
		$.ajax({
			type : "GET",
			url : 'pcInventAsset/listStoreName',
			success : function(response) {
				store = response.data; //接收所有存放地名称
				for (var i = 0; i < store.length; i++) {
					option = document.createElement("option");
					option.value = store[i];
					option.text = store[i];
					selections.options.add(option);
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		})
	}

	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			batch : $("#inventoryBatchParams").val()
			//repositoryBelong : search_value_btn,
			//keyword : search_keyWord_btn,
		}
		return temp;
	}
	//表格刷新
	function tableRefresh() {
		$('#storageList').bootstrapTable('refresh', {
			query : {}
		});
	}
	// 表格初始化
	function storageListInit() {
		$('#storageList')
				.bootstrapTable(
						{
							height : 390,
							locale : "zh-CN",
							columns : [
									{
										checkbox : true,
										visible : true
									//是否显示复选框  
									},
									{
										field : 'goodsID',
										title : '资产编号'
									//sortable: true
									},
									{
										field : 'goodsName',
										title : '资产名称'
									},
									{
										field : 'goodsPerson',
										title : '领用人'
									},
									{
										field : 'goodsDepart',
										title : '部门',
									/* visible : false */
									},
									{
										field : 'goodsType',
										title : '损溢类型',
									/* visible : false */
									},
									{
										field : 'goodsAddr',
										title : '存放地'
									},
									{
										field : 'spanAddr',
										title : '扫描地'
									},
									{
										field : 'goodsStatus',
										title : '盘点状态'
									},
									{
										field : 'goodsDate',
										title : '盘点日期'
									},
									{
										field : 'goodsBatch',
										title : '盘点批次'
									},
									{
										field : 'operation',
										title : '操作',
										formatter : function(value, row, index) {
											
											if(inventoryStatus=="结束"){// 结束的盘点批次不允许编辑
												var s = '<button class="btn btn-info btn-sm edit "disabled="disabled"><span>修改</span></button>';
												var d = '<button class="btn btn-danger btn-sm delete" disabled="disabled"><span>删除</span></button>';
												var fun = '';
												return s + ' ' + d;
											}else{//运行的的盘点批次允许编辑
												var s = '<button class="btn btn-info btn-sm edit"><span>修改</span></button>';
												var d = '<button class="btn btn-danger btn-sm delete"><span>删除</span></button>';
												var fun = '';
												return s + ' ' + d;
											}
											
										},
										events : {
											// 操作列中编辑按钮的动作
											'click .edit' : function(e, value,
													row, index) {
												selectID = row.goodsID;
												selectBatch = row.goodsBatch;
												rowEditOperation(row);
											},
											'click .delete' : function(e,
													value, row, index) {
												selectID = row.goodsID;
												selectBatch = row.goodsBatch;
												$('#deleteWarning_modal')
														.modal('show');
											}
										}
									} ],
							url : 'pcInventAsset/getAssetByBatch',
							onLoadError : function(status) {
								handleAjaxError(status);
							},
							method : 'GET',
							queryParams : queryParams,
							sidePagination : "server",
							dataType : 'json',
							pagination : true,
							pageNumber : 1,
							pageSize : 5,
							pageList : [ 5, 10, 25, 50, 100 ],
							paginationLoop : true,
							//sortable: true,
							clickToSelect : true
						});
	}

	// 行编辑操作
	function rowEditOperation(row) {
		$('#edit_modal').modal("show");
		$('#storage_form_edit').bootstrapValidator("resetForm", true);

		$('#storage_assetID_edit').text(row.goodsID);
		$('#storage_assetName_edit').text(row.goodsName);

		$('#storage_assetStore_edit').val(row.goodsAddr);
		$('#storage_assetPerson_edit').val(row.goodsPerson);
		$('#storage_assetDicProfitType_edit').val(row.goodsType);
		$('#storage_Type_edit').val(row.goodsStatus);
	}

	// 编辑盘点信息
	function editStorageAction() {
		$('#edit_modal_submit').click(function() {
			$('#storage_form_edit').data('bootstrapValidator').validate();
			if (!$('#storage_form_edit').data('bootstrapValidator').isValid()) {
				return;
			}

			var data = {
				goodsID : $('#storage_assetID_edit').text(),
				goodsName : $('#storage_assetName_edit').text(),

				goodsAddr : $('#storage_assetStore_edit').val(),
				goodsPerson : $('#storage_assetPerson_edit').val(),

				goodsType : $('#storage_assetDicProfitType_edit').val(),
				goodsStatus : $('#storage_Type_edit').val(),

				goodsBatch : selectBatch
			}
			$.ajax({
				type : "POST",
				url : 'pcInventAsset/updateinventory',
				dataType : "json",
				contentType : "application/json",
				data : JSON.stringify(data),
				success : function(response) {
					$('#edit_modal').modal("hide");
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = "库存信息更新成功";
					} else if (response.result == "error") {
						type = "error";
						msg = "库存信息更新失败"
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#edit_modal').modal("hide");
					// handle error
					handleAjaxError(xhr.status);
				}
			});
		});
	}

	// 刪除盘点信息
	function deleteStorageAction() {
		$('#delete_confirm').click(function() {
			var data = {
				goodsID : selectID,
				goodsBatch : selectBatch
			}
			$.ajax({
				type : "GET",
				url : "pcInventAsset/deleteinventory",
				dataType : "json",
				contentType : "application/json",
				data : data,
				success : function(response) {
					$('#deleteWarning_modal').modal("hide");
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = "库存信息删除成功";
					} else {
						type = "error";
						msg = "库存信息删除失败";
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#deleteWarning_modal').modal("hide");
					handleAjaxError(xhr.status);
				}
			});

			$('#deleteWarning_modal').modal('hide');
		})
	}
	//批量操作获取复选框选中的每行的goodsID
	function chechedgoodsID() {
		var rows = $('table').bootstrapTable('getSelections');
		var goodsID = new Array();
		if (!rows || rows.length == 0) {
			alert("您没有选择要操作的数据");
		} else {
			for (var i = 0; i < rows.length; i++) {
				goodsID[i] = rows[i].goodsID;
			}
		}
		return goodsID;
	}
	// 批量操作
	function exportStorageAction() {
		//批量编辑
		var goodsID = new Array();
		$('#export_edit').click(function() {
			goodsID = chechedgoodsID(); //获取编辑的ID
			search_keyWord = $('#edit_type').text(); //获取编辑的字段
			search_editcode = $("#search_edit_type").val();//获取编辑的内容
			selectBatch = select_batch; //获取批次号
			if (search_keyWord == "请选择") {
				alert("请选择要修改的属性");
			}
			if (search_editcode == "") {
				alert("请输入要编辑的内容");
			} else if (search_keyWord == "领用人") {
				if (goodsID.length != 0) {
					var data = {
						goodsID : goodsID,
						keyword : search_keyWord,
						editcode : search_editcode,
						goodsBatch : selectBatch
					}
					$.ajax({
						type : "GET",
						url : 'pcInventAsset/BatchupdateAssetLog',
						dataType : "json",
						contentType : "application/json",
						data : data,
						traditional : true,
						success : function(response) {
							$('#edit_modal').modal("hide");
							var type = "";
							var msg = "";
							var append = '';
							if (response.result == "success") {
								type = "success";
								msg = "库存信息更新成功";
							} else {
								type = "error";
								msg = "库存信息更新失败"
							}
							showMsg(type, msg, append);
							tableRefresh();
						},
						error : function(xhr, textStatus, errorThrown) {
							$('#edit_modal').modal("hide");
							handleAjaxError(xhr.status);
						}
					});
				}
			} else if (search_keyWord == "存放地") {
				var sto = "";
				if (goodsID.length != 0) {
					for (var i = 0; i < store.length; i++) {
						if (search_editcode == store[i]) {
							//取出编辑字段
							sto = search_editcode;
						}
					}
					if (sto != "") {
						var data = {
							goodsID : goodsID,
							keyword : search_keyWord,
							editcode : sto,
							goodsBatch : selectBatch
						}
						$.ajax({
							type : "GET",
							url : 'pcInventAsset/BatchupdateAssetLog',
							dataType : "json",
							contentType : "application/json",
							data : data,
							traditional : true,
							success : function(response) {
								$('#edit_modal').modal("hide");
								var type;
								var msg;
								var append = '';
								if (response.result == "success") {
									type = "success";
									msg = "库存信息更新成功";
								} else if (resposne == "error") {
									type = "error";
									msg = "库存信息更新失败"
								}
								showMsg(type, msg, append);
								tableRefresh();
							},
							error : function(xhr, textStatus, errorThrown) {
								$('#edit_modal').modal("hide");
								handleAjaxError(xhr.status);
							}
						});
					} else {
						alert("无此存放地，请重新确认存放地名称！");
					}
				}
			} else if (search_keyWord == "盘点状态") {
				if (goodsID.length != 0) {
					var data = {
						goodsID : goodsID,
						keyword : search_keyWord,
						editcode : search_editcode,
						goodsBatch : selectBatch
					}
					$.ajax({
						type : "GET",
						url : 'pcInventAsset/BatchupdateAssetLog',
						dataType : "json",
						contentType : "application/json",
						data : data,
						traditional : true,
						success : function(response) {
							$('#edit_modal').modal("hide");
							var type;
							var msg;
							var append = '';
							if (response.result == "success") {
								type = "success";
								msg = "库存信息更新成功";
							} else if (resposne.result == "error") {
								type = "error";
								msg = "库存信息更新失败"
							}
							showMsg(type, msg, append);
							tableRefresh();
						},
						error : function(xhr, textStatus, errorThrown) {
							$('#edit_modal').modal("hide");
							handleAjaxError(xhr.status);
						}
					});
				}
			} else if (search_keyWord == "损益类型") {
				var dic = "";
				if (goodsID.length != 0) {
					for (var i = 0; i < dicprofitloss.length; i++) {
						if (search_editcode == dicprofitloss[i]) {
							//取出编辑字段
							dic = search_editcode;
						}
					}
					if (dic != "") {
						var data = {
							goodsID : goodsID,
							keyword : search_keyWord,
							editcode : dic,
							goodsBatch : selectBatch
						}
						$.ajax({
							type : "GET",
							url : 'pcInventAsset/BatchupdateAssetLog',
							dataType : "json",
							contentType : "application/json",
							data : data,
							traditional : true,
							success : function(response) {
								$('#edit_modal').modal("hide");
								var type;
								var msg;
								var append = '';
								if (response.result == "success") {
									type = "success";
									msg = "库存信息更新成功";
								} else if (resposne == "error") {
									type = "error";
									msg = "库存信息更新失败"
								}
								showMsg(type, msg, append);
								tableRefresh();
							},
							error : function(xhr, textStatus, errorThrown) {
								$('#edit_modal').modal("hide");
								handleAjaxError(xhr.status);
							}
						});
					} else {
						alert("无此损益类型，请重新确认损益类型名称！");
					}
				}
			} else {
				return;
			}

		});
		//批量删除
		$('#export_delete').click(function() {
			goodsID = chechedgoodsID();
			selectBatch = select_batch;
			if (goodsID.length != 0) {
				var data = {
					goodsID : goodsID,
					goodsBatch : selectBatch
				}
				$.ajax({
					type : "GET",
					url : "pcInventAsset/Batchdeleteinventory",
					dataType : "json",
					contenType : "application/json",
					data : data,
					traditional : true,
					success : function(response) {
						$('#deleteWarning_modal').modal("hide");
						var type;
						var msg;
						var append = '';
						if (response.result == "success") {
							type = "success";
							msg = "盘点信息删除成功";
						} else {
							type = "erroe";
							msg = "盘点信息删除失败";
						}
						showMsg(type, msg, append);
						tableRefresh();
					},
					error : function(xhr, textStatus, erroeThrown) {
						$('#deleteWarning_modal').modal("hide");
						handleAjaxError(xhr, status);
					}
				});
			}
			$('#deleteWarning_modal').modal('hide');
		})
	}
	//显示下拉编辑的内容
	function edit_bind() {
		$("#search_edit_type").bind(
				"input",
				function() {
					var True = new Array();
					var edty = $("#edit_type").text();
					if (edty == "存放地") {
						$("#wlmslist").find('option').remove();
						True = store;
						for (var i = 0; i < True.length; i++) {
							$("#wlmslist").append(
									'<option " value="'+True[i]+'"></option>');
						}
					} else if (edty == "盘点状态") {
						$("#wlmslist").find('option').remove();
						True = inventorystatus;
						for (var i = 0; i < True.length; i++) {
							$("#wlmslist").append(
									'<option " value="'+True[i]+'"></option>');
						}
					} else if (edty == "损益类型") {
						$("#wlmslist").find('option').remove();
						True = dicprofitloss;
						for (var i = 0; i < True.length; i++) {
							$("#wlmslist").append(
									'<option " value="'+True[i]+'"></option>');
						}
					} else {
						;
					}
				});
	}
</script>
<div class="panel panel-default">
	<ol class="breadcrumb">
		<li>资产盘点</li>
	</ol>
	<div class="panel-body">
		<input type="hidden" id="inventoryBatchParams"
			value="<%=request.getParameter("inventoryBatch")%>"> <input
			type="hidden" id="inventoryStatusParams"
			value="<%=request.getParameter("status")%>">
		<!-- 搜索组件 -->
		<!-- <div class="row">
			<div class="col-md-1  col-sm-2">
				<div class="btn-group">
					<button class="btn btn-default dropdown-toggle"
						data-toggle="dropdown">
						<span id="search_type">所有</span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(0)" class="dropOption">所有</a></li>
						<li><a href="javascript:void(0)" class="dropOption">资产编号</a></li>
						<li><a href="javascript:void(0)" class="dropOption">资产名称</a></li>
						<li><a href="javascript:void(0)" class="dropOption">领用人</a></li>
					   
						<li><a href="javascript:void(0)" class="dropOption">存放地</a></li>
						<li><a href="javascript:void(0)" class="dropOption">损溢类型</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-5 col-sm-5">
				<div>
					<div class="col-md-3 col-sm-3">
						<input id="search_input_type" type="text" class="form-control"
							placeholder="查询内容">
					</div>
					<div class="col-md-2 col-sm-2">
						<button id="search_button" class="btn btn-success">
							<span class="glyphicon glyphicon-search"></span> <span>查询</span>
						</button>
					</div>
				</div>
			</div>
		</div>
 -->		<!-- 批量操作组件 -->
		<div class="row" style="margin-top: 25px" id="edit_menu">
			<div class="col-md-1  col-sm-2">
				<div class="btn-group">
					<button class="btn btn-default dropdown-toggle"
						data-toggle="dropdown">
						<span id="edit_type">请选择</span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(0)" class="dropOption">请选择</a></li>
						<li><a href="javascript:void(0)" class="dropOption">领用人</a></li>
						<%--<li><a href="javascript:void(0)" class="dropOption">部门</a></li> --%>
						<li><a href="javascript:void(0)" class="dropOption">存放地</a></li>
						<li><a href="javascript:void(0)" class="dropOption">盘点状态</a></li>
						<li><a href="javascript:void(0)" class="dropOption">损益类型</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div class="col-md-3 col-sm-3">
					<input type="text" class="form-control" list="wlmslist"
						id="search_edit_type" name="search_edit_type" placeholder="编辑内容">
					<datalist id="wlmslist">
					</datalist>
				</div>
				<div class="col-md-2 col-sm-2">
					<button class="btn btn-sm btn-primary" id="export_edit">
						<span class="glyphicon glyphicon-pencil"></span> <span>批量修改</span>
					</button>
				</div>
				<div class="col-md-2 col-sm-2">
					<button class="btn btn-sm btn-danger" id="export_delete">
						<span class="glyphicon glyphicon-trash"></span> <span>批量删除</span>
					</button>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 15px">
			<div class="col-md-12">
				<table id="storageList" class="table table-striped"></table>
			</div>
		</div>
	</div>
</div>

<!-- 删除提示模态框 -->
<div class="modal fade" id="deleteWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认删除该条库存信息</h3>
						<p>(注意：一旦删除该条库存信息，将不能恢复)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="delete_confirm">
					<span>确认删除</span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- 关闭开关提示模态框 -->
<div class="modal fade" id="closeWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认结束此次盘点</h3>
						<p>(注意：一旦结束此次盘点，将不能恢复)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="close_confirm">
					<span>确认结束</span>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 编辑库存模态框 -->
<div id="edit_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">资产盘点信息</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="storage_form_edit"
							style="margin-top: 25px">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产编号：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_assetID_edit" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产名称：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_assetName_edit" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>存放地：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="storage_assetStore_edit">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>领用人：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="storage_assetPerson_edit" name="storage_assetPerson"
										placeholder="领用人">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>损溢类型：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control"
										id="storage_assetDicProfitType_edit">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>盘点状态：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="storage_Type_edit">
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-success" type="button" id="edit_modal_submit">
					<span>确认更改</span>
				</button>
			</div>
		</div>
	</div>
</div>