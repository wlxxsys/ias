<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>导入资产表</title>
<script>
	var import_step = 1;
	var import_start = 1;
	var import_end = 3;
	$(function() {

		importAssetInfoAction();
		inloadingSystem();
		exportGoodsAction();
	})

	// 导入货物信息
	function importAssetInfoAction() {
		$('#import_AssetInfo').click(function() {
			$('#import_modal').modal("show");
		});

		$('#previous').click(function() {
			if (import_step > import_start) {
				var preID = "step" + (import_step - 1)
				var nowID = "step" + import_step;

				$('#' + nowID).addClass("hide");
				$('#' + preID).removeClass("hide");
				import_step--;
			}
		})

		$('#next').click(function() {
			if (import_step < import_end) {
				var nowID = "step" + import_step;
				var nextID = "step" + (import_step + 1);

				$('#' + nowID).addClass("hide");
				$('#' + nextID).removeClass("hide");
				import_step++;
			}
		})

		$('#file').on("change", function() {
			$('#previous').addClass("hide");
			$('#next').addClass("hide");
			$('#submit').removeClass("hide");
		})

		$('#submit').click(
				function() {
					var nowID = "step" + import_end;
					$('#' + nowID).addClass("hide");
					$('#uploading').removeClass("hide");

					// next
					$('#confirm').removeClass("hide");
					$('#submit').addClass("hide");

					// ajax
					$.ajaxFileUpload({
						url : "PCHanerExcel/importAssetInfoExcel",
						secureuri : false,
						dataType : 'json',
						fileElementId : "file",
						success : function(response, status) {
							var total = 0; // 导入总数
							var importSuccess = 0;// 导入的有效数据总数
							var errorCount = 0; //导入错误数据数
							var repeat = 0; // 导入的重复数据个数
							var msg1 = "资产信息导入成功";
							var msg2 = "资产信息导入失败";
							var msg3 = "上传文件格式不符合要求，请使用模板导入";
							var info;
							if (response.result == "success") {

								$.each(response.errerImportInfo, function(
										index, elem) {
									$("#errorImportInfo").append(
											"<tr><td>" + elem.imUnitName
													+ "</td><td>"
													+ elem.imAssetRange
													+ "</td><td>"
													+ elem.imDocumentNum
													+ "</td><td>"
													+ elem.imAssetName
													+ "</td><td>"
													+ elem.imAssetModel
													+ "</td><td>"
													+ elem.imAssetPrice
													+ "</td><td>"
													+ elem.imFactory
													+ "</td><td>"
													+ elem.imBuyDate
													+ "</td><td>"
													+ elem.imTakePeople
													+ "</td><td>"
													+ elem.imRemark
													+ "</td></tr>");
								});

							}

							$('#import_progress_bar').addClass("hide");
							if (response.result == "success") {
								total = response.total;
								importSuccess = response.importSuccess;
								errorCount = response.errorCount;
								repeat = response.repeat;
								info = msg1;
								$('#import_success').removeClass('hide');
								info = info + ",总条数：" + total + ",有效条数:"
										+ importSuccess + ",重复条数:" + repeat
										+ ",失败条数:" + errorCount;
							} else {
								if (response.msg == "errorFormal") {
									info = msg3;
									$('#import_error').removeClass('hide');
								} else {
									info = msg2;
									$('#import_error').removeClass('hide');
								}
							}
							$('#import_result').removeClass('hide');
							$('#import_info').text(info);
							$('#confirm').removeClass('disabled');
						},
						error : function(response, status) {
							// handler error
							handleAjaxError(status);
						}
					})
				})


		$('#confirm').click(function() {
			// modal dissmiss
			importModalReset();
		})

		// 导入货物模态框重置
		function importModalReset() {
			var i;
			for (i = import_start; i <= import_end; i++) {
				var step = "step" + i;
				$('#' + step).removeClass("hide")
			}
			for (i = import_start; i <= import_end; i++) {
				var step = "step" + i;
				$('#' + step).addClass("hide")
			}
			$('#step' + import_start).removeClass("hide");

			$('#').removeClass("hide");
			$('#import_result').removeClass("hide");
			$('#import_success').removeClass('hide');
			$('#import_error').removeClass('hide');
			$('#import_progress_bar').addClass("hide");
			$('#import_result').addClass("hide");
			$('#import_success').addClass('hide');
			$('#import_error').addClass('hide');
			$('#import_info').text("");
			$('#file').val("");

			$('#previous').removeClass("hide");
			$('#next').removeClass("hide");
			$('#submit').removeClass("hide");
			$('#confirm').removeClass("hide");
			$('#submit').addClass("hide");
			$('#confirm').addClass("hide");

			$('#file').on("change", function() {
				$('#previous').addClass("hide");
				$('#next').addClass("hide");
				$('#submit').removeClass("hide");
			})

			import_step = 1;
		}
	}

	// 转存入盘点系统
	function inloadingSystem() {

		$("#inAssetInfoLoading").click(function() {

			$('#onloading').removeClass("hide");
			$('#inLoad_modal').modal("show");

			$.ajax({
				url : "PCHanerExcel/inLoadToAsset",
				type : "GET",
				dataType : "json",
				success : function(response) {
					var total = 0;
					var info;
					var msg1 = "资产信息转存成功";
					var msg2 = "资产信息转存失败";
					$('#onloading_progress_bar').addClass("hide");
					if (response.result == "success") {
						total = response.total;
						info = msg1;
						$('#onloading_success').removeClass('hide');
					} else {
						info = msg2;
						$('#onloading_error').removeClass('hide');
					}
					//alert(total);
					info = info + ",总条数：" + total;
					$('#onloading_result').removeClass('hide');
					$('#onloading_info').text(info);
				},
				error : function(response, status) {
					handleAjaxError(status);
				}
			})
		});
	}

	
	// 导出错误报表触发事件
	function exportGoodsAction() {
		
		$('#errorInAssetInfoLoading').click(function() {
			exportForm();
		})
	}
	
	// 获取导出报表数据
	function exportForm() {
		var url = "PCHanerExcel/exportErrorAssets";
        //创建一个a标签进行下载
        var a = document.createElement('a');
        a.href = url;
        $('body').append(a);  // 修复firefox中无法触发click
        a.click();
        $(a).remove();
	}
</script>

</head>
<body>

	<div class="panel panel-default" id="body">
		<ol class="breadcrumb">
			<li>导入资产信息管理</li>
		</ol>
		<div class="row" style="margin-top: 25px">
			<button class="btn btn-sm btn-default" id="import_AssetInfo"
				style="margin-left: 50px; margin-bottom: 20px">
				<span class="glyphicon glyphicon-import"></span> <span>上传文件</span>
			</button>

			<button class="btn btn-sm btn-default" id="inAssetInfoLoading"
				style="margin-left: 30px; margin-bottom: 20px">
				<span>转存进盘点系统</span>
			</button>

			<button class="btn btn-sm btn-default" id="errorInAssetInfoLoading"
				style="margin-left: 30px; margin-bottom: 20px">
				<span>导出失败资产</span>
			</button>
		</div>
		<div class="panel panel-default">
			<ol class="breadcrumb">
				<li>导入失败资产信息</li>
			</ol>
		</div>
		<div class="row" style="margin-top: 15px">
			<div class="col-md-12">
				<table id="errorImportInfo" class="table table-striped">
					<tr>
						<th>单位名称</th>
						<th>资产编号范围</th>
						<th>单据号</th>
						<th>资产名称</th>
						<th>型号规格</th>
						<th>单价</th>
						<th>厂家</th>
						<th>购置日期</th>
						<th>领用人</th>
						<th>备注</th>
					</tr>

				</table>

			</div>
		</div>
	</div>


	<!-- 导入货物信息模态框 -->
	<div class="modal fade" id="import_modal" table-index="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
		data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" type="button" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">导入资产信息</h4>
				</div>
				<div class="modal-body">
					<div id="step1">
						<div class="row" style="margin-top: 15px">
							<div class="col-md-1 col-sm-1"></div>
							<div class="col-md-10 col-sm-10">
								<div>
									<h4>点击下面的下载按钮，下载导入电子表格模板</h4>
								</div>
								<div style="margin-top: 30px; margin-buttom: 15px">

									<a class="btn btn-info" download="电子表格模板.xlsx"
										href="${pageContext.request.contextPath}/file/download/error_import_AssetInfo.xlsx"
										target="_blank"> <span
										class="glyphicon glyphicon-download"></span> <span>下载</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div id="step2">
						<div class="row" style="margin-top: 15px">
							<div class="col-md-1 col-sm-1"></div>
							<div class="col-md-10 col-sm-10">
								<div>
									<h4>请按照电子表格中指定的格式填写需要添加的一个或多个资产信息</h4>
								</div>
								<div class="alert alert-info"
									style="margin-top: 10px; margin-buttom: 30px">
									<p>注意：表格中除损益类型以外，都不能为空，否则则该条信息将不能成功导入；</p>
									<p>支持.xlsx结尾的表格，不是还请先另存为该格式</p>
								</div>
							</div>
						</div>
					</div>
					<div id="step3" class="hide">
						<div class="row" style="margin-top: 15px">
							<div class="col-md-1 col-sm-1"></div>
							<div class="col-md-8 col-sm-10">
								<div>
									<div>
										<h4>请点击下面上传文件按钮，上传填写好的资产信息电子表格</h4>
									</div>
									<div style="margin-top: 30px; margin-buttom: 15px">
										<span class="btn btn-info btn-file"> <span> <span
												class="glyphicon glyphicon-upload"></span> <span>上传文件</span>
										</span>
											<form id="import_file_upload">
												<input type="file" id="file" name="file">
											</form>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="hide" id="uploading">
						<div class="row" style="margin-top: 15px" id="import_progress_bar">
							<div class="col-md-1 col-sm-1"></div>
							<div class="col-md-10 col-sm-10"
								style="margin-top: 30px; margin-bottom: 30px">
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-success"
										role="progreessbar" aria-valuenow="60" aria-valuemin="0"
										aria-valuemax="100" style="width: 100%;">
										<span class="sr-only">请稍后...</span>
									</div>
								</div>
								<!-- 
							<div style="text-align: center">
								<h4 id="import_info"></h4>
							</div>
							 -->
							</div>
							<div class="col-md-1 col-sm-1"></div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-4"></div>
							<div class="col-md-4 col-sm-4">
								<div id="import_result" class="hide">
									<div id="import_success" class="hide"
										style="text-align: center;">
										<img src="media/icons/success-icon.png" alt=""
											style="width: 100px; height: 100px;">
									</div>
									<div id="import_error" class="hide" style="text-align: center;">
										<img src="media/icons/error-icon.png" alt=""
											style="width: 100px; height: 100px;">
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4"></div>
						</div>
						<div class="row" style="margin-top: 10px">
							<div class="col-md-3 col-sm-3"></div>
							<div class="col-md-6 col-sm-6" style="text-align: center;">
								<h4 id="import_info"></h4>
							</div>
							<div class="col-md-3 col-sm-3"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn ben-default" type="button" id="previous">
						<span>上一步</span>
					</button>
					<button class="btn btn-success" type="button" id="next">
						<span>下一步</span>
					</button>
					<button class="btn btn-success hide" type="button" id="submit">
						<span>&nbsp;&nbsp;&nbsp;提交&nbsp;&nbsp;&nbsp;</span>
					</button>
					<button class="btn btn-success hide disabled" type="button"
						id="confirm" data-dismiss="modal">
						<span>&nbsp;&nbsp;&nbsp;确认&nbsp;&nbsp;&nbsp;</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	<!-- 转存模态框 -->
	<div class="modal fade" id="inLoad_modal" table-index="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
		data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" type="button" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">转存资产信息</h4>
				</div>
				<div class="modal-body">

					<div class="hide" id="onloading">
						<div class="row" style="margin-top: 15px"
							id="onloading_progress_bar">
							<div class="col-md-1 col-sm-1"></div>
							<div class="col-md-10 col-sm-10"
								style="margin-top: 30px; margin-bottom: 30px">
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-success"
										role="progreessbar" aria-valuenow="60" aria-valuemin="0"
										aria-valuemax="100" style="width: 100%;">
										<span class="sr-only">请稍后...</span>
									</div>
								</div>
							</div>
							<div class="col-md-1 col-sm-1"></div>
						</div>
						<div class="row" style="margin-top: 10px">
							<div class="col-md-3 col-sm-3"></div>
							<div class="col-md-6 col-sm-6" style="text-align: center;">
								<h4 id="onloading_info"></h4>
							</div>
							<div class="col-md-3 col-sm-3"></div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-4"></div>
							<div class="col-md-4 col-sm-4">
								<div id="onloading_result" class="hide">
									<div id="onloading_success" class="hide"
										style="text-align: center;">
										<img src="media/icons/success-icon.png" alt=""
											style="width: 100px; height: 100px;">
									</div>
									<div id="onloading_error" class="hide"
										style="text-align: center;">
										<img src="media/icons/error-icon.png" alt=""
											style="width: 100px; height: 100px;">
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4"></div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>