<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script>
	var select_college = "";
	var selectDepartName = "";
	$(function() {
		getAllCollege();//获取所有学校显示在下拉列表中
		searchCollege();//查询学校下的所有部门
		storageListInit();// 表格初始化
		formValidator();//验证表单
		editStorageAction();// 编辑部门信息
		deleteStorageAction();// 刪除部门信息
		addDepart();// 添加部门

	})
	//获取本部门后端所有学校显示在下拉列表中
	function getAllCollege() {
		var selections = document.getElementById("get_college");
		$.ajax({
			type : "GET",
			url : 'PCCollegeHandler/listCollege',
			success : function(response) {
				//alert(response.data);
				var batch = new Array();
				batch = response.data;
				var option = document.createElement("option");
				for (var i = 0; i < batch.length; i++) {
					option = document.createElement("option");
					option.value = batch[i];
					option.text = batch[i];
					selections.options.remove(option);
				}
				option.value = "请选择学校";
				option.text = "请选择学校";
				selections.options.add(option);
				for (var i = 0; i < batch.length; i++) {
					option = document.createElement("option");
					option.value = batch[i];
					option.text = batch[i];
					selections.options.add(option);
				}
				$("#present_batch").val(batch[batch.length - 1]);
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}

	//按学校查询部门
	function searchCollege() {
		$("#button").click(function() {
			select_college = $('#get_college option:selected').text();//选中的文本
			if (select_college == "请选择学校") {
				alert("请选择你所在学校");
			} else {
				tableRefresh();
			}

		})
	}

	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			collegeName : select_college
		}
		return temp;
	}
	//表格刷新
	function tableRefresh() {
		$('#storageList').bootstrapTable('refresh', {
			query : {}
		});
	}

	// 表格初始化
	function storageListInit() {
		$('#storageList')
				.bootstrapTable(
						{
							columns : [
									{
										field : 'departName',
										title : '部门名称'
									},
									{
										field : 'collegeName',
										title : '学校名称'
									},
									{
										field : 'departRemark',
										title : '备注'
									},
									{
										field : 'operation',
										title : '操作',
										formatter : function(value, row, index) {
											var s = '<button class="btn btn-info btn-sm edit"><span>修改</span></button>';
											var d = '<button class="btn btn-danger btn-sm delete"><span>删除</span></button>';
											var fun = '';
											return s + ' ' + d;
										},
										events : {
											// 操作列中编辑按钮的动作
											'click .edit' : function(e, value,
													row, index) {
												selectDepartName = row.departName;
												rowEditOperation(row);
											},
											'click .delete' : function(e,
													value, row, index) {
												selectDepartName = row.departName;
												$('#deleteWarning_modal')
														.modal('show');
											}
										}
									} ],
							url : 'PCDepartInfoHandler/listDepart',
							onLoadError : function(status) {
								handleAjaxError(status);
							},
							method : 'GET',
							queryParams : queryParams,
							sidePagination : "server",
							dataType : 'json',
							pagination : true,
							pageNumber : 1,
							pageSize : 5,
							pageList : [ 5, 10, 25, 50, 100 ],
							paginationLoop : true,
							//sortable: true,
							clickToSelect : true
						});
	}

	// 添加部门信息
	function addDepart() {
		$('#add_depart').click(function() {
			$('#add_college').val(select_college);
			$('#add_modal').modal("show");
		});
		$('#add_modal_submit').click(
				function() {
					var data = {
						departName : $("#add_departname").val(),
						collegeName : select_college,
						departRemark : $("#add_remrk").val()
					}
					$.ajax({
						url : "PCDepartInfoHandler/addDepart",
						type : "post",
						dataType : "json",
						contentType : "application/json; charset=utf-8",
						data : JSON.stringify(data),
						success : function(response) {

							$('#add_modal').modal("hide");
							var msg;
							var type;
							var append = '';
							if (response.result == "success") {
								type = "success";
								msg = "部门添加成功";
							} else if (response.result == "error") {

								type = "error";
								msg = response.msg;
							}
							showMsg(type, msg, append);
							tableRefresh();

							// reset
							$('#add_departname').val("");
							$('#add_remrk').val("");
							$('#add_depart_form').bootstrapValidator(
									"resetForm", true);
						},
						error : function(xhr, textStatus, errorThrown) {
							$('#add_modal').modal("hide");
							// handler error
							handleAjaxError(xhr.status);
						}
					});
				});
	}
	// 行编辑操作
	function rowEditOperation(row) {

		$('#edit_modal').modal("show");

		$('#edit_depart_form').bootstrapValidator("resetForm", true);
		$('#edit_college').val(row.collegeName);
		$('#edit_departname').val(row.departName);
		$('#edit_remrk').val(row.departRemark);
	}
	// 编辑部门信息
	function editStorageAction() {
		$('#edit_modal_submit').click(function() {

			$('#edit_depart_form').data('bootstrapValidator').validate();
			if (!$('#edit_depart_form').data('bootstrapValidator').isValid()) {
				return;
			}

			var data = {
				oldDepartName : selectDepartName,
				newDepartName : $('#edit_departname').val(),
				departRemark : $('#edit_remrk').val(),
				collegeName : select_college
			}
			$.ajax({
				type : "POST",
				url : 'PCDepartInfoHandler/updateDepart',
				dataType : "json",
				contentType : "application/json",
				data : JSON.stringify(data),
				success : function(response) {
					$('#edit_modal').modal("hide");
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = response.msg;
					} else if (response.result == "error") {
						type = "error";
						msg = response.msg;
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#edit_modal').modal("hide");
					// handle error
					handleAjaxError(xhr.status);
				}
			});
		});
	}
	function formValidator() {
		$("#add_depart_form").bootstrapValidator({
			message : 'This value is not valid',
			feedbackIcons : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				departName : {
					message : 'The departName is not valid',
					validators : {
						notEmpty : {
							message : '部门名称不能为空'
						}
					}
				}
			}
		});
	}

	// 刪除部门信息
	function deleteStorageAction() {
		$('#delete_confirm').click(function() {
			var data = {
				selectDepartName : selectDepartName
			}
			$.ajax({
				type : "GET",
				url : "PCDepartInfoHandler/deleteObject",
				dataType : "json",
				contentType : "application/json",
				data : data,
				success : function(response) {
					$('#deleteWarning_modal').modal("hide");
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = response.msg;
					} else {
						type = "error";
						msg = response.msg;
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#deleteWarning_modal').modal("hide");
					handleAjaxError(xhr.status);
				}
			});

			$('#deleteWarning_modal').modal('hide');
		})
	}
</script>
<div class="panel panel-default">
	<ol class="breadcrumb">
		<li>部门管理</li>
	</ol>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-7 col-sm-7">

				<div class="col-md-5 col-sm-5">
					<select id="get_college" class="form-control">
					</select>
				</div>
				<div class="col-md-2 col-sm-2">
					<button id="button" class="btn btn-success">
						<span class="glyphicon glyphicon-search"></span> <span>查询</span>
					</button>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 25px">
			<div class="col-md-5">
				<button class="btn btn-sm btn-default" id="add_depart">
					<span class="glyphicon glyphicon-plus"></span> <span>添加部门</span>
				</button>
			</div>
			<div class="col-md-5"></div>
		</div>
		<div class="row" style="margin-top: 15px">
			<div class="col-md-12">
				<table id="storageList" class="table table-striped"></table>
			</div>
		</div>
	</div>
</div>

<!-- 删除提示模态框 -->
<div class="modal fade" id="deleteWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认删除该部门</h3>
						<p>(注意：一旦删除该部门，与他相关的所有信息也将删除)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="delete_confirm">
					<span>确认删除</span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- 添加部门信息模态框 -->
<div id="add_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">添加部门</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="add_depart_form"
							style="margin-top: 25px">

							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>部门名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="add_departname"
										name="add_departname" placeholder="部门名称">
								</div>
							</div>

							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>学校名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" disabled="disabled" class="form-control"
										id="add_college" name="add_college" placeholder="学校名称">
								</div>
							</div>

							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>备注：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<textarea rows="8" cols="30" id="add_remrk"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-success" type="button" id="add_modal_submit">
					<span>提交</span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- 编辑库存模态框 -->
<div id="edit_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">部门信息</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="edit_depart_form"
							style="margin-top: 25px">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>部门名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="edit_departname"
										name="edit_departname" placeholder="部门名称">
								</div>
							</div>

							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>学校名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" disabled="disabled" class="form-control"
										id="edit_college" name="edit_college" placeholder="学校名称">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>备注：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<textarea rows="8" cols="30" id="edit_remrk"></textarea>
								</div>
							</div>

						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-success" type="button" id="edit_modal_submit">
					<span>确认更改</span>
				</button>
			</div>
		</div>
	</div>
</div>