<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
.alert {
	display: none;
	position: fixed;
	top: 50%;
	left: 50%;
	min-width: 300px;
	max-width: 600px;
	transform: translate(-50%,-50%);
	z-index: 99999;
	text-align: center;
	padding: 15px;
	border-radius: 3px;
}

.alert-success {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
}

</style>

<script>
	var selector_college_name = "";// 选择的学校名称
	var selector_depart_name = "";//选择的部门名称
	var selector_depart_name_edit = "";//选择的编辑框中的部门名称
	var selector_depart_name_add = "";// 选择的添加框中国的部门名称
	var search_keyWord = "none";
	var search_value = "";
	var select_goodsID;
	var key_storeName;
	var search_depart = "none";
	var store=new Array();//保存所有的存放地名称
	var dicprofitloss=new Array();//保存所有的损益类型名称
	var depart=new Array();//保存所有的部门名称
	var inventorystatus=new Array();//保存所有的盘点状态名称
	$(function() {
		optionAction();
		optionActionedit();
		searchAction();
		//getdepart();	//获取所有部门（查询按钮边上）
		getadd_depart();	//获取所有部门（添加框）
		getdepart_edit()//获取所有部门（编辑框）
		storageListInit();
		bootstrapValidatorInit();
		datePickerInit();
		//search_store();	//获取所有存放地（编辑框）
		//getadd_store();//获取所有存放地（添加框）
		addStorageAction();	//添加资产信息
		edit_bind();//模糊提示存放地
		
		
		getAlldicProfitLoss();	//获取所有损益类型（添加框）
		getdicprofitloss();	//获取所有损益类型（编辑框）
		editStorageAction();	//编辑
		deleteStorageAction();//删除
		editStorageActionList();//批量编辑
		deleteStorageActionList();//批量删除
		exportAssetInfoAction();	//导出表格
		
		getAllCollege();
		getAllDepart();
		//getAllStoreAdd();//获取所有存放地（添加框）
		//getAllStoreEdit();//获取所有存放地（编辑框）
		
	})
	// 获取选择的学校名称
	$(".selector_college").click(function() {
		selector_college_name = $(this).val();
		$(".selector_depart").html("");
		selector_depart_name = "";
		getAllDepart();
	});
	// 获取选择的部门名称
	$(".selector_depart").click(function() {
		selector_depart_name = $(this).val();
		search_key = $('#depart_selector').val();
	})
	// 获取编辑框中选择的部门名称
	$("#get_depart").click(function(){
		selector_depart_name_edit = $(this).val();
		$("#storage_goodsAddr_edit").html("");
		getAllStoreEdit();
	})
	// 获取添加框中选择的部门名称
	$("#add_depart").click(function(){
		selector_depart_name_add = $(this).val();
		$("#add_goodsAddr").html("");
		getAllStoreAdd();
	})
	//获取登陆者所在学校的所有部门
	function getAllDepart() {
		var data = {
			college : selector_college_name
		}
		$.ajax({
			type : "GET",
			url : 'PCStoreHandler/listDepartName',
			dataType : "json",
			data : data,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				//添加到查询
				var selections = document
						.getElementsByClassName("selector_depart");
				if (batch.length < 1) {
					for (var j = 0; j < selections.length; j++) {
						var option = document.createElement("option");
						option.value = "";
						option.text = "---选择部门---";
						selections[j].options.add(option);
					}
				}

				for (var i = 0; i < batch.length; i++) {
					for (var j = 0; j < selections.length; j++) {
						var option = document.createElement("option");
						option.value = batch[i];
						option.text = batch[i];
						selections[j].options.add(option);
					}
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	
	
	//获取后端所有学校显示在下拉列表中
	function getAllCollege() {
		var selections = document.getElementsByClassName("selector_college");
		$.ajax({
			type : "GET",
			url : 'PCCollegeHandler/listCollege',
			//async :false,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				for (var i = 0; i < batch.length; i++) {
					for (var j = 0; j < selections.length; j++) {
						var option = document.createElement("option");
						option.value = batch[i];
						option.text = batch[i];
						selections[j].options.add(option);
					}
					;
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	
	// 获取编辑模块用户选择部门的存放地
	function getAllStoreEdit() {
		var data = {
			depart : selector_depart_name_edit
		};
		$.ajax({
			url : "storeManage/listStoresByDepart",
			type : "GET", 
			data : data,
			dataType : "json",
			//async : false,
			contentType : "application/json",
			success : function(response) {
				var selections=document.getElementById("storage_goodsAddr_edit");
				if (response.result == "success") {
					if (response.stores.length < 1) {
						var option = document.createElement("option");
						option.value = "";
						option.text = "---无存放地---";
						selections.options.add(option);
					}
					$.each(response.stores, function(index, elem) {
						$("#storage_goodsAddr_edit").append(
								"<option value='" + elem.storeName + "'>"
										+ elem.storeName + "存放地</option>");
					});
				} else if (response.result == "error") {
					var option = document.createElement("option");
					option.value = "";
					option.text = "---存放地---";
					selections.options.add(option);
				}

			},
			error : function(response) {
				//alert("error");
				//$("#store_selector").append(
				//	"<option value='-1'>加载失败</option>");
			}
		})
	}
	
	// 获取添加框中用户选择部门的所有存放地
	function getAllStoreAdd() {
		var data = {
			depart : selector_depart_name_add
		};
		$.ajax({
			url : "storeManage/listStoresByDepart",
			type : "GET", 
			data : data,
			dataType : "json",
			//async : false,
			contentType : "application/json",
			success : function(response) {
				var selections=document.getElementById("add_goodsAddr");
				if (response.result == "success") {
					if (response.stores.length < 1) {
						var option = document.createElement("option");
						option.value = "";
						option.text = "---无存放地---";
						selections.options.add(option);
					}
					$.each(response.stores, function(index, elem) {
						$("#add_goodsAddr").append(
								"<option value='" + elem.storeName + "'>"
										+ elem.storeName + "存放地</option>");
					});
				} else if (response.result == "error") {
					var option = document.createElement("option");
					option.value = "";
					option.text = "---存放地---";
					selections.options.add(option);
				}

			},
			error : function(response) {
				//alert("error");
				//$("#store_selector").append(
				//	"<option value='-1'>加载失败</option>");
			}
		})
	}
	
	//获取所有的存放地名称显示在添加
	function getadd_store(){
		var selections=document.getElementById("add_goodsAddr");
		$.ajax({
			type:"GET",
			url:'pcInventAsset/listStoreName',
			success : function(response) {
				store=response.data;	//接收所有存放地名称
				for(var i =0;i<store.length;i++){
		             option = document.createElement("option");
		                 option.value = store[i];
		                 option.text= store[i];
		             selections.options.add(option);
		         }
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		})
	}
	//获取所有损益类型（添加框）
	function getAlldicProfitLoss(){
		var selections=document.getElementById("add_dicProfitLoss");
		$.ajax({
			type:"GET",
			url:'PCAssetInfoHandler/listdicProfitLoss',
			success : function(response) {
				dicProfitLoss=response.data;
				var option = document.createElement("option");
                option.value = "请选择损益类型";
                option.text= "请选择损益类型";
             	selections.options.add(option);
				for(var i =0;i<dicProfitLoss.length;i++){
		             option = document.createElement("option");
		             option.value = dicProfitLoss[i];
		             option.text= dicProfitLoss[i];
		             selections.options.add(option);
		         }
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	
	//获取所有损益类型（编辑框）
	function getdicprofitloss(){
		var selections=document.getElementById("get_dicprofitloss");
		$.ajax({
			type:"GET",
			url:'PCAssetInfoHandler/listdicProfitLoss',
			success : function(response) {
				dicProfitLoss=response.data;
				var option = document.createElement("option");
                option.value = "请选择损益类型";
                option.text= "请选择损益类型"; 
             	selections.options.add(option);
				for(var i =0;i<dicProfitLoss.length;i++){
		             option = document.createElement("option");
		             option.value = dicProfitLoss[i];
		             option.text= dicProfitLoss[i];
		             selections.options.add(option);
		         }
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	// 下拉框選擇動作
	function optionAction() {
		$(".dropOption").click(function() {
			var type = $(this).text();
			$("#search_type").text(type);
		})
	}
	
	// 搜索动作
	function searchAction() {
		$('#search_button').click(function() {
			search_keyWord = $('#search_type').text();
			var hei=$('#search_input_repository').val();
			if(hei!=""){
				search_value = $('#search_input_type').val();
				<c:if test="${sessionScope.loginUserRole=='超级管理员' }"> search_depart = $('#search_input_repository').val(); </c:if> 
				<c:if test="${sessionScope.loginUserRole!='超级管理员' }"> search_depart = $('#search_input_repository').text(); </c:if> 
				tableRefresh();
			}else{
				alert("请选择查询的部门");	
			}
		});
	}
	
	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			repositoryBelong : search_value,
			keyword : search_keyWord,
			departName : search_depart
		}
		return temp;
	}

	// 表格初始化
	function storageListInit() {
		$('#storageList')
				.bootstrapTable(
						{
							columns : [
								{
								checkbox : true,
								visible : true
							//是否显示复选框  
							},
							{
								field : 'goodsID',
								title : '资产编号'
							},
							{
								field : 'goodsName',
								title : '资产名称'
							},
							{
								field : 'goodsPerson',
								title : '领用人'
							},
							{
								field : 'goodsDepart',
								title : '部门', 
							},
							{
								field : 'goodsType',
								title : '损溢类型',
							},
							{
								field : 'goodsAddr',
								title : '存放地'
							},
							{
								field : 'operation',
								title : '操作',
								formatter : function(value, row, index) {
									var s = '<button class="btn btn-info btn-sm edit"><span>编辑</span></button>';
									var d = '<button class="btn btn-danger btn-sm delete"><span>删除</span></button>';
									var x = '<button class="btn btn-info btn-sm check"><span>详细信息</span></button>';
									var fun = '';
									return s + ' ' + d + ' ' + x;
								},
								events : {
									// 操作列中编辑按钮的动作
									'click .edit' : function(e, value,
											row, index) {
										select_goodsID = row.goodsID;
										rowEditOperation(row);
									},
									'click .delete' : function(e,
											value, row, index) {
										select_goodsID = row.goodsID;
										$('#deleteWarning_modal')
												.modal('show');
									},
									'click .check' : function(e,
											value, row, index) {
										select_goodsID = row.goodsID;
										rowCheckOperation(row);
									}
								}
							} ],
					url : 'PCAssetInfoHandler/query',
					onLoadError : function(status) {
						handleAjaxError(status);
					},
					method : 'GET',
					queryParams : queryParams,
					sidePagination : "server",
					dataType : 'json',
					pagination : true,
					pageNumber : 1,
					pageSize : 5,
					pageList : [ 5, 10, 25, 50, 100 ],
					clickToSelect : true
				});
	}

	// 表格刷新
	function tableRefresh() {
		$('#storageList').bootstrapTable('refresh', {
			query : {}
		});
	}
	//获取所有部门显示在查询旁白的下拉列表中
	<!--
	function getdepart(){
		var selections=document.getElementById("search_input_repository");
		$.ajax({
			type:"GET",
			url:'PCAssetInfoHandler/listdepartNum',
			success : function(response) {
				depart=response.data;
				var option = document.createElement("option");
                //option.value = "请选择部门";
                //option.text= "请选择部门";
             	//selections.options.add(option);
				for(var i =0;i<depart.length;i++){
		             option = document.createElement("option");
		             option.value = depart[i];
		             option.text= depart[i];
		             selections.options.add(option);
		         }
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	-->
	// 编辑资产信息部门下拉框
	function getdepart_edit() {
		var selections=document.getElementById("get_depart");
		$.ajax({
			type:"GET",
			url:'PCAssetInfoHandler/listAdddepartNum',
			success : function(response) {
				depart=response.data;
				var option = document.createElement("option");
                option.value = "请选择部门";
                option.text= "请选择部门";
             	selections.options.add(option);
				for(var i =0;i<depart.length;i++){
		             option = document.createElement("option");
		             option.value = depart[i];
		             option.text= depart[i];
		             selections.options.add(option);
		         }
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	
	//添加框获取所有部门显示在下拉列表中
	function getadd_depart(){
		var selections=document.getElementById("add_depart");
		$.ajax({
			type:"GET",
			url:'PCAssetInfoHandler/listdepartNum',
			success : function(response) {
				var departname=response.data;
				var option = document.createElement("option");
                //option.value = "请选择部门";
               // option.text= "请选择部门";
             	//selections.options.add(option);
				for(var i =0;i<departname.length;i++){
		             option = document.createElement("option");
		             option.value = departname[i];
		             option.text= departname[i];
		             selections.options.add(option);
		         }
			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}
	
	// 行编辑操作
	function rowEditOperation(row) {
		$('#edit_modal').modal("show");
		$('#storage_form_edit').bootstrapValidator("resetForm", true);
		$('#storage_goodsID_edit').text(row.goodsID);
		$('#storage_goodsName_edit').text(row.goodsName);
		$('#storage_goodsPerson_edit').val(row.goodsPerson);
		$('#get_depart').val(row.goodsDepart);
		$('#get_dicprofitloss').val(row.goodsType);
		$('#storage_goodsAddr_edit').val(row.goodsAddr);
	}
	
	// 添加模态框数据校验
	function bootstrapValidatorInit() {
		$("#storage_form").bootstrapValidator({
			message : 'This is not valid',
			feedbackIcons : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			excluded : [ ':disabled' ],
			fields : {
				add_goodsID : {
					validators : {
						notEmpty : {
							message : '资产编号不能为空'
						},
						stringLength: {
                            max: 8,
                            min:8,
                            message: '请输入长度为8位'
                        }
					}
				},
				add_goodsName : {
					validators : {
						notEmpty : {
							message : '资产名称不能为空'
						}
					}
				},
				add_goodsPerson : {
					validators : {
						notEmpty : {
							message : '领用人不能为空，没有可以为*'
						}
					}
				},
				add_repositoryID : {
					validators : {
						notEmpty : {
							message : '资产类型规格不能为空'
						}
					}
				},
				add_goodsprice : {
					validators : {
						notEmpty : {
							message : '单价不能为空'
						}
					}
				},
				add_documentnum : {
					validators : {
						notEmpty : {
							message : '单据号不能为空'
						}
					}
				},
				add_goodsfactory : {
					validators : {
						notEmpty : {
							message : '厂家不能为空'
						}
					}
				},
				add_goodsremrk : {
					validators : {
						notEmpty : {
							message : '备注不能为空'
						}
					}
				}
			}
		})
	}

	// 日期选择器初始化
	function datePickerInit(){
		$('.form_date').datetimepicker({
			format:'yyyy-mm-dd',
			language : 'zh-CN',
			endDate : new Date(),
			weekStart : 1,
			todayBtn : 1,
			autoClose : 1,
			todayHighlight : 1,
			startView : 2,
			forceParse : 0,
			minView:2
		});
	}
	// 查看详细信息操作
	function rowCheckOperation(row) {
		$('#check_modal').modal("show");
		$('#storage_form_check').bootstrapValidator("resetForm", true);
		$('#storage_goodsID_check').text(row.goodsID);
		$('#storage_goodsName_check').text(row.goodsName);
		$('#storage_goodsPerson_check').text(row.goodsPerson);
		$('#storage_goodsDepart_check').text(row.goodsDepart);
		$('#storage_goodsType_check').text(row.goodsType);
		$('#storage_goodsAddr_check').text(row.goodsAddr);
		$('#storage_repositoryID_check').text(row.repositoryID);
		$('#storage_goodsprice_check').text(row.goodsprice);
		$('#storage_documentnum_check').text(row.documentnum);
		$('#storage_goodsbuydate_check').text(row.goodsbuydate);
		$('#storage_goodsfactory_check').text(row.goodsfactory);
		$('#storage_goodsremrk_check').text(row.goodsremrk);
	}

	// 编辑库存信息
	function editStorageAction() {
		$('#edit_modal_submit').click(function() {
			$('#storage_form_edit').data('bootstrapValidator').validate();
			if (!$('#storage_form_edit').data('bootstrapValidator').isValid()) {
				return;
			}
			var data = {
				goodsID : $('#storage_goodsID_edit').text(),
				goodsName : $('#storage_goodsName_edit').text(),
				goodsPerson : $('#storage_goodsPerson_edit').val(),
				goodsDepart : $('#get_depart').val(),
				goodsType : $('#get_dicprofitloss').val(),
				goodsAddr : $('#storage_goodsAddr_edit').val()
			}
			$.ajax({
				type : "POST",
				url : 'PCAssetInfoHandler/updateAssetLog',
				dataType : "json",
				contentType : "application/json",
				data : JSON.stringify(data),
				success : function(response) {
					$('#edit_modal').modal("hide");
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = "库存信息更新成功";
					} else if (response.result == "error") {
						type = "error";
						msg = "库存信息更新失败"
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#edit_modal').modal("hide");
					// handle error
					handleAjaxError(xhr.status);
				}
			});
		});
	}
	// 下拉编辑选项动作
	function optionActionedit() {
		$(".editOption").click(function() {
			var type = $(this).text();
			$("#edit_type").text(type);
		})
	}
	//批量操作获取复选框选中的每行的goodsID
	function chechedgoodsID(){
		var rows = $('table').bootstrapTable('getSelections');
		var goodsID=new Array();
		if (!rows || rows.length == 0) {
	   		alert("您没有选择要操作的数据");
		}
	   	else{
	   		for(var i=0;i<rows.length;i++)
	   			{
	   			goodsID[i]=rows[i].goodsID;
	   			}
	   	}
	   	return goodsID;
	}
	//批量编辑
	function editStorageActionList() {
		var goodsID=new Array(); 
		$('#export_edit').click(function() {
			goodsID=chechedgoodsID(); //获取编辑的ID
			search_keyword = $('#edit_type').text();
		   	search_editcode=$("#edit_input_type").val();//获取编辑的内容
		   	if(search_keyword=="编辑选项")
		   	{
		   		alert("请选择要修改的属性");
		   	}
			if(search_editcode=="")
			{
				alert("请输入要编辑的内容");
			}
			else if(goodsID.length!=0)
			{
				var data={
						goodsID : goodsID,
		  				keyWord : search_keyword,
		  				editcode : search_editcode
		  			}
			  	$.ajax({
					type : "GET",
					url : 'PCAssetInfoHandler/updateAssetInfoList',
					dataType : "json",
					contentType : "application/json",
					data : data,
					traditional: true, 
					success : function(response) {
						$('#edit_modal').modal("hide");
						var type;
						var msg;
						var append = '';
						if (response.result == "success") {
							type = "success";
							msg = "库存信息更新成功";
						} else if (resposne == "error") {
							type = "error";
							msg = "库存信息更新失败"
						}
						showMsg(type, msg, append);
						tableRefresh();
					},
					error : function(xhr, textStatus, errorThrown) {
						$('#edit_modal').modal("hide");
						handleAjaxError(xhr.status);
					}
				});
			}
		else
			{
			return;
			}
		});
	}

	// 刪除库存信息
	function deleteStorageAction() {
		$('#delete_confirm').click(function() {
			var data = {
				"goodsID" : select_goodsID
			}
			$.ajax({
				type : "GET",
				url : "PCAssetInfoHandler/deleteAssetInfo",
				dataType : "json",
				contentType : "application/json",
				data : data,
				success : function(response) {
					$('#deleteWarning_modal').modal("hide");
					var type;
					var msg;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = "库存信息删除成功";
					} else {
						type = "error";
						msg = "库存信息删除失败";
					}
					showMsg(type, msg, append);
					tableRefresh();
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#deleteWarning_modal').modal("hide");
					// handle error
					handleAjaxError(xhr.status);
				}
			})
			$('#deleteWarning_modal').modal('hide');
		})
	}
	
	//显示下拉编辑的内容
	function edit_bind(){
		$("#edit_input_type").bind("input",function(){
			var True=new Array();
			var edty=$("#edit_type").text();
			if(edty=="存放地"){
					$("#wlmslist").find('option').remove();
					True=store;
					for(var i=0;i<True.length;i++)
						{
							$("#wlmslist").append('<option " value="'+True[i]+'"></option>');
						}
			}else if(edty=="损益类型"){
				$("#wlmslist").find('option').remove();
				True=dicProfitLoss;
				for(var i=0;i<True.length;i++)
					{
						$("#wlmslist").append('<option " value="'+True[i]+'"></option>');
					}
				}
		})
	}

	//批量删除
	function deleteStorageActionList() {
		$('#export_delete').click(function(){
			goodsID=chechedgoodsID();
			var data = {
					goodsID : goodsID
				}
			  $.ajax({
				  type:"GET",
				  url:"PCAssetInfoHandler/deleteAssetInfoList",
				  dataType:"json",
				  contenType:"application/json",
				  data:data,
				  traditional: true, 
				  success:function(response){
						$('#deleteWarning_modal').modal("hide");
						var type;
						var msg;
						var append='';
						if(response.result=="success"){
							type="success";
							msg="库存信息删除成功";
						}else{
							type="erroe";
							msg="库存信息删除失败";
						}
						showMsg(type,msg,append);
						tableRefresh();
					},
					error:function(xhr,textStatus,erroeThrown){
						$('#deleteWarning_modal').modal("hide");
						handleAjaxError(xhr,status);
					}
			  })
			$('#deleteWarning_modal').modal('hide');
		})
	}
	// 添加库存信息
	function addStorageAction() {
		$('#add_storage').click(function() {
			$('#add_modal').modal("show");
		});

		$('#add_modal_submit').click(function() {
			$("#storage_form").data("bootstrapValidator").validate();
			if(!$("#storage_form").data("bootstrapValidator").isValid()){
				return;
			}
			var data = {
					goodsID : $('#add_goodsID').val(),
					goodsName : $('#add_goodsName').val(),
					goodsPerson : $('#add_goodsPerson').val(),
					goodsDepart : $('#add_depart').val(),
					goodsType : $('#add_dicProfitLoss').val(),
					goodsAddr : $('#add_goodsAddr').val(),
					repositoryID : $('#add_repositoryID').val(),
					goodsprice : $('#add_goodsprice').val(),
					documentnum : $('#add_documentnum').val(),
					goodsbuydate : $('#add_goodsbuydate').val(),
					goodsfactory : $('#add_goodsfactory').val(),
					goodsremrk : $('#add_goodsremrk').val()
			}
			$.ajax({
				type : "POST",
				url : "PCAssetInfoHandler/addAssetInfo",
				dataType : "json",
				contentType : "application/json",
				data : JSON.stringify(data),
				success : function(response) {
					$('#add_modal').modal("hide");
					var msg;
					var type;
					var append = '';
					if (response.result == "success") {
						type = "success";
						msg = "库存信息添加成功";
					} else if (response.result == "error") {
						if(response.msg == "exit"){
							msg = "该资产已存在";
						}else{
							msg = "库存信息添加失败";
						}
						type = "error";
					}
					showMsg(type, msg, append);
					tableRefresh();
					$('#storage_goodsID').val(""),
					$('#storage_goodsName').val(""),
					$('#storage_goodsPerson').val(""),
					$('#storage_goodsDepart').val(""),
					$('#storage_goodsType').val(""),
					$('#storage_goodsAddr').val(""),
					$('#storage_repositoryID').val(""),
					$('#storage_goodsprice').val(""),
					$('#storage_documentnum').val(""),
					$('#storage_goodsbuydate').val(""),
					$('#storage_goodsfactory').val(""),
					$('#storage_goodsremrk').val(""),
					$('#storage_form').bootstrapValidator("resetForm", true);
				},
				error : function(xhr, textStatus, errorThrown) {
					$('#add_modal').modal("hide");
					// handle error
					handleAjaxError(xhr.status);
				}
			})
		})
	}
	
	// 获取导出报表数据
	function exportForm() {
        var url = 'PCAssetInfoHandler/exportAssetInfo_v1';
        //创建一个a标签进行下载
        var a = document.createElement('a');
        a.href = url+'?keyType='+search_keyWord+'&keyValue='+search_value;
        $('body').append(a);  // 修复firefox中无法触发click
        a.click();
        $(a).remove();
	}
 	function exportAssetInfoAction(){
		$('#export_storage').click(function() {
			exportForm();
		})
	} 
	
</script>
<div class="panel panel-default">
	<ol class="breadcrumb">
		<li>资产信息管理</li>
	</ol>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-1  col-sm-2">
				<div class="btn-group">
					<button class="btn btn-default dropdown-toggle"
						data-toggle="dropdown">
						<span id="search_type">所有</span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(0)" class="dropOption">所有</a></li>
						<li><a href="javascript:void(0)" class="dropOption">资产编号</a></li>
						<li><a href="javascript:void(0)" class="dropOption">资产名称</a></li>
						<li><a href="javascript:void(0)" class="dropOption">领用人</a></li>
					   
						<li><a href="javascript:void(0)" class="dropOption">存放地</a></li>
						<li><a href="javascript:void(0)" class="dropOption">损溢类型</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-9 col-sm-9">
				<div>
					<div class="col-md-3 col-sm-3">
						<input id="search_input_type" type="text" class="form-control"
							placeholder="查询内容">
					</div>
					<shiro:hasRole name="超级管理员">
						<div class="col-md-3 col-sm-3">
							<select style="width: 20;" name="" id="college_selector"
								class="form-control selector_college" border="none" outline="none">
								<option value="">---选择学校---</option>
							</select>
						</div>
						<div class="col-md-3 col-sm-3">
							<select style="width: 20;" name="" id="search_input_repository"
								class="form-control selector_depart" border="none" outline="none">
								<option value="">---选择部门---</option>
							</select>
						</div>
					</shiro:hasRole>
					<div class="col-md-2 col-sm-2">
						<button id="search_button" class="btn btn-success">
							<span class="glyphicon glyphicon-search"></span> <span>查询</span>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 25px">
			<div class="col-md-1  col-sm-2">
				<div class="btn-group">
					<button class="btn btn-default dropdown-toggle"
						data-toggle="dropdown">
						<span id="edit_type">编辑选项</span> <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
					    <li><a href="javascript:void(0)" class="editOption">编辑选项</a></li>
						<li><a href="javascript:void(0)" class="editOption">领用人</a></li>
						<li><a href="javascript:void(0)" class="editOption">存放地</a></li>
						<li><a href="javascript:void(0)" class="editOption">损益类型</a></li>
						 <shiro:hasRole name="超级管理员"><li><a href="javascript:void(0)" class="editOption">部门</a></li></shiro:hasRole>
					</ul>

				</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div class="col-md-3 col-sm-3">
					<input id="edit_input_type" type="text" class="form-control" list="wlmslist"
						placeholder="编辑内容">
						<datalist  id="wlmslist">
                     </datalist>
				</div>
				<div class="col-md-2 col-sm-2">
					<button class="btn btn-sm btn-primary" id="export_edit">
						<span class="glyphicon glyphicon-pencil"></span> <span>批量编辑</span>
					</button>
				</div>
				<div class="col-md-2 col-sm-2">
					<button class="btn btn-sm btn-danger" id="export_delete">
						<span class="glyphicon glyphicon-trash"></span> <span>批量删除</span>
					</button>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 25px">
			<div class="col-md-5">
				<button class="btn btn-sm btn-default" id="add_storage">
					<span class="glyphicon glyphicon-plus"></span> <span>添加资产信息</span>
				</button>
				<button class="btn btn-sm btn-default" id="export_storage">
					<span class="glyphicon glyphicon-export"></span> <span>导出</span>
				</button>
			</div>
			<div class="col-md-5"></div>
		</div>

		<div class="row" style="margin-top: 15px">
			<div class="col-md-12">
				<table id="storageList" class="table table-striped"></table>
			</div>
		</div>
	</div>
</div>

<!-- 添加库存信息模态框 -->
<div id="add_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">添加资产记录</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="storage_form"
							style="margin-top: 25px">

							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产编号：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control" id="add_goodsID"
										name="add_goodsID" placeholder="资产编号">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产名称：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="add_goodsName" name="add_goodsName"
										placeholder="资产名称">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产类型规格：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="add_repositoryID" name="add_repositoryID"
										placeholder="资产类型规格">
								</div>
							</div>
							
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>存放地：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="add_goodsAddr"></select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>领用人：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="add_goodsPerson" name="add_goodsPerson"
										placeholder="领用人">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>部门：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="add_depart"></select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>单价：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="add_goodsprice" name="add_goodsprice"
										placeholder="单价">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>损溢类型：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="add_dicProfitLoss"></select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>单据号：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="add_documentnum" name="add_documentnum"
										placeholder="单据号">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>购置日期：</span>
								</label>
								<div class="col-md-8 col-sm-8">
										<input class="form_date form-control" id="add_goodsbuydate" name="add_goodsbuydate" placeholder="购置日期">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>厂家：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="add_goodsfactory" name="add_goodsfactory"
										placeholder="厂家">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>备注：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<textarea rows="8" cols="30" id="add_goodsremrk"></textarea>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-success" type="button" id="add_modal_submit">
					<span>提交</span>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 删除提示模态框 -->
<div class="modal fade" id="deleteWarning_modal" table-index="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" style="text-align: center;">
						<img src="media/icons/warning-icon.png" alt=""
							style="width: 70px; height: 70px; margin-top: 20px;">
					</div>
					<div class="col-md-8 col-sm-8">
						<h3>是否确认删除该条库存信息</h3>
						<p>(注意：一旦删除该条库存信息，将不能恢复)</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-danger" type="button" id="delete_confirm">
					<span>确认删除</span>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 编辑库存模态框 -->
<div id="edit_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">编辑资产信息</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="storage_form_edit"
							style="margin-top: 25px">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产编号：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_goodsID_edit" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产名称：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_goodsName_edit" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>存放地：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="storage_goodsAddr_edit" name="storage_goodsAddr"></select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>领用人：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<input type="text" class="form-control"
										id="storage_goodsPerson_edit" name="storage_goodsPerson"
										placeholder="领用人">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>部门：</span>
								</label>
								<div class="col-md-8 col-sm-8">
									<select class="form-control" id="get_depart" name="storage_goodsType"></select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>损溢类型：</span>
								</label>
								<div class="col-md-8 col-sm-8">
								    <select class="form-control" id="get_dicprofitloss" name="storage_goodsType"></select>
							    </div>
							</div>
						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="button" data-dismiss="modal">
					<span>取消</span>
				</button>
				<button class="btn btn-success" type="button" id="edit_modal_submit">
					<span>确认更改</span>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- 查看详细信息模态框 -->
<div id="check_modal" class="modal fade" table-index="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">资产详细信息</h4>
			</div>
			<div class="modal-body">
				<!-- 模态框的内容 -->
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<form class="form-horizontal" role="form" id="storage_form_check"
							style="margin-top: 25px">
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产编号：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_goodsID_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产名称：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_goodsName_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>资产类型规格：</span>
								</label>
								<div class="col-md-4 col-sm-4">
									<p id="storage_repositoryID_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>存放地：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsAddr_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>领用人：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsPerson_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>部门：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsDepart_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>单价：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsprice_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>损溢类型：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsType_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>单据号：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_documentnum_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>购置日期：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsbuydate_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>厂家：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsfactory_check" class="form-control-static"></p>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-4 col-sm-4"> <span>备注：</span>
								</label>
								<div class="col-md-4 col-sm-4">
								    <p id="storage_goodsremrk_check" class="form-control-static"></p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-md-1 col-sm-1"></div>
				</div>
			</div>
		</div>
	</div>
</div>