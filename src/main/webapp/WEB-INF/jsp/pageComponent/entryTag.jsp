<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<html>
<head>
<meta charset="UTF-8">
<title>录入标签</title>


<script language="javascript"
	src="${pageContext.request.contextPath}/js/YOWORFIDReaderBase.js"></script>
<script language="javascript"
	src="http://127.0.0.1:8008/YOWOCloudRFIDReader.js"></script>
<script language="javascript"
	src="${pageContext.request.contextPath}/js/RfidConfiguration.js"></script>
<style type="text/css">
#dateId {
	width: 360px;
	margin: 20px auto;
	border: 0px;
	font-size: 1.4em;
}
</style>

</head>

<div class="panel panel-default">
	<ol class="breadcrumb">
		<li>录入标签</li>
	</ol>
	<div class="panel-body">
		<div class="row">
			<div class="container text-center">
				<h2 class="font-weight-bold">NFC读写应用</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="container">
					<br> <br> <br>
					<form class="form-inline">
						<label for="nfcid">NFC卡号:</label> <input name="CardNo" type="text"
							class="form-control" id="CardNo" placeholder="NFC卡号"
							disabled="disabled"><br> <br> <label for="data">数
							&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp据:</label> <input type="text"
							class="form-control" id="DataRead" placeholder="数据"
							disabled="disabled"> <br> <br>

						<div class="col-md-3 col-sm-3 button">
							<button type="button" class="btn btn-primary read" id="readData"
								onclick="ReadBlock()">读卡</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="container">
					<br> <br> <br>
					<form class="form-inline">
						<label for="nfcidwrite">识别数据:</label> <input type="text"
							class="form-control" id="DataWrite" placeholder="请输入资产编号"><br>
						<br>
						<div class="row">
							<div class="col-md-9 col-sm-9">
								<div class="row">
									<shiro:hasRole name="超级管理员">
										<div class="col-md-4 col-sm-4">
											<label for="" class="form-label">选学校：</label> <select name=""
												id="college_selector" class="form-control" border="none"
												outline="none">
											</select>
										</div>
									</shiro:hasRole>
									<shiro:hasRole name="超级管理员">
										<div class="col-md-4 col-sm-4">
											<label for="" class="form-label">选部门：</label> <select name=""
												id="depart_selector" class="form-control" border="none"
												outline="none">
												<option value="none">---部门---</option>
											</select>
										</div>
									</shiro:hasRole>
									<div class="col-md-4 col-sm-4">
										<label for="" class="form-label">选存放地：</label> <select name=""
											id="store_selector" class="form-control">
											<option value="">---存放地---</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</form>

					<div class="col-md-6 col-sm-6">
						<button type="button" class="btn btn-primary write" id="writeData"
							onclick="WriteBlock()">写卡</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top: 15px">
		<div class="col-md-12">
			<table id="assetList" class="table table-striped">
				<tbody id="tbody_result"></tbody>
			</table>
		</div>
	</div>

	<!-- 链接设备异常警示模态框 -->
	<div class="modal fade" id="not_connect_nfc" table-index="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" type="button" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">警告</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3 col-sm-3" style="text-align: center;">
							<img src="media/icons/warning-icon.png" alt=""
								style="width: 70px; height: 70px; margin-top: 20px;">
						</div>
						<div class="col-md-8 col-sm-8">
							<input type="text" name="dateId" id="dateId" value="" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" type="button" data-dismiss="modal">
						<span>重试</span>
					</button>
				</div>
			</div>
		</div>
	</div>

	<!-- 链接云服务异常警示模态框 -->
	<div class="modal fade" id="not_load_cloud" table-index="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" type="button" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">警告</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3 col-sm-3" style="text-align: center;">
							<img src="media/icons/warning-icon.png" alt=""
								style="width: 70px; height: 70px; margin-top: 20px;">
						</div>
						<div>
							<h4>创建服务连接失败，未下载，点击下载！</h4>
							<h4>若以下载，点击重试</h4>
						</div>
						<div style="margin-top: 30px; margin-buttom: 15px">

							<a class="btn btn-info" download="电子表格模板.xlsx"
								href="http://www.youwokeji.com.cn/CloudReader/YOWORFIDReaderCloudForWeb.exe"
								target="_blank"> <span class="glyphicon glyphicon-download"></span>
								<span>下载</span>
							</a>
						</div>
					</div>



				</div>
				<div class="modal-footer">
					<button class="btn btn-default" type="button" data-dismiss="modal"
						id="modelButtonId">
						<span>重试</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function() {
		loadTextarea();
		getAllCollege();
		getAllStore();
		loadrfidReader();
	})
	var selector_college_name = "";// 选择的学校名称
	var selector_depart_name = "";//选择的部门名称
	var selector_store_name = ""; //选择的存放地名称
	var td_num = null;
	var select_asset_num = null;//选择要写入的资产编号

	// 获取选择的学校名称
	$("#college_selector").click(function() {
		selector_college_name = $(this).val();
		$("#depart_selector").html("");
		$("#store_selector").html("");
		selector_depart_name = "";
		getAllDepart();
		getAllStore();
	});
	
	// 获取选择的部门名称
	$("#depart_selector").click(function() {
		selector_depart_name = $(this).val();
		$("#store_selector").html("");
		getAllStore();
	})
	
	// 获取选择的存放地名称
	$('#store_selector').click(function() {
		selector_store_name = $(this).val();
		tableRefresh();
	});
	
	//获取后端所有学校显示在下拉列表中
	function getAllCollege() {
		var selections = document.getElementById("college_selector");
		$.ajax({
			type : "GET",
			url : 'PCCollegeHandler/listCollege',
			//async :false,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				for (var i = 0; i < batch.length; i++) {
					var option = document.createElement("option");
					option.value = batch[i];
					option.text = batch[i];
					selections.options.add(option);
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}

	//获取登陆者所在学校的所有部门
	function getAllDepart() {
		var data = {
			college : selector_college_name
		}
		$.ajax({
			type : "GET",
			url : 'PCStoreHandler/listDepartName',
			dataType : "json",
			//async : false,
			data : data,
			success : function(response) {
				var batch = new Array();
				batch = response.data;
				//添加到查询
				var selections = document.getElementById("depart_selector");
				if (batch.length < 1) {
					var option = document.createElement("option");
					option.value = "";
					option.text = "---部门---";
					selections.options.add(option);
				}
				for (var i = 0; i < batch.length; i++) {
					option = document.createElement("option");
					option.value = batch[i];
					option.text = batch[i];
					selections.options.add(option);
				}

			},
			error : function(xhr, textStatus, errorThrown) {
				$('#edit_modal').modal("hide");
			}
		});
	}

	// 获取登陆者选择部门的存放地
	function getAllStore() {
		var data = {
			depart : selector_depart_name
		};
		$.ajax({
			url : "storeManage/listStoresByDepart",
			type : "GET", 
			data : data,
			dataType : "json",
			//async : false,
			contentType : "application/json",
			success : function(response) {
				var selections = document.getElementById("store_selector");
				if (response.result == "success") {
					if (response.stores.length < 1) {
						var option = document.createElement("option");
						option.value = "";
						option.text = "---存放地---";
						selections.options.add(option);
					}
					$.each(response.stores, function(index, elem) {
						$("#store_selector").append(
								"<option value='" + elem.storeName + "'>"
										+ elem.storeName + "存放地</option>");
					});
				} else if (response.result == "error") {
					var option = document.createElement("option");
					option.value = "";
					option.text = "---存放地---";
					selections.options.add(option);
				}

			},
			error : function(response) {
				//alert("error");
				//$("#store_selector").append(
				//	"<option value='-1'>加载失败</option>");
			}
		})
	}

	// 加载选择存放地的资产
	function loadTextarea() {
		$("#assetList").bootstrapTable({
			locale : "zh-CN",
			columns : [ {
				field : "assetNum",
				title : "资产编号"
			}, {
				field : "assetName",
				title : "资产名称"
			} ],
			url : "PCAssetInfoHandler/listAssetNumsByStore",
			method : 'GET',
			queryParams : queryParams,
			sidePagination : "server",//分页方式，服务器端分页
			dataType : 'json',
			pagination : true,//是否开启分页
			pageNumber : 1,//初始化加载第一页
			pageSize : 5,//每页的记录行数
			pageList : [ 5, 10, 25, 50, 100 ],//可供选择的每页的行数
			clickToSelect : true //是否启用点击选中行

		});

	}

	// 表格刷新
	function tableRefresh() {
		$("#assetList").bootstrapTable('refresh', {
			query : {}
		});
	}

	// 点击获取资产编号
	$("#assetList tbody").on("click", "tr", function() {
		var td = $(this).find("td");
		select_asset_num = td.eq(0).text();
		$("#DataWrite").val(select_asset_num);
	});

	// 分页查询参数
	function queryParams(params) {
		var temp = {
			limit : params.limit,
			offset : params.offset,
			storeName : selector_store_name
		}
		return temp;
	}
	//加载rfid云服务，异常判断
	function loadrfidReader() {
		if (rfidreader == null) {
			var data1 = "创建服务连接失败，若未下载，请先下载安装！若以下载，点击重试";
			$('#not_load_cloud').modal('show');
			$('#modelButtonId').click(function() {
				parent.document.getElementById("entryTag").click();
			});
		}

	}
	
</script>
</html>
