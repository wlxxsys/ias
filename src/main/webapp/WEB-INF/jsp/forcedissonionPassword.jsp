<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/bootstrap-table.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery.mloading.css">

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-table.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.md5.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.mloading.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/mainPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/ajaxfileupload.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
<title>强制修改密码</title>
<script type="text/javascript">
	$(function() {
		update_but(); //修改密码
	})
	
	// 密码加密模块
	function passwordEncrying(password){
		var str1 = $.md5(password);
		//var str2 = $.md5(str1 + userID);
		return str1;
	}
	/*修改密码按钮*/
	function update_but() {
		$("#update_but").click(function() {
			var oldpassword = $("#oldpassword").val();
			var newpassword = $("#newpassword").val();
			var truepassword = $("#truepassword").val();
		
			//验证两次密码是否一致
			if (newpassword == truepassword) {
				//两次密码一致
				//验证密码强度等级
				var Grade = checkPass(truepassword);
				if (Grade == 3) {
					//符合密码强度要求
					var data = {
						oldpassword : oldpassword,
						newpassword : truepassword
					}
					$.ajax({
						url : "Forced",
						type : "post",
						dataType : "json",
						contentType : "application/json",
						data : JSON.stringify(data),
						success : function(response) {
							var result = response.result;
							var msg = response.msg;
							if (result == 'error') {
								//修改失败
								if (msg == "登陆状态异常，请重新登陆") {
									document.location.href = "./index.jsp";
								} else {
									alert(msg);
								}
							} else {
								//修改成功
								alert(msg + ",点击确定返回登陆");
								try {
									document.location.href = "./index.jsp";
								} catch (e) {
									alert("跳转失败");
								}

							}

						},
						error : function(response) {
							alert("请求失败");
						}
					});
				} else if (Grade == -1) {
					alert("密码长度必须大于8位且小于20位");
				} else {
					if (Grade == 0) {
						alert("密码长度必须大于8位且小于20位");
					} else {
						alert("密码必须同时包含：字母、数字、符号");
					}
				}
			} else {
				alert("两次密码不一致！");
				//清空输入框
				$("#oldpassword").html("");
				$("#newpassword").html("");
				$("#truepassword").html("");
			}
		})
	}
	//验证密码复杂度
	function checkPass(password) {
		if (password.length < 8) {
			return 0;
		}
		if (password.length > 20) {
			return -1;
		}
		var grade = 0;
		if (password.match(/([a-z])+/)) {
			grade++;
		}
		if (password.match(/([0-9])+/)) {
			grade++;
		}
		if (password.match(/([A-Z])+/)) {
			grade++;
		}
		if (password.match(/[^a-zA-Z0-9]+/)) {
			grade++;
		}
		return grade;
	}
</script>
</head>
<body>
	<div style="text-align: center;">
		<h1>修改初始密码</h1>
	</div>

	<div class="panel-body">
		<!--  修改密码主体部分 -->
		<div class="row">
			<div class="col-md-4 col-sm-2"></div>
			<div class="col-md-4 col-sm-8">

				<form action="" method="post" class="form-horizontal">
					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4">
							输入原密码: </label>
						<div class="col-md-8 col-sm-8">
							<input type="password" class="form-control" id="oldpassword"
								name="oldpassword">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4">
							输入新密码: </label>
						<div class="col-md-8 col-sm-8">
							<input type="password" class="form-control" id="newpassword"
								name="newpassword">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="control-label col-md-4 col-sm-4">
							确认新密码: </label>
						<div class="col-md-8 col-sm-8 has-feedback">
							<input type="password" class="form-control" id="truepassword"
								name="truepassword">
						</div>
					</div>

					<div>
						<div class="col-md-4 col-sm-4"></div>
						<div class="col-md-4 col-sm-4">
							<button type="button" id="update_but" class="btn btn-success">
								&nbsp;&nbsp;&nbsp;&nbsp;确认修改&nbsp;&nbsp;&nbsp;&nbsp;</button>
						</div>
						<div class="col-md-4 col-sm-4"></div>
					</div>
				</form>
			</div>
			<div class="col-md-4 col-sm-2"></div>
		</div>

		<div class="row">
			<div class="col-md-3 col-sm-1"></div>
			<div class="col-md-6 col-sm-10">
				<div class="alert alert-info" style="margin-top: 50px">
					<p>登录密码修改规则说明：</p>
					<p>1.密码长度为8~20位。</p>
					<p>2.至少包含数字、字母、特殊符号</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-1"></div>
		</div>
	</div>
</body>
</html>