package util;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.xml.internal.org.jvnet.fastinfoset.VocabularyApplicationData;

import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.UserInfoMapper;

/**
 * 
 *  
 * @Description:测试分页插件   
 * @author: maotao 
 * @date:   2019年7月27日 下午3:12:58       
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestPageHelper {

	@Test
	public void testPageHelper() {
		 //1、获得mapper代理对象  
	    //初始化一个spring容器     
	    ApplicationContext   applicationContext = null;
	       try{  
	           //获得spring上下文对象  
	           applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");  
	         }  
	         catch (Exception ex) {  
	          ex.printStackTrace();  
	         } 
	       // 拿到一个代理对象 
	       UserInfoMapper bean = applicationContext.getBean(UserInfoMapper.class);
	       
	       //2、设置分页处理 
	       PageHelper.offsetPage(1, 5);
	       //执行查询
	       List<UserInfo> list = bean.listUserByDepartNum("1");
	       if(list != null & list.size()>0){  
	           int i = 0;  
	           for(UserInfo item : list){  
	               System.out.println(item.getUserCount()+","+(i+1));//输出商品的标题，一页20行  
	               i++;  
	           }  
	       } 
	       //4、取分页后的结果  
	       //包装list       
	       PageInfo<UserInfo> pageInfo = new PageInfo<>(list);  
	       long total = pageInfo.getTotal();//总记录数  
	       System.out.println("total:"+total);  
	       int pages = pageInfo.getPages();  
	       System.out.println("pages:"+pages);//总页数  
	       int pageSize= pageInfo.getPageSize();  
	       System.out.println("pageSize:"+pageSize);//每页的展示数  
	}
   
    
}
