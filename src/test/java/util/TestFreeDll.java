package util;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Vector;
public class TestFreeDll {
	 static {
		  // 首先确保这些dll文件存在
		  System.load("E://javadata//ias//src//main//java//YW60x.dll");
		 }

		 /**
		  * 卸载已经装载的dll
		  *
		  * @param dllName
		  *            库名，如Decode.dll
		  */
		 

		 private synchronized void freeDll(String dllName) {
		  try {
		   ClassLoader classLoader = this.getClass().getClassLoader();
		   Field field = ClassLoader.class.getDeclaredField("nativeLibraries");
		   field.setAccessible(true);
		   Vector<Object> libs = (Vector<Object>) field.get(classLoader);
		   Iterator<Object> it = libs.iterator();
		   Object o;
		   while (it.hasNext()) {
		    o = it.next();
		    Field[] fs = o.getClass().getDeclaredFields();
		    boolean hasInit = false;
		    for (int k = 0; k < fs.length; k++) {
		     if (fs[k].getName().equals("name")) {
		      fs[k].setAccessible(true);
		      String dllPath = fs[k].get(o).toString();
		      if (dllPath.endsWith(dllName)) {
		       hasInit = true;
		      }
		     }
		    }
		    if (hasInit) {
		     Method finalize = o.getClass().getDeclaredMethod(
		       "finalize", new Class[0]);
		     finalize.setAccessible(true);
		     finalize.invoke(o, new Object[0]);
		     it.remove();       
		     libs.remove(o);
		    }
		   }

		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		 }
		 
		 public static void main(String args[]) {
			 TestFreeDll t = new TestFreeDll();
		 // t.freeDll("YW60x.dll");
		  System.out.println("success");
		  System.load("E://javadata//ias//src//main//java//YW60x.dll");
		  System.out.println();
		 }
}
