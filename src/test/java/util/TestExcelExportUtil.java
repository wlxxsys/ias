package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.metadata.Table;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.util.handlexcel.ExcelExportUtil_;

/**
 * 
 * 
 * @Description:测试export
 * @author: maotao
 * @date: 2019年8月8日 下午7:54:40
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class TestExcelExportUtil {
	@Autowired
	AssetInfoService assetInfoService;

	/**
	 * 
	 * @Title: testExportExcel   
	 * @Description: 测试C/S架构下的导出excel文件到指定路径下   
	 * @param:       
	 * @return: void      
	 * @throws
	 */
	@Test
	public void testExportExcel() {
		HttpServletResponse response = null;
		// System.out.println(FileSystemView.getFileSystemView().getHomeDirectory().getPath());
		// 获取模板文件路径
		String tempPath = Class.class.getClass().getResource("/").getPath() + "\\file\\download\\assetInfo.xlsx";
		// 获取用户桌面路径
		String path = FileSystemView.getFileSystemView().getHomeDirectory().getPath();
		UserInfo loginUser = new UserInfo();
		loginUser.setDepartNum("1");
		// 获取导出信息
		//List<Map<String, Object>> exportInfo = assetInfoService.listExportAssetInfo("资产编号", "11001055", loginUser);
		/*if (exportInfo != null) {
			ExcelExportUtil_ ex = new ExcelExportUtil_();
			ex.exportExcel(tempPath, path, response, exportInfo, "1");
		}*/
	}
	
	@Test
	public void testEasyExcel() throws FileNotFoundException{
		 // 生成EXCEL并指定输出路径
        OutputStream out = new FileOutputStream("E:\\excel.xls");
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX);
 
        // 设置SHEET
        Sheet sheet = new Sheet(1, 0);
        sheet.setSheetName("sheet1");
 
        // 设置标题
        Table table = new Table(1);
        List<List<String>> titles = new ArrayList<List<String>>();
        titles.add(Arrays.asList("用户ID"));
        titles.add(Arrays.asList("名称"));
        titles.add(Arrays.asList("年龄"));
        titles.add(Arrays.asList("生日"));
        table.setHead(titles);
 
        // 查询数据导出即可 比如说一次性总共查询出100条数据
        List<List<String>> userList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            userList.add(Arrays.asList("ID_" + i, "小明" + i, String.valueOf(i), new Date().toString()));
        }
 
        writer.write0(userList, sheet, table);
        writer.finish();
        
       
	}
	  
	@Test
    public void simpleFill() {

        // 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
		String path_1 = System.getProperty("user.dir");
		System.out.println(path_1);
        String templateFileName =
        		path_1 + File.separator +"src"+File.separator +"main"+File.separator +"webapp"+File.separator + "file" + File.separator + "download" + File.separator + "emp.xlsx";
        System.out.println(templateFileName);

        // 方案1 根据对象填充

        String fileName = TestFileUtil.getPath() + "simpleFill1" + System.currentTimeMillis() + ".xlsx";

        // 这里 会填充到第一个sheet， 然后文件流会自动关闭

        /*FillData fillData = new FillData();

        fillData.setName("张三");

        fillData.setNumber(5.2);

        EasyExcel.write(fileName).withTemplate(templateFileName).sheet().doFill(fillData);
*/

        /*// 方案2 根据Map填充

        fileName = TestFileUtil.getPath() + "simpleFill" + System.currentTimeMillis() + ".xlsx";

        // 这里 会填充到第一个sheet， 然后文件流会自动关闭

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("name", "张三");

        map.put("number", 5.2);

        EasyExcel.write(fileName).withTemplate(templateFileName).sheet().doFill(map);*/

    }
	
    @Test
    public void complexFill() {

        // 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替

        // {} 代表普通变量 {.} 代表是list的变量

        String templateFileName =

        		TestFileUtil.getPath() + "file" + File.separator + "download" + File.separator + "emp.xlsx";


        String fileName = TestFileUtil.getPath() + "complexFill" + System.currentTimeMillis() + ".xlsx";

        ExcelWriter excelWriter = EasyExcel.write(fileName).withTemplate(templateFileName).build();

        WriteSheet writeSheet = EasyExcel.writerSheet().build();

        // 这里注意 入参用了forceNewRow 代表在写入list的时候不管list下面有没有空行 都会创建一行，然后下面的数据往后移动。默认 是false，会直接使用下一行，如果没有则创建。

        // forceNewRow 如果设置了true,有个缺点 就是他会把所有的数据都放到内存了，所以慎用

        // 简单的说 如果你的模板有list,且list不是最后一行，下面还有数据需要填充 就必须设置 forceNewRow=true 但是这个就会把所有数据放到内存 会很耗内存

        // 如果数据量大 list不是最后一行 参照下一个

        /*FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();

        excelWriter.fill(data(), fillConfig, writeSheet);

        excelWriter.fill(data(), fillConfig, writeSheet);

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("date", "2019年10月9日13:28:28");

        map.put("total", 1000);

        excelWriter.fill(map, writeSheet);

        excelWriter.finish();*/

    }
    
    /*private List<FillData> data() {
        List<FillData> list = new ArrayList<FillData>();
        for (int i = 0; i < 10; i++) {
            FillData fillData = new FillData();
            list.add(fillData);
            fillData.setName("张三");
            fillData.setNumber(5.2);
        }
        return list;
    }*/

}
