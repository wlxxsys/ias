package util;

import org.apache.log4j.Logger;

public class TestLog4j {

	private static Logger logger = Logger.getLogger(TestLog4j.class);
	
	public static void main(String[] args) {
		
		logger.debug("这是debug 信息");
		logger.info("这是info 信息");
		logger.error("这是error 信息");

	}

}
