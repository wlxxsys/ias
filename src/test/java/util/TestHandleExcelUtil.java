package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.aspectj.internal.lang.annotation.ajcDeclareAnnotation;
import org.jcp.xml.dsig.internal.MacOutputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.mapper.AssetInfoMapper;
import cn.tit.ias.service.AssetInfoService;
import cn.tit.ias.util.HandleExcelUtil;
import cn.tit.ias.util.handlexcel.HandleExcelUtil_;
import jdk.nashorn.internal.runtime.arrays.IteratorAction;

/**
 * 
 * 
 * @Description:测试HandleExcelUtil
 * @author: maotao
 * @date: 2019年6月7日 下午7:10:14
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class TestHandleExcelUtil {

	@Autowired
	AssetInfoMapper mapper;
	@Autowired
	AssetInfoService service;

	@Test
	public void excel() {
		Map<String, List<List<String>>> map;
		try {

			map = new HandleExcelUtil().read("C:\\Users\\admin\\Desktop\\user.xlsx");
			HandleExcelUtil hand = new HandleExcelUtil();
			System.out.println(map);
			//System.out.println(hand.read(map));
		} catch (EncryptedDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@Test
	public void testGetfromTotalExcel()
			throws EncryptedDocumentException, FileNotFoundException, InvalidFormatException, IOException {
		// 测试 getfromTotalAssetExcel()方法
		Map<String, List<Map<String, Object>>> entityPo = new HashMap<String, List<Map<String, Object>>>();
		Map<String, List<List<Object>>> files = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		File file = new File("C:\\Users\\admin\\Desktop\\导入表格模板.xlsx");
		files = HandleExcelUtil_.readExcel(file, 5, 50);
		boolean checkResult = HandleExcelUtil_.checkExcelFormat(files);
		System.out.println(checkResult);
		entityPo = HandleExcelUtil_.getFromTotalAssetExcel(files);
		System.out.println(entityPo.get("sheet"));
		System.out.println(files.get("sheet"));
		list = entityPo.get("sheet");
		Iterator<Map<String, Object>> iterator = list.iterator();
		List<Map<String, Object>> newList = new ArrayList<>();
		while (iterator.hasNext()) {
			Map<String, Object> newMap = new HashMap<>();
			newMap = iterator.next();
			newMap.put("departNum", "1");
			try {
				if (newMap.get("imAssetRange") != "" && newMap.get("imAssetRange") != null
						&& newMap.get("imDocumentNum") != "" && newMap.get("imDocumentNum") != null)
					
				mapper.importAssetToTemportaryTable(newMap);
				newList.add(newMap);
			} catch (Exception e) {
				System.out.println("失败");
				newList.add(newMap);
			}
		}
		System.out.println(newList);
		
	}
	@Test
	public void testGetfromTotalExcel_1() throws EncryptedDocumentException, FileNotFoundException, InvalidFormatException, IOException {
		Map<String, List<Map<String, Object>>> entityPo = new HashMap<String, List<Map<String, Object>>>();
		Map<String, List<List<Object>>> files = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		File file = new File("C:\\Users\\admin\\Desktop\\网络与信息中心1.xlsx");
		files = HandleExcelUtil_.readExcel(file, 5, 50); 
		System.out.println(files.get("sheet"));
		list = entityPo.get("sheet");
		System.out.println(files);
//		Iterator<Map<String, Object>> iterator = list.iterator();
//		List<Map<String, Object>> newList = new ArrayList<>();
//		while (iterator.hasNext()) {
//			Map<String, Object> newMap = new HashMap<>();
//			newMap = iterator.next();
//			newMap.put("departNum", "1");
//			try {
//				if (newMap.get("imAssetRange") != "" && newMap.get("imAssetRange") != null
//						&& newMap.get("imDocumentNum") != "" && newMap.get("imDocumentNum") != null)
//					
//				mapper.importAssetToTemportaryTable(newMap);
//				newList.add(newMap);
//			} catch (Exception e) {
//				System.out.println("失败");
//				newList.add(newMap);
//			}
//		}
//		System.out.println(newList);
	}
	@Test
	public void exportExcle() {
		AssetInfo condition = new AssetInfo();
		condition.setDepartNum("1");
		List<AssetInfo> assets = mapper.listObjectByCondition(condition);
		String[] head ={"资产编号","存放地","部门","单据号","资产名称","规格型号","单价","厂家","购置日期","领用人","备注","损益类型"};
		HandleExcelUtil_.exportExcel(service.packageExportData(assets),head);
	}
	
	
	
	
	
	


}
