package service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.AssetInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestAssetInfoServiceImpl{
	
	@Autowired
	AssetInfoService service;
	@Test
	public void insert()
	{
		AssetInfo a=new AssetInfo();
		a.setAssetNum("11000010");
		a.setAssetName("交换机");
		a.setAssetModel("UTFcs");
		a.setAssetPrice((float)1.11);
		a.setAssetFactory("富士康");
		a.setAssetDocumentNum("1");
		a.setAssetBuyDate("2019-5-20");
		a.setAssetTakePeople("杨老师");
		a.setAssetRemrk("1");
		a.setStoreNum(1);
		a.setDicProfitLossNum(1);
		a.setDepartNum("3");
		service.addObject(a);
		System.out.println("添加成功");
	}
	@Test
	public void get()
	{
		AssetInfo assetInfo =service.getObject("11000010");
		System.out.println(assetInfo.toString());
	}

	@Test
	public void delete(){
		if(service.deleteObject("11000010")==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}
	}
	
	@Test
	public void deleteinventory(){
		if(service.deleteinventory("11000007","2019030002")==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}
	}
	@Test
	public void update(){
		AssetInfo assetInfo = new AssetInfo();
		assetInfo.setAssetNum("11000006");
		assetInfo.setAssetName("路由器");
		assetInfo.setAssetModel("2");
		assetInfo.setAssetPrice((float)2.21);
		assetInfo.setAssetFactory("lal");
		assetInfo.setAssetDocumentNum("2");
		assetInfo.setAssetBuyDate("2019-1-1");
		assetInfo.setAssetTakePeople("小王");
		assetInfo.setAssetRemrk("2");
		assetInfo.setStoreNum(2);
		assetInfo.setDicProfitLossNum(1);
		assetInfo.setDepartNum("2");
		service.updateObject(assetInfo);
		System.out.println("修改成功！");
	}
	@Test
	public void listAll(){
		ArrayList<AssetInfo> arrayList = new ArrayList<>();
		arrayList.addAll(service.listAllObject());
		System.out.println(arrayList);
	}
	@Test
	public void getmaxBatch()
	{
		System.out.println(service.getMaxBatch("2"));
	}
	
	@Test
	public void getallbatch() 
	{	
		System.out.println(service.getAssetByBatch(0,5,"2019010001"));
	}

	@Test
	public void listAssetInfoNumsByStore() {
		List<String> aList = service.listAssetInfoNumsByStore(1);
		System.out.println(aList);
	}
	
	@Test
	public void listAssetBydepartNum(){
		AssetInfo condition = new AssetInfo();
		condition.setDepartNum("1");
		System.out.println(service.listObjectByCondition(condition));
	
	}
	@Test
	public void updateInventory() {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("inventoryBatch","2019010001");
		condition.put("assetNum","11001056");
		condition.put("inventoryStatusId", 1);
		service.updateInventory(condition);
	}
	@Test
	public void listInventoryByCondition() {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("inventoryBatch","2019010001");
	}
	   
	@Test
	public void listAssetByInventoryBatch(){
		Thread t = Thread.currentThread();
		Map<String, Object>map = service.getAssetByBatch_v1(0, 0, "2020010012");
		System.out.println(map.get("data").toString());
		System.out.println(map.get("total"));
	}
	

}
