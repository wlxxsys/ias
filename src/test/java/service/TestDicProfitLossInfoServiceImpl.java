package service;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.DicProfitLossInfo;
import cn.tit.ias.service.DicProfitLossInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestDicProfitLossInfoServiceImpl {
	@Autowired
	DicProfitLossInfoService service;
	@Test
	public void insert()
	{
		DicProfitLossInfo dp=new DicProfitLossInfo();
		dp.setDicProfitLossNum(3);
		dp.setDicProfitLossName("盘盈");
		service.addObject(dp);
		System.out.println("添加成功");
	}
	@Test
	public void get()
	{
		DicProfitLossInfo dicprofitlossInfo =service.getObject(3);
		System.out.println(dicprofitlossInfo.toString());
	}
	@Test
	public void getdicprolossnum()
	{
		DicProfitLossInfo dicprofitlossInfo =service.getDicProfitLossByName("无盈亏");
		System.out.println(dicprofitlossInfo.getDicProfitLossNum());
	}
	

	
	@Test
	public void delete(){	
		if(service.deleteObject(3)==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}	
	}
	
	@Test
	public void listAll(){
		ArrayList<DicProfitLossInfo> arrayList = new ArrayList<>();
		arrayList.addAll(service.listAllObject());
		System.out.println(arrayList);
	}
	@Test
	public void update(){
		DicProfitLossInfo dicprofitlossInfo = new DicProfitLossInfo();
		dicprofitlossInfo.setDicProfitLossNum(1);
		dicprofitlossInfo.setDicProfitLossName("存在");
		service.updateObject(dicprofitlossInfo);
		System.out.println("修改成功！");
	}
}
