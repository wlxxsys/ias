package service;

import java.util.ArrayList;

import javax.sound.midi.VoiceStatus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import cn.tit.ias.entity.DicUserRoleInfo;
import cn.tit.ias.service.DicUserRoleInfoService;
/**
 * 
 *  
 * @Description:测试DicUserRoleInfoServiceImpl   
 * @author: maotao 
 * @date:   2019年7月4日 上午10:28:45       
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestDicUserRoleInfoServiceImpl {
    @Autowired
	DicUserRoleInfoService service;
    @Test
    public void listObject() {
    	ArrayList<DicUserRoleInfo> list = new ArrayList<>();
    	list.addAll(service.listAllObject());
    	System.out.println(list);
    }
    @Test
    public void getUserByName() {
    	DicUserRoleInfo role = service.getUserByName("管理员");
    	System.out.println(role);
    }
    
    @Test
    public void insert()
	{
    	DicUserRoleInfo d=new DicUserRoleInfo();
		d.setUserRoleId(4);
		d.setUserRoleName("宇宙无敌管理员");
		service.addObject(d);
		System.out.println("添加成功");
	}
	@Test
	public void get()
	{
		DicUserRoleInfo dicprofitlossInfo =service.getObject(3);
		System.out.println(dicprofitlossInfo.toString());
	}
	@Test
	public void delete()
	{	
		if(service.deleteObject(4)==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}	
	}
	@Test
	public void update(){
		DicUserRoleInfo dicuserroleInfo = new DicUserRoleInfo();
		dicuserroleInfo.setUserRoleId(3);
		dicuserroleInfo.setUserRoleName("宇宙无敌管理员");
		service.updateObject(dicuserroleInfo);
		System.out.println("修改成功！");
	}


}
