package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.service.CollegeInfoService;
/**
 * 
 *  
 * @Description:Test CollegeInfoServiceImpl   
 * @author: maotao 
 * @date:   2019年5月28日 下午9:18:27       
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestCollegeInfoServiceImpl {
	@Autowired
	CollegeInfoService service;
	@Test
	public void insert() {
		CollegeInfo c = new CollegeInfo();
		c.setCollegeName("太原工业学院5区");
	    if(!service.getCollegeByName(c.getCollegeName()).getCollegeName().equals(c.getCollegeName())){
	    	int a=service.addObject(c);
	    	Assert.assertEquals(1, a);
	    	System.out.println("添加成功");
	    	
	    }
	    else{
	    	System.out.println("添加失败");
	    }
		
		
	}	
	@Test
	public void get(){
		
//		CollegeInfo collegeInfo = service.getObject(1001);
//		System.out.println(collegeInfo.toString());
		int collegeNum = service.getCollegeByName("太原工业学院").getCollegeNum();
		System.out.println(collegeNum);
	}
	@Test
	public void delete(){	
		if(service.deleteObject(1008)==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}	
	}
	
	
	@Test
	public void listAll(){
		ArrayList<CollegeInfo> arrayList = new ArrayList<>();
		arrayList.addAll(service.listAllObject());
		System.out.println(arrayList);
	}
	@Test
	public void update(){
		CollegeInfo aCollegeInfo = new CollegeInfo();
		aCollegeInfo.setCollegeName("皇家太共");
		aCollegeInfo.setCollegeNum(1002);
		service.updateObject(aCollegeInfo);
		}
	@Test
	public void page(){
		CollegeInfo condition = new CollegeInfo();
		
		Map<String, Object> map = service.listCollegeObjByConditionPage(0, 2, condition);
		List<CollegeInfo> listCollege = (List<CollegeInfo>) map.get("data");
		System.out.println(listCollege);
		System.out.println(map.get("total"));
	}

}
