package service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.service.StoreInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestStoreInfoServiceImpl{
	@Autowired
	StoreInfoService service;
	@Test
	public void insert()
	{
		StoreInfo d=new StoreInfo();
		d.setStoreNum(3);
		d.setStoreName("主楼9075");
		d.setStoreDoorNum("90075");
		d.setDepartNum("1");
		service.addObject(d);
		System.out.println("添加成功");
	}
	@Test
	public void delete(){
		if(service.deleteObject(2)==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}
	}
	@Test
	public void update(){
		StoreInfo storeInfo = new StoreInfo();
		storeInfo.setStoreNum(2);
		storeInfo.setStoreName("主楼9075");
		storeInfo.setStoreDoorNum("窗");
		storeInfo.setDepartNum("2");
		service.updateObject(storeInfo);
		System.out.println("修改成功！");
	}
	@Test
	public void get()
	{
		StoreInfo storeInfo =service.getObject(1);
		System.out.println(storeInfo.toString());
	}
	@Test
	public void getbyName()
	{
		StoreInfo storeInfo =service.getStoreByName("主楼9075");
		System.out.println(storeInfo.toString());
	}
	@Test
	public void listAll(){
		ArrayList<StoreInfo> arrayList = new ArrayList<>();
		arrayList.addAll(service.listAllObject());
		System.out.println(arrayList);
	}
	@Test
	public void listAllByDepart() {
		List<StoreInfo>list = service.listStoreByDepart("1");
		System.out.println(list);
	}
}
