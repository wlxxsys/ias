package service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sun.org.apache.bcel.internal.generic.NEW;

import cn.tit.ias.entity.SystemLogInfo;
import cn.tit.ias.service.SystemLogService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestSystemlogServiceImpl {
	@Autowired
	SystemLogService logService;
	@Test
	public void listLoginRecordTest() {
		Map<String, Object> condition = new HashMap<>();
		condition.put("departNum","1");
		condition.put("endDate", new Date());
		//condition.put("accessType", "all");
		Map<String, Object> result = logService.getLoginRecordByConditionPage(0, 5, condition);
		System.out.println(result.get("total"));
    }
}
