package service;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.DepartInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestDepartInfoServiceImpl {
	@Autowired
	DepartInfoService service;
	@Test
	public void insert()
	{
		DepartInfo d=new DepartInfo();
		//d.setDepartNum("3");
		d.setDepartName("经管系");
		d.setDepartRemark("哈老师");
		d.setCollegeNum(1001);
		int result = service.addObject(d);
		if(result == 1){
			System.out.println("添加成功");
		}
		else{
			System.out.println("添加失败");
		}
	}
	@Test
	public void get()
	{
		DepartInfo departInfo =service.getObject("1");
		System.out.println(departInfo.toString());
	}

	@Test
	public void getbyName()
	{
		DepartInfo departInfo =service.getDepartByName("经管系");
		System.out.println(departInfo.toString());
	}
	@Test
	public void delete(){
		if(service.deleteObject("3")==1){
			System.out.println("删除成功");
		}else{
			System.out.println("删除失败");
		}
	}
	@Test
	public void update(){
		DepartInfo departInfo = new DepartInfo();
		departInfo.setDepartNum("1");
		departInfo.setDepartName("计算机工程系");
		departInfo.setDepartRemark("杨老师");
		service.updateObject(departInfo);
		System.out.println("修改成功！");
	}
	@Test
	public void listAll(){
		ArrayList<DepartInfo> arrayList = new ArrayList<>();
		arrayList.addAll(service.listAllObject());
		System.out.println(arrayList);
	}
}
