package service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.service.UserInfoService;

/**
 * 
 * 
 * @Description:测试UserInfoServiceImpl
 * @author: maotao
 * @date: 2019年7月4日 下午4:49:22
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class TestUserInfoServiceImpl {
    @Autowired
	UserInfoService service;
    @Test
    public void listObject(){
    	ArrayList<UserInfo> list = new ArrayList<>();
    	list.addAll(service.listAllObject());
    	System.out.println(list);
    }
    @Test
    public void getObject() {
        System.out.println(service.getObject("100001"));
    }
    @Test
    public void getUserByName() {
    	System.out.println(service.getUserByUserName("张三"));
    }
    @Test
    public void listObjectByDepartNum() {
    	System.out.println(service.listAllObject("1"));
    }
    @Test
    public void addObject() {
    	UserInfo user = new UserInfo();
    	user.setUserCount("100002");
    	user.setUserName("李四");
    	user.setUserPassward("123");
    	user.setUserRoleId(2);
    	user.setDepartNum("1");
    	service.addObject(user);
    }
    @Test
    public void deleteObject() {
    	service.deleteObject("100002");
    }
    @Test
    public void updateObject() {
    	UserInfo user = new UserInfo();
    	user.setUserCount("100002");
    	user.setUserName("王五");
    	service.updateObject(user);
    }
}
