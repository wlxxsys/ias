 package mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.microsoft.schemas.office.visio.x2012.main.impl.VisioDocumentDocument1Impl;

import cn.tit.ias.entity.AssetInfo;
import cn.tit.ias.entity.DicProfitLossInfo;
import cn.tit.ias.mapper.AssetInfoMapper;
import cn.tit.ias.mapper.DicProfitLossInfoMapper;
import cn.tit.ias.util.handlexcel.HandleAssetNumRange;
import jxl.common.AssertionFailed;

/**
 * 
 * 
 * @Description:测试AssetMapper
 * @author: maotao
 * @date: 2019年7月27日 下午4:28:24
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class TestAssetMapper {

	@Autowired
	AssetInfoMapper mapper;
	@Autowired
	DicProfitLossInfoMapper profitLossInfoMapper;

	@Test
	public void listAssetByPage() {
		// System.out.println(mapper.listAssetInfoByPage(1));
	}
	@Test
	public void getInventoryByBranch() {
		System.out.println(mapper.getAssetByBatch("2019010001", 0, 5));
	}

	@Test
	public void getInventoryByBatchCount() {
		System.out.println(mapper.getAssetByBachCount("2019010001"));
	}
	@Test
	public void importTable() {
		Map<String, List<Map<String, Object>>> mapresult = new HashMap<String, List<Map<String, Object>>>();
		List<Map<String, Object>> rowList = new ArrayList<>();
		Map<String, Object> cloumnlist = new HashMap<>();

		cloumnlist.put("imUnitName", "计算机房(1602)");
		cloumnlist.put("imAssetRange", "12000699");
		cloumnlist.put("imDocumentNum", "11001555");
		cloumnlist.put("imAssetName", "专用电缆-UPS专用");
		cloumnlist.put("imAssetModel", "最大负荷100KV");
		cloumnlist.put("imAssetPrice", 20000);
		cloumnlist.put("imFactory", "深圳艾默生网络能源有限公司");

		cloumnlist.put("imBuyDate", "2012-06-29");
		cloumnlist.put("imTakePeople", "刘雪岭");

		cloumnlist.put("imRemark", " *");
		cloumnlist.put("imAbsoluteQuantity", "1");
		cloumnlist.put("imProfitLoss", "无盈亏");
		rowList.add(cloumnlist);

		System.out.println(rowList);
		mapper.importAssetToTemportaryTable(cloumnlist);
	}

	/** 批量插入到资产表中*/
	@Test
	public void addObjectBranch() {
		List<Map<String, Object>> list = new ArrayList<>();
		list = mapper.getTemportaryTable("1");
		List<AssetInfo> assets;
		
		Map<String, Object>profitLossType = new HashMap<>();
		List<DicProfitLossInfo> profitLossInfos = profitLossInfoMapper.listAllObject();
		for (int i = 0; i < profitLossInfos.size(); i++) {
			profitLossType.put(profitLossInfos.get(i).getDicProfitLossName(), profitLossInfos.get(i).getDicProfitLossNum());
		}
		assets = HandleAssetNumRange.handleExcel(list,profitLossType);
		mapper.addObjectByBranch(assets);
	}
	@Test
	public void testListExportAssetInfo(){
		AssetInfo assetInfo = new AssetInfo();
		assetInfo.setAssetTakePeople("maotao");
		assetInfo.setDepartNum("1");
		System.out.println(mapper.listObjectByCondition(assetInfo));
		}
	
	/** 创建新批次，批量插入到盘点表*/
	@Test
	public void createBranchInsertInventory() {
		AssetInfo condition = new AssetInfo();
		condition.setDepartNum("1");
		List<AssetInfo>assetInfos = mapper.listObjectByCondition(condition);
		String batch = "2019010011";
		//System.out.println(assetInfos);
		List<Map<String, Object>> newBranchAssets = new ArrayList<>();
		Iterator<AssetInfo> iterator = assetInfos.iterator();
		while(iterator.hasNext()) {
			Map<String,Object> newBranchAsset = new HashMap<>();
			AssetInfo temp = iterator.next();
			newBranchAsset.put("inventoryBatch",batch);
			newBranchAsset.put("assetNum",temp.getAssetNum());
			newBranchAsset.put("inventoryStatusId",0);
			newBranchAsset.put("inventoryDate","001-01-01");
			newBranchAsset.put("departNum",temp.getDepartNum());
			newBranchAsset.put("assetTakePeople",temp.getAssetTakePeople());
			newBranchAsset.put("dicProfitLossNum",temp.getDicProfitLossNum());
			newBranchAsset.put("storeNum", temp.getStoreNum());
			newBranchAssets.add(newBranchAsset);
		}
		mapper.createBatchBybranch(newBranchAssets);
	}
	@Test
	public void listInventoryStatusObjTest(){
		List<Map<String,Object>>list = mapper.listInventoryStatusObj();
		for (Map<String, Object> map : list) {
			System.out.println(map);
		}
	}
	@Test
	public void getInventoryStatusObjByID(){
		System.out.println(mapper.getInventoryStatusObjByInventoryStatusId(0));
	}
	@Test
	public void getInventoryStatusObjByName(){
		System.out.println(mapper.getInventoryStatusObjByInventoryStatusName("未盘点"));
	}
	@Test
	public void updateInventory(){
		Map<String, Object> map = new HashMap<>();
		map.put("inventoryBatch","2019010001");
		map.put("assetNum","11001056");
		map.put("inventoryDate","2019-09-07");
		//map.put("inventoryStatusId", 1);
		map.put("spanAdress", 2);
		System.out.println(map);
		mapper.updateInventory(map);
	}
	@Test
	public void selectObjectByCondition() {
		AssetInfo assetInfo = new AssetInfo();
		//assetInfo.setAssetNum("11001065");
		//assetInfo.setAssetName("亚克力字");
		//assetInfo.setAssetTakePeople("穆红鹰");
		assetInfo.setDepartNum("1");
		//assetInfo.setStoreNum(1);
		//assetInfo.setDicProfitLossNum(1);
		
		System.out.println(mapper.listObjectByCondition(assetInfo));
	}
	
	@Test
	public void getInventoryParameterByCondition() {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("inventoryBatch","2019010001");
		condition.put("assetNum", "11001056");
		//condition.put("inventoryStatusId","inventoryStatusId");
		//condition.put("storeNum", "storeNum");
		condition.put("dicProfitLossNum", "dicProfitLossNum");
		//condition.put("assetTakePeople", "assetTakePeople");
		System.out.println(mapper.getInventoryParameterByCondition(condition));
	}
	@Test
	public void getInventoryObjByCondition() {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("inventoryBatch","2019010001");
		condition.put("assetNum", "11001056");
		System.out.println(mapper.getInventoryObjByCondition(condition));
	}
	
	@Test
	public void cleartemportary() {
		mapper.clearImportAssetTemportaryTable("1");
	}
	@Test
	public void testlistInventoryAssetNumberByCondition(){
		Map<String, Object>condition = new HashMap<>();
		condition.put("inventoryBatch","2020010001");
		condition.put("inventoryStatusName","未盘点");
		int result = mapper.listInventoryAssetNumberByCondition(condition);
		System.out.println("统计结果"+result);
	}
	
	@Test
	public void testlistInventoryAssetNumberByCondition_v1(){
		Map<String, Object>condition = new HashMap<>();
		condition.put("inventoryBatch","2020010001");
		//condition.put("dicProfitLossNum", 7);
		//condition.put("storeNum", 1);
		int result = mapper.listInventoryAssetNumberByCondition_v1(condition);
		System.out.println("统计结果"+result);
	}
	@Test
	public void testgetCountByConditon(){
		AssetInfo condition = new AssetInfo();
		condition.setStoreNum(1);
		condition.setDicProfitLossNum(1);
		int result = mapper.getCountByConditon(condition);
		System.out.println(result);
	}
	@Test
	public void testSwitch(){
		mapper.createSwitch("2020010002","1");
	}
	@Test
	public void testSwitch_v1(){
		Map<String,Object> condition = new HashMap<>();
		condition.put("departNum", "1");
		System.out.println(mapper.getSwitch_v1(condition));
	}
	@Test
	public void testAddInventoryBatchInfo(){
		Map<String, Object>batch = new HashMap<String,Object>();
		Date time = new Date();
		batch.put("departNum", "10001");
		batch.put("inventoryBatch","1000012e3");
		batch.put("runStatus",1);
		batch.put("createUser", "毛涛");
		batch.put("createTime", time);
		int result = mapper.addInventoryBatchInfo(batch);
		if(result == 1){
			System.out.println("添加成功");
		}else {
			System.out.println("添加失败");
		}
	}
	@Test
	public void testDeleteInventoryBatchInfo(){
		int result = mapper.deleteInventoryBatchInfo("2020010011");
		if(result == 1){
			System.out.println("删除成功");
		}else {
			System.out.println("删除失败");
		}
			
		
	}
	@Test
	public void testListInventoryBatchInfoByDepart(){
		List<Map<String, Object>> result = mapper.listInventoryBatchInfoByDepart("10001");
		System.out.println(result);
	}
	@Test
	public void testGetInventoryBatchInfoByBatch(){
		Map<String, Object> result = mapper.getInventoryBatchInfoByBatch("2020010011");
		System.out.println(result);
	}
	@Test
	public void testUpdateInventoryBatchInfo(){
		Map<String, Object> inventoryInfo = new HashMap<>();
		inventoryInfo.put("runStatus",0);
		inventoryInfo.put("inventoryBatch", "1000012e3");
		int result = mapper.updateInventoryBatchInfo(inventoryInfo);
		if(result == 1){
			System.out.println("更新成功");
		}else{
			System.out.println("更新失败");
		}
	}

	@Test
	public void testGetInventoryBatch_v1(){
		List<Map<String, Object>> result = mapper.getAssetByBatch_v1("2020010001");
		System.out.println("-----"+result);
		System.out.println("-----"+result.size());
	}
}
