package mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * 
 *  
 * @Description:测试storemapper   
 * @author: maotao 
 * @date:   2019年9月7日 下午9:48:58       
 *
 */

import cn.tit.ias.entity.StoreInfo;
import cn.tit.ias.mapper.StoreInfoMapper;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestStoreMapper {
	
	@Autowired
	StoreInfoMapper mapper;
	@Test
	public void listStoreObjByCondition() {
		StoreInfo condition = new StoreInfo();
		//condition.setDepartNum("1");
		condition.setStoreDoorNum("7001");
		System.out.println(mapper.listStoreObjByCondition(condition));
	}
	@Test
	public void addObject() {
		StoreInfo store=new StoreInfo();
		store.setStoreName("xixi");
		store.setDepartNum("1");
		store.setStoreDoorNum("窗");
		mapper.addObject(store);
		System.out.println("添加成功");
	}

}
