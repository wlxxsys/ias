package mapper;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.CollegeInfo;
import cn.tit.ias.mapper.CollegeInfoMapper;
/**
 * 
 *  
 * @Description:测试collegeInfo的dao层   
 * @author: maotao 
 * @date:   2019年6月1日 下午5:59:17       
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestCollegeMapper {
	@Autowired
    CollegeInfoMapper collegeInfoMapper;
	@Test
	public void get(){
		CollegeInfo  c= collegeInfoMapper.getCollegeByName("太原工业学院");
		System.out.println(c.toString());
	}
    
    
}
