package mapper;

import org.jcp.xml.dsig.internal.MacOutputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.UserInfo;
import cn.tit.ias.mapper.UserInfoMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestUserMapper {

	@Autowired
	UserInfoMapper mapper;
	@Test
	public void listUserByDepartNum() {
		System.out.println(mapper.listUserByDepartNum("1"));
	}
	@Test
	public void addObject() {
		UserInfo userInfo = new UserInfo();
		userInfo.setUserCount("10009");
		userInfo.setUserName("jjjj");
		userInfo.setUserPassward("123");
		userInfo.setDepartNum("1");
		userInfo.setUserRoleId(1);
		mapper.addObject(userInfo);
		
	}
}
