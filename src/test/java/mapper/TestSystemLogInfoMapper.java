package mapper;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import cn.tit.ias.entity.SystemLogInfo;
import cn.tit.ias.mapper.SystemLogInfoMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class TestSystemLogInfoMapper {

	@Autowired
	SystemLogInfoMapper systemLogInfoMapper;
	@Test
	public void addLoginRecord() {
		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		map.put("userCount", "100001");
		map.put("userName", "王三");
		map.put("accessType", "login");
		map.put("accessTime", date);
		map.put("accessIp","11111111");
		map.put("departNum","1");
		int result = systemLogInfoMapper.insertLoginRecord(map);
		if(result == 1)
			System.out.println("success");
		else
			System.out.println("error");
	}
	
	@Test
	public void deleteLoginRecord() {
		systemLogInfoMapper.deleteLoginRecordByDepartNum("1");
	}
	@Test
	public void listLoginRecord() {
		Map<String, Object> conditon = new HashMap<>();
		//conditon.put("userCount",null);
		//conditon.put("accessType",null);	
		//conditon.put("startDate", null);
		//conditon.put("endDate",null);
		conditon.put("departNum","1");
		//{accessType=, userCount=, endDate=, departNum=1, startDate=}
		List<Map<String, Object>>result = systemLogInfoMapper.getUserOperationRecordByCondition(conditon);
		//System.out.println(systemLogInfoMapper.getLoginRecord("100001"));
		System.out.println(result);
	}
	

}
