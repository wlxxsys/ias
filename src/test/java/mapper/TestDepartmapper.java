package mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.DepartInfo;
import cn.tit.ias.mapper.DepartInfoMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class TestDepartmapper {

	@Autowired
	DepartInfoMapper departInfoMapper;
	@Test
	public void listDepartObjByConditionTest() {
		DepartInfo condition = new DepartInfo();
		condition.setCollegeNum(1001);
		//condition.setDepartName("计算机工程系");
		condition.setDepartNum("1");
		System.out.println(departInfoMapper.listDepartObjByCondition(condition));
	}
	@Test
	public void getDepartNum(){
		System.out.println(departInfoMapper.getTotalDepartNum(1001));
	}
	@Test
	public void getMaxDepartNum() {
		System.out.println(departInfoMapper.getMaxDepartNum());
	}
}
