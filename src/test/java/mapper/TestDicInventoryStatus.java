package mapper;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.tit.ias.entity.DicInventoryStatus;
import cn.tit.ias.mapper.DicInventoryStatusMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestDicInventoryStatus {

	@Autowired
	DicInventoryStatusMapper mapper;
	@Test
	public void testSelectByName() {
		
		DicInventoryStatus name = mapper.getDicInventoryStatusByName("未盘点");
		System.out.println(name.getInventoryStatusName());
		
	}
	@Test
	public void testSelectAll() {
		List<DicInventoryStatus> list = mapper.listAllObject();
		for (DicInventoryStatus dicInventoryStatus : list) {
			System.out.println(dicInventoryStatus.getInventoryStatusName());
		}
	}
}
